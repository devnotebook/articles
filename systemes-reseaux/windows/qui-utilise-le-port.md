Qui utilise le port ?
=====================

[:type]: # "Astuce"
[:created_at]: # "2023/05/30"
[:modified_at]: # "2023/05/30"
[:tags]: # "window systemes-reseaux fonctions-utiles powershell"

Sous Windows, pour savoir quel processus utilise un port, lancez la commande `Get-Process` dans **Powershell** :

Exemple pour le port `5433` :

```powershell
Get-Process -Id (Get-NetTCPConnection -LocalPort 5433).OwningProcess
```
