Modifier le chemin par défaut de l'invite de commande Windows
=============================================================

[:type]: # "Astuce"
[:sources]: # "[korben.info](http://korben.info/changez-le-chemin-par-defaut-de-cmdexe.html)"
[:created_at]: # "2012/11/10"
[:modified_at]: # "2017/05/15"
[:tags]: # "window systemes-reseaux"

S'il vous arrive d’utiliser l’outil de ligne de commande DOS sous Windows, vous savez que par défaut, 
celui-ci se positionne dans votre répertoire personnel. 
C'est assez pénible car à chaque fois, vous êtes obligé de faire plein de `cd machin` pour arriver au répertoire de votre choix.

## Solution badasse ##

Pour éviter de refaire cette manip à chaque fois (surtout si c'est pour se rendre toujours au même endroit), 
vous pouvez modifier le chemin utilisé par défaut.

Il suffit d’aller dans la base de registre (Démarrer > Exécuter... > regedit) et de naviguer 
jusqu’à la clé `HKEY_CURRENT_USER \ Software \ Microsoft \ Command Processor`.

Créez ensuite une nouvelle valeur chaîne dans la zone de droite et nommez-la `Autorun`.

Modifiez ensuite cette clé et donnez-lui la valeur suivante en remplaçant bien sûr `mon\chemin` par celui de votre choix :

```bash
cd /d d:\mon\chemin
```

Et voilà, les changements s'appliquent instantanément sans besoin de redémarrer. 
(Relancez tout de même votre invite de commande si elle était déjà ouverte.)

**Remarque importante :**

Cette manip semble poser problème dans certains scripts batch utilisant ```%CD%```.

## Solution pacifique ##

Rendez-vous dans le répertoire où vous souhaitez ouvrir votre invite de commande, via l'explorateur Windows.

Faites `Shift` + `Clic droit` et choisissez **Ouvrir un invite de commande ici**. Et voilà c'est fait.

Cette solution est beaucoup moins intrusive et plus simple mais vous devez refaire la manip à chaque fois.
