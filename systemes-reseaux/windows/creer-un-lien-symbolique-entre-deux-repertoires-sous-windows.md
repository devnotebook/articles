Créer un lien symbolique entre deux répertoires sous Windows
============================================================

[:type]: # "Astuce"
[:created_at]: # "2014/12/04"
[:modified_at]: # "2020/11/08"
[:tags]: # "window systemes-reseaux fonctions-utiles"

Pour créer un lien symbolique sous Windows, il faut utiliser la commande `symlink` :

```bash
mklink [[/D] | [/H] | [/J]] <Lien> <Cible>
```

Pour cela, lancez une invite de commande :

* Menu démarrer
* Exécuter...
* Tapez `cmd` et validez

Si par exemple vous souhaitez pouvoir accéder au répertoire `C:\Windows\System32\drivers\etc` 
directement via `C:\etc`, tapez cette commande :

```bash
mklink /J C:\etc C:\Windows\System32\drivers\etc
```

**Remarques :**
* Selon le lien que vous souhaitez créer, vous devrez peut-être être admin de votre machine 
(ou lancer l'invite de commande en mode administrateur).
* Si vous voulez créer un lien vers un répertoire qui n'existe pas (encore) sur votre système, 
utilisez l'option `/D` et non pas `/J`. Cela créera un **lien symbolique** et non pas une **jonction**.
