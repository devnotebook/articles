Vérifier la configuration d'Apache avec WampServer
==================================================

[:type]: # "Astuce"
[:created_at]: # "2015/08/12"
[:modified_at]: # "2017/05/15"
[:tags]: # "windows systemes-reseaux apache"

Lorsque vous lancez les services de WampServer, il arrive qu'Apache n'arrive pas à démarrer 
à cause d'une erreur de syntaxe dans sa configuration.

Pour savoir quel fichier (`httpd.conf`, `httpd-vhosts.conf`, ...) et quelle ligne pose problème, 
vous pouvez lancer Apache en ligne de commande :

```bash
cd D:\Dev\wamp\bin\apache\apache2.2.22\bin
httpd
```

**Remarque :**
Adaptez le chemin en fonction d'où est installé WampServer et la version d'Apache que vous utilisez.

**Exemple :**
![Vérifier la configuration d'Apache avec WampServer](./verifier-la-configuration-d-apache-avec-wampserver-01.png)
