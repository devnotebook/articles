Utiliser l'autocomplétion de l'invite de commande Windows
=========================================================

[:type]: # "Astuce"
[:created_at]: # "2013/04/18"
[:modified_at]: # "2017/05/15"
[:tags]: # "window systemes-reseaux"

Sous Linux, vous avez l'habitude d'utiliser la touche `TAB` pour trouver automatiquement 
la fin du nom d'un fichier ou d'un répertoire.

La même fonctionnalité existe pour Windows sous DOS, mais vous devez utiliser des 
antislash `\` au lieu des slash `/` dans vos chemins.
