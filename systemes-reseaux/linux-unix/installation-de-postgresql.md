Installation de PostgreSQL
==========================

[:type]: # "Astuce"
[:version]: # "Debian 8, PostgreSQL 9.4"
[:sources]: # "[wiki.debian.org](https://wiki.debian.org/PostgreSql)"
[:created_at]: # "2017/07/21"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux postgresql debian"

**Remarque :**

Si vous préférez MySQL, vous pouvez [suivre ce tutoriel](http://www.lafermeduweb.net/billet/tutorial-creer-un-serveur-web-complet-sous-debian-2-php5-et-mysql-164.html#mysql) à la place.

Il vous faudra peut-être également ajouter la ligne suivante au fichier `/etc/apache2/apache2.conf` 
puis redémarrer Apache :

```apacheconfig
Include /etc/phpmyadmin/apache.conf
```

## Installation ##

* Exécutez simplement la commande suivante :

```bash
sudo apt-get install postgresql postgresql-client postgresql-doc
```

* Vérifiez votre version de postgres (ex: 9.4)
```bash
ls /etc/postgresql/
```
* Forcez postgreSQL à utiliser l'UTF-8. (Adaptez les commandes suivantes avec la bonne version) :

```bash
sudo  pg_dropcluster --stop 9.4 main
sudo  pg_createcluster --start -e UTF-8 9.4 main
```

## Configuration générale ##

### Connexion distante ###

Par défaut, PostgreSQL n'est pas accessible à distance (ex: avec votre IDE sur votre poste de dev).

Pour l'autoriser, modifiez le fichier le fichier de configuration `/etc/postgresql/<version>/main/pg_hba.conf`, 
en remplaçant la ligne suivante par celle du dessous :

```
#local   all         all                               peer
local   all         all                               trust
```

Redémarrez le service `postgresql` :

```bash
sudo /etc/init.d/postgresql reload
```

### Adminer ###

Quand on utilise MySQL, on le couple souvent avec PHPMyAdmin, pour pouvoir l'administrer via une interface web. 
Côté PostgreSQL, il n'y a pas d'outil aussi abouti.

[Adminer](https://www.adminer.org/) ne s'en tire tout de même pas si mal. 
D'ailleurs, il permet aussi de se connecter à plein d'autres SGBD, dont MySQL.

Pour l'installer, suivez les commandes suivantes :

```bash
sudo mkdir /var/www/adminer
sudo cd /var/www/adminer
sudo wget https://www.adminer.org/latest.php
sudo mv latest.php index.php
sudo chown -R phpuser:phpuser .
```

Ensuite si vous utilisez les virtual host pour votre site, ajoutez-en un pour adminer. 
Pour cela créez par exemple le fichier `/etc/sites-available/adminer.conf` :

```apacheconfig
<VirtualHost *:80>
    ServerName db.adminer.dev
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/adminer
 
    ErrorLog ${APACHE_LOG_DIR}/adminer.error.log
    CustomLog ${APACHE_LOG_DIR}/adminer.access.log combined
</VirtualHost>
```

**Remarques :**

* Vous pouvez bien sur adapter ces configurations, en particulier le nom de domaine `db.adminer.dev`.
* Vous devrez probablement ajouter ce nom de domaine dans le fichier `hosts` de votre poste de 
dev pour pouvoir l'utiliser.

Activez maintenant ce virtual host et redémarrez Apache :

```bash
sudo a2ensite adminer
sudo service apache2 reload
```

## Configuration pour votre site ##

Par défaut, la base de données `postgres` a été créée. Son propriétaire s'appelle aussi `postgres`. 
Un nouvel utilisateur système `postgres` a lui aussi été créé.

* Connectez vous en tant qu'utilisateur postgres (qui est administrateur postgreSQL) :

```bash
sudo su - postgres
```

* Créez un nouvel utilisateur :

```bash
createuser --interactive mypguser
```

Avec les options suivantes :

* Super-utilisateur : `Non`
* Créateur de base de données : `Oui`
* Création de nouveaux rôles : `Non`

Modifiez son mot de passe :

```bash
psql
> ALTER USER mypguser WITH PASSWORD 'new_password';
```

* Créez une nouvelle base de données :

```bash
createdb -O mypguser mypgdatabase
```

* Si besoin, connectez-vous-y pour rendre votre utilisateur **propriétaire du schéma public** :

```bash
psql mypgdatabase
> ALTER SCHEMA public OWNER TO mypguser;
```

* Redémarrez le service **postgresql** :

```bash
sudo /etc/init.d/postgresql reload
```

## Utilisation ##

Connectez-vous à la base avec le nouvel utilisateur :

```bash
psql -d mypgdatabase -U mypguser
```

Si vous n'avez pas d'erreur, alors c'est prêt (`\q` pour quitter la console postgreSQL).
