Changer l'utilisateur d'Apache
==============================

[:type]: # "Astuce"
[:created_at]: # "2016/04/20"
[:modified_at]: # "2017/05/04"
[:tags]: # "linux-unix systemes-reseaux apache"

Il est parfois utile de choisir avec quel utilisateur système Apache est exécuté.

Pour le modifier, éditez le fichier `/etc/apache2/envvars` et modifiez les lignes suivantes :

```bash
export APACHE_RUN_USER=www-data
export APACHE_RUN_GROUP=www-data
```

`www-data` est la valeur par défaut. Il suffit de mettre l'utilisateur et le groupe que vous souhaitez.
