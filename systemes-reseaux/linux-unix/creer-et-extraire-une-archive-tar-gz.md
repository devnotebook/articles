Créer et extraire une archive tar.gz
====================================

[:type]: # "Astuce"
[:created_at]: # "2012/06/06"
[:modified_at]: # "2017/05/03"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Pour **créer** une archive `tar.gz` sans le dossier conteneur, utilisez la commande :

```bash
cd mon/repertoire/conteneur
tar -czf ../archive.tar.gz ./
```

Pour **extraire** une archive `tar.gz`, utilisez la commande :

```bash
tar -xvzf archive.tar.gz
```
