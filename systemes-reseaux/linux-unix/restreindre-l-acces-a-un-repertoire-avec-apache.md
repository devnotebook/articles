Restreindre l'accès à un répertoire avec Apache
===============================================

[:type]: # "Astuce"
[:version]: # "Aapche 2.4"
[:sources]: # "[httpd.apache.org](http://httpd.apache.org/docs/2.4/fr/howto/auth.html)"
[:created_at]: # "2015/08/10"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux apache"

Pour restreindre l'accès à un répertoire ou à un site entier, vous pouvez le protéger par un mot de passe :

![Authentification HTTP](./restreindre-l-acces-a-un-repertoire-avec-apache-01.png)

## Prérequis ##

### Modules Apache ###

Pour pouvoir utiliser cette fonctionnalité d'Apache, vous aurez besoin des modules suivants : 
**mod_auth_basic**, **mod_authn_file** et **mod_authz_user**.

#### Génération du fichier de mots de passe ####

Vous devez également générer un fichier de mots passe, via l'utilitaire **htpasswd** fourni avec Apache. 
Il se trouve probablement dans le répertoire `bin/` de votre installation apache.

Pour cela, lancez la commande suivante :

```bash
htpasswd -c /chemin/vers/un/repertoire/protege/passwords nom_utilisateur
```

**Remarques :**

* Vous par exemple créer le fichier `passwords` dans `/usr/local/apache/passwd/`.
* Pour ajouter un autre utilisateur, utilisez la même commande sans l'option `-c`.

Le fichier généré pourra ressembler à a ça :

```
user1:.G.h/4RfP93fd
user2:RlPRITNDHefEpl 
```

## Configuration de base ##

Cette configuration peut se faire au niveau de votre virtualhost :

```apacheconfig
<VirtualHost *:80>
    DocumentRoot /var/www/mon_site
    ServerName mon-site.com
 
    <Directory /var/www/mon_site>
        Options FollowSymLinks
        Order allow,deny
        allow from all
    </Directory>
 
    <Directory /var/www/mon_site/protected>
        Order allow,deny
        Allow from all
        AllowOverride None
 
        AuthType Basic
        # Intitulé de la mire de connexion
        AuthName "Acces restreint"
        # Type de provider
        AuthBasicProvider file
        # Fichier contenant les utilisateurs et leur mots de passe cryptés
        AuthUserFile /chemin/vers/mon/fichier/passwords
        Require valid-user
    </Directory>
</VirtualHost>
```

**Explications :**

* Ce virtualhost définit des règles différentes selon les dossiers du site :
  * La racine du site (`mon_site/`) et ses sous-répertoires sont accessibles à tous
  * Le répertoire `protected/` lui, est protégé par un mot de passe
* L'authentification basique est utilisée.
* On indique le fichier contenant les utilisateurs et leurs mots de passe cryptés, autorisés à accéder au répertoire.

**Remarque :**

Pour plus de sécurité, il est également judicieux de [passer le site en HTTPS](/node/47).

## Des groupes d'utilisateurs ##

Si vous voulez gérer des accès avec plusieurs utilisateurs, il est possible de les organiser par groupe.

Vous pourrez ainsi définir que tel utilisateur appartient à tel ou tel groupe, et que tel ou tel groupe à accès à 
tel ou tel répertoire.

Le virtualhost est alors un peu modifié :

```apacheconfig
<VirtualHost *:80>
    DocumentRoot /var/www/mon_site
    ServerName mon-site.com
 
    <Directory /var/www/mon_site>
        Options FollowSymLinks
        Order allow,deny
        allow from all
    </Directory>
 
    <Directory /var/www/mon_site/protected>
        Order allow,deny
        Allow from all
        AllowOverride None
 
        AuthType Basic
        # Intitulé de la mire de connexion
        AuthName "Acces restreint"
        # Type de provider
        AuthBasicProvider file
        # Fichier contenant les utilisateurs et leur mots de passe cryptés
        AuthUserFile /chemin/vers/mon/fichier/passwords
        # Fichier contenant les groupes et leurs utilisateurs
        AuthGroupFile /chemin/vers/mon/fichier/groups
        Require group_name
    </Directory>
</VirtualHost>
```

**Explication :**

La propriété `AuthUserFile` a été ajoutée, pour définir où se trouve le fichier contenant la liste des groupes. 
La règle de restriction a également changé, elle définit maintenant le nom du groupe ayant accès au répertoire.

**Remarques :**

* Pour utiliser ce système de groupes, vous aurez besoin du module `mod_authz_groupfile` d'Apache.
* Le fichier `groups` contient quelque chose comme ça :

    ```
    group1: user1 user2
    group2: user2 user3
    ```

* Contrairement au fichier `passwords`, il n'est pas crypté et peut donc être créé via un éditeur de texte classique.
