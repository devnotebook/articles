Virtual host avec contexte d'URL
================================

[:type]: # "Astuce"
[:created_at]: # "2017/12/15"
[:modified_at]: # "2020/11/07"
[:tags]: # "linux-unix systemes-reseaux apache"

Si vous voulez accéder à votre application via un contexte spécifique, 
il faut ajouter cette ligne dans votre virtual host apache :

```apacheconfig
Alias /mon-contexte /chemin/vers/mon_appli/web
```

Si votre virtual host répond au nom de domaine `www.mon-site.com` par exemple,
votre site sera maintenant accessible via l'URL `www.mon-site.com/mon-contexte`.
