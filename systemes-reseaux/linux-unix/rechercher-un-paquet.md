Rechercher un paquet
====================

[:type]: # "Astuce"
[:version]: # "Debian 7+"
[:sources]: # "[doc.ubuntu-fr.org](https://doc.ubuntu-fr.org/apt-get)"
[:created_at]: # "2017/03/31"
[:modified_at]: # "2017/05/04"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Si vous êtes sous une distribution avec **apt** d'installé, vous pouvez rechercher un paquet avec la commande suivante :

```bash
apt-cache search terme1 terme2
```

**Remarques :**

* Si vous indiquez plusieurs termes, seuls les résultats avec chacun d'eux seront retournés
* Les regexp sont gérées (au moins en partie). Ex : `apt-cache search php.*memcach`.

Si vous avez **aptitude** d'installé, une commande plus simple à mémoriser existe :

```bash
aptitude search terme1 terme2
```
