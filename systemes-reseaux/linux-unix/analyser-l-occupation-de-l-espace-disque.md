Analyser l’occupation de l’espace disque
========================================

[:type]: # "Astuce"
[:sources]: # "[dev.yorhel.nl](https://dev.yorhel.nl/ncdu)"
[:created_at]: # "2023/03/15"
[:modified_at]: # "2023/03/15"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Sous Linux, l’utilitaire **ncdu** (pour NCurses Disk Usage) permet de trouver quel fichier ou répertoire vous 
bouffe tout votre espace disque. 

Il est disponible sur les dépôts officiels **Debian** et **Ubuntu** et donc facile à installer.

```bash
sudo apt install ncdu
ncdu
```

**Note** : **ncdu** a été réécrit dans une v2 encore récente. Selon la version des dépôts, c’est peut-être la v1 qui sera installée.
