Installation d'un environnement LAPP sous Debian
================================================

[:type]: # "Astuce"
[:version]: # "Debian 8 (netinstall)"
[:sources]: # "[www.lafermeduweb.net](http://www.lafermeduweb.net/billet/tutorial-creer-un-serveur-web-complet-sous-debian-1-apache-160.html#confDebian)"
[:created_at]: # "2017/07/20"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux debian"

Cette suite d'articles donne un exemple d'installation d'un environnement LAPP sous Debian, 
destiné au développement (= pas pour la production). 
Il peut tout à fait servir de base pour un LAMP (= MySQL à la place de PostgreSQL)

## Le wizard d'installation de Debian ##

Dans cet exemple les configurations choisies sont en italique.

* Choisissez *64 bit install* au boot sur le CD.
* Choisissez la langue et la localisation -> *France*
* Indiquez le nom de la machine (= celui visible sur le réseau) -> *debian-server*
* Indiquez le domaine réseau -> (laisser vide si vous n'êtes pas dans un réseau d'entreprise, sinon renseignez-le)
* Indiquez et confirmer le mot de passe root -> ****** 
* Indiquez le nom de l'utilisateur courant à créer -> phpuser
* Indiquez les identifiants (login/mot de passe) pour cet utilisateur -> *phpuser/phpuser*
* Choisissez la méthode de partitionnement -> *Assisté - utiliser un disque entier*
* Choisissez le disque à partitionner -> *<le seul disponible>*
* Choisissez le schéma de partitionnement -> *Tout dans une seule partition*
* Terminez le partitionnement et appliquez les changements
* Appliquez les changements sur les disques
* Configurez l'outil de gestion des paquets :
  * Langue -> *France*
  * Miroir -> *ftp.fr.debian.org*
  * Mandataire -> *<vide>*
* Refusez l'envoi de statistiques à propos des paquets installés
* Sélectionnez les ensembles de logiciels que vous voulez installer (au moins **Serveur SSH**) :
  * Environnement de bureau Debian (= mode graphique)
  * Serveur d'impression
  * *Serveur SSH*
  * *Utilitaires usuels du système*
* Acceptez l'installation de GRUB
* Continuez en démarrant le nouveau système

## Modification du système ##

### Installation de sudo ###

* Connectez-vous en administrateur -> `root/******`
* Installez la commande **sudo** :

```bash
apt-get install sudo
```

### Configuration du sudo ###

Ajoutez l'utilisateur `phpuser` au groupe sudo :

```bash
adduser phpuser sudo
```

### Autoriser la connexion SSH en root ###

Par défaut, Debian 8 n'autorise pas cette connexion, 
ce qui est gênant lors de l'utilisation d'un outil comme **WinSCP**, **Filezilla** ou **Nautilus** pour explorer 
les fichiers.

Pour l'autoriser, modifiez le fichier `/etc/ssh/sshd_config` et remplacer la ligne :

```
PermitRootLogin without-password
```

par

```
PermitRootLogin yes
```

Redémarrez ensuite le service SSH :

```bash
service ssh restart
```

### Amélioration de la console ###

Modifier le fichier `.bashrc` permet d'améliorer l'affichage de la console et 
d'ajouter des alias de commande.

* Connectez-vous avec l'utilisateur -> *phpuser/phpuser*
* Modifiez le fichier `.bashrc` avec nano et décommentez les lignes suivantes :

```
alias ll='ls -l'
alias la='ls -1'
alias l='ls -CF'
```

Si vous voulez avoir une console colorée, décommentez la ligne suivante :

```
force_color_prompt=yes
```

et modifiez la ligne suivante en la remplaçant par celle du dessous :

```
#PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ ' 
PS1='\[\e[33;01m\]\u \[\033[31;01m\][\[\033[0;37m\]\w\[\033[31;01m\]] \$\[\033[00m\] '
```

* Ajouter cette ligne à la fin du fichier, pour pouvoir exécuter les commandes de /usr/sbin facilement :

```
export PATH=$PATH:/usr/sbin
```

* Enregistrez les modifications avec Ctrl+X

### Mise à jour de la distribution et de la liste des paquets ###

* Lancez les commandes suivantes pour procéder à la mise à jour :

```bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
```

* Acceptez d'installer les éventuels paquets à mettre à jour
* Modifiez le fichier `/etc/apt/sources.list` avec nano en tant qu'administrateur :

```
deb http://security.debian.org/ etch/updates main contrib non-free
```

* Mettez à jour la liste des dépôts et installez les bibliothèques de base :

```bash
sudo apt-get update
sudo apt-get install build-essential
```

### Autre ###

Installez les outils dont vous avez besoin 
(ex: [Screen](./linux-unix/screen-le-multiplexeur-de-terminaux), ...).
