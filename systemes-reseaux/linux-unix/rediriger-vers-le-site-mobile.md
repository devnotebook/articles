Rediriger vers le site mobile
=============================

[:type]: # "Astuce"
[:version]: # "Apache 2.2"
[:created_at]: # "2015/08/10"
[:modified_at]: # "2010/11/08"
[:tags]: # "linux-unix systemes-reseaux apache"

Si vous avez deux versions de votre site, une **desktop** et une autre **mobile**, vous voudrez probablement qu'un 
utilisateur sur mobile soit automatiquement redirigé vers la version adaptée.

Cette configuration peut être définie au niveau de vos virtualhost.

## Cas basique ##

Imaginons que vous ayez deux virtualhost basiques, un pour chaque version.

### Site desktop ###

```apacheconfig
<VirtualHost *:80>
    DocumentRoot /var/www/site_desktop
    ServerName www.mon-site.com
 
    <Directory /var/www/site_desktop>
        Options FollowSymLinks
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
```

### Site mobile ###

```apacheconfig
<VirtualHost *:80>
    DocumentRoot /var/www/site_mobile
    ServerName m.mon-site.com
 
    <Directory /var/www/site_mobile>
        Options FollowSymLinks
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
```

Si vous voulez rediriger un utilisateur qui accède au site desktop via son mobile, vers le site mobile, 
le premier virtualhost deviendra :

```apacheconfig
<VirtualHost *:80>
    DocumentRoot /var/www/site_desktop
    ServerName www.mon-site.com
 
    <IfModule mod_rewrite.c>
        RewriteEngine on
 
        # Si le client est un navigateur mobile
        RewriteCond %{HTTP_USER_AGENT} mobi [NC]
 
        # Redirection vers le site mobile
        RewriteRule ^(.*)$ http://m.mon-site.com [R=301,L]
    </IfModule>
 
    <Directory /var/www/site_desktop>
        Options FollowSymLinks
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
```

**Explications :**

* Si le module de réécriture d'Apache est présent, on l'active
* Si le nom du navigateur (`HTTP_USER_AGENT`) contient la chaîne `mobi`, on applique la règle de redirection, 
vers le site mobile

**Remarque :**

Vous pouvez bien sûr faire l'inverse (mobile vers desktop), en ajoutant un `!` :

```apacheconfig
<VirtualHost *:80>
    DocumentRoot /var/www/site_mobile
    ServerName m.mon-site.com
 
    <IfModule mod_rewrite.c>
        RewriteEngine on
 
        # Si le client n'est pas un navigateur mobile
        RewriteCond %{HTTP_USER_AGENT} !mobi [NC]
 
        # Redirection vers le site desktop
        RewriteRule ^(.*)$ http://www.mon-site.com [R=301,L]
    </IfModule>
 
    <Directory /var/www/site_mobile>
        Options FollowSymLinks
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
```

## Règles supplémentaires ##

Selon votre architecture ou vos besoins, vous serez sans doute amené à ajouter d'autres conditions (`RewriteCond`) avant 
la règle de réécriture (`RewriteRule`) :

```apacheconfig
# 1. Si le client n'est pas sur iPad
RewriteCond %{HTTP_USER_AGENT} !ipad [NC]
 
# 2. Si le nom de domaine appelé ne commence pas par "m."
RewriteCond %{HTTP_HOST} !^m\..*
 
# 3. Si l'URL demandée ne commence pas par "/api/mobile/"
RewriteCond %{REQUEST_URI} !(/api/mobile/)
```

**Explications :**

1. Les iPad ayant une résolution importante, il peut être préférable de conserver la version desktop pour leurs utilisateurs. 
Vous pouvez également ajouter d'autres tablettes : `!ipad|tablet [NC]`.
Plus d'informations sur le [site Mozilla](https://developer.mozilla.org/fr/docs/Detection_du_navigateur_en_utilisant_le_user_agent).
2. Si vous avez un serveur apache en frontal, qui gère les deux noms de domaine (**mon-site.com** et **m.mon-site.com**) dans un 
seul virtualhost, vous aurez besoin de cette ligne pour éviter une boucle de redirection infinie.
3. Si votre site desktop fournit des webservices à votre site mobile qui les consomme en AJAX, 
il ne faut pas que les requêtes en question soient redirigées.

## Laisser le choix à l'utilisateur ##

En général, un site mobile ne propose pas toutes les fonctionnalités du site desktop. 
De plus, en fonction de la taille de l'appareil de l'utilisateur, ce dernier peut préférer utiliser le site desktop.

L'idéal est de proposer un lien vers la version desktop sur le site mobile (éventuellement l'inverse).

Le problème, c'est qu'avec les règles de redirection définies précédemment, l'utilisateur sera automatiquement 
redirigé en cliquant sur le lien.

Une solution est d'ajouter un paramètre à l'url, et de stocker l'information en cookie. 
Le lien depuis le site mobile vers le site desktop aura par exemple pour URL `http://www.mon-site?mobile=0`.

Pour cela, la règle concernant la redirection doit être améliorée :

```apacheconfig
<IfModule mod_rewrite.c>
    RewriteEngine on
  
    # Si l'URL contient le paramètre 'mobile', égal à 1
    RewriteCond %{QUERY_STRING} (^|&)mobile=1(&|$)
    # On ajoute un cookie
    RewriteRule ^ - [CO=mobile:1:%{HTTP_HOST}]
 
    # Si l'URL contient le paramètre 'mobile', égale à 0
    RewriteCond %{QUERY_STRING} (^|&)mobile=0(&|$)
    # On ajoute un cookie
    RewriteRule ^ - [CO=mobile:0:%{HTTP_HOST}]
 
    # Si l'URL contient le paramètre 'mobile', égale à 1
    RewriteCond %{QUERY_STRING} (^|&)mobile=0(&|$)
    # On saute la prochaine RewriteRule
    RewriteRule ^ - [S=1]
 
    # Si le client est un navigateur mobile
    RewriteCond %{HTTP_USER_AGENT} mobi [NC]
 
    # Si le cookie n'est pas égal à 0
    RewriteCond %{HTTP:Cookie} !\mobile=0(;|$)
 
    # Redirection vers le site desktop
    RewriteRule ^(.*)$ http://m.mon-site.com [R=301,L]
</IfModule>
```
