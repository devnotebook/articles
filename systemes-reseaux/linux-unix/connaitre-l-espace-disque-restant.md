Connaître l'espace disque restant
=================================

[:type]: # "Astuce"
[:created_at]: # "2016/02/24"
[:modified_at]: # "2017/05/04"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Sous linux, vous pouvez rapidement connaître l'espace disque restant grâce à la commande :

```bash
df -h
```

**Note :** comme pour beaucoup d'autres commandes Linux, l'option `-h` permet un affichage **plus humain**, notamment
pour le poids des fichiers (ie. 1.2mo au lieu de 1200000).
