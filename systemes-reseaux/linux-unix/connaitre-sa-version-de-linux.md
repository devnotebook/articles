Connaitre sa version de Linux
=============================

[:type]: # "Astuce"
[:created_at]: # "2015/04/15"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Pour connaitre votre distribution et sa version, utilisez la commande suivante :

```bash
lsb_release -a
```

**Remarque :**

Cette commande n'est pas forcément installée par défaut sur votre machine. 
Elle se trouve sûrement dans le dépôt officiel et peut être ajoutée grâce à **yum** ou **apt**.
