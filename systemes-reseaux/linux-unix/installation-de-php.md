Installation de PHP
===================

[:type]: # "Astuce"
[:version]: # "Debian 8, PHP 5.6"
[:sources]: # "[www.lafermeduweb.net](http://www.lafermeduweb.net/billet/tutorial-creer-un-serveur-web-complet-sous-debian-2-php5-et-mysql-164.html)"
[:created_at]: # "2017/07/21"
[:modified_at]: # "2017/11/28"
[:tags]: # "linux-unix systemes-reseaux php debian"

## Installation ##

### PHP et ses extensions ###

Pour installer la version de PHP disponible par défaut sur le dépôt (PHP 5.6 pour Debian 8), 
procédez comme suit. Pour installer PHP 7, [suivez cet article](./linux-unix/installer-php-7-sous-debian-8) à la place.

* Installez PHP via le gestionnaire de paquets :

```bash
sudo apt-get install libapache2-mod-php5
```

* Installez les extensions dont vous avez besoin. 
Probablement au moins `gd`, `mcrypt`, `php-pear`, `intl` :

```bash
sudo apt-get install php5-gd php5-mcrypt php-pear php5-intl
```

Pour `curl`, il s'agit du paquet `php5-curl`.

### Configuration de PHP ###

* Modifiez le fichier `/etc/php5/apache2/php.ini`. 
À la fin de la section `[Miscellaneaous]`, ajoutez la ligne suivante pour spécifier la locale à utiliser :

```ini
date.timezone = "Europe/Paris"
```

* Redémarrez Apache :

```bash
sudo service apache2 restart
```

### Vérification ###

* Supprimez le fichier `index.html` de votre site (ex : /var/www/mon-site/index.html) et 
créez le fichier `index.php` à la place :

```php
<?php
phpinfo();
?>
```

* Appelez l'URL de votre site et vérifier qu'Apache vous retourne bien toute la configuration de PHP.

## Composer ##

Si vous avez besoin de **Composer**, installez-le via ces commandes :

```bash
sudo apt-get install curl 
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer 

# Vérification
composer --version
```
