Bypasser un runlevel
====================

[:type]: # "Astuce"
[:created_at]: # "2019/12/19"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux"

Pendant le lancement du système, des services sont démarrés.  
L'ordre de lancement a une importance, car beaucoup de services ont besoin d'autres services pour fonctionner.

Sous linux il y a donc la notion de **runlevel** (cf. [wikipédia](https://fr.wikipedia.org/wiki/Run_level)).
Les runlevels (niveau d'exécution) vont de 0 à 6 et se font un à un dans l'ordre croissant.

Il arrive qu'une erreur se produise lors du lancement d'un service à un certain runlevel, bloquant ainsi le démarrage.  
Il est possible de forcer le passage au niveau suivant, même si tout n'est pas encore terminé.

Pour cela appuyez sur les touches suivantes durant l'initialisation : `CTRL` + `ALT` + `F<NIVEAU>`  
Avec `<NIVEAU>` le niveau de runlevel vers lesquels passer (ex: `CTRL` + `ALT` + `F4` pour passer au niveau 4).
