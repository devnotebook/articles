Autoriser une machine distante à accéder à un serveur web
=========================================================

[:type]: # "Astuce"
[:created_at]: # "2013/02/13"
[:modified_at]: # "2017/05/03"
[:tags]: # "linux-unix systemes-reseaux apache"

Par défaut, le pare-feu bloque les ports utilisés par Apache, à savoir `80` et `443`.

Pour autoriser une machine distante à se connecter à Apache, ajoutez les lignes suivantes au 
début du fichier `/etc/sysconfig/iptables` :

```
-A RH-Firewall-1-INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A RH-Firewall-1-INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
```

Redémarrez maintenant le pare-feu pour qu'il prenne en compte la modification :

```bash
service iptables restart
```
