Installer PHP 7 sous Debian 8
=============================

[:type]: # "Astuce"
[:version]: # "Debian 8, PHP 7"
[:created_at]: # "2017/07/21"
[:modified_at]: # "2017/11/28"
[:tags]: # "linux-unix systemes-reseaux php debian"

Par défaut, les dépôts de Debian 8 propose d'installer **PHP 5.6**. 
Si vous voulez la version 7, procédez ainsi.

* Ajoutez un nouveau dépôt pour `apt`, en tant que super administrateur :

```bash
echo 'deb http://packages.dotdeb.org jessie all' > /etc/apt/sources.list.d/dotdeb.list
wget -O- https://www.dotdeb.org/dotdeb.gpg | apt-key add -
apt update
```

* Installez PHP 7, avec les extensions que vous souhaitez. 
Probablement au moins `gd`, `mcrypt`, `php-pear`, `intl` :

```bash
apt-get -y install php7.0 libapache2-mod-php7.0 php-pear php7.0-gd php7.0-mcrypt php7.0-intl
```

Globalement, les paquets portent le même nom que ceux pour **PHP 5.6**.
