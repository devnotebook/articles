Connaître le poids et le nombre de fichiers d'un répertoire
===========================================================

[:type]: # "Astuce"
[:created_at]: # "2017/05/18"
[:modified_at]: # "2017/05/18"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

## Poids total ##

Pour connaître le poids total des fichiers (et sous-répertoires) que contient un répertoire, 
vous pouvez utiliser la commande `du` :

```bash
du -shL mon/repertoire
```

**Explication :**

Les options `s`, `h` et `L` permettent respectivement de 
**faire la somme**, afficher un **poids lisible** par un humain (en Ko, Mo, ...) 
plutôt qu'en octets et **suivre les liens symboliques** (plutôt que de compter le poids du lien).

## Nombre de fichiers ##

Pour connaître le nombre de fichiers il faut cette fois composer avec plusieurs commandes :

```bash
find -L mon/repertoire -type f | wc -l
```

**Explications :**

* La commande `find` avec l'option `-type f` permet de lister les fichiers
* L'option `-L` permet de suivre les liens symboliques
* La commande `wc -l` compte le nombre de lignes (ici celles retournées par `find`)
