Erreur au lancement d'Apache
============================

[:type]: # "Erreur"
[:sources]: # "[thesystemadministrator.net](http://thesystemadministrator.net/apache/error-usrlocalapachemoduleslibphp5-so-cannot-restore-segment-prot-after-reloc-permission-denied-resolution)"
[:created_at]: # "2013/02/11"
[:modified_at]: # "2017/05/03"
[:tags]: # "linux-unix systemes-reseaux apache"

Lorsque vous lancez Apache sous Linux, via une commande du type :

```bash
/sbin/service apache start
```

Vous pouvez obtenir l'erreur suivante :

```
Syntax error on line xxx of /usr/local/apache2/conf/httpd.conf: Cannot load /usr/local/apache2/modules/libphp5.so into server: /usr/local/apache2/modules/libphp5.so: cannot restore segment prot after reloc: Permission denied
```

Pour corriger l'erreur, exécutez la commande suivante :

```bash
chcon -t textrel_shlib_t /usr/local/apache2/modules/libphp5.so
```
