Afficher les tâches planifiées
==============================

[:type]: # "Astuce"
[:created_at]: # "2013/04/16"
[:modified_at]: # "2017/05/03"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Sous Linux, pour savoir les tâches planifiées pour l'utilisateur courant, utilisez la commande suivante :

```bash
crontab -l
```
