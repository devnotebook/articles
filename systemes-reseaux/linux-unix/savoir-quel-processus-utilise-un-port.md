Savoir quel processus utilise un port
=====================================

[:type]: # "Astuce"
[:sources]: # "[www.croc-informatique.fr](https://www.croc-informatique.fr/2008/09/trouver-le-processus-qui-utilise-un-port-rseau-sur-linux/)"
[:created_at]: # "2019/04/09"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Sous Linux, vous pouvez utiliser la commande suivante pour savoir quel processus utilise un port :

```bash
# Pour le port 80
netstat -tlnp | grep 80
```

Cela retournera par exemple :

```
tcp6       0      0 :::80                   :::*                    LISTEN      32198/apache2
```

L'ID du processus étant ici `32198`.

**Remarque :**
Pour voir tous les processus, il sera peut-être nécessaire de lancer la commande en tant que `root` (ou avec `sudo`).
