Connaître les modules chargés par Apache
========================================

[:type]: # "Astuce"
[:version]: # "CentOS 5"
[:sources]: # "[dan.drydog.com](http://dan.drydog.com/apache2php.html)"
[:created_at]: # "2013/02/12"
[:modified_at]: # "2017/05/03"
[:tags]: # "linux-unix systemes-reseaux apache"

Sous Linux, pour connaitre les modules chargés par Apache, utilisez la commande :

```bash
/usr/local/apache/bin/httpd -M
```

**Remarque :**

Selon votre distribution, remplacez `httpd` par `apache` ou `apachectl`.
