Monter automatiquement un disque partagé au démarrage
=====================================================

[:type]: # "Astuce"
[:version]: # "Debian 8.4, VirtualBox 5.x"
[:created_at]: # "2016/04/15"
[:modified_at]: # "2017/05/04"
[:tags]: # "linux-unix systemes-reseaux"

Avec Virtualbox, on peut partager des répertoires depuis son hôte vers la machine virtuelle.

Par défaut, Virtualbox les monte dans `/media/sf_<nom_partage>` et attribut le répertoire à l'utilisateur `vboxsf`.

Pour monter automatiquement le répertoire à l'endroit de votre choix, vous pouvez modifiez le 
fichier `/etc/rc.local`, et ajouter cette ligne :

```bash
mount -o uid=500,gid=500,umask=002 --bind /media/sf_<nom_partage> /mon/repertoire/cible
```

**Remarque :**

Les options `uid` et `gid` permettent d'attribuer le répertoire à un utilisateur et à un groupe spécifique. 
`500` correspond par exemple à l'utilisateur d'Apache `www-data` (la plupart du temps).
