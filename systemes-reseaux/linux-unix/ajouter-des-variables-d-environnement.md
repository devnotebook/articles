Ajouter des variables d'environnement
=====================================

[:type]: # "Astuce"
[:created_at]: # "2012/12/03"
[:modified_at]: # "2017/05/03"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Pour ajouter une variable d'environnement ou en modifier la valeur, utilisez la commande :

```bash
export INSTALL4J_JAVA_HOME='/var/lib/jdk1.6.0_33'
```

en remplaçant `INSTALL4J_JAVA_HOME` par le nom de la variable à définir et en modifiant la valeur entre **'quotes'**.

Pour que cette variable soit définie automatiquement lorsque vous utilisez la console, il faut modifier le fichier 
`.bashrc` dans le dossier **home** de l'utilisateur pouvant utiliser cette variable. 
Pour l'utilisateur `root` par exemple, il s'agit du fichier `/root/.bashrc`.

Dans ce fichier, ajoutez la même commande :

```bash
export INSTALL4J_JAVA_HOME='/var/lib/jdk1.6.0_33'
```
