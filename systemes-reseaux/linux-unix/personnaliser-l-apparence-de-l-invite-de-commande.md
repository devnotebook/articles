Personnaliser l'apparence de l'invite de commande
=================================================

[:type]: # "Astuce"
[:sources]: # "[www.generation-linux.fr](http://www.generation-linux.fr/index.php?post%2F2009%2F02%2F02%2FBash-%3A-personnalisation-de-l-invite=)"
[:created_at]: # "2015/02/20"
[:modified_at]: # "2017/05/04"
[:tags]: # "linux-unix systemes-reseaux bash"

Sous Linux, vous pouvez personnaliser l'invite de commande, pour y mettre un peu de couleur ou modifier ce qu'il y a 
avant le prompt.

Ex :

![Invite de commande personnalisé](./personnaliser-l-apparence-de-l-invite-de-commande-01.png)

Pour cela, éditez/créez le fichier `~/.bashrc` de votre utilisateur en y ajoutant cette ligne :

```bash
PS1='\[\e[33;01m\]\u \[\033[31;01m\][\[\033[0;37m\]\w\[\033[31;01m\]] \$\[\033[00m\] '
```

Dans cette ligne, les blocs de la forme `\[\e[33;01m\]`, `\[\033[31;01m\]` et `\[\033[0;37m\]` définissent les 
couleurs à utiliser : respectivement jaune, rouge et gris clair.

`\u` affiche l'utilisateur et `\w` le chemin courant. `\$` affiche **$** pour un utilisateur et **#** pour le root.

Il y a plein d'autres paramètres disponibles, pour afficher l'heure, le nom de la machine, ...
