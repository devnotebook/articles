Installation d'Apache
=====================

[:type]: # "Astuce"
[:version]: # "Debian 8, Apache 2.4"
[:sources]: # "[www.lafermeduweb.net](http://www.lafermeduweb.net/billet/tutorial-creer-un-serveur-web-complet-sous-debian-1-apache-160.html#InstApache2)"
[:created_at]: # "2017/07/20"
[:modified_at]: # "2020/11/02"
[:tags]: # "linux-unix systemes-reseaux apache debian"

## Installation ##

Lancez simplement la commande suivante :

```bash
sudo apt-get install apache2
```

## Configuration générale ##

### Modification du charset ###

* Éditez le fichier de configuration du charset :

```bash
sudo nano /etc/apache2/conf-available/charset.conf
```

* Décommentez la ligne :

```apacheconfig
AddDefaultCharset UTF-8
```

### Utilisateur Apache ###

Pour éviter des problèmes de droits, vous pouvez modifier l'utilisateur et le groupe unix utilisés par Apache.

Par défaut avec Debian, il s'agit de `www-data:www-data`.

Pour en changer, modifiez le fichier `/etc/apache2/envvars` avec les droits administrateur :

```apacheconfig
export APACHE_RUN_USER=phpuser 
export APACHE_RUN_GROUP=phpuser
```

### Activation du module de réécriture d'URL ###

Pour activer le module `rewrite` d'Apache et redémarrez Apache, utilisez ces commandes :

```bash
sudo a2enmod rewrite
sudo service apache2 restart
```

## Configuration propre à votre site ##

### Initialisation de votre site ###

* Créez le répertoire racine pour votre site :

```bash
cd /var/www
sudo mkdir mon-site
sudo chown -R phpuser:phpuser mon-site
```

* Créez un fichier `index.html` à l'intérieur, contenant par exemple `<h1>Hello world !</h1>`

### Configuration du virtual host ###

Si vous souhaitez accéder à votre site via un nom de domaine spécifique (plutôt que par son IP), 
ou si vous en avez besoin de plusieurs pour accéder à votre application, 
il vous faut configurer un virtual host.

Créez un nouveau fichier `.conf` dans le répertoire des sites disponibles d'Apache 
`/etc/apache2/sites-available/`. (Ex: /etc/apache2/sites-available/mon-site.conf).

```apacheconfig
<VirtualHost *:80>
    ServerAdmin admin@mon-site.com
    ServerName www.mon-site.com
    ServerAlias *.mon-site.com
    DocumentRoot /var/www/mon-site/
    <Directory /var/www/mon-site/>
        Options FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
    ErrorLog ${APACHE_LOG_DIR}/mon-site.log
    ServerSignature Off
</VirtualHost>
```

**Explications :**

* `ServerAdmin` : Adresse email de la personne à prévenir en cas de problème côté Apache.
* `ServerName` : Nom de domaine que vous souhaitez associer au serveur. 
Il doit être dans les DNS du serveur (ou si vous êtes en mode dev, dans votre fichier `/etc/hosts`).
* `ServerAlias` : Autres domaines ou sous domaines qui prendront en compte le même fichier vHost.
* `DocumentRoot` : Répertoire vers lequel Apache redirigera les adresses IP et ports spécifiés 
plus haut (*:80).
* `Directory` : Cette instruction permet d'ajouter des options et règles au répertoire web :
  * `FollowSymLinks` : Active le suivi des liens symboliques dans ce répertoire.

Activez votre virtual host, désactivez éventuellement celui par défaut puis redémarrez Apache :

```bash
sudo a2ensite mon-site
sudo a2dissite default
sudo service apache2 restart
```

## Vérification ##

Appelez la page d'accueil de votre site via un navigateur ou wget depuis votre répertoire utilisateur :

```bash
cd ~
wget mon-site.com
cat index.html
```

Vous devez retrouver le contenu de votre fichier `index.html` : 
```html
<h1>Hello world !</h1>
```

