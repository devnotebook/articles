Configurer un site en HTTPS
===========================

[:type]: # "Astuce"
[:version]: # "Apache 2.2"
[:created_at]: # "2015/08/06"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux apache"

## Prérequis ##

Pour pouvoir utiliser le [SSL/TLS](https://fr.wikipedia.org/wiki/Transport_Layer_Security) et passer votre site 
en HTTPS, les paquets suivants doivent être installés sur votre serveur :

* **openssl** : API de chiffrement
* **mod_ssl** : Module apache

De plus, vous aurez besoin d'un certificat pour effectuer le chiffrement.

Celui-ci doit être obtenu auprès d'une [autorité de certification](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_certification) et 
stocké dans un endroit sécurisé de votre serveur.

À des fins de test, ou pour un site privé, il est possible de générer son propre certificat et de l'auto-signer.

### Générer un certificat auto-signé ###

Pour cela vous devez probablement être connecté en root sur votre serveur (ou sudo) :

```bash
# Génération d'une clé privée
openssl genrsa -out my_certif.key 2048
# Génération d'un fichier CSR
openssl req -new -key my_certif.key -out my_certif.csr
# Génération de la clé auto-signée
openssl x509 -req -days 365 -in my_certif.csr -signkey my_certif.key -out my_certif.crt
```

Vous avez maintenant deux fichiers particuliers :

* `my_certif.crt`
* `my_certif.key`

## Configuration Apache ##

### httpd.conf ###

Voici les paramètres standards d'Apache pour utiliser le HTTPS :

```apacheconfig
##
## SSL Global Context
##
## All SSL configuration in this context applies both to
## the main server and all SSL-enabled virtual hosts.
##
 
LoadModule ssl_module modules/mod_ssl.so
 
#
# When we also provide SSL we have to listen to the
# the HTTPS port in addition.
#
Listen 443
 
# Necessary if you have several virtual hosts on 443 port
NameVirtualHost *:443 
 
# Pass Phrase Dialog:
# Configure the pass phrase gathering process.
# The filtering dialog program ('builtin' is a internal
# terminal dialog) has to provide the pass phrase on stdout.
SSLPassPhraseDialog builtin
 
# Inter-Process Session Cache:
# Configure the SSL Session Cache: First the mechanism
# to use and second the expiring timeout (in seconds).
SSLSessionCache shmcb:/var/cache/mod_ssl/scache(512000)
SSLSessionCacheTimeout 300
 
# Semaphore:
# Configure the path to the mutual exclusion semaphore the
# SSL engine uses internally for inter-process synchronization.
SSLMutex default
 
# Pseudo Random Number Generator (PRNG):
# Configure one or more sources to seed the PRNG of the
# SSL library. The seed data should be of good random quality.
# WARNING! On some platforms /dev/random blocks if not enough entropy
# is available. This means you then cannot use the /dev/random device
# because it would lead to very long connection times (as long as
# it requires to make more entropy available). But usually those
# platforms additionally provide a /dev/urandom device which doesn't
# block. So, if available, use this one instead. Read the mod_ssl User
# Manual for more details.
SSLRandomSeed startup file:/dev/urandom 256
SSLRandomSeed connect builtin
#SSLRandomSeed startup file:/dev/random 512
#SSLRandomSeed connect file:/dev/random 512
#SSLRandomSeed connect file:/dev/urandom 512
 
#
# Use "SSLCryptoDevice" to enable any supported hardware
# accelerators. Use "openssl engine -v" to list supported
# engine names. NOTE: If you enable an accelerator and the
# server does not start, consult the error logs and ensure
# your accelerator is functioning properly.
#
SSLCryptoDevice builtin
#SSLCryptoDevice ubsec
```

Très souvent, apache propose un fichier de configuration dédié à ce paramétrage (ex: `ssl.conf`). 
Vérifiez qu'il est bien inclus par le `httpd.conf`, ou créez-le.

### Virtualhost ###

Il faut ensuite configurer un virtualhost pour le port `443` (celui par défaut pour le HTTPS) :

```apacheconfig
<VirtualHost *:443>
DocumentRoot /var/www/mon_site
ServerName mon-site.com
 
# SSL Engine Switch:
# Enable/Disable SSL for this virtual host.
SSLEngine on
 
# SSL Protocol support:
# List the enable protocol levels with which clients will be able to
# connect. Disable SSLv2 access by default:
SSLProtocol all -SSLv2
 
# SSL Cipher Suite:
# List the ciphers that the client is permitted to negotiate.
# See the mod_ssl documentation for a complete list.
SSLCipherSuite ALL:!ADH:!EXPORT:!SSLv2:RC4+RSA:+HIGH:+MEDIUM:+LOW
 
# Server Certificate:
# Point SSLCertificateFile at a PEM encoded certificate. If
# the certificate is encrypted, then you will be prompted for a
# pass phrase. Note that a kill -HUP will prompt again. A new
# certificate can be generated using the genkey(1) command.
SSLCertificateFile /chemin/vers/certifs/my_certif.crt
 
# Server Private Key:
# If the key is not combined with the certificate, use this
# directive to point at the key file. Keep in mind that if
# you've both a RSA and a DSA private key you can configure
# both in parallel (to also allow the use of DSA ciphers, etc.)
SSLCertificateKeyFile /chemin/vers/certifs/my_certif.key
 
# SSL Protocol Adjustments:
# The safe and default but still SSL/TLS standard compliant shutdown
# approach is that mod_ssl sends the close notify alert but doesn't wait for
# the close notify alert from client. When you need a different shutdown
# approach you can use one of the following variables:
# o ssl-unclean-shutdown:
# This forces an unclean shutdown when the connection is closed, i.e. no
# SSL close notify alert is send or allowed to received. This violates
# the SSL/TLS standard but is needed for some brain-dead browsers. Use
# this when you receive I/O errors because of the standard approach where
# mod_ssl sends the close notify alert.
# o ssl-accurate-shutdown:
# This forces an accurate shutdown when the connection is closed, i.e. a
# SSL close notify alert is send and mod_ssl waits for the close notify
# alert of the client. This is 100% SSL/TLS standard compliant, but in
# practice often causes hanging connections with brain-dead browsers. Use
# this only for browsers where you know that their SSL implementation
# works correctly.
# Notice: Most problems of broken clients are also related to the HTTP
# keep-alive facility, so you usually additionally want to disable
# keep-alive for those clients, too. Use variable "nokeepalive" for this.
# Similarly, one has to force some clients to use HTTP/1.0 to workaround
# their broken HTTP/1.1 implementation. Use variables "downgrade-1.0" and
# "force-response-1.0" for this.
SetEnvIf User-Agent ".*MSIE.*" \
nokeepalive ssl-unclean-shutdown \
downgrade-1.0 force-response-1.0
 
<Directory /var/www/mon_site>
    #Options FollowSymLinks
    #AllowOverride None
    Order allow,deny
    allow from all
</Directory>
</VirtualHost>
```

**Explication :**

Le virtualhost contient les informations habituelles (nom de domaine, répertoire racine, ...) mais également des 
configurations propres au SSL/TLS et les chemins vers les certificats que vous avez générés ou obtenus 
auprès d'une Autorité de certification.

De plus, il répond sur le port `443` et non le `80` habituel.

### Forcer le HTTPS ###

Pour éviter que les utilisateurs du site n'aient aucune réponse lorsqu'ils tapent 
`(http://)mon-site.com` dans leur navigateur, il est judicieux de rediriger les accès en HTTP vers l'URL en HTTPS.

Pour cela, ajoutez un second virtualhost gérant la redirection :

```apacheconfig
<VirtualHost *:80>
    DocumentRoot /var/www/mon_site
    ServerName mon-site.com
    Redirect permanent / https://mon-site.com:443/
</VirtualHost>
```
