Créer un lien symbolique
========================

[:type]: # "Astuce"
[:created_at]: # "2017/03/02"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Pour créer un lien symbolique sou linux, il suffit d'utiliser la commande suivante :

```bash
ln -s chemin/vers/la/cible/du/lien chemin/vers/lien
```

Le premier paramètre contient le répertoire ou **fichier à cibler**, et le second le chemin/nom du **lien à créer**.

Les chemins peuvent-être relatifs ou absolus.
