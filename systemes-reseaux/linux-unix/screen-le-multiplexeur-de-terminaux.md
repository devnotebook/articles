Screen : le multiplexeur de terminaux
=====================================

[:type]: # "Astuce"
[:sources]: # "[doc.ubuntu-fr.org](http://doc.ubuntu-fr.org/screen)"
[:created_at]: # "2015/03/19"
[:modified_at]: # "2020/11/08"
[:tags]: # "linux-unix systemes-reseaux outils"

Screen permet d'ouvrir plusieurs terminaux dans une même console, de passer de l'un à l'autre et de les mémoriser pour 
les récupérer plus tard.

Si vous avez une interface graphique sous Linux, vous avez souvent plusieurs consoles d'ouvertes, ou même une seule avec 
plusieurs onglets.

Mais si vous n'avez pas d'interface graphique, ou si vous accédez à votre machine linux avec PuTTY, vous n'avez pas cette possibilité. 
C'est là que Screen entre en jeu.

## Installation ##

Screen est disponible sur les dépôts standards. Vous pouvez donc l'installez simplement via **apt** ou **yum**.

**Remarque :**

Sous Debian, vous devrez peut-être ajouter des dépôts supplémentaires ([Ex: Debian 7.x](http://librox.net/spip.php?article86))

## Utilisation ##

Voici un cas d'utilisation classique de Screen.

* Vous accédez à votre serveur en SSH depuis votre **machine A** au boulot.
* Vous tapez plusieurs commandes dont une partie connectée avec l'utilisateur **user1**, et l'autre avec **user2**.
* Vous quittez le boulot et rentrez chez vous. Vous souhaitez récupérer sur votre **machine B** votre console telle 
que laissée en quittant la **machine A**.

Tout ça est possible avec Screen et ces quelques commandes

* Accédez au serveur en SSH comme d'habitude, depuis votre **machine A** au boulot
* Créez un nouveau screen en nommant la session :

    ```bash
    screen -S ma_session
    ```

* Exécutez une commande, connecté en **user1**
* Ouvrez un nouveau terminal avec `CTRL + A`, puis `C`
* Changez d'utilisateur (**user2**) et exécutez une commande
* Naviguez vers l'autre terminal (connecté en **user1**) avec `CTRL + A`, puis `N`
* Exécutez une commande, connecté en **user1**
* Détachez le screen (il reste actif mais vous n'y êtes plus connecté) avec `CTRL + A`, puis `D`
* Éteignez la **machine A**, retournez à la maison
* Allumez votre **machine B** à la maison, et connectez-vous au serveur en SSH comme d'habitude.
* Récupérez votre session screen :

    ``` bash
    screen -r
    ```

Vous récupérez ainsi les deux terminaux dans l'état où vous les avez laissés.

**Remarque :**

Si vous avez plusieurs sessions en cours, `screen -r` vous en affichera la liste. 
Il faudra utiliser `screen -r nom_de_session` pour choisir celle que vous souhaitez récupérer.


## Bonus ##

1. Pour fermer le terminal courant : `CTRL + D`.
Si c'était le seul, cela quittera screen.
2. Pour naviguer rapidement entre les deux derniers terminaux utilisés : `CTRL + A`, puis `A`
3. Pour pouvoir scroller dans votre terminal lorsque tout n'est pas affiché : 
`CTRL + A`, puis `ECHAP`
Vous pouvez maintenant naviguer avec les flèches et `pageUp`/`pageDown`.
4. Pour se connecter à un screen encore attaché : `screen -x ma_session`
5. Pour supprimer une session, connectez-vous-y et tapez `exit`
6. Pour forcer une session à se détacher (par exemple si vous avez fermé votre terminal sans avoir détaché votre session) : `screen -d ma_session`
7. Pour renommer une session : `CTRL + A`puis tapez `:nom_session new_nom_session` et validez avec `Entrée`
8. Pour personnaliser l'apparence de screen, et même lancer/récupérer automatiquement un screen dès votre connexion, 
[suivez cet article](https://fiat-tux.fr/tag/screen/) et ses commentaires.

