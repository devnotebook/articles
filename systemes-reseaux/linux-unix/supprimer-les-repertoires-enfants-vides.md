Supprimer les répertoires enfants vides
=======================================

[:type]: # "Astuce"
[:created_at]: # "2012/06/05"
[:modified_at]: # "2017/05/03"
[:tags]: # "linux-unix systemes-reseaux bash fonctions-utiles"

Pour supprimer récursivement tous les répertoires vides d'une arborescence, utilisez la commande :

```bash
find -depth -type d -empty -exec rmdir {} \;
```
