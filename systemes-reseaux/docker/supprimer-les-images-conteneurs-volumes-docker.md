Supprimer les images/conteneurs/volumes docker
==============================================

[:type]: # "Astuce"
[:sources]: # "[www.bowlman.org](https://www.bowlman.org/2017/10/01/docker-supprimer-des-images-et-les-containers/)"
[:created_at]: # "2019/09/19"
[:modified_at]: # "2019/12/19"
[:tags]: # "docker systemes-reseaux fonctions-utiles"

Pour supprimer tous les conteneurs docker :

```bash
docker rm $(docker ps -a -q)
```

Pour supprimer toutes les images docker :

```bash
docker rmi $(docker images -q)
```

Pour supprimer tous les volumes docker :

```bash
docker volume prune
```
