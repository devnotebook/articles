Docker écrase le réseau VPN
===========================

[:type]: # "Erreur"
[:sources]: # "[www.lullabot.com](https://www.lullabot.com/articles/fixing-docker-and-vpn-ip-address-conflicts)"
[:created_at]: # "2020/05/22"
[:modified_at]: # "2020/05/22"
[:tags]: # "docker systemes-reseaux"

Lorsque docker crée des réseaux virtuels entre ses conteneurs, il utilise des plages IP.  
En général pas de problème, il utilise des plages non couramment utilisées.

Par contre, si ses plages habituelles ne sont pas disponibles (ou pour d'autres raisons ?), 
il est possible qu'il en utilise une autre, par exemple `192.168.x.x`.  
Cela peut alors être problématique, surtout si vous utilisez un VPN ou des ressources réseaux en parallèle, 
car cette plage est classiquement utilisée.
On peut alors avoir un conflit d'adressage et les ressources réseaux ou du VPN peuvent ne plus être accessibles.

Pour corriger ça, une première approche consiste à killer le réseau (créé par docker) qui pose problème puis à le recréer.  
Pour cela :

```bash
# Listage des réseaux créés par docker
docker network list

# Identifiez le réseau correspondant au conteneur qui a causé le problème (copiez son ID)
# Pour vérifier la plage IP qu'il utilise :
docker network inspect <ID>

# Suppression du réseau qui pose problème
docker network rm <ID>
```

Si cela ne suffit pas, essayez la suite de la [procédure proposée ici](https://www.lullabot.com/articles/fixing-docker-and-vpn-ip-address-conflicts).
