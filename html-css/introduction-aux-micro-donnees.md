Introduction aux micro-données
==============================

[:type]: # "Astuce"
[:created_at]: # "2015/07/29"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

## Principe ##

Les micro-données permettent de renforcer la sémantique de votre code HTML. 
Elles sont interprétées par les principaux moteurs de recherche (Google, Yahoo, Bing, ...).

Par exemple, si vous présentez un film, vous aller pouvoir indiquer que telle valeur est le titre du film, 
telle autre est sa date de sortie, son réalisateur, ...

C'est ce genre de données qui aide Google à créer les encarts à droite des résultats de recherche :

![Recherche Google](./introduction-aux-micro-donnees-01.png)

De très nombreuses choses peuvent être décrites grâce aux micro-données (évènement sportif, adresse, personne, 
recette de cuisine, ...).

Elles sont de plus organisées avec un système d'héritage (comme en langage objet). 
Le site [schema.org](http://schema.org/docs/full.html) récapitule tout ça de manière arborescente.

## Application ##

Les micro-données se présentent sous forme d'attributs HTML, utilisables sur n'importe quelle balise du DOM à l'intérieur du body.

Exemple :

```html
<p itemtype="http://schema.org/PostalAddress" itemscope="">
    <span itemprop="streetAddress">1 Grande Rue</span><br />
    <span itemprop="postalCode">75000</span>
    <span itemprop="addressLocality">Paris</span>
    <meta itemprop="addressCountry" content="FR" />
</p>
```

Cet exemple va afficher un paragraphe avec les informations suivantes :

```
1 Grande Rue
75000 Paris
```

**Explications :**

* Pour annoncer que le paragraphe va contenir une **adresse postale**, on lui ajoute l'attribut `itemtype`, 
avec pour valeur l'URL du schéma de données à utiliser. 
On ajoute également l'attribut `itemscope` pour lier la micro-donnée à l'élément `p`.
* Pour détailler les [différentes parties de l'adresse](https://schema.org/PostalAddress), on les délimite via une 
balise HTML (`span` ici par exemple) à laquelle on ajoute l'attribut `itemprop` contenant le nom de la propriété correspondante.
* Certaines propriétés sont obligatoires et doivent être définies même si on ne veut pas les voir à l'écran. 
Pour les ajouter au code HTML sans les afficher (et sans CSS particulier), on utilise une balise `meta` autofermante, 
dont le contenu est spécifié via l'attribut `content`.

## Bonus ##

Pour valider vos micro-données et savoir comment Google les interprétera, 
il fournit un outil de validation à cette adresse : 
<https://developers.google.com/structured-data/testing-tool/>

Le site **alsacréations** propose [un article plus détaillé](http://www.alsacreations.com/article/lire/1509-microdata-microformats-schema-semantique.html) 
sur les micro-données.
