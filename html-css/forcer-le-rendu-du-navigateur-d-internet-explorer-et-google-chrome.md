Forcer le rendu du navigateur d'Internet Explorer et Google Chrome
==================================================================

[:type]: # "Astuce"
[:created_at]: # "2013/04/19"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Lorsque vous affichez une page sous **Internet Explorer**, il arrive très souvent que ce navigateur n'utilise pas le 
moteur de rendu le plus récent. Par exemple, vous naviguez avec **IE9** et pourtant le moteur de rendu utilisé est 
celui d'**IE7**.

Cela est problématique car toutes les propriétés CSS reconnues par IE9 mais pas par IE7 ne fonctionnent donc pas.

Ce comportement peut également se produire avec Google Chrome. 
Pour connaitre le moteur de rendu utilisé par IE, appuyez sur `F12` (à partir de **IE8**).

Pour éviter ce problème, vous pouvez forcer le navigateur à utiliser son dernier moteur de rendu, 
via une balise `meta` dans votre `head` HTML :

```html
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
```
