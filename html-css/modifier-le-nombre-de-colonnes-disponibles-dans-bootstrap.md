Modifier le nombre de colonnes disponibles dans Bootstrap
=========================================================

[:type]: # "Astuce"
[:version]: # "Bootstrap 3"
[:created_at]: # "2013/04/19"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Le système de grid de Bootstrap est pratique, mais parfois insuffisant. 
Il propose seulement **12** largeurs de colonne, ce qui limite les possibilités d'affichage.

Ce nombre de colonnes est heureusement paramétrable grâce aux fichiers **.less** des sources de Bootstrap. 
Il vous suffit de modifier et compiler ces sources pour générer une feuille Bootstrap avec le nombre de colonnes de votre choix.

Pour **Bootstrap v3.0.0**, il faut par exemple modifier le fichier `variables.less` :

```css
// Grid system
// --------------------------------------------------
 
// Number of columns in the grid system
@grid-columns:              12;
// Padding, to be divided by two and applied to the left and right of all columns
@grid-gutter-width:         30px;
// Point at which the navbar stops collapsing
@grid-float-breakpoint:     @screen-tablet;
```

**Remarque :**

Si vous n'êtes pas familier avec les fichiers less, vous pouvez utiliser 
le [formulaire de customisation de Boostrap](http://getbootstrap.com/customize/) proposé sur le site officiel.
Il permet de modifier non seulement le nombre de colonnes, mais beaucoup d'autres paramètres.
