Centrer verticalement en CSS
============================

[:type]: # "Astuce"
[:created_at]: # "2015/07/17"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Pour centrer un élément par rapport à son conteneur, il faut utiliser la propriété CSS `vertical-align` 
avec pour valeur `middle` :

```css
vertical-align: middle;
```

Initialement, cette propriété n'était utilisable que dans un tableau, où les cellules ont un display particulier.

Elle est maintenant disponible pour tout élément dont le **display** est `inline-block`.

Voici un exemple avec une image, un bloc de texte et un tableau centrés verticalement par rapport à un conteneur :

![Centrer verticalement en CSS](./centrer-verticalement-en-css-01.png)

Voici le code HTML associé :

```html
<html class="no-js" lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Centrer verticalement en CSS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">[...]</style>
</head>
<body>
    <div class="container">
        <!-- Image -->
        <div class="child item-1">
            <img src="image.jpg" alt="" />
        </div>
 
        <!-- Texte -->
        <p class="child item-2">
            Duis eget eros neque. Nunc ante ex, scelerisque vehicula orci in, ultrices lacinia lorem. Aenean tincidunt a felis gravida egestas. Suspendisse potenti. Nullam congue commodo euismod. Sed non rutrum orci. Proin eu neque finibus augue aliquam hendrerit in vitae odio. Aenean sagittis mauris quis eros egestas rutrum. Nam tempor ante id lacus placerat dignissim. Aliquam mollis ligula nec justo consequat, id laoreet libero tincidunt. Sed accumsan hendrerit velit et fermentum. Morbi aliquet rhoncus ante vitae posuere. Integer malesuada augue turpis, vel egestas ligula pharetra nec. Quisque ut malesuada mi, non consequat nunc. Nullam posuere, enim quis ullamcorper cursus, lectus nunc pulvinar tellus, in dignissim augue lacus eget sapien.<br />
            Sed mollis metus tortor. Proin pulvinar augue sed augue sodales, vel interdum mi interdum.
        </p>
 
        <!-- Tableau -->
        <table class="child item-3">
            <tr>
                <th>Colonne 1</th>
                <th>Colonne 2</th>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
            </tr>
        </table>
    </div>
</body>
</html>
```

Et le CSS :

```css
img {
    max-width: 100%;
}
.container {
    display: inline-block; /* Ou block */
    line-height: 400px; /* Facultatif si l'un des enfants prend toute la hauteur */
    background-color: #EAF3FB;
}
.child {
    display: inline-block; /* Obligatoire */
    padding: 20px;
    line-height: 1; /* Facultatif si le parent n'a pas de line-height. */
    vertical-align: middle; /* Obligatoire */
    background-color: #FFFFFF;
}
.item-1 {
    width: calc(15% - 40px);
}
.item-2 {
    width: calc(45% - 40px);
}
.item-3 {
    width: calc(40% - 8px); /* Dernier élément donc -40px inutile */
}
```

**Remarque :**

Seules les propriétés marquées comme obligatoires/facultatives concernent le centrage vertical.

**Bonus :**

* `max-width: 100%;` évite que l'image ne dépasse de son conteneur lorsque la fenêtre est rétrécie en largeur
* `width: calc(40% - 40px);` permet de définir la taille de l'élément en pourcentage par rapport à son parent, 
tout en utilisant un padding. 
En toute logique **la somme des padding left et right doit être soustraite au %**. 
Il est à noter que le dernier élément n'a pas besoin de `-40px`, car son padding est intégré automatiquement 
lors du calcul de sa largeur. (`calc()` est reconnu depuis IE9 seulement)
* `width: calc(40% - 8px);` permet de gérer la marge automatique ajoutée entre les éléments inline-block. 
Pour une taille de police non modifiée, un espace fait `4px` de large. 
On a 3 éléments, donc 2 espaces, soient les `8px`. Sans cette histoire d'espace `width: 40%;` aurait suffit.
