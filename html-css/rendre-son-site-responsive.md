Rendre son site responsive
==========================

[:type]: # "Astuce"
[:created_at]: # "2013/04/19"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Pour qu'un site s'affiche correctement quel que soit le terminal de 
visualisation (mobile, tablette, ordinateur, ...), il faut adapter son code HTML et surtout son CSS. 
En effet, en fonction de la résolution du terminal, l'apparence d'un site peut être totalement dégradée.

## Côté HTML ##

Le nombre de **pixels affichables** par un terminal mobile ne correspond pas au nombre de **pixels CSS 
utilisables** par le navigateur. 
Un [article complet](http://www.alsacreations.com/article/lire/1490-comprendre-le-viewport-dans-le-web-mobile.html) sur le sujet est disponible sur le site **alsacréations**.

Concrètement, pour synchroniser ces deux "types de pixels", ajoutez la balise `meta` suivante dans votre `head` HTML:

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
```

## Côté CSS, les media queries ##

CSS 3 introduit un nouvel élément : **les media queries**. Elles permettent de définir des propriétés de manière conditionnelle.

Vous pouvez par exemple définir que tout un bloc CSS ne doit être interprété que si la taille de la fenêtre est 
inférieure à `400px`. Cela est très utile dans le cas du responsive, car on peut ainsi influer sur la taille 
des blocs de la page, leurs marges, s'ils doivent apparaître ou non, et tout ça en fonction de la résolution du 
terminal.

Voici comment utiliser les media queries directement dans vos feuilles CSS :

```css
/* Écrans normaux - entre 860px et 1140px */
@media only screen and (min-width: 53.75em) and (max-width: 71.25em) {
 
    /* Largeur de la page */
    div#page {
        width: 100%;
    }
 
    /* Menu 2 */
    #menu2 {
        display: none;
    }
}
 
/* Écrans moyens - entre 592px et 859px */
@media only screen and (min-width: 37em) and (max-width: 53.6875em) {
 
    /* Largeur de la page */
    div#page {
        width: 90%;
    }
}
 
/* Petits écrans - en dessous 592px */
@media only screen and (max-width: 36.9375) {
 
    /* Largeur de la page */
    div#page {
        width: 80%;
    }
}
```

Cet exemple présente 3 fourchettes de tailles répandues, pour lesquelles l'affichage est amélioré.

**Explications :**

* On ajoute un niveau d'imbrication à la feuille css en ajoutant des blocs media query.
* Dans chacun de ces blocs on définit les propriétés css à appliquer uniquement si la condition est vraie.
* Les conditions utilisées dans ces media queries définissent une largeur maxi et/ou mini d'écran. (D'autres conditions sont possibles.)

**Remarques :**

* IE 8 et inférieur ne reconnaissent pas les media queries, ainsi que Firefox 3. 
La [librairie Respond.js](https://github.com/scottjehl/Respond) permet alors de les simuler.
* Vous pouvez également définir ces conditions de media queries directement dans votre head HTML, 
via l'attribut `media` de la balise `link`.

[Un article complet](http://www.alsacreations.com/article/lire/930-css3-media-queries.html) sur les media queries est 
présent sur le site **alsacréations**.
