Éviter les flottements en CSS avec clear
========================================

[:type]: # "Astuce"
[:created_at]: # "2013/09/24"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Lorsque vous faites flotter un bloc en CSS, le suivant essaiera de s'afficher à droite du premier (avec `float: left;`), 
ou à sa gauche (avec `float: right;`).

Si vous voulez qu'il s'affiche à la ligne, vous avez plusieurs solutions :

* Supprimer le float du premier bloc
* Définir des largeurs au deux éléments, pour qu'ils ne rentrent pas côte à côte.
* Utiliser la propriété `clear: left`.

La propriété `clear` peut prendre principalement 4 valeurs : `none` (par défaut), `left`, `right` et `both`.

* Si la valeur sur un bloc est à `left`, et qu'un flottant se trouve **à sa gauche**, le bloc ira **à la ligne**.
* Si la valeur sur un bloc est à `right`, et qu'un flottant se trouve **à sa droite**, le bloc ira **à la ligne**.
* Si la valeur sur un bloc est à `both`, et qu'un flottant se trouve **à sa droite ou à sa gauche**, le bloc ira **à la ligne**.
* Si la valeur sur un bloc est à `none`, qu'un flottant se trouve **à côté et qu'il a la place** il **se positionnera à côté**.

Exemple d'utilisation :

```html
<div style="width: 90px;">Mon bloc conteneur avec une largeur de 90px</div>
    <div style="float: left; width: 30px;">Mon premier flottant</div>
    <div style="float: left; width: 30px;">Mon deuxième flottant</div>
    <div style="clear: left; width: 30px;">Mon bloc à afficher en dessous</div>
</div>
```

**Explication :**

Le troisième bloc a la place de s'afficher à droite des deux premiers.

* Sans `clear: left;`, il s'affiche **à côté** d'eux.
* Avec `clear: left;`, il s'affiche **en dessous**.
