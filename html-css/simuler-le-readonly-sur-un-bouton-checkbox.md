Simuler le readonly sur un bouton checkbox
==========================================

[:type]: # "Astuce"
[:created_at]: # "2013/06/18"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Dans un formulaire, vous pouvez brider les éléments input, pour ne pas qu'ils soient modifiables par les utilisateurs.

Cela revient au même qu'un champ de type hidden, sauf qu'il sera visible par l'utilisateur. 
Malheureusement cet attribut ne fonctionne que sur les champs input de type `text`.

```html
<!-- Champ texte : le readonly fonctionne -->
<input type="text" name="champ1" id="champ1" value="valeur1" readonly="readonly" />
 
<!-- Champ checkbox : le readonly ne fonctionne pas -->
<input type="checkbox" name="champ2" id="champ2" value="valeur2" readonly="readonly" />
```

Pour simuler le même fonctionnement avec un champ input de type `checkbox`, vous pouvez utiliser l'attribut `disabled` :

```html
<!-- Champ checkbox avec disabled -->
<input type="checkbox" name="champ2" id="champ2" value="valeur2" disabled="disabled" />
<input type="hidden" name="champ2" id="champ2Hidden" value="valeur2" />
```

**Explications :**

* La checkbox étant `disabled`, sa valeur ne sera pas envoyée par le navigateur à la page cible du formulaire. 
* Pour parer à ça, on la couple a un input de type `hidden`, avec le même attribut `name` et la même valeur.
C'est lui qui enverra la donnée.
