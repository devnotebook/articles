Utiliser une police de caractère particulière pour son site
===========================================================

[:type]: # "Astuce"
[:created_at]: # "2012/07/02"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Pour personnaliser un site entier ou un titre, on peut utiliser des polices non standards (c'est à dire pas `Arial`, ...).

Pour cela il y a deux solutions possibles :

* Utiliser les polices web Google (choix parmi près de 1000 polices).
* Utiliser votre propre police, dont vous possédez les fichiers de police (`.tiff`, ...)

Les deux solutions consistent via CSS, à indiquer le chemin vers un fichier de police à utiliser. 
Dans le premier cas, la police est hébergée sur les serveurs Google et dans le second sur le vôtre.

## Polices web Google ##

* Allez à l'adresse [http://www.google.com/webfonts/](http://www.google.com/webfonts/),
* Choisissez votre police (ex: Cabin)
* Incluez le fichier CSS fourni par Google dans votre page web. Ex :

    ```html
    <link href='http://fonts.googleapis.com/css?family=Cabin' rel='stylesheet' type='text/css' />
    ```
    
* Vous pouvez maintenant utiliser la police comme une police standard :
    
    ```css
    body {
        font-family: 'Cabin';
    }
    ```

## Votre propre police ##

Si vous souhaitez utiliser votre propre police, vous devez avoir toute une série de fichiers de polices, 
pour que celle-ci soit reconnue par tous les navigateurs (testé avec ie7).

Le site [http://www.fontsquirrel.com](http://www.fontsquirrel.com) permet de créer tous les fichiers nécessaires 
à partir d'un fichier de police (ex : `VintlysHand.ttf`). Une fois les fichiers générés et téléchargés :

* Copiez les fichiers de police dans l'arborescence de votre site (seulement `.ttf`, `.woff`, `.svg`, `.eot`).
* Déclarer la police dans votre fichier css :

    ```css
    @font-face {
        font-family: 'VintlysHandRegular';
        src: url('ma_police-webfont.eot');
        src: url('ma_police-webfont.eot?#iefix') format('embedded-opentype'),
             url('ma_police-webfont.woff') format('woff'),
             url('ma_police-webfont.ttf') format('truetype'),
             url('ma_police-webfont.svg#VintlysHandRegular') format('svg');
        font-weight: normal;
        font-style: normal;
    }
    ```

* Vous pouvez maintenant l'utiliser comme une police standard :

    ```css
    body {
        font-family: 'VintlysHandRegular';
    }
    ```

## Une police Google en local ##

Pour des raisons de performances ou d'accès au réseau, vous pouvez choisir d'héberger une police Google 
sur votre serveur. Pour récupérer cette police :

* Affichez le CSS de Google dans Firefox ou Chrome, puis avec IE, puis avec Safari 
(ex : [http://fonts.googleapis.com/css?family=Cabin](http://fonts.googleapis.com/css?family=Cabin)).
* Copiez les URL des polices que vous trouverez dans le CSS et téléchargez_les 
(ex : [http://themes.googleusercontent.com/static/fonts/cabin/v5/K6ngFdK5haaaRGBV8waDwA.ttf](http://themes.googleusercontent.com/static/fonts/cabin/v5/K6ngFdK5haaaRGBV8waDwA.ttf)).
* Renommez tous les fichiers avec un nom de la forme `<nom_police>.<ext>`, et mettez le tout dans un 
répertoire `fonts` (ou autre) là où se trouvent vos CSS.
* Déclarez votre police dans un fichier CSS :

    ```css
    @font-face {
        font-family: 'MaPolice';
        src: url('fonts/nom_police.eot');
        src: url('fonts/nom_police.eot?#iefix') format('embedded-opentype'),
             url('fonts/nom_police.woff') format('woff'),
             url('fonts/nom_police.ttf') format('truetype'),
             url('fonts/nom_police.svg#MaPolice') format('svg');
        font-weight: normal;
        font-style: normal;
    }
    ```
