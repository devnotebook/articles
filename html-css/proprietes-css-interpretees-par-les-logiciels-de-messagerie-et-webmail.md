Propriétés CSS interprêtées par les logiciels de messagerie et webmail
======================================================================

[:type]: # "Marque-page"
[:created_at]: # "2013/04/12"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Lorsque vous créez des mails en HTML, il est très difficile d'avoir le même rendu sur tous les logiciels 
de messagerie et sur tous les webmails.

Le problème vient de l’interprétation non standard du CSS, en particulier par Outlook 2007/2010/2013.

Voici la liste des propriétés reconnues par les logiciels de messagerie et webmails les plus répandues :
<http://www.campaignmonitor.com/css/>
