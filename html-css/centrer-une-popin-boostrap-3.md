Centrer une popin Boostrap 3
============================

[:type]: # "Astuce"
[:version]: # "Bootstrap 3"
[:created_at]: # "2018/04/03"
[:modified_at]: # "2018/04/04"
[:tags]: # "html-css"

Sur le [site de Bootstrap](https://getbootstrap.com/docs/3.3/javascript/#modals-sizes), 
voici le code pour afficher un bouton ouvrant une popin :

```html
<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>
```

Par défaut la popin sera placée en haut de la fenêtre, ce qui n'est pas toujours très joli :

![Popin Bootstrap 3 non centrée verticalement](./centrer-une-popin-boostrap-3-01.png)

Si vous préférez que vos popin soient centrées verticalement, vous pouvez ajouter ces quelques lignes 
à votre feuille de style :

```css
.modal.in .modal-dialog {
    -webkit-transform: translate(0, calc(50vh - 50%));
    -ms-transform: translate(0, 50vh) translate(0, -50%);
    -o-transform: translate(0, calc(50vh - 50%));
    transform: translate(0, 50vh) translate(0, -50%);
}
```

Vous obtenez alors ce résultat :

![Popin Bootstrap 3 centrée verticalement](./centrer-une-popin-boostrap-3-02.png)
