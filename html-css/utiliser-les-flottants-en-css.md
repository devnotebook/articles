Utiliser les flottants en CSS
=============================

[:type]: # "Astuce"
[:created_at]: # "2013/04/19"
[:modified_at]: # "2017/05/15"
[:tags]: # "html-css"

Pour mettre des blocs côte à côte en CSS, il faut faire flotter l'un deux.

Voici un exemple simple avec une structure en 4 blocs comme on en voit souvent : 
un en-tête, une colonne de menus, une colonne principale et un pied de page.

![Exemple de layout en colonnes](./utiliser-les-flottants-en-css-01.png)

## Code HTML ##

Le code html est très simple :

```html
<div id="global-container">
    <div id="header">...</div>
    <div id="navigation">...</div>
    <div id="content">...</div>
    <div id="footer">...</div>
</div>
```

**Explication :**

On a un `div` pour chaque zone du site, et un autre pour le conteneur global.

## Code CSS ##

Le CSS se compose également de 5 parties, une pour chaque div :

```css
#global-container {
    padding: 10%;
    background-color: #CACACA;
}
#header {
    height: 100px;
    margin-bottom: 20px;
    background-color: #868686;
}
#navigation {
    height: 300px;
    width: 20%;
    float: left;
    margin-right: 5%;
    background-color: #262626;
}
#content {
    height: 300px;
    overflow: hidden;
    background-color: #555555;
}
#footer {
    height: 80px;
    margin-top: 20px;
    background-color: #868686;
}
```

**Explications :**

* Le bloc de navigation est positionné à gauche grâce à la propriété `float: left`.
* Le bloc de contenu est bien positionné à droite (et pas en dessous), grâce à la propriété `overflow: hidden`.
* La marge entre les deux est définie sur le bloc de navigation car c'est lui qui est flottant.
* Le bloc de navigation peut flotter par rapport au bloc de contenu, car **il est écrit AVANT**, dans le code HTML.
