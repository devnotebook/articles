Modifier le JDK ou le JRE sous Windows
======================================

[:type]: # "Astuce"
[:created_at]: # "2012/08/31"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

Lorsque vous installez un JDK ou un JRE, il est probable qu'il y ait déjà un autre JRE présent.

Pour que Windows utilise le nouveau, vous devez modifier une variable d'environnement :

- appuyez sur **Windows** + **Pause**
- Paramètres système avancés
- Bouton **Variables d'environnement**
- Sélectionner la variable système à modifier : **Path**.

Vous devez ajouter au tout début le chemin vers le dossier **bin** présent dans le dossier d'installation de votre nouveau JRE ou JDK.
Ex: 

```
C:\Program Files (x86)\Java\jre1.8.0_31\bin
```

Sous Windows 10, vous avez une liste de chemin, il suffit d'en ajouter un et de le déplacer tout en haut.
Pour les versions antérieures de Windows, tous les chemins sous mis bout à bout, et *séparés par une virgule*.
Par exemple, après avoir installé le JRE 1.8 :

![Modifier le JDK ou le JRE sous Windows](./modifier-le-jdk-ou-le-jre-sous-windows-01.png)

Validez ensuite avec ok pour chaque fenêtre.


## Vérifications

Pour vérifier que votre variable est bien prise en compte, ouvrez une invite de commande et tapez :

```bash
echo %Path%
```

Pour vérifier la version de **java** et **javac** courante, tapez les commandes :
```bash
java -version
javac -version
```
