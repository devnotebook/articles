Utiliser l'ajax-refresh pour recharger un élément du DOM
========================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2012/12/18"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

JCMS intègre un framework pour utiliser de l'AJAX dans les pages d'un site.

Pour recharger un élément du DOM à partir d'une jsp, voici le code à utiliser :

```javascript
var id          = 'mon_element';
var url         = 'plugins/MonPlugin/jsp/ajax/maJsp.jsp';
var value       = $(id).value;
var params      = 'key=' + value;
 
// Rechargement de l'élément
JCMS.ajax.Refresh._refresh(id, url, {
    history: false,
    formParams: params
});
```

* L'id est celui de l'élément dont le contenu doit être rechargé (innerHTML).
* L'url est le chemin vers la jsp (et pas jspf) qui génère le contenu.
* `params` est la liste des paramètres à ajouter à l'url, qui pourront être récupérés dans la jsp appelée :

```java
String keyValue = request.getParameter("key");
```
