Utiliser les tests unitaires avec JCMS
======================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/05/21"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Pour utiliser les tests unitaires et **JUnit** dans vos projets JCMS, Jalios fournit quelques classes utilitaires, 
dont `JcmsTestCase`.

Plus de détails dans les articles suivants :

- [Méthode d'initialisation au début de la classe de test](./methode-d-initialisation-au-debut-de-la-classe-de-test.md)
- [Vider toutes les tables de la base de données Derby](./vider-toutes-les-tables-de-la-base-de-donnees-derby.md)
- [Utiliser un logger dans les classes de test](./utiliser-un-logger-dans-les-classes-de-test.md)
