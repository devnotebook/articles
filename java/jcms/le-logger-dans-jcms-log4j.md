Le logger dans JCMS : log4j
===========================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/05/28"
[:modified_at]: # "2017/10/25"
[:tags]: # "jcms java"

Par défaut, JCMS utilise **log4j** pour gestionnaire de logs.

## Utilisation générale de log4j ##

Voici comment l'utiliser dans une classe :

```java
public class MyClass {
 
    /** Logger log4j */
    private static final Logger logger = Logger.getLogger(MyClass.class);
 
    public MyClass() {
        logger.info("Je suis dans le constructeur par défaut de ma classe.");
    }
```

**Explications :**

* Déclarez un nouvel attribut de classe statique qui utilise la méthode `getLogger()` de log4j.
* Passez votre classe en argument de cette méthode.
* Utilisez le logger en appelant ses méthodes `debug()`, `info()`, `warn()`, `error()`
ou `fatal()`, avec le message en argument, plus éventuellement une exception.

## Configuration de log4j ##

Log4j est configuré via le fichier `log4j.xml`. Dans JCMS, ce fichier se trouve dans `WEB-INF/data/`.

Si dans un nouveau projet JCMS vous créez un package pour y mettre vos classes Java, 
il est probable que les logs ne fonctionnent pas. En effet, log4j n'a pas connaissance de ce nouveau package.

Pour l'en informer, éditez le fichier `log4j.xml` et ajoutez :

```xml
<!-- Logger de classes personnalisées --> 
<logger name="mon.package" additivity="false">
    <level value="DEBUG" />
    <appender-ref ref="JCMS" />
    <appender-ref ref="CONSOLE" />
    <appender-ref ref="LOGFILE" />
    <appender-ref ref="PLUGIN" />
</logger>
```

**Explications :**

* L'attribut `name` définit pour quels packages utiliser log4j. 
(Les sous-packages seront automatiquement logués.)
* L'attribut `value` de l'élément `<level>` permet de déterminer quel niveau de log appliquer. 
En développement on utilise souvent `DEBUG` ou `INFO` alors qu'en production on se contentera 
de `INFO` ou `WARN`. Concrètement, avec le niveau `WARN`, les méthodes `logger.debug()` 
et `logger.info()` n'auront aucun effet (suivant cet ordre : `debug` < `info` < `warn` 
< `error` < `fatal`).
* Les `<appender>` sont les sorties à utiliser : fichier de log, console, ...
* Vous pouvez déclarer autant de logger que vous le souhaitez dans le fichier `log4j.xml`.

