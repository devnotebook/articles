Autoriser plusieurs soumissions de formulaire
=============================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2015/01/05"
[:modified_at]: # "2017/10/25"
[:tags]: # "jcms java"

Par mesure de sécurité, JCMS limite le nombre de soumissions des formulaires. 
Il faut attendre environ une minute entre chaque soumission.

Ce comportement peut-être éviter en ajoutant cette classe css sur la balise `form` du formulaire : 
`noSingleSubmitButton`.
