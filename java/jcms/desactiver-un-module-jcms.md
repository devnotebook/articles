Désactiver un module JCMS
=========================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/03/12"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Pour empêcher un module d'être chargé au démarrage de JCMS, modifiez son `plugin.xml`.

Passez l'attribut `initialize` de l'élément racine à `false`.
