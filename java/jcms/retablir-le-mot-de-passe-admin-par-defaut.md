Rétablir le mot de passe admin par défaut
=========================================

[:type]: # "Astuce"
[:version]: # "JCMS 7, JPlatform 10"
[:created_at]: # "2013/10/14"
[:modified_at]: # "2020/11/08"
[:tags]: # "jcms java"

Si vous avez perdu le mot passe administrateur, ou si vous souhaitez le modifier directement dans le store, 
ajoutez une ligne de la forme suivante à la fin :

```xml
<member stamp="j_99999" id="j_2" op="update" author="" mdate="1091526019085" mobile="" name="Admin" password="ISMvKXpXpadDiUoOSoAfww==" />
```

Cette ligne remplace le mot de passe actuel par `admin`.

**Remarques :**

* Remplacez la valeur de `stamp` par la même valeur que votre dernière ligne de store, en l'incrémentant.
* Faites de même pour la date de modification (`mdate`).

## JPlatform 10 (merci Axel pour cette MAJ)

Sur JPlatform 10, le chiffrement des mots de passe a changé. La ligne à ajouter pour le réinitialiser devient :

```xml
<member stamp="agi_7217" id="j_2" op="update" mdate="1528122795429" login="admin" name="Admin" password="$2a$10$gbCw7UaAI1kL7PXqodTY9OmzqsdFBufD063oG6ebQT/Zi2PBELX56" />
```
