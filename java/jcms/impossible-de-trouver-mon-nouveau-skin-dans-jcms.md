Impossible de trouver mon nouveau skin dans JCMS
================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2015/01/09"
[:modified_at]: # "2017/10/25"
[:tags]: # "jcms java"

Je viens de créer un nouveau skin dans JCMS, mais malgré un redémarrage ou le vidage du work de tomcat, 
il n'apparaît pas dans la liste de choix du skin de mes portlets.

Par défaut, pour ajouter un nouveau skin il suffit de créer la jsp et de la déclarer dans le fichier 
`plugin.xml`. Par exemple :

```xml
<types>
    <templates type="AbstractPortletSkinable">
       <template name="newSkin" file="doNewSkin.jsp" usage="box">
             <label xml:lang="en">My new skin</label>
             <label xml:lang="fr">Mon nouveau skin</label>
       </template>
    </templates>
</type>
```

Il existe cependant un option méconnue dans le CMS : la possibilité d'activer des skins différents 
pour chaque espace de travail.

Pour cela :

* Choisissez un espace de travail en back-offfice
* Allez dans l'**Espace d'administration fonctionnelle** > **Types de publication** > **Portlet**
* Éditez **[Abstract] Portlet Skinable**.
* Cochez les skins à activer dans l'espace de travail

Par défaut dans JCMS, aucun n'est coché. Tous les nouveaux skins sont donc immédiatement disponibles.

Mais à partir du moment où l'un d'eux est coché, vous devrez repasser par cette interface à chaque création 
de skin pour l'activer manuellement.
