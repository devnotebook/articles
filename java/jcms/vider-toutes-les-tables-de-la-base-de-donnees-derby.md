Vider toutes les tables de la base de données Derby
===================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/05/21"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Si vous utilisez Derby dans vos tests unitaires, vous aurez sans doute besoin de réinitialiser votre base 
entre chaque test.

Pour supprimer le contenu de toutes les tables d'un coup, vous pouvez utiliser cette fonction :

```java
/**
 * Vide toutes les tables de la base de données, dont le nom commence par "G_" ou "J_".
 * 
 * @throws SQLException
 */
public static void derbyClearAllTables() throws SQLException {
 
    ResultSet tables;
    Statement stat;
 
    final String schema = "jdbc:derby:jcmsdbunit";
 
    // Récupération de la liste des tables de la base de données
    final Connection connection = DriverManager.getConnection(schema);
    tables = connection.getMetaData().getTables(null, null, null, null);
 
    final Set<String> tableNameSet = new HashSet<String>();
 
    // Parcours des tables de la base de données
    while (tables.next()) {
 
        final String tableName = tables.getString("TABLE_NAME");
 
        if (tableName.startsWith("G_") || tableName.startsWith("J_")) {
 
            tableNameSet.add(tableName);
        }
    }
 
    // Parcours des tables à vider
    for (final String tableName : tableNameSet) {
 
        // Nettoyage de la table
        stat = connection.createStatement();
        stat.executeUpdate("DELETE FROM " + tableName);
        connection.commit();
    }
 
    if (!tables.isClosed()) {
        tables.close();
    }
}
```

**Remarque :**

Les tables ne commençant ni par `G_` ni par `J_` sont probablement des tables utiles au fonctionnement 
interne de Derby. Elles ne sont pas à vider.
