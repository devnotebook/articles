Ne pas compiler les .less à chaque démarrage
============================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2012/12/10"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Si vous n'utilisez pas les fichiers `.less` dans vos modules et que vous ne modifiez pas ceux de JCMS, 
il est inutile de les recompiler à chaque redémarrage, car les fichiers `.css` générés ne changeront pas.

Pour éviter cette compilation automatique, ajoutez cette propriété au `custom.prop` :

```ini
channel.less-compile.startup: false
```
