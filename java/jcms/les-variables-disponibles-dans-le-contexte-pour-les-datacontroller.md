Les variables disponibles dans le contexte, pour les DataController
===================================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/05/23"
[:modified_at]: # "2017/10/25"
[:tags]: # "jcms java"

Lorsque vous créez un DataController et que vous surchargez les méthodes `beforeWrite()`, 
`checkWrite()` et `afterWrite()`, le dernier paramètre de la méthode (la `Map`), 
contient tout le contexte disponible.

Dans cette variable il y a notamment :

* `request` : L'objet `HttpServletRequest` de tomcat.
* `response` : L'objet `HttpServletResponse` de tomcat.
* `formHandler` : Le `FormHandler` utilisé lors de l'édition du contenu.

Et dans le cas d'une modification de donnée, il y a également :

* `Data.previous` : La dernière version de la `Data` avant modification.
