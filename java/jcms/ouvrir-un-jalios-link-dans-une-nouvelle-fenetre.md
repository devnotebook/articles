Ouvrir un jalios:link dans une nouvelle fenêtre
===============================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/03/18"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

JCMS fournit un tag pour créer des liens : `<jalios:link>`.

Pour que ce lien ouvre la cible dans une nouvelle fenêtre, il faut utiliser l'attribut `htmlAttributes` du tag. 
Par exemple :

```xml
<jalios:link data='<%= doc %>' htmlAttributes='target="_blank"' title='<%= glp("jcmsplugin.myplugin.target-blank.title") %>'>
    <%= doc.getTitle(userLang) %>
</jalios:link>
```

**Remarque :**

Pour l’accessibilité, il est conseillé de préciser l'attribut `title` pour informer 
l'utilisateur de l'ouverture dans une nouvelle fenêtre.
