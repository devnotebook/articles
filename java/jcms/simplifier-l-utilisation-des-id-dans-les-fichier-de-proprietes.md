Simplifier l'utilisation des id dans les fichier de propriétés
==============================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/03/12"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

JCMS permet de simplifier la récupération d'une Data à partir de son id, stocké dans un fichier de propriétés.

Par exemple, pour stocker l'id d'une catégorie et la récupérer, vous feriez

```ini
jcmsplugin.myplugin.category.my-category.id: j_6540
```

et

```java
String categoryID = channel.getProperty("jcmsplugin.myplugin.category.my-category.id");
Category category = channel.getCategory(categoryID);
```

Utilisez 

```ini
$jcmsplugin.myplugin.category.my-category.id
```

et
```java
Category category = channel.getCategory("$jcmsplugin.myplugin.category.my-category.id");
```

**Explication :**

Comme son nom commence par `$`, JCMS sait qu'il s'agit d'une propriété contenant un id, 
et peut directement récupérer la Data associée.
