La méthode getStringArrayProperty() de la classe Channel
========================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/03/28"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Dans JCMS, la méthode `getStringArrayProperty()` de la classe `Channel` permet de récupérer un tableau 
de valeurs définies dans une même propriété.

Par exemple :

```java
Channel.getChannel.getStringArrayProperty("jcmsplugin.myplugin.name-list", new String[] {})
```

avec dans un `plugin.prop` la propriété suivante :

```ini
jcmsplugin.myplugin.name-list: Jean Louis David Roger,Virgule
```

retournera un tableau (`String[]`) de quatre éléments : `Jean`, `Louis`, `David` et `Roger,Virgule`.

**Remarques :**

* Le séparateur par défaut est l'espace (ou tabulation et autres chaînes d'espace)
* Pour modifier le séparateur, il faut l'indiquer au début de la liste de valeurs, avec un `@`.
     
```ini
jcmsplugin.myplugin.name-list: @,Jean Louis David Roger,Virgule
```
La méthode appelée précédemment retournera cette fois un tableau de deux valeurs : `Jean Louis David` et `Virgule`.
