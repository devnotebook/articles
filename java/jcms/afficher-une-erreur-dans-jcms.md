Afficher une erreur dans JCMS
=============================

[:type]: # "Astuce"
[:sources]: # "[community.jalios.com/jcms-docs](https://community.jalios.com/jcms-docs/7.0.0/javadoc/com/jalios/jcms/context/JcmsContext.html#setInfoMsg(java.lang.String))"
[:version]: # "JCMS 7"
[:created_at]: # "2014/11/27"
[:modified_at]: # "2017/10/25"
[:tags]: # "jcms java"

Dans JCMS (7 et +) les erreurs sont affichées dans un bloc ressemblant à ça.

![Affichage d'une erreur dans JCMS](./afficher-une-erreur-dans-jcms-01.png)


## Transmettre le message ##

Vous devez tout d'abord transmettre votre message à JCMS.

* Si vous êtes dans une JSP, utilisez l'une ou l'autre de ces méthodes :

```java
// Ajoute un message d'erreur dans la request
setErrorMsg("Le téléchargement du document a échoué", request);

// Ajoute un message d'erreur dans la session
setErrorMsgSession("Le téléchargement du document a échoué", request);
```

* Pour faire la même chose dans une classe Java, utilisez l'une de celles-ci :

```java
    // Ajoute un message d'erreur dans la request
    JcmsContext.setErrorMsg("An error occured while saving your content", request);
    
    // Ajoute un message d'erreur dans la session
    JcmsContext.setErrorMsgSession("An error occured while saving your content", request);
```

**Remarque :**

Des méthodes équivalentes existent pour les messages d'information et d'avertissement.

## Afficher le message ##

Une fois le message transmis, il reste à l'afficher. Il suffit pour cela d'inclure ce bout de code :

```java
<%@ include file='/jcore/doMessageBox.jsp' %>
```

Cette JSP fournie par JCMS, va récupérer tous les messages d'information, d'avertissement ou d'erreur présents 
en session, dans les paramètres de la request et dans le contexte de la page. 
Chaque message trouvé est affiché.

**Remarque :**

Depuis la version 7.1 JCMS utilise **Bootstrap**. 
Si vous êtes en version antérieure, le message d'erreur ne ressemblera pas à celui ci-dessus.
