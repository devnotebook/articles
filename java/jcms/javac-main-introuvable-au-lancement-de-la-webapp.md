javac/Main introuvable au lancement de la webapp
================================================

[:type]: # "Erreur"
[:version]: # "JCMS 6"
[:created_at]: # "2012/12/03"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Lorsque vous lancer une application JCMS avec tomcat **1.5**, vous pouvez avoir cette erreur :

```bash
java.lang.NoClassDefFoundError: com/sun/tools/javac/Main
```

Elle signifie qu'il manque l'archive `tools.jar` au classpath.

Pour l'ajouter à votre projet au lancement de l'application, effectuez ces quelques étapes :

* Clic-droit sur le projet
* **Run As** > **Run Configurations...**
* Onglet **Classpath**
* Cliquez sur **User Entries** puis sur le bouton **Advanced...**
* Sélectionnez **Add Variable String**:
* Copiez-collez le chemin vers le fichier `tools.jar` qui se trouve dans le répertoire `lib/` 
de votre jdk (ex: `D:\Dev\DevPack\jdk\lib\tools.jar`)
