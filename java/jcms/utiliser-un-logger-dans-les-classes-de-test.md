Utiliser un logger dans les classes de test
===========================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/05/23"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Dans vos classes de test, vous pouvez utiliser le gestionnaire de log **slf4j** 
Il s'agit d'une façade qui peut utiliser plusieurs loggers au choix, comme **log4j** par exemple.

Voici comment utiliser ce gestionnaire :

```java
public class MyClassTest extends JcmsTestCase {
 
    /** Logger **/
    private static Logger logger;
 
    /**
     * Initialise un contexte pour une suite de tests, avec des méthodes d'initialisation et de finalisation.
     * 
     * @return une configuration de suite de tests
     */
    public static TestSetup suite() {
 
        return new TestSetup(new TestSuite(MyClassTest.class)) {
 
            @BeforeClass
            @Override
            protected void setUp() throws Exception {
 
                logger = LoggerFactory.getLogger(MyClassTest.class);
                logger.debug("Début des tests unitaires pour la classe " + MyClass.class.getSimpleName() + ".");
            }
            @AfterClass
            @Override
            protected void tearDown() throws Exception {
                logger.debug("Fin des tests unitaires.");
            }
        };
    }
```

**Explications :**

* Déclarez le logger dans votre classe de test.
* Créez la méthode statique `suite()`, pour pouvoir initialiser des éléments au début des tests.
* Utilisez la méthode `LoggerFactory.getLogger()`, pour initialiser le logger.
* Utilisez le logger comme avec **log4j** (ex : `logger.info()`, `logger.debug()`, ...).

**Remarque :**

Pour pouvoir utiliser slf4j avec log4j, vous devez inclure ces deux jars dans votre projet : 
[slf4j-api-1.5.8.jar](https://framagit.org/devnotebook/articles/-/blob/master/_files/slf4j-api-1.5.8.jar) et 
[slf4j-log4j12-1.5.8.jar](https://framagit.org/devnotebook/articles/-/blob/master/_files/slf4j-log4j12-1.5.8.jar).
