Méthode d'initialisation au début de la classe de test
======================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/05/21"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Pour effectuer un traitement une fois au début des tests, on utilise généralement la méthode suivante 
dans la classe de test :

```java
@BeforeClass
public static void oneTimeSetUp() {
 
    logger = LoggerFactory.getLogger(MyClassTest.class);
    logger.info("Début des tests unitaires.");
}
```

Cela permet d'initialiser un logger, configurer l'environnement à utiliser pour tous les tests, ...

Cette méthode ne semble pas fonctionner avec JCMS (dans eclipse en tous cas). 
Vous pouvez faire l'équivalent grâce à la méthode `suite()` à ajouter dans votre classe de test :

```java
/**
 * Initialise un contexte pour une suite de tests, avec des méthodes d'initialisation et de finalisation.
 * 
 * @return une configuration de suite de tests
 */
public static TestSetup suite() {
 
    return new TestSetup(new TestSuite(MyClassTest.class)) {
 
        @Before
        @Override
        protected void setUp() throws Exception {
            logger = LoggerFactory.getLogger(MyClassTest.class);
            logger.info("Début des tests unitaires.");
        }
 
        @After
        @Override
        protected void tearDown() throws Exception {
            logger.info("Fin des tests unitaires.");
        }
    };
}
```

**Remarque :**

La méthode `tearDown()` permet d'effectuer un traitement une fois après tous les tests.
