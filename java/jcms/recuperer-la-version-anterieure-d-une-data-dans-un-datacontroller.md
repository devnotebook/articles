Récupérer la version antérieure d'une Data, dans un DataController
==================================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2013/05/23"
[:modified_at]: # "2017/10/25"
[:tags]: # "jcms java"

Si vous avez besoin de comparer l'ancienne et la nouvelle version d'une `Data`, 
dans un DataController (par exemple dans `afterWrite()`), 
vous aurez besoin d'utiliser le **contexte** en paramètre de la méthode.

Pour obtenir cette version, utilisez quelque chose comme :

```java
final Article oldArticle = (Article) context.get("Data.previous");
```
