Choisir le portail d'affichage d'une publication
================================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2012/11/12"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Lorsque vous affichez une publication en Full display, le portail d'affichage choisi sera celui de la 
première catégorie à laquelle est est rattachée. 
Si cette catégorie n'a pas de portail qui lui est propre, celui de sa catégorie parente est utilisée, 
et ainsi de suite jusqu'à trouver un portail d'affichage.

Si le contenu n'est pas catégorisé, le portail d'affichage par défaut sera utilisé.

Pour forcer une publication à s'afficher dans un portail spécifique, on peut ajouter un paramètre à l'url : 
`&portal=<portal_id>`.

Avec un tag `jalios:link` on obtient donc quelque chose comme :

```xml
<jalios:link data='<%= publication%>' paramNames='<%= paramNames %>' paramValues='<%= paramValues %>'>
```

**Explication :**

* `paramNames` : Tableau de String contenant les paramètres à ajouter à l'url (ici: `{"portal"}`)
* `paramValues` : Valeurs des paramètres du premier tableau, dans l'ordre correspondant 
(ici: `{channel.getProperty("jcmsplugin.portail.full-display.id")}`)

**Remarque :**

JCMS 7.1 comporte un bug à propos de ce tag, et les paramètres `paramsNames` et `paramValues` 
ne fonctionnent pas. On doit alors utiliser le paramètre `params` :

```xml
<jalios:link data='<%= publication %>' params='<%= "portal=" + channel.getProperty("jcmsplugin.portail.full-display.detail.id") %>'>
```

Ce paramètre reçoit une valeur de la forme `<paramName>=<paramValue>`. 
Il remplace tous les autres paramètres transmis initialement dans l'url et est donc à éviter
lorsque `paraNames` et `paramValues` fonctionnent.
