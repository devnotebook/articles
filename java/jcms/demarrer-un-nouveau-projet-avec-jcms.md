Démarrer un nouveau projet avec JCMS
====================================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2012/10/17"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Cet article explique comment démarrer un nouveau projet **JCMS 7.X** avec un 
développement en modules synchronisés.

## Environnement ##

* Commencez par récupérer et déployer un devpack JCMS. ()Par exemple, extraire le devpack dans `D:\Dev`).
* Lancez eclipse via le lanceur `eclipse.bat`.

## Création du projet ##

* Créez un **nouveau projet Java** avec la configuration par défaut.
* Récupérez et extrayez une **webapp JCMS 7.X vierge** à la racine de ce nouveau projet.
* Clic-droit > **Refresh** sur le projet.

## Configuration du projet ##

Il faut préciser à eclipse quelles classes compiler et avec quelles bibliothèques.

* Clic-droit > **Properties** sur le projet
* Java Build Path > **Source** :
    * Supprimez le dossier `src/` existant, et ajoutez le dossier `WEB-INF/classes/`.
    * Choisissez ce même dossier pour qu'y soient générées les .class (default output folder).
* Java Build Path > **Libraries** > **Add Library...** > **User Libraries...** : 
    * Créez une **bibliothèque Tomcat6** (elle sera réutilisable pour tous vos futurs projets) :
        * Cliquez sur **New...**, nommez votre bibliothèque `Tomcat6` et validez.
        * Cliquez sur **Add JARs...** et parcourez l'arborescence jusqu'au dossier `tomcat6/lib/` 
        de votre Devpack.
        * Sélectionnez les fichiers `ecj-3.7.jar`, `el-api.jar`, `jasper.jar`, `jasper-el.jar`, 
        `jsp-api.jar`, `servlet-api.jar` et validez.
    * De la même manière créez la **bibliothèque MonProjet**, en sélectionnant cette fois tous les fichiers 
    présents dans le dossier `WEB-INF/lib/` de JCMS.
    * Cochez maintenant ces deux bibliothèques et validez, pour qu'elles soient ajoutées à votre projet.
* Validez les propriétés de votre projet.

## Lancement de l'application JCMS ##

Pour pouvoir lancer l'application, vous devez indiquer le paramétrage de tomcat 
et les configurations d'exécution d'eclipse.

* Copiez le fichier [MonProjet.launch](https://framagit.org/devnotebook/articles/-/blob/master/_files/MonProjet.launch) 
à la racine de votre projet.
* Dans ce fichier, remplacez `MonProjet` par le nom de votre projet, 
et `D:/Dev/DevPack/` par le chemin vers votre devpack.
* Si votre workspace ou les dossiers `jdk16/` et `tomcat6/` ne se trouvent pas à la racine 
de votre devpack, modifiez les chemins correspondants en conséquence.
* Éditez le fichier `tomcat.xml` à la racine de votre projet en adaptant au besoin le chemin 
vers la racine de votre application et le contexte (`/jcms` par défaut). Exemple :

```xml
<Context path="/jcms" docBase="D:/Dev/workspaces/MonProjet" debug="0"></Context>
```

* Clic-droit > **Refresh sur le projet**
* Cliquez sur la petite flèche près du bouton ![Démarrer JCMS](./demarrer-un-nouveau-projet-avec-jcms-01.png) 
 et cliquez sur **MonProjet**.
* Accédez au site à l'url [http://localhost:8080/jcms](http://localhost:8080/jcms), 
(`/jcms` étant le contexte choisi dans le `tomcat.xml`).
