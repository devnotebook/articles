Erreur au démarrage de JCMS : lucene
====================================

[:type]: # "Erreur"
[:version]: # "JCMS 7"
[:created_at]: # "2013/03/28"
[:modified_at]: # "2017/09/08"
[:tags]: # "jcms java"

Si votre lanceur de projet eclipse est mal configuré, vous pouvez obtenir l'erreur suivante au démarrage de JCMS, 
pendant l'initialisation des dépendances des plugins :

```
09:59:34,749 FATAL [JCMS] [ChannelInitServlet] - An exception occured while initializing JCMS. The site is not available.
java.lang.NoClassDefFoundError: org/apache/lucene/analysis/standard/StandardAnalyzer
    at java.lang.ClassLoader.defineClass1(Native Method)
    ...
```

Pour éviter cette erreur, vérifiez l'onglet **Classpath** de votre lanceur eclipse et supprimez votre projet 
des **User Entries** :

![User Entries](./erreur-au-demarrage-de-jcms-lucene-01.png)
