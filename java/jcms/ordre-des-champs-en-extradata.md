Ordre des champs en ExtraData
=============================

[:type]: # "Astuce"
[:version]: # "JCMS 7"
[:created_at]: # "2014/11/25"
[:modified_at]: # "2017/10/25"
[:tags]: # "jcms java"

Lors de l'affichage des champs en ExtraData, JCMS utilise **l'ordre l'alphabétique des clés** des propriétés 
d'ExtraData.

Si votre `plugin.prop` contient les propriétés suivantes :

```
extra.Category.jcmsplugin.monplugin.premierchamp
extra.Category.jcmsplugin.monplugin.champ2
extra.Category.jcmsplugin.monplugin.champ3
extra.Category.jcmsplugin.monplugin.dernierchamp
```

Les champs s'afficheront dans cet ordre en back-office :

* champ2
* champ3
* dernierchamp
* premierchamp
