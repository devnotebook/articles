Le tag @see de la Javadoc
=========================

[:type]: # "Astuce"
[:created_at]: # "2013/04/29"
[:modified_at]: # "2020/11/07"
[:tags]: # "java"

Lorsque vous écrivez la Javadoc d'une méthode, vous pouvez ajouter un lien vers une autre méthode, grâce au tag **@see**.

Pour cela vous devez respecter cette syntaxe :

```java
/**
 * Méthode d'exemple.
 * @see {@link mypackage.MyClass#someMethod}
 */
public void myMethod() {
 
}
```

ou

```java
/**
 * Méthode d'exemple.
 * @see {@link mypackage.MyClass#someMethod(Arg1Class, String)}
 */
public void myMethod() {
 
}
 
/**
 * Méthode utilisée par myMethod.
 */
public void someMethod(Arg1Class arg1, String arg2) {
 
}
```

**Remarque :**

Pour ajouter plusieurs liens, séparez-les simplement par une virgule.
