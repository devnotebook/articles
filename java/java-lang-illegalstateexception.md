java.lang.IllegalStateException
===============================

[:type]: # "Erreur"
[:created_at]: # "2015/03/21"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

Il arrive de rencontrer des ```IllegalStateException``` lorsqu'on effectue des redirections dans des servlets.
 
En général ça se produit quand on a déjà envoyé des données ou qu'on a déjà demandé une redirection dans notre requête HTTP.

Pour éviter cela, il suffit d'utiliser la méthode ```isCommited()``` en remplaçant

```java
// ServletResponse response

if (response != null) {
 response.sendRedirect("http://www.google.fr");
 return;
}
```

par

```java
// ServletResponse response

if (response != null && !response.isCommited()) {
 response.sendRedirect("http://www.google.fr");
 return;
}
```
