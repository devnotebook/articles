Ajouter un espace (ou autre) tous les n caractères
==================================================

[:type]: # "Astuce"
[:created_at]: # "2014/12/03"
[:modified_at]: # "2017/04/20"
[:tags]: # "java fonctions-utiles"

Si vous souhaitez formater un numéro de téléphone, un IBAN, ou n'importe quelle chaîne en y ajoutant régulièrement un séparateur, cette fonction peut vous être utile :

```java
/**
 * Formate une chaîne en ajoutant un séparateur tous les <code>length</code> caractères.
 * 
 * @param string Chaîne à formater
 * @param separator Séparateur à ajouter
 * @param length Taille des groupes de caractères à séparer
 * @return La chaîne formatée
 */
public static String addChar(String string, String separator, int length) {
 
    if (string != null && string.length() > length) {
 
        string = string.replaceAll("([^\\s]{" + length + "})", "$0" + separator);
    }
 
    return string;
}
```
