Un décompilateur Java dans Eclipse
==================================

[:type]: # "Marque-page"
[:created_at]: # "2014/07/30"
[:modified_at]: # "2020/11/07"
[:tags]: # "java outils"

L'application [Java Decompiler](http://jd.benow.ca/) permet de décompiler un fichier **.class** simple ou tous ceux présents dans un JAR.

Le site de l'application fournit **un plugin Eclipse** qui ne fonctionne pas avec les dernières versions (testé avec **Eclipse Juno**). 
Plusieurs fork ont été créés et [ce site](http://mchr3k.github.io/jdeclipse-realign/) en propose un qui fonctionne : **JDEclipse-Realign**.

Pour l'installer :

- Cliquez sur Help > Install New Software... dans le menu d'Eclipse
- Ajoutez un nouveau site via le bouton **Add...**
- Choisissez un nom (ex: JDEclipse-Realign) et cette URL : <http://mchr3k-eclipse.appspot.com/>
- Cochez JavaDecompiler Eclipse Plug-in > **JD-Eclipse (Realign Edition)** et suivez la procédure d'installation

Une fois installé et Eclipse redémarré, vérifiez l'association des fichiers :

- Windows > Preferences > General > Editors > File Associations
- Pour ***.class without source** sélectionnez **Class File Editor** par défaut

Si vous ouvrez un fichier **.class** (via `F3` ou `Ctrl + clic` sur le nom d'une classe) vous devriez maintenant voir la source décompilée.
