Afficher les retours à la ligne en JSTL
=======================================

[:type]: # "Astuce"
[:created_at]: # "2013/11/19"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

Si vous permettez à des contributeurs de saisir un texte avec des retours à la ligne (= textarea), 
vous aurez sans doute besoin de les afficher ensuite.

Il suffit de remplacer les retours charriots Java (```\n```) en retour à la ligne HTML (```<br />```) :

```java
<c:set var="newline" value="<%= \"\n\" %>" />
${fn:replace(myAddress, newline, "<br />")}
```

**Remarque :**

Pour pouvoir utiliser la fonction de remplacement de JSTL, vous devez ajouter cet import :

```java
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
```
