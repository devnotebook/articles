Afficher la valeur de tous les champs via la méthode toString()
===============================================================

[:type]: # "Astuce"
[:created_at]: # "2013/06/14"
[:modified_at]: # "2017/04/20"
[:tags]: # "java fonctions-utiles"

Lorsque vous créez une nouvelle classe en Java, elle hérite de la méthode toString() de la classe Object. 
Par défaut, cette méthode n'affiche rien de bien compréhensible.

Vous pouvez la surcharger pour afficher lisiblement les valeurs de tous les champs de votre objet de cette manière :

```java
@Override
public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
}
```
Grâce à l'introspection et à la librairie **org.apache.commons**, ces quelques lignes de code fonctionneront pour toutes vos classes.
