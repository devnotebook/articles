Utiliser les Singletons en Java avec du multithreading
======================================================

[:type]: # "Astuce"
[:sources]: # "[http://christophej.developpez.com/tutoriel/java/singleton/multithread/](http://christophej.developpez.com/tutoriel/java/singleton/multithread/)"
[:created_at]: # "2013/05/15"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

Dans un environnement multithread, le comportement d'un singleton peut être faussé.

Pour éviter ce problème, il suffit d'ajouter le qualificatif **synchronized** à la méthode du singleton :

```java
public static synchronized Singleton getInstance() { 
 
    if (instance == null) {
        instance = new Singleton();
    }
 
    return instance;
}
```
