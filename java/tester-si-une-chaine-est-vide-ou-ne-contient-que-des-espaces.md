Tester si une chaîne est vide ou ne contient que des espaces
============================================================

[:type]: # "Astuce"
[:created_at]: # "2014/11/14"
[:modified_at]: # "2017/04/20"
[:tags]: # "java fonctions-utiles"

Voici une méthode pour tester si une chaîne est vide ou contient uniquement des espaces :

```java
/**
 * Retourne si la chaîne en argument est vide ou contient uniquement des espaces.
 * 
 * @param string Chaîne
 * @return <code>true</code> si la chaîne est nulle, vide, ou s'il ne contient que des caractères espaces (ex: \n \r \s, ...) y compris les espaces insécables.
 */
public static boolean isEmpty(String string) {
 
    boolean isEmpty = true;
 
    if (string != null) {
 
        isEmpty = "".equals(string.replaceAll("[\\s|\\u00A0]+", ""));
    }
 
    return isEmpty;
}
```

**Remarque :**

```\u00A0``` représente les espaces insécables. 
Si on utilise uniquement ```\s``` dans le remplacement, ils ne seront pas considérés comme des espaces.
