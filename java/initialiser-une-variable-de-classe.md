Initialiser une variable de classe
==================================

[:type]: # "Astuce"
[:created_at]: # "2014/12/05"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

Lorsqu'on utilise des variables statiques, on peut souvent les initialiser directement à leur déclaration :

```java
public class MaClasse {
 
    public static String myStaticString = "Ma chaîne";
    public static String[] myStaticArray = {"Ma chaîne 1", "Ma chaîne 2"};
}
```

Mais si vous avez une ```Collection``` ou un autre objet en variable de classe, vous ne pourrez pas l'initialiser de cette manière.

Vous pouvez donc déclarer un bloc statique, qui sera exécuté une seule fois, à la première utilisation de la classe. 
Dans ce bloc, appelez une fonction statique qui se chargera d'initialiser votre variable :

```java
public class MaClasse {
 
    public static List<String> myStaticList = new ArrayList<String>();
 
    static {
        MaClasse.initMyStaticList();
    }
 
    private static void initMyStaticList() {
 
        MaClasse.myStaticList.add("Ma chaîne 1");
        MaClasse.myStaticList.add("Ma chaîne 2");
    }
}
```
