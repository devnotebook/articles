Impossible de mapper des sous-classes avec JSON
===============================================

[:type]: # "Erreur"
[:created_at]: # "2013/07/23"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

Si vous utilisez un mapping JSON en Java, vous pouvez rencontrer l'erreur suivante :

```
org.codehaus.jackson.map.JsonMappingException: No suitable constructor found for type
```

Cette erreur se produit si vous utilisez un Bean global contenant une ou plusieurs classes internes. Exemple :

```java
public class MyGlobalClass {
 
    private List<SubClass> subClassList;
 
    public class SubClass {
 
        private String id;
    }
}
```

Pour ne pas avoir cette erreur, déclarez vos classes internes **statiques** dans la classe globale :

```java
public static class SubClass {
     
    private String id;
}
```
