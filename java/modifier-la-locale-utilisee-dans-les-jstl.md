Modifier la locale utilisée dans les JSTL
=========================================

[:type]: # "Astuce"
[:created_at]: # "2013/12/04"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

Dans les JSTL, la locale permet de déterminer comment formater une date ou un nombre. 
Par défaut, c'est la **JVM** qui détermine cette valeur.

Pour modifier cette valeur pour toute une session, il suffit d'utiliser :

```java
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="fr_FR" scope="session" />
```

Vous pouvez aussi paramétrer cette valeur pour toute une portlet en ajoutant ces lignes au **web.xml**, directement sous la racine ```<web-app>``` :

```xml
<context-param>
    <param-name>javax.servlet.jsp.jstl.fmt.locale</param-name>
    <param-value>fr_FR</param-value>
</context-param>
```
