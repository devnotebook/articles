Utiliser la réflexivité
=======================

[:type]: # "Astuce"
[:version]: # "Java 5+"
[:created_at]: # "2012/28/28"
[:modified_at]: # "2017/04/20"
[:tags]: # "java"

## Instancier un nouvel objet d'une classe

```java
/**
 * Retourne une publication du type en paramètre en remplissant automatiquement ses champs, et après l'avoir créer dans JCMS.
 * 
 * @param clazz La classe du type de contenu souhaité
 * @return une publication du type souhaité
 */
public static <T extends Publication> T fillPublication(Class<T> clazz) {
 
    Publication publication = null;
    if (Util.notEmpty(clazz)) {
 
        try {
            publication = clazz.newInstance();
 
        } catch (InstantiationException e) {
 
            LOGGER.error("Impossible d'instancier dynamiquement un objet de la classe \"" + clazz.getSimpleName() + "\"", e);
 
        } catch (IllegalAccessException e) {
 
            LOGGER.error("Impossible d'instancier dynamiquement un objet de la classe \"" + clazz.getSimpleName() + "\"", e);
        }
    }
    return clazz.cast(publication);
}
```

## Appeler une méthode d'une classe

```java
Publication publication = new Article();
Class<? extends Publication> clazz;
 
Method method = clazz.getMethod("getName");
String name= (String) method.invoke(publication, null);
```
L'intérêt de ce code est d'appeler une méthode de l'objet publication sans savoir que c'est un article.

## Instancier un tableau d'objets d'une classe

```java
int size = 3;
Objet[] publicationArray = (Publication[]) java.lang.reflect.Array.newInstance(Publication.class, size);
```
