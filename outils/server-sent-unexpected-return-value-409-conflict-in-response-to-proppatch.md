Server sent unexpected return value (409 Conflict) in response to PROPPATCH
===========================================================================

[:type]: # "Erreur"
[:created_at]: # "2013/11/26"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils"

Si vous avez l'erreur suivante en commitant sur SVN, c'est sans doute à cause d'un caractère spécial dans votre commentaire.

```
Command              Commit 
Error                Commit failed (details follow): 
Error                At least one property change failed; repository is unchanged 
Error                Server sent unexpected return value (409 Conflict) in response to PROPPATCH 
Error                request for 
Error                '/subversion/folder/!svn/ayt/r45g987g-h111-ty78-96x3-55gt9pljmo4r/0123' 
Completed! 
```
