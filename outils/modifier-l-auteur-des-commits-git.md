Modifier l'auteur des commits Git
=================================

[:type]: # "Astuce"
[:created_at]: # "2023/03/30"
[:modified_at]: # "2023/03/30"
[:tags]: # "outils git"
[:sources]: # "[www.git-tower.com](https://www.git-tower.com/learn/git/faq/change-author-name-email)"

Il est possible de modifier l’auteur de tous les anciens commits d’un dépôt Git.

Pour cela, utilisez la commande suivante, après l’avoir personnalisée selon vos besoins :

```bash
git filter-branch --env-filter '
WRONG_EMAIL="wrong@email.com"
NEW_NAME="John Doe"
NEW_EMAIL="j.doe@email.com"

if [ "$GIT_COMMITTER_EMAIL" = "$WRONG_EMAIL" ]
then
    export GIT_COMMITTER_NAME="$NEW_NAME"
    export GIT_COMMITTER_EMAIL="$NEW_EMAIL"
fi
if [ "$GIT_AUTHOR_EMAIL" = "$WRONG_EMAIL" ]
then
    export GIT_AUTHOR_NAME="$NEW_NAME"
    export GIT_AUTHOR_EMAIL="$NEW_EMAIL"
fi
' --tag-name-filter cat -- --branches --tags
```

**Explications** :

On indique l’**adresse email utilisée** par tous les commits que l’on souhaite modifier, 
ainsi que le **nom d’utilisateur** et l’**adresse email correcte** que l’on souhaite utiliser à la place.
