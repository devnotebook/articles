Suivre les liens symboliques
============================

[:type]: # "Astuce"
[:version]: # "VirtualBox 4.1.8"
[:sources]: # "[askubuntu.com](http://askubuntu.com/a/446328)"
[:created_at]: # "2016/10/17"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils"

Si vous utilisez Virtual Box sous Windows pour émuler des VM sous Linux, avec des répertoires partagés, 
vous aurez surement des problèmes de création de liens symboliques dans ces répertoires.

Il suffit de créer les liens symboliques côté Windows et non pas côté VM.
Cependant, cela peut ne pas être suffisant, si vous utilisez une version **VirtualBox 4.1.8 ou ultérieure**.

Dans ce cas, il vous faut modifier un paramètre désactivé par défaut, via la commande suivante :

```bash
"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe" setextradata "<nom_de_la_vm>" VBoxInternal2/SharedFoldersEnableSymlinksCreate/<nom_du_partage> 1
```
