Retirer le versionnement d'un fichier avec IntelliJ/PhpStorm
============================================================

[:type]: # "Astuce"
[:sources]: # "[stackoverflow.com](http://stackoverflow.com/a/21703813/2674501)"
[:created_at]: # "2017/04/18"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils"

PhpStorm et IntelliJ proposent d'ajouter un fichier au versionnement (ex: git add), mais ne permettent pas de le retirer.

Une solution de contournement simple consiste à le faire en ligne de commande :

```bash
git rm --cached mon_fichier
git commit -m 'suppression du fichier'
```

pour un répertoire entier, ajoutez l'option `-r`écursive :

```bash
git rm -r --cached mon_repertoire
git commit -m 'suppression du répertoire'
```
