Créer un patch avec TortoiseSVN
===============================

[:type]: # "Astuce"
[:created_at]: # "2020/11/02"
[:modified_at]: # "2020/11/02"
[:tags]: # "outils"

**TortoiseSVN** permet de générer des patchs pour passer d'une version à une autre du projet.

Il y a deux types de patch :

* Les patchs contenant **toutes les lignes des fichiers modifiés** qui ne sont pas encore "commités".
* Les patchs contenant **l'arborescence de tous les fichiers modifiés** entre deux tags.

## Description ##

Le premier type génère un fichier `.patch`, qui ressemble à quelque chose comme ça :

```patch
Index: settings/siteaccess/bo/site.ini.append.php
===================================================================
--- settings/siteaccess/bo/site.ini.append.php    (révision 864)
+++ settings/siteaccess/bo/site.ini.append.php    (copie de travail)
@@ -37,4 +37,23 @@
 CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
 TranslationList=
 
+[DebugSettings]
+DebugOutput=enabled
+DebugToolbar=enabled
+DebugRedirection=disabled
+
+[TemplateSettings]
+Debug=disabled
+
+[DatabaseSettings]
+SQLOutput=disabled
+
 */ ?>
\ No newline at end of file
```

On peut y voir pour chaque fichier modifié, les lignes ajoutées et supprimées depuis le dernier commit.

Le second type génère l'arborescence et les fichiers entiers.

## Utilisation

### Pour générer un fichier .patch, il suffit de :

* Faire un clic-droit sur l'arborescence
* \> TortoiseSVN
* \> Create patch...
* Sélectionner les fichiers à patcher
* Choisir (via le bouton **Options**) si on considère les modifications d'espaces blancs.

### Pour utiliser ce fichier, il faut : ###

* Le placer à l'endroit d'où on l'a généré
* Faire un clic-droit sur le fichier
* \> TortoiseSVN
* \> Apply patch...

La liste des fichiers modifiés apparait, et on peut voir pour chacun d'eux les lignes impactées. 
On peut alors choisir de patcher tous les fichiers ou seulement certains.

### Pour générer toute l'arborescence des fichiers modifiés entre deux tags, il faut :

* Faire un clic-droit sur l'arborescence
* \> TortoiseSVN
* \> Repo-browser
* Faire un clic-droit dans l'arborescence, sur la version **la plus récente** à comparer
* \> Mark for comparison
* Faire un clic-droit dans l'arborescence, sur la version **la plus ancienne** à comparer
* \> Compare URLs

La liste des fichiers modifiés entre les deux versions apparaît. Il suffit alors de :

* Les sélectionner tous
* Faire un clic-droit sur un fichier sélectionné
* \> Export selection to...
* Choisir où générer les fichiers
