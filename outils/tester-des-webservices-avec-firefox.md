Tester des webservices avec Firefox
===================================

[:type]: # "Marque-page"
[:created_at]: # "2013/06/20"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils"

Il est simple de tester un webservice via un navigateur web lorsque l'appel est une requête de type `GET`, 
mais c'est beaucoup moins évident pour les requêtes `POST`, `PUT` et autres.

L'extension [RESTClient](https://addons.mozilla.org/fr/firefox/addon/restclient) de Firefox permet de faire tout cela. 

![REST Client](./tester-des-webservices-avec-firefox-01.png)

Grâce à elle, vous pouvez envoyer des données au format texte, JSON ou autre, et afficher la réponse de manière lisible.
Il est possible d'ajouter des header personnalisés, ou de s'authentifier auprès du webservice.

Si le webservice retourne du JSON, la réponse sera présentée avec indentation et coloration syntaxique.

De plus, vous pouvez sauvegarder vos requêtes et les réutiliser par la suite.
