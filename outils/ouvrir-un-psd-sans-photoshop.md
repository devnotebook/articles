Ouvrir un PSD sans Photoshop
============================

[:type]: # "Marque-page"
[:created_at]: # "2018/02/09"
[:modified_at]: # "2018/02/09"
[:tags]: # "outils"

Vous êtes sous Linux, ou vous n'avez pas Photoshop ? Vous avez besoin d'ouvrir
correctement un fichier psd ?

Si c'est juste pour du découpage et pas de la créa, **la version online fera très bien l'affaire !**

C'est ce que propose le site [https://www.photopea.com/](https://www.photopea.com/).
