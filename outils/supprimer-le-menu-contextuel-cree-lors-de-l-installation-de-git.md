Supprimer le menu contextuel créé lors de l'installation de Git
===============================================================

[:type]: # "Astuce"
[:created_at]: # "2020/08/22"
[:modified_at]: # "2020/11/08"
[:tags]: # "outils windows"

Lors de l'installation de **Git** sous Windows, vous avez la possibilité d'activer un menu contextuel proposant 
des raccourcis vers les tâches les plus courantes de Git (commit, pull, ...).

Le problème c'est que **TortoiseGit** propose la même fonctionnalité en mieux. Si vous utilisez ces deux logiciels, 
vous obtenez donc deux menus contextuels identiques...

Pour supprimer celui créé par Git, utilisez une des commandes suivantes.

Pour Windows 32 bits :

```bash
cd C:\Program Files\Git\git-cheetah
regsvr32 /u git_shell_ext.dll
```

Pour Windows 64 bits :

```bash
cd C:\Program Files\Git\git-cheetah
regsvr32 /u git_shell_ext64dll
```
