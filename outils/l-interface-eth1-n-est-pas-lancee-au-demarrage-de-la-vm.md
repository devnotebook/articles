L'interface eth1 n'est pas lancée au démarrage de la VM
=======================================================

[:type]: # "Erreur"
[:version]: # "Debian 8.4, VirtualBox 5.x"
[:sources]: # "[unix.stackexchange.com](http://unix.stackexchange.com/questions/37122/virtualbox-two-network-interfaces-nat-and-host-only-ones-in-a-debian-guest-on)"
[:created_at]: # "2016/04/15"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils debian"

Avec Virtualbox, vous déclarez parfois deux interfaces réseaux pour votre VM (ex: connexion par pont + réseau privé hôte).

Souvent, la deuxième interface n'est pas reconnue au démarrage de la VM.

Pour éviter cela, modifiez le fichier `/etc/network/interfaces`, en ajoutant ces lignes, et redémarrez la VM :

```
allow-hotplug eth1
iface eth1 inet dhcp
```
