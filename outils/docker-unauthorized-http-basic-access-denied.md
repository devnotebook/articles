Docker – unauthorized: HTTP Basic: Access denied
================================================

[:type]: # "Astuce"
[:created_at]: # "2024/11/06"
[:modified_at]: # "2024/11/06"
[:tags]: # "docker outils"

Gitlab semble privilégier l’authentification via **jeton d’accès personnel**, pour se connecter au 
**registre de conteneurs** docker.

**Remarque** : Dans cet article, on considère que l’instance Gitlab hébergeant le registre de conteneurs avec lequel
vous souhaitez interagir a pour URL <https://my-gitlab.com>. Remplacez-la par la vôtre, sans oublier d’y ajouter
le port, si besoin.

Si vous n'êtes pas authentifié via un jeton, vous obtenez l’erreur suivante à chaque `git pull` ou `git push` :

```bash
# Utilisez l’URL de votre instance gitlab privée ou celle de gitlab.com
> docker pull my-gitlab.com/my-project-group/my-project/my/image:latest
Error response from daemon: Head "https://my-gitlab.com/my-project-group/my-project/my/image/manifests/latest": 
unauthorized: HTTP Basic: Access denied. The provided password or token is incorrect or your account has 2FA enabled 
and you must use a personal access token instead of a password. 
See https://my-gitlab.com/help/user/profile/account/two_factor_authentication#troubleshooting
```


## Génération d’un jeton d’accès personnel

Pour éviter cela, il vous faut **générer un nouveau jeton d’accès** dans Gitlab :

- Accédez à la page **Personal access tokens** de
  gitlab (<https://my-gitlab.com/-/user_settings/personal_access_tokens>)
- Cliquez sur le bouton « Ajouter un nouveau jeton »
- Précisez un nom de jeton (évitez les guillemets `"` et antislash `\`)
- Définissez éventuellement la date d’expiration du jeton
- Cochez les cases **read_api**, **read_registry** (et éventuellement **write_registry** si vous comptez faire des
  `docker push`)
- Sauvegardez le jeton généré dans un espace sécurisé

## Authentification avec le jeton

Authentifiez-vous auprès de votre instance Gitlab, en ligne de commande :

```bash
docker login my-gitlab.com --username "My token name" --password "glpat-as767nCwxmEfDhf_yHA7"
```

Cette commande revient à celle de login classique de docker, avec :

- pour **nom d’utilisateur**, le nom du jeton tel qu’affiché dans le tableau listant les jetons
- pour **mot de passe**, le jeton sauvegardé

**Remarque** :   
Comme docker l’indique en message d’alerte au lancement de la commande, celle-ci n’est pas sécurisée.
En effet, votre mot de passe sera stocké dans l’historique bash des commandes lancées.

Pour éviter cela, stockez plutôt le jeton dans un fichier :

```bash
# Créez un fichier ~/my_token.txt contenant votre jeton d’accès
cat ~/my_token.txt | docker login my-gitlab.com --username "My token name" --password-stdin
rm ~/my_token.txt
```

ou en variable d’environnement :

```bash
echo "$MY_TOKEN" | docker login my-gitlab.com --username "My token name" --password-stdin
```
