Afficher du JSON avec Firefox
=============================

[:type]: # "Marque-page"
[:created_at]: # "2013/09/05"
[:modified_at]: # "2017/12/15"
[:tags]: # "outils"

Lorsque vous affichez un document JSON dans Firefox, vous obtenez quelque chose comme :

```
{"errorMessageCustom":null,"errorCodeCustom":0,"errorMessage":null,"errorCode":0,"results":[{"message":"Le cache des requêtes sql  a bien été rechargé."}],"nbResults":1,"ok":true}
```

Si vous êtes en train de tester des services web qui retournent du JSON et que vous voulez afficher les résultats,
cette extension Firefox vous sera très utile : [JSON Lite](https://addons.mozilla.org/fr/firefox/addon/json-lite/).

Pour le même JSON, vous obtiendrez un affichage bien plus agréable :

![Afficher du JSON](./afficher-du-json-avec-firefox-01.png)
