Tester ses regexp
=================

[:type]: # "Marque-page"
[:created_at]: # "2016/05/15"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils"

Le site [https://regex101.com](https://regex101.com) propose un outil complet de test d'expressions régulières :

![https://regex101.com](./tester-ses-regexp-01.png)
