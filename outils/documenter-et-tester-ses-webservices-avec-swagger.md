Documenter et tester ses webservices avec Swagger
=================================================

[:type]: # "Marque-page"
[:created_at]: # "2013/07/31"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils"

Si vous créez des webservices et les distribuez, il est utile de fournir également la 
documentation expliquant comment les utiliser.

Il existe un outil proposant une interface web pour tester des webservices, 
qui détaille également leur utilisation : [Swagger](http://swagger.io/swagger-ui/).

![Swagger UI](./documenter-et-tester-ses-webservices-avec-swagger-01.png)

Cet outil gère tous les types de requête HTTP (`GET`, `POST`, `PUT`, `DELETE`, ...). 
Il permet d'envoyer des données au format JSON ou autre, d'afficher les réponses avec coloration syntaxique, les codes d'erreur, ...

Chaque webservice est détaillé dans une structure de données au format JSON. 
Cette structure peut être générée automatiquement pour de nombreux langages et Frameworks, notamment en utilisant des 
annotations particulières dans vos méthodes.
