Un formateur et validateur JSON
===============================

[:type]: # "Marque-page"
[:created_at]: # "2013/09/11"
[:modified_at]: # "2017/04/21"
[:tags]: # "outils"

Voici un site pour valider et formater du JSON :
 
[http://jsonformatter.curiousconcept.com](http://jsonformatter.curiousconcept.com)
 
![Formateur JSON](./un-formateur-et-validateur-json-01.png)
 
![Résultat du formateur JSON](./un-formateur-et-validateur-json-02.png)
