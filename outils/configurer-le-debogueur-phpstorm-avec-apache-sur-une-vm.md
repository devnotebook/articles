Configurer le débogueur PHPStorm avec Apache sur une VM
=======================================================

[:type]: # "Astuce"
[:version]: # "Debian 8, PHP 5.x"
[:sources]: # "[www.jetbrains.com](https://www.jetbrains.com/help/phpstorm/2017.1/configuring-xdebug.html)"
[:created_at]: # "2017/03/30"
[:modified_at]: # "2020/11/07"
[:tags]: # "outils"

Si vous utilisez une VM LAMP et PHPStorm, vous pouvez utiliser son débogueur pas à pas et 
visualiser/modifier vos variables en temps réel au cours de l'exécution.

Pour cela, plusieurs étapes sont nécessaires.

## Installation de XDebug

Connectez-vous à votre VM, et lancez la commande d'installation :

```bash
sudo apt-get install php5-xdebug
```

Activez ensuite le débogage distant de xdebug en éditant le fichier ```/etc/php5/apache2/conf.d/20-xdebug.ini```. 
Ajoutez-y les lignes suivantes :

```ini
xdebug.remote_enable=1
xdebug.remote_host=192.168.0.1
xdebug.remote_port=9000
xdebug.max_nesting_level=500
```

**Remarques :**

Remplacez l'IP par celle de la machine sur laquelle vous lancez PHPStorm.
La dernière ligne évite une erreur de récursion lors du débogage

## Ajout d'un environnement distant dans PHPStorm

Ajoutez une nouvelle connexion SSH à votre VM :

* Allez dans **File** > **Settings...** > **Build, Execution, Deployment** > **Deployment**
* Cliquez sur le ```+``` vert à droite, pour ajouter un environnement
* Choisissez un nom
* Dans **Connection**, choisissez le type **SFTP**, et renseignez toutes les informations
* Pour le **Root path**, indiquez le chemin vers le répertoire contenant vos sources sur votre VM (ex : ```/var/www/mon-site```), ou un répertoire parent (ex : ```/var/www```)
* Dans **Mappings**, remplissez les 3 champs de chemin
  * **Local path** : la racine de votre projet sur la machine qui lance PHPStorm
  * **Deployment path** : le chemin relatif depuis le Root path défini précédemment, vers votre projet sur la VM (ex : ```/``` ou ```/mon-site```)
  * **Web path** :  ```/``` si vous utilisez un nom de domaine pointant directement vers votre projet
* Validez

Précisez le mapping :

* Allez dans **File** > **Settings...** > **Languages & Frameworks** > **PHP** > **Server**
* Sélectionnez le serveur que vous venez de configurer
* Pour chaque répertoire contenant des fichiers PHP à déboguer, cliquez à droite, dans la colonne **Absolute path on the server**
* Saisissez le chemin absolu vers le répertoire correspondant sur votre VM (ex: ```/var/www/monsite/web``` ou ```/var/www/monsite/vendor```)
* Validez

## Installation d'une extension pour navigateur

Pour déclencher le débogage, il faut ajouter un cookie à vos requêtes lors de votre navigation.

L'extension [easy Xdebug](https://addons.mozilla.org/fr/firefox/addon/easy-xdebug/) pour Firefox 
et [XDebug Helper](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc) pour Chrome 
permettent d'activer/désactiver ce cookie automatiquement via un bouton.

Le [site officiel pour PHPStorm](https://confluence.jetbrains.com/display/PhpStorm/Browser+Debugging+Extensions) liste 
d'autres extensions pour d'autres navigateurs.

Activez le débogage dans votre navigateur.

## Activation dans PHPStorm

Cliquez sur le bouton **Start Listening for PHP Debug Connections** présent dans la barre d'outil de PHPStorm (téléphone gris avec un sens interdit rouge et un insecte vert).

## Début du débogage

Affichez une page de votre site.

Lors du premier débogage, une popup apparaît dans PHPStorm vous demandant de confirmer le débogage (normalement la configuration présentée dans la popup n'a pas besoin d'être modifiée).

Pour les suivants, vous devriez voir apparaître une petite notification dans PHPStorm vous disant que le débogage a fonctionné, 
mais ne s'est pas arrêté.
Dans cette notification, vous pouvez activer l’arrêt automatique à la première ligne du fichier index.php. 
Sinon, ajoutez un point d'arrêt (clic dans la marge de gauche du fichier PHP à déboguer). 
Rechargez la page.
