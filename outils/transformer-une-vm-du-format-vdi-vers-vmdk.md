Transformer une VM du format vdi vers vmdk
==========================================

[:type]: # "Astuce"
[:sources]: # "[kodira.de](http://kodira.de/2013/06/import-virtualbox-vms-in-vmware-esxi/)"
[:created_at]: # "2015/04/17"
[:modified_at]: # "2020/11/07"
[:tags]: # "outils"

Si vous travaillez avec Virtualbox, vos VM ont peut-être le format par défaut proposé par le logiciel : **vdi**.

Malheureusement beaucoup de serveurs de virtualisation ne reconnaissent pas ce format mais plutôt le **vmdk**.

Voici une ligne de commande permettant d'effectuer la conversion de la VM de **vdi vers vmdk** :

```bash
vboxmanage clonehd ma_vm.vdi ma_nouvelle_vm.vmdk -format VMDK -variant Fixed,ESX
```

**Remarque :**

Vous devez **éteindre la VM** à copier et **stopper Virtualbox** avant de lancer la commande.
