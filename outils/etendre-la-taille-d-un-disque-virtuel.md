Étendre la taille d'un disque virtuel
=====================================

[:type]: # "Marque-page"
[:sources]: # "[stackoverflow.com](https://stackoverflow.com/questions/11659005/how-to-resize-a-virtualbox-vmdk-file) | [www.rootusers.com](https://www.rootusers.com/use-gparted-to-increase-disk-size-of-a-linux-native-partition/)"
[:created_at]: # "2017/08/08"
[:modified_at]: # "2017/08/08"
[:tags]: # "outils"

Si vous avez créé un disque avec VirtualBox et qu'il est trop petit, vous pouvez l'agrandir.

Vous pouvez voir les disques d'une VM en cliquant-droit dessus : **Configuration...** > **Stockage**.

![Configuration des disques](./etendre-la-taille-d-un-disque-virtuel-01.png)

## Modifications côté Virtual box ##

Avant toute chose, **prenez le temps de créer un clone intégral de votre VM**.
En cas de problème vous n'aurez ainsi rien perdu.

### Prérequis ###
Tout d'abord, il faut que votre disque soit au format **vdi** (et pas **vmdk**).
Si ce n'est pas le cas, utilisez la commande suivante :

```bash
VBoxManage clonehd "source.vmdk" "cloned.vdi" --format vdi
```
Si votre disque a une taille fixe et non pas allouée dynamiquement, ajoutez l'option ```--variant Standard```
à la commande précédente pour en changer (vous pourrez revenir à l'état précédent par la suite).

### Ajout d'espace disque ###
Il faut ensuite modifier la taille du disque, via la commande :

```bash
VBoxManage modifyhd "cloned.vdi" --resize 51200
```
```51200``` est la nouvelle taille en Mo.

### Retour au format initial ###
Vous avez maintenant un disque au format **vdi**, avec une taille dynamique.
Pour retrouver un disque au format vmdk, et/ou de taille fixe, inversez la première commande :
```bash
VBoxManage clonehd "cloned.vdi" "source.vmdk" --format vmdk --variant Fixed
```

## Modification côté VM ##

Votre disque a maintenant la taille souhaitée, mais la partition n'a pas changé.
Vous avez donc tout un espace disque non alloué.

* Téléchargez un iso de [gparted](http://gparted.fr/telecharger/)
* Insérez-le dans le lecteur virtuel de votre VM
* Bootez dessus
* Suivez [ce tutoriel](https://www.rootusers.com/use-gparted-to-increase-disk-size-of-a-linux-native-partition/)
* Redémarrez votre VM

Vous devriez maintenant avoir un disque dur plus grand et fonctionnel !
