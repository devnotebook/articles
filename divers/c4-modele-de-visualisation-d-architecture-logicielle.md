C4 - Modèle de visualisation d'architecture logicielle
======================================================

[:type]: # "Marque-page"
[:created_at]: # "2021/07/14"
[:modified_at]: # "2021/07/14"
[:tags]: # "divers"

C4 est une proposition d'organisation graphique pour représenter une architecture logicielle 
sous forme de schémas.

Documentation : <https://c4model.com>

## Échelles

Le premier principe propose d'utiliser différentes échelles, pour différents schémas,
plutôt qu'un seul, tentaculaire et incompréhensible.

Les 4 échelles, de la plus générale à la plus détaillée constituent les 4C.

![4 niveaux](./c4-modele-de-visualisation-d-architecture-logicielle-01.png)

### Context

C'est le niveau le plus haut, le contexte : ce qui se passe autour de l'application.  
Celle-ci est représentée en un bloc au centre, autour duquel gravitent :

- les utilisateurs
- les autres systèmes interagissant avec elle

### Containers

C'est le niveau représentant les différentes briques logicielles de l'application : les conteneurs.  
C'est un peu le même diagramme que le précédent, mais avec un zoom dans la boîte centrale.

Exemples de conteneurs :

- une webapp Angular
- une API Symfony
- Une BDD PostgreSQL

### Components

Les composants représentent les différents besoins fonctionnels auxquels répond un conteneur.  
Un schéma de ce niveau permet de visualiser l'organisation générale du conteneur.

Par exemple, on pourra trouver :

- l'inscription
- la gestion des utilisateurs
- l'affichage des actualités
- la newsletter

Cela correspond souvent aux premiers niveaux de namespace/package/répertoires/etc.

### Code

C'est le niveau qui zoome sur l'un des composant. Il est beaucoup plus anecdotique que
les précédents, et pourrait représenter un diagramme de classes, par exemple.

### Conclusion

Le but du modèle n'est pas d'imposer un nombre d'échelles. De toute façon, et particulièrement
en informatique, tout est poupées russes. On peut toujours zoomer et créer un nouveau schéma 
augmentant le niveau de détails.

Le but est plutôt de savoir découper ses schémas pour qu'ils restent pertinents et lisibles.  
C'est ainsi seulement, qu'ils ajoutent de la valeur.

## Représentation  

Le second aspect important du modèle soutien ce même objectif. On peut le résumer ainsi : 
**un schéma doit se suffire à lui-même**.

Il doit donc contenir suffisamment de texte (dans les blocs et sur leurs liaisons), 
un titre et une légende.

La comparaison avec une carte de géographie est très bien trouvée, car c'est exactement 
la même méthode. Une carte doit montrer quelque chose, sans besoin d'une page d'explication à côté.

## Outils

Pour faciliter la création de schémas, le site officiel recense plusieurs outils, dont :

- [PlantUML](https://plantuml.com/fr/) : un langage permettant de générer des schémas à partir de fichiers textuels
- [diagrams.net](https://www.diagrams.net/) : une interface graphique gratuite en ligne
