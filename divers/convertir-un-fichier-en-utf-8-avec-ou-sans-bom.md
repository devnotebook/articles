Convertir un fichier en UTF-8 avec ou sans BOM
==============================================

[:type]: # "Astuce"
[:created_at]: # "2013/04/30"
[:modified_at]: # "2017/04/21"
[:tags]: # "divers"

Un fichier encodé en UTF-8 peut avoir un caractère spécial tout au début, qu'on appelle **BOM**.

Certains éditeurs ont besoin du BOM pour afficher correctement du texte en UTF-8 (accents, caractères spéciaux, ...). 
C'est le cas de Wordpad sous Windows. D'autres plus "intelligents", comme Notepad++ et la plupart des éditeurs 
sous Linux, détectent l'encodage du document et ajoute le BOM si besoin.

Pour ajouter manuellement (ou programmatiquement) ce caractère, il suffit d'utiliser son code ASCII : ```\uFEFF```.

Sous Windows pour savoir si un fichier UTF-8 est encodé avec ou sans BOM, vous pouvez l'ouvrir avec **Notepad++** et 
cliquer sur l'onglet **Encodage**.
