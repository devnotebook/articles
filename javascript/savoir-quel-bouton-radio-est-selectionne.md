Savoir quel bouton radio est sélectionné
========================================

[:type]: # "Astuce"
[:created_at]: # "2013/07/22"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript jquery"

Si dans un formulaire vous avez deux boutons radio "Oui"/"Non et que vous avez besoin de savoir lequel est sélectionné, 
vous pouvez utiliser la fonction suivante :

```javascript
/**
 * Retourne si l'élément radio est à true.
 * 
 * @param elementName Nom de l'élément (attribut "name")
 */
function isChecked(elementName) {
 
    var element = jQuery('input[name=' + elementName + ']:checked');
    
    return element != null && element.val() == 1;
}
```

**Remarques :**

* Cette fonction suppose que le bouton radio "Oui" a pour valeur `1` et l'autre `0`.
* Cette méthode peut être utilisée pour une liste de radio plus importante. 
Il suffit de modifier le test `element.val() == 1` par une autre valeur que `1`.
