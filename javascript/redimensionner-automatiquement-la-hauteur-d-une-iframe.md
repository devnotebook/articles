Redimensionner automatiquement la hauteur d'une iframe
======================================================

[:type]: # "Astuce"
[:created_at]: # "2013/07/22"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript"

Si vous utilisez une iframe dans une page, vous pouvez fixer sa largeur et sa hauteur. 
Si vous ne le faite pas, ses dimensions seront limitées par le navigateur et tout le contenu de 
l'iframe ne sera pas forcément visible sans scroller.

Pour agrandir l'iframe en fonction de son contenu, vous pouvez utiliser cette fonction :

```javascript
/**
 * Redimensionne automatiquement la hauteur de l'iframe contenant la page courante.
 * (La page courante est celle dans laquelle est appelée cette fonction.)
 *
 * @param marginHeight Marge verticale en pixel à appliquer (facultatif, 25 par défaut)
 */
function autoResizeIFrame(marginHeight) {
 
    marginHeight = marginHeight || 25;
    
    var id;
    
    // Si la page courante est dans une iframe
    if (frameElement) { 
        try { 
            id = frameElement.attributes['id'].value; 
        } catch (e) { 
            if (window.console && console.log) {
                console.log(e.message);
            }
        }
    }
    
    // Si la page contenant l'iframe a un ID
    if (id) {    
        if (document.getElementById) {        
            // Firefox
            if (navigator.appName == 'Netscape') {            
                parent.document.getElementById(id).style.height = this.document.body.offsetHeight + marginHeight + 'px';
            // IE
            } else {                        
                marginHeight += 5;
                parent.document.getElementById(id).style.height = this.document.body.scrollHeight + marginHeight + 'px';
            }            
        // Opera
        } else if (!window.opera && !document.mimeType && document.all && document.getElementById) {        
            marginHeight -= 7;
            parent.document.getElementById(id).style.height = this.document.body.offsetHeight + marginHeight + 'px';
        }
        
        // Retour au haut de la page
        window.parent.scrollTo(0, 0);
    }
}
```

Pour l'utiliser, il suffit d'appeler la fonction depuis l'iframe, à la fin du chargement de la page 
(ou après avoir modifier le contenu de l'iframe via Javascript).

**Remarque :**

Cette fonction n'est utilisable que si la page qui inclue l'iframe et la page à l'intérieur ont le même domaine. 
Cette restriction est une sécurité propre à Javascript pour éviter à une page de modifier le contenu d'une autre.
