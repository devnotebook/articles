SessionStorage vs LocalStorage
==============================

[:type]: # "Astuce"
[:created_at]: # "2022/01/14"
[:modified_at]: # "2022/01/14"
[:tags]: # "javascript"

Dans une application ou un site web, on peut stocker des informations dans 
le navigateur de l'utilisateur, pour un nom de domaine spécifique.

Deux approches principales : les cookies et le `Storage`. Pour la seconde, deux possibilités :
`sessionStorage` ou `localStorage`.

## cookie vs Storage

La principale différence, c'est que les cookies sont accessibles côté client **et** côté serveur.
À l'inverse, il n'y a aucun moyen côté serveur d'accéder au contenu ni du `localStorage` 
ni du `sessionStorage`.

**Remarque :**

Quand on parle de "sessions" pour le `sessionStorage`, il n'y a aucun rapport avec les sessions 
entre client/serveur comme on peut les manipuler en PHP, par exemple.

## session vs local

Les données stockées en **sessionStorage** sont rattachées à un onglet.  
Si on recharge la page au sein d'un même onglet, les données sont conservées.
En revanche, elles expirent à sa fermeture.

Les données stockées en **sessionStorage** sont rattachées à un onglet.

**Remarque :** 

Selon le navigateur, si on restaure un onglet fermé précédemment, on récupère le `sessionStorage` 
tel qu'il était avant fermeture. C'est le comportement pour les navigateurs basés sur Chrome. 
De même, si on duplique un onglet ouvert, la copie hérite d'une copie du `sessionStorage`
de l'onglet original.

## Pour aller plus loin

- Interface Storage : [Doc Mozilla](https://developer.mozilla.org/fr/docs/Web/API/Storage)
- Utilisation : [alsacréations](https://www.alsacreations.com/article/lire/1402-web-storage-localstorage-sessionstorage.html)
- Discussion/comparaison : [Stackoverflow](https://stackoverflow.com/questions/5523140/html5-local-storage-vs-session-storage)

