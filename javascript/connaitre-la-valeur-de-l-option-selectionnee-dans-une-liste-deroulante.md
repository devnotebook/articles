Connaître la valeur de l'option sélectionnée dans une liste déroulante
======================================================================

[:type]: # "Astuce"
[:created_at]: # "2013/09/09"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript jquery"

Pour savoir quelle option de votre liste déroulante est sélectionnée, vous pouvez utilisez quelque chose comme ça :

```javascript
jQuery('select[name="mySelect"] option:selected').val();
```

ou

```javascript
jQuery('#mySelectID').find('option:selected').val();
```
