Afficher le numéro de version
=============================

[:type]: # "Astuce"
[:created_at]: # "2024/05/21"
[:modified_at]: # "2024/05/21"
[:tags]: # "javascript typescript"

Pour simplifier les mises à jour de votre application, il est préférable de n'indiquer le numéro de version qu'à un 
seul endroit. Cet endroit peut être un fichier mis à jour manuellement ou via la CI. Par exemple le `package.json`.

Ex :

```json
{
  "name": "my-app",
  "version": "0.0.1",
  [...]
}
```

Pour récupérer le numéro de ce fichier, vous pouvez utiliser simplement les **imports** javascript : 

```typescript
// src/app/App.tsx
import { version } from '../../package.json';
```

Si vous utilisez typescript, cela nécessite d'activer l'option `resolveJsonModule` dans `tsconfig.json` :

```json
{
  "compilerOptions": {
    [...]
    "resolveJsonModule": true,
    [...]
  }
}
```
