Rediriger une page depuis une iframe
====================================

[:type]: # "Astuce"
[:created_at]: # "2013/11/13"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript"

Si vous êtes dans une iframe et que vous souhaitez rediriger la page qui la contient, 
vous pouvez utiliser ces quelques lignes :

```javascript
var url = "https://targeturl.com"
 
// Si on est dans une iframe
if (frameElement) {                
    window.top.location.href = url;
}
```

**Remarque :**
Ce code javascript doit se trouver dans l'iframe. 
