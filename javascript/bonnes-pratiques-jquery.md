Bonnes pratiques jQuery
=======================

[:type]: # "Astuce"
[:created_at]: # "2016/09/12"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript jquery"

## Isoler son code ##

Pour éviter que ses variables et fonctions ne soient disponibles partout et ne rentrent en conflit entre-elles, 
il est important d'isoler son code.

Pour cela, plusieurs solutions sont possibles, en fonction des cas.

### Code à usage unique ###

Cela consiste à englober son code pour que les variables et fonctions deviennent locales.

```javascript
(function($) {
 
    // Code à exécuter
    // [...]
})(jQuery);
```

### Code à usage régulier ###

Cela consiste à placer ses variables et fonctions dans un **namespace** autre que celui par défaut (= lorsqu'on n'en précise pas). 

```javascript
$.myNamespace = {
    multiply: function(x,y) {
        return (x * y);
    }
};
 
// Appel
var calc = $.myNamespace.multiply(1, 1);
```

### Code affectant des éléments du DOM 1 ###

On utilise le namespace de jQuery : `$` ou `jQuery`.

```javascript
$.fn.myFunction = function(params) {
 
    this.css('color', 'red');
    return this;
};
 
// Appel
$('.element').myFonction();
```

**Explication :**

On crée un [plugin jQuery](http://learn.jquery.com/plugins/), que l'on appelle sur un ensemble d'éléments 
du DOM (ex: `$('.element')`). Tous ces éléments sont impactés une fois, sans mémorisation de leur état (= **stateless**).

### Code affectant des éléments du DOM 2 ###

```javascript
$.widget('ui.trsb.truc.myWidget', [$.ui.widgetParent,]{
    "options": {},
    myWidgetFunction(){...},
    _create: function(){...},
    _destroy: function(){...},
    _setOptions: function(key, value){...},
});
 
// Appel
$('.element').myWidget();
$('.element').myWidgetFunction();
```

**Explications :**

On crée un [widget jQuery](https://jqueryui.com/widget/#default) (nécessite jQueryUI), que l'on appelle sur un ensemble d'éléments 
du DOM (ex: `$('.element')`). Tous ces éléments sont impactés avec mémorisation de leur état (= **statefull**). 

Les widgets sont utiles lorsque l'on veut proposer plusieurs méthodes d'action (ex: un accordéon avec une fonction pour dérouler, 
et une autre pour replier).

## Normes de codage et optimisations ##

### Sélecteurs ###

```javascript
// Pour des raisons de compatibilité, préférez
$('#container').find('.children').hide();
 
// à 
$('#container .children').hide();
 
// ou à 
$('.children', '#container').hide();
```

### Éviter le tout jQuery ###

```javascript
$('#link').on('click', function() {
 
    // Préférez
    this.id
 
    // à
    $this.attr(id');
});
```

### Chargement du DOM ###

```javascript
// Préférez 
$(function() {
 
});
 
// à 
$(document).ready(function() {
 
});
```

### Gestion des évènements ###

```javascript
// Préférez 
$('#element').on('click', function() {
 
});
 
// à 
$('#element').click(function() {
 
});
```

### No conflict ###

```javascript
// Préférez 
(function($) {
 // [...] Code sans conflit
})(jQuery);
 
// à 
jQuery.noConflict();
// [...] Code sans conflit
```

### Appels AJAX ###

```javascript
// Préférez 
$.ajax()
 
// à 
$.getJSON();
```
