Timepicker
==========

[:type]: # "Marque-page"
[:created_at]: # "2013/09/04"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript jquery"

JQuery UI fournit un datepicker pour agrémenter les champs date d'un petit calendrier. 
Malheureusement il ne propose pas l'équivalent pour un champ heure.

## Premier Datepicker ##

C'est ce que propose ce site : [http://trentrichardson.com/examples/timepicker/](http://trentrichardson.com/examples/timepicker/).

![Premier timepicker](./timepicker-01.png)

Ce timepicker étends jQuery UI et permet de le combiner ou non avec le datepicker déjà existant.

**Remarque :**

Le timepicker a été traduit dans de nombreuses langues. Les traductions sont disponibles sur 
[ce repo git](https://github.com/trentrichardson/jQuery-Timepicker-Addon/tree/master/src).

## Second Datepicker ##

Si vous utilisez le framework Bootstrap, ce site propose un autre timepicker un peu plus agréable à utiliser : 
[http://jdewit.github.io/bootstrap-timepicker/](http://jdewit.github.io/bootstrap-timepicker/).

![Second timepicker](./timepicker-02.png)
