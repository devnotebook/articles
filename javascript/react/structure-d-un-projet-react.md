Structure d'un projet React
===========================

[:type]: # "Astuce"
[:version]: # "React 8"
[:created_at]: # "2024/05/14"
[:modified_at]: # "2024/05/14"
[:tags]: # "react javascript typescript"

Comme pour pas mal d'aspects, React ne propose pas vraiment de solution standard pour organiser la structure de son 
projet.

Voici quelques pistes possibles, se rapprochant de ce qu'on peut trouver dans l'écosystème React.

## Structure simple

```
src/
- components/
  - MyComponent/
    - MyComponent.tsx
- helpers/
- hooks/
- model/
  - MyEntity.ts
- services/
```

On regroupe ici chaque élément par type (les hooks avec les hooks, les composants avec les composants, les interfaces 
du modèle entre elles…). Au fur et à mesure que le projet grossit, on ajoute des sous répertoires. Par exemple :

```
src/
- components/
  - forms/
    - fields/
      - MyField/
        - MyField.tsx
    - MyForm/
        - MyForm.tsx
  - pages/
    - MyPage.tsx
- helpers/
- hooks/
- model/
  - interfaces/
    - MyEntity.ts
  - enums/
    - MyEnum.ts
- services/
```

## Atomic design

Cette approche se focalise sur le découpage des composants, et nous pousse à réfléchir à leurs responsabilités. 
Pour le reste, on peut conserver la structure précédente.

```
src/
- components/
  - atoms/
    - MyField/
      - MyField.tsx
  - molecules/
    - MyForm/
      - MyForm.tsx
  - organisms/
    - MyForm/
      - MyForm.tsx
```

Le principe, c'est de hiérarchiser les composants en 3 groupes :

- les **atomes**, sont les composants élémentaires de plus petit niveau, à la base de tout. On y trouve typiquement 
des boutons, des champs de formulaires… Bref, des composants réutilisables et sans aucun rapport avec le métier.
- les **molécules** constituent le niveau intermédiaire, et représentent des ensembles fonctionnels réutilisables. 
On peut y ranger les formulaires, les listes…
- les **organismes** correspondent au niveau le plus haut, typiquement à une page simple. Par exemple une liste de 
tâches, avec des filtres et une pagination.

Les dépendances sont interdites vers un niveau supérieur. Autrement dit, vos **atomes** ne doivent dépendre de rien.

Vous en saurez plus avec cet article de blog : <https://blog.logrocket.com/atomic-design-react-native>.

**Bonus** :

Si vous devez gérer des pages complexes, qui proposent plusieurs fonctionnalités indépendantes, mais sur un même écran, 
vous pouvez ajouter un quatrième niveau `pages/`, qui servira de passe-plat entre le **routeur** et les **organismes**.

Cela permet aux **organismes** de s'affranchir des paramètres d'URL et ajoute plus de souplesse si tout à coup, 
vous devez remplacer votre page de création d'utilisateur par une fenêtre modale sur votre page de liste.

De plus, comme pour la structure simple, vous pouvez créer des sous-répertoires pour regrouper vos composants (ex : 
`buttons/`, `fields/`, `lists/`, `cards/`…).

## Features

Le problème des deux types de structures précédentes, c'est que les fichiers sont disséminés un peu partout, plutôt 
que d'être regroupés par zone fonctionnelle. Si vous voulez supprimer une page, vous devrez peut-être supprimer des 
fichiers dans `model/`, `components/` et `hooks/`.

Pour éviter cela, vous pouvez utiliser la structure proposée sur le dépôt 
[bulletproof-react](https://github.com/alan2207/bulletproof-react/blob/master/docs/project-structure.md).

Le principe est de créer un répertoire `features/`, contenant autant de sous-répertoires que de zones fonctionnelles
de votre application. Dans chacun d'eux, on y retrouvera la structure simple (voire Atomic design) présentée 
précédemment.

Par exemple :

![plusieurs features](./structure-d-un-projet-react-01.png)

Cette structure n'a d'intérêt que si l'on respecte une **hiérarchie entre les features**. Une première feature peut dépendre 
d'une ou plusieurs autres, mais ces deux autres ne doivent pas dépendre de la première. Pour résumer, **attention aux 
dépendances cycliques**.

L'exercice n'est pas forcément simple, et si l'on n'arrive pas à éviter les dépendances bidirectionnelles entre deux 
composantes métier, c'est qu'elles sont trop intriquées pour les séparer en deux features distinctes.  
On peut alors éventuellement utiliser une troisième feature (ex : `core`), dont dépendront les deux autres, ou placer
les fichiers transversaux dans des répertoires à la racine de `src/` comme proposé par **bulletproof-react**.
