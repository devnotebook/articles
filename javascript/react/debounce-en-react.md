Debounce en React
=================

[:type]: # "Astuce"
[:version]: # "React 16.8"
[:created_at]: # "2023/03/30"
[:modified_at]: # "2023/03/30"
[:tags]: # "react javascript typescript"
[:sources]: # "[stackoverflow.com](https://stackoverflow.com/a/61140811/2674501)"

Régulièrement en Javascript, on veut écouter les changements sur un élément, mais ne les prendre en compte qu’à 
intervalles réguliers.  
Par exemple, si on écoute le déplacement de la souris pour lancer un traitement, on n’a pas besoin de l’exécuter
à chaque changement de position, mais uniquement tous les 300 ou 500ms.  

Pour éviter l’effet « rebond », on utilise alors une fonction de **debounce**, ou dans React, un **hook**.

## Script

```typescript
import { useEffect, useState } from 'react';

/**
 * Delays the value change, so it’s only committed once during the specified duration.
 *
 * Usage :
 * <pre>
 * const debouncedValue = useDebouncedValue(realTimeValue, 500);
 * <pre>
 */
export const useDebouncedValue = <T>(input: T, duration = 500): T => {
  const [debouncedValue, setDebouncedValue] = useState(input);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDebouncedValue(input);
    }, duration);

    return () => {
      clearTimeout(timeout);
    };
  }, [input, duration]);

  return debouncedValue;
};
```

## Exemple d’utilisation

On veut afficher un message dans la console au changement de valeur du champ de formulaire,  
mais limiter ce traitement avec un **debounce**.

```tsx
import React, { useEffect, useState } from 'react';
import { useDebouncedValue } from './utils/debounce';

export default (() => {
  const [myValue, setMyValue] = useState('');
  const myDebouncedValue = useDebouncedValue(myValue, 500);

  useEffect(() => {
    console.log('This log is displayed when the value of debouncedFilterNom changes', myDebouncedValue);
  }, [myDebouncedValue]);

  const handleValueChange = async (event: any) => {
    setMyValue(event.target.value);
  };

  return (
    <form>
      <input type="text" onChange={handleValueChange} value={myValue} />
    </form>
  );
}) as React.FC;
```
