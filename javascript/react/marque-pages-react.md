Marque-pages React
==================

[:type]: # "Marque-page"
[:version]: # "React 16.8"
[:created_at]: # "2023/04/06"
[:modified_at]: # "2024/05/14"
[:tags]: # "react javascript"

Contrairement à Angular, React propose peu de solutions par défaut aux problèmes les plus courants.
Il faut souvent ajouter des modules en dépendance, et il est parfois difficile de ne pas se perdre dans la
jungle des modules pour React.

Voici une liste de modules et de pratiques utiles pour les besoins courants.

## Socle

### Mise en place

On peut initialiser facilement une application vierge React + [Vite](https://vitejs.dev/guide/) avec typescript, 
via la commande `npm create vite@latest my-app -- --template react-ts`.

### Routage

Le module [react-router](https://v5.reactrouter.com/web/guides/quick-start) fait ça très bien.

### Appels API

Pour récupérer des données auprès d’une API ou via d’autres méthodes asynchrones, on peut utiliser
[SWR](https://swr.vercel.app/docs/getting-started) ou [react-query](https://react-query-v3.tanstack.com/quick-start).

### État

Si on souhaite gérer un état globale de l’application au niveau local, on peut utiliser
[redux](https://react-redux.js.org/introduction/getting-started)
avec [redux-toolkit](https://redux-toolkit.js.org/introduction/getting-started)
ou alors [recoil](https://recoiljs.org/fr/docs/introduction/getting-started).

### Formulaires

Le module [react-hook-form](https://react-hook-form.com/get-started) fait ça très bien.
Il supporte notamment [Yup](https://react-hook-form.com/get-started/#SchemaValidation) pour gérer la validation de
champs.

### Internationalisation

Le module [react-i18next](https://react.i18next.com/getting-started) permet de gérer une appli multilingue.

## Design

### CSS

Pour restreindre des classes CSS à un composant en particulier, on peut utiliser le concept
de [modules CSS](https://create-react-app.dev/docs/adding-a-css-modules-stylesheet).

### Material

Les principales bibliothèques graphiques sont disponibles pour React, et notamment
[Material UI](https://mui.com/material-ui/getting-started/overview/).
Elle fonctionne très bien, sauf avec le **hook-form**. C’est problématique et cela nécessite
de recréer un composant « Contrôlable », pour chaque type de champ de formulaire.

## Qualité

### Tests

Plusieurs bibliothèques pour les tests fonctionnent bien avec React,
notamment  [jest](https://jestjs.io/fr/docs/tutorial-react).

### Lint

On peut combiner [prettier](https://prettier.io) et [eslint](https://github.com/jsx-eslint/eslint-plugin-react),
pour normaliser la base de code. Il existe des extensions à eslint spécialement pour React, afin de détecter et remonter
si certaines mauvaises pratiques sont utilisées.

### Documentation des composants

Pour documenter et présenter une bibliothèque de composants, on peut utiliser [Storybook](https://storybook.js.org).
Cela permet de manipuler et tester chaque composant, dans une interface dédiée.

## Autres

### Base de donnée locale

Pour utiliser plus facilement la base de données locale IndexedDB, on peut y accéder
via [dexie](https://dexie.org/docs/Tutorial/React).

### Authentification avec JWT

Pour extraire les données d’un jeton JWT, on peut utiliser [jwt-decode](https://github.com/auth0/jwt-decode).
