Modèle de composant
===================

[:type]: # "Marque-page"
[:version]: # "React 16.8"
[:created_at]: # "2023/04/06"
[:modified_at]: # "2023/04/06"
[:tags]: # "react javascript"

Voici un modèle de composant React minimal :

```typescript
// MyComponent.tsx
import React, { ReactNode } from 'react';
import styles from "./MyComponent.module.scss";

interface Props {
  children: ReactNode;
  notRequiredProperty?: string;
}

export const MyComponent: React.FC = ({ children, notRequiredProperty }: Props) => {
  return <div className={styles.myComponentClass}>{children}< /div>;
};
```

**Explications** :

- Le composant est utilisable dans un autre composant, via `<MyComponent children={<p>EXAMPLE</p>} />`
- On importe le fichier `MyComponent.module.scss`, dans lequel se trouve une règle CSS pour la
  classe `myComponentClass`.
- Le composant est un wrapper, auquel on passe un sous-composant via la propriété `children`.
- On déclare l’interface `Props` pour définir les propriétés du composant. Cela facilite l’autocomplétion et
  permet la validation du typage à la compilation.
- La propriété `notRequiredProperty` est facultative lorsqu’on appelle le composant.

## Héritage

Si on souhaite créer un wrapper pour un composant existant, on veut souvent qu’il accepte les mêmes propriétés.
Dans ce cas, on peut déclarer l’interface et le composant ainsi :

```typescript
type Props = SomeComponentToWrapProps & {
  extraProperty1: string
};

export const MyComponent: React.FC = ({ extraProperty1, inheritedProp1, ...others }: Props) => {
  // [...]
};
```

**Explications** :

- Les propriétés du composant sont l’union de celles de `SomeComponentToWrapProps`, avec celle déclarée ici.
- On indique les quelques propriétés dont on a explicitement besoin, et on peut accéder à toutes les autres via
  l’objet `others`.
