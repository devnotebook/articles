Modifier les métadonnées
========================

[:type]: # "Astuce"
[:version]: # "React 18"
[:created_at]: # "2024/05/21"
[:modified_at]: # "2024/05/21"
[:tags]: # "react javascript"

Dans une application React, vous pouvez définir des métadonnées via des balises `<meta>` à l'intérieur de la
balise `<head>`, directement dans le fichier `index.html`.

```html
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8"/>
    <base href="/"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, interactive-widget=overlays-content">
    <meta name="description" content="A sample app that introduces useMetaTag and useTitleTag hooks"/>
    <title>My Awesome app v1.1</title>
</head>
<body>
<div id="root"></div>
<script type="module" src="/src/main.tsx"></script>
</body>
</html>
```

Malheureusement, ces données sont statiques et ne s'adaptent pas dynamiquement à la page en cours. Pour palier cela,
la solution consiste à modifier ou ajouter ces balises dynamiquement dans le DOM en javascript.

Le projet [React Helmet](https://github.com/nfl/react-helmet#readme) devrait convenir à cette tâche, mais ne semble plus
maintenu.

Voici deux hooks simples permettant de gérer vous-même vos balises `<meta>` et `<title>`.

**Balises `<meta>`** :

```typescript
/**
 * Crée ou met à jour dynamiquement une balise meta dans le head de la page.
 *
 * @param {string} name Valeur de l'attribut `name` de la balise
 * @param {string} content Valeur de l'attribut `content` de la balise
 */
export const useMetaTag = (name: string, content: string) => {
    let metaElement = document.querySelector(`meta[name='${name}']`);
    if (!metaElement) {
      metaElement = document.createElement('meta');
      metaElement.setAttribute('name', name);
      document.head.appendChild(metaElement);
    }

    metaElement.setAttribute('content', content);
  };
```

**Balise `<title>`** :

```typescript
/**
 * Crée ou met à jour dynamiquement une balise title dans le head de la page.
 *
 * @param {string} title Titre de la page
 */
export const useTitleTag = (title: string) => {
    let titleElement = document.querySelector('title');
    if (!titleElement) {
      titleElement = document.createElement('title') as HTMLTitleElement;
      document.head.appendChild(titleElement);
    }

    titleElement.textContent = title;
  };
```

**Exemple d'utilisation** :

```typescript
import { useNavigation } from './helpers/useNavigation.ts';
import { useTitleTag } from './helpers/useTitleTag.ts';
import { version } from '../../package.json';

export const App = () => {
  useMetaTag('description', 'A sample app that introduces useMetaTag and useTitleTag hooks');
  useTitleTag(`My Awesome app - v${version}`);
}
```

**Note** : Si vous souhaitez des métadonnées spécifiques à une certaine page, utiliser ces hooks dans votre composant
associé à votre route plutôt que dans le `App.tsx`. 
