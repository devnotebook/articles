Utiliser les promesses
======================

[:type]: # "Astuce"
[:created_at]: # "2018/10/10"
[:modified_at]: # "2018/10/14"
[:tags]: # "javascript"

## Introduction ##

Lorsqu'on fait un appel à un webservice ou qu'on exécute une grosse requête en base de données, cela prend un certain temps.
On sait ce qu'on est sensé récupérer mais pas quand, ce qui semble plutôt gênant pour lancer la suite du traitement (ex: afficher les résultats).

Ce qu'on appelle une promesse, c'est ce qu'on espère recevoir comme résultat (un tableau, un flux, un objet, ... ce qu'on veut).
On code la suite du traitement à réaliser comme si on avait ce résultat, sauf qu'on part de l'objet Promise à la place.

## Exemples ##

Si on n'utilise pas les promesses et qu'on utilise une fonction asynchrone, cela peut ressembler à ça :

```javascript
function getNumberFromWebservice(successCllback, errorCallback) {
    try {
        // On récupère les données
        let number = xxx();
        successCllback(number);
    } catch {
        errorCallback('Impossible de récupérer le nombre.');
    }
}

getResultsFromWebservice(function (number) {
    console.log('Affichage du résultat :');
    console.log(number);
}, function (error) {
    console.error(error);
});
```

Cette méthode fonctionne bien, mais à quoi cela ressemblerait-il si on appelait une seconde fonction asynchrone au retour de la première ?

```javascript
function getNumberFromWebservice1(successCllback, errorCallback) {
    try {
        // On récupère les données
        let number = xxx();
        successCllback(number);
    } catch {
        errorCallback('Impossible de récupérer le nombre.');
    }
}

function getNumberFromWebservice2(number1, successCllback, errorCallback) {
    try {
        // On récupère les données
        let number2 = yyy();
        successCllback(number1 + number2);
    } catch {
        errorCallback('Impossible de récupérer le nombre.');
    }
}

function displayResult(sum) {
    console.log('Affichage de la somme :');
    console.log(sum);
}

function displayError(error) {
    console.error(error);
}

// /!\ Début de callback hell /!\
getResultsFromWebservice1(function (number1) {
    getResultsFromWebservice2(number1, function (sum) {
        displayResult(sum);
    }, function (error) {
        displayError(error);
    });
}, function (error) {
    displayError(error);
});
```

Ca devient vraiment peu lisible dès qu'on multiplie les fonctions asynchrones à chaîner.
On a des callback de callback de callback (cf. [Callback hell](http://callbackhell.com/)).

Les promesses permettent une syntaxe plus lisible :

```javascript
// Pas de callback hell, on évite l'écriture en imbrication
getResultsFromWebservice1()
    .then(function(number1) { 
        return getResultsFromWebservice2(number1);
    }),
    .then(function(sum) {
        console.log('Affichage de la somme :');
        console.log(sum);
    }),
    .catch(function (error) {
        console.error(error);
    });

// Ou même plus court avec la syntaxe allégée
getResultsFromWebservice1()
    .then((number1) => getResultsFromWebservice2(number1)),
    .then((sum) => displayResult(sum)),
    .catch((error) => displayError(error));
```

Le principe est le même sauf qu'on n'a **plus besoin de passer les callback en paramètres** à chaque fonction.
Par contre pour que ça marche, il faut modifier un peu chaque fonction pour qu'elle ne retourne non plus un nombre mais un **objet Promise** :

```javascript
function getNumberFromWebservice1() {
    return new Promise((resolve, reject) => {
        // On récupère les données
        let number = xxx();
        resolve(number);
    });
}

function getNumberFromWebservice2(number1) {
    return new Promise((resolve, reject) => {
        // On récupère les données
        let number2 = yyy();
        resolve(number1 + number2);
    });
}
```

**Remarque :**

Les try/catch ont été retirés dans les fonctions. Cela veut dire qu'en a cas d'erreur levée par l'appel aux webservices (xxx() et yyy()),
ce sera le message de l'erreur qui sera affiché, et non plus le message `'Impossible de récupérer le nombre.'`.


## Aller plus loin ##

Les promesses existent depuis un moment en javascript.
Il y a pas mal de cas d'utilisation :

* exécuter trois tâches asynchrones une à une
* exécuter trois tâches asynchrones une à une et en transmettant à chaque fois le résultat à la suivante
* exécuter plusieurs tâches asynchrones en parallèle, puis une dernière quand elles sont toutes terminées
* exécuter 2 séries de 3 tâches asynchrones en parallèle
* ...

La plupart des framework proposent une couche supplémentaire pour gérer ces cas d'utilisation.
Dans le monde de **NodeJS** par exemple, on utilise très souvent la lib [async](https://caolan.github.io/async/).

**Documentation Mozilla Développeur** :

* [Objet Promise](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Promise).
* [Guide d'utilisation](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_promesses).
