Introduction à NodeJS
=====================

[:type]: # "Marque-page"
[:created_at]: # "2018/10/10"
[:modified_at]: # "2018/10/10"
[:tags]: # "javascript"

## Exercices ##

Pour faire ses premiers pas avec **NodeJS**, le site [nodeschool.io](https://nodeschool.io/fr-fr/) propose des ateliers de développement. 

Concrètement, il s'agit de petites applications NodeJS qui affichent un énoncé, et sont capables d'éxécuter 
votre programme pour vérifier qu'il correspond aux consignes.
L'un des ateliers utiles pour débuter s'appelle [learnyounode](https://github.com/workshopper/learnyounode).

![Accueil learnyounode](./introduction-a-nodejs-01.png)

Pour l'utiliser [il vous faut NodeJS](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) :

```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Ensuite ça se passe avec **npm** (inclus vec NodeJS) :

```bash
npm install -g learnyounode
learnyounode
```

## Express ##

Le [framework Express](http://expressjs.com/en/4x/api.html) semble très largement répandu dans l'univers de NodeJS, 
dès lors qu'on souhaite créer une application web (API REST, site front, ...).

Mozilla Developpeur propose [un bon tutoriel pour démarrer avec Express](https://developer.mozilla.org/fr/docs/Learn/Server-side/Express_Nodejs).  
Il s'agît d'un cas concret de création d'appplication web, pour gérer une bibliothèque (de livres).  
Une partie front interroge un back REST full qui stocke les données dans une base MongoDB.

(Il y a également un [atelier sur le sujet](https://github.com/azat-co/expressworks) proposé sur **nodeschool.io**)

## Mongoose ##

Pour utiliser une base MongoDB avec NodeJS, il existe [l'API Mongoose](https://mongoosejs.com/docs/queries.html).  
(Elle est utilisée dans le tutoriel précédent.)
