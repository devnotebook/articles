Scroller automatiquement jusqu'à un élément
===========================================

[:type]: # "Astuce"
[:version]: # "jQuery <1.9, ou avec le plugin jQuery Migrate"
[:created_at]: # "2013/09/05"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript jquery"

Voici une petite fonction pour scroller automatiquement vers le haut jusqu'à un élément :

```javascript
/**
 * Scrolle automatiquement vers le haut jusqu'à l'élément dont le sélecteur est en argument.
 *
 * @param selector string Sélecteur de l'élément vers lequel scroller
 * @param nbMs Nombre de millisecondes pour effectuer l'animation de scroll (facultatif, 10 par défaut)
 */
function scrollToElement(selector, nbMs) {
 
    nbMs = nbMs || 10;
    var documentBody = (($.browser.chrome)||($.browser.safari)) ? document.body : document.documentElement;
    jQuery(documentBody).animate({scrollTop: jQuery(selector).offset().top}, nbMs, 'linear');
}
```

**Remarques :**

* JQuery doit être inclus dans la page.
* Depuis **jQuery 1.9**, la propriété `browser` a été retirée et transférée dans le plugin 
[jQuery Migrate](https://github.com/jquery/jquery-migrate/). Il faut donc ajouter ce plugin pour pouvoir l'utiliser.
