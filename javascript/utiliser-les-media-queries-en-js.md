Utiliser les media queries en JS
================================

[:type]: # "Astuce"
[:created_at]: # "2015/07/30"
[:modified_at]: # "2017/05/05"
[:tags]: # "javascript"

Dans une feuille CSS, si vous voulez appliquer un style particulier à élément, pour un petit écran, vous faites quelque chose comme ça :

```css
@media only screen and (max-width: 36.9375em) {
    .col {
        float: none;
        width: 100%;
    }
}
```

Il est possible d'utiliser une syntaxe similaire en Javascript :

```javascript
if (window.matchMedia != undefined
    && window.matchMedia('(max-width: 53.6875em)').matches) {
    alert('Mon écran est tout petit !').
}
```

**Explication :**

On vérifie que le navigateur comprend cette syntaxe et si oui on ajoute une condition sur la taille de la fenêtre.
