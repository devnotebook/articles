Optimiser l'affichage des animations javascript
===============================================

[:type]: # "Astuce"
[:created_at]: # "2013/01/28"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript jquery"

Lorsque vous utilisez des animations en Javascript avec jQuery, il arrive que les animations s'emmêlent si elles 
sont exécutées rapidement les unes après les autres.

Par exemple, si vous construisez un menu en accordéon et que vous cliquez très vite de lien en lien, 
les menus n'ont pas forcément le temps de se déplier puis se replier.

Vous pouvez remédier à ce problème en utilisant la fonction `stop()` de jQuery, pour que les animations en 
cours s'arrêtent avant d'en commencer de nouvelles.

Par exemple :
```javascript
// Arrêt de toutes les animations en cours sur les éléments du menus
jQuery('#right-menu li.sub-menu-container ul.sub-menu').stop(true, true);
```

Toutes les animations en cours sur les éléments `ul.submenu` sont annulées/s'arrêtent instantanément.

**Remarque :**

Le second paramètre passé à `true`, permet de terminer les animations en cours instantanément. 
S'il est à `false`, les animations se terminent normalement mais toutes celles en attente sont annulées.
