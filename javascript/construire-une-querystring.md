Construire une queryString
==========================

[:type]: # "Marque-page"
[:created_at]: # "2023/03/30"
[:modified_at]: # "2023/03/30"
[:tags]: # "javascript typescript"

Voici une fonction pour construire une **queryString** à partir d’un objet.

Version **JavaScript** :

```javascript
export const buildQueryString = (parameters) => {
  const paramParts = Object.entries(parameters)
    .map(([key, value]) => `${key}=${encodeURI(value)}`);

  return paramParts.length > 0 ? `?${paramParts.join('&')}` : '';
};
```

Version **TypeScript** :

```typescript
export const buildQueryString = (parameters: { [key: string]: string }) => {
  const paramParts = Object.entries(parameters)
    .map(([key, value]) => `${key}=${encodeURI(value)}`);

  return paramParts.length > 0 ? `?${paramParts.join('&')}` : '';
};
```

**Explications** :

- Chaque propriété de l’objet devient une clé de la queryString, dont la valeur échappée.
- Les paramètres sont séparés par `&` et `?` est ajouté en préfixe de la chaîne.
