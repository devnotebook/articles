Impossible de charger les dépendances depuis https://jcenter.bintray.com
========================================================================

[:type]: # "Erreur"
[:created_at]: # "2021/09/16"
[:modified_at]: # "2021/09/16"
[:tags]: # "javascript"

Si vous construisez des APK Android avec **Cordova**, vous pouvez rencontrer ce genre d'erreur :

```
[...]
> Task :app:preReleaseBuild FAILED
FAILURE: Build failed with an exception.
* What went wrong:
Could not resolve all files for configuration ':app:releaseRuntimeClasspath'.
> Could not resolve com.android.support:support-v4:27.+.
  Required by:
      project :app
   > Failed to list versions for com.android.support:support-v4.
      > Unable to load Maven meta-data from https://jcenter.bintray.com/com/android/support/support-v4/maven-metadata.xml.
         > Could not get resource 'https://jcenter.bintray.com/com/android/support/support-v4/maven-metadata.xml'.
            > Could not GET 'https://jcenter.bintray.com/com/android/support/support-v4/maven-metadata.xml'.
               > Read timed out
[...]
```

Cela est dû à la coupure du dépôt **jcenter**.

La solution pour le remplacer est décrite dans plein de tickets sur StakOverflow (ex: [Question #67418153](https://stackoverflow.com/questions/67418153/android-studio-gradle-please-remove-usages-of-jcenter-maven-repository-from)).  
En bref : il faut modifier le fichier `build.gradle`.

Malheureusement, vous n'avez peut-être pas accès à ce fichier, si c'est une Plateforme d'Intégration Continue qui se charge du build.  
Dans ce cas, ce fichier n'est probablement pas versionné, mais générée automatiquement dans un répertoire nommé `platforms/`.

Entre chaque build, veillez à bien **supprimer ce répertoire `platforms/`**. 
Ainsi, le fichier `build.gradle` sera régénéré en utilisant des dépôts corrects.
