Placer une boîte de dialogue au niveau d'un bouton
==================================================

[:type]: # "Astuce"
[:created_at]: # "2013/11/26"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript jquery"

Par défaut, les boîtes de dialogue de jQuery UI sont centrées verticalement et horizontalement. 
Cependant, le centrage vertical n'est pas très intelligent et agit par rapport à la hauteur de l'écran, 
peu importe où on est dans la page.

Imaginons par exemple que votre écran fasse `1200px` de hauteur et votre page `2000px`. 
Placez un bouton qui ouvre la pop-up tout en bas de la page, scrollez jusqu'à lui et cliquez dessus. 
La pop-in s'ouvrira à `600px` (environ) du haut de la page, alors que vous serez tout en bas.

Voici une astuce pour la placer à hauteur du bouton :

```javascript
jQuery('#my-button').on('click', function(event) {
 
    event.preventDefault();
 
    jQuery('#my-dialog-confirm').dialog({
        "resizable": false,
        "draggable": false,
        "modal": true,
        "buttons" : {
            "Oui" : function() { 
                jQuery(this).dialog("close");
                
                // Traitement à effectuer
                // [...]
            },
            "Non" : function() {                
                jQuery(this).dialog("close");
            }
        }
    });
    
    // Positionnement de la pop-in
    var topOffset = jQuery(this).offset().top - (jQuery('#my-dialog-confirm').height() / 2);
    jQuery('#my-dialog-confirm').dialog( 'option', 'position', ['center', topOffset] );
});
```

**Explications :**

* Au clic sur le bouton, l'élément avec l'ID `my-dialog-confirm``` s'ouvre en fenêtre de dialogue de confirmation, 
avec les boutons Oui et Non.
* Après l'ouverture de la pop-in, on la positionne au centre de l'écran en largeur, 
et la centre en hauteur au niveau du bouton.
