Savoir si un case à cocher est cochée
=====================================

[:type]: # "Astuce"
[:created_at]: # "2014/02/11"
[:modified_at]: # "2017/07/18"
[:tags]: # "javascript jquery"

Si dans un formulaire vous avez une checkbox et que vous voulez savoir si elle est cochée, 
vous pouvez utiliser la fonction [is()](http://api.jquery.com/is/) de jQuery :

```javascript
jQuery('#ma_checkbox_id').is(':checked')
```

Par exemple, pour loguer un message lorsqu'on coche/décoche une checkbox :

```javascript
jQuery('#ma_checkbox_id').on('change', function() {
    
    // Si la checkbox est cochée
    if (jQuery(this).is(':checked')) {
        
        console.log('Je viens d\'être cochée !');
        
    } else {
        
        console.log('Je viens d\'être décochée !');
    }
});
```
