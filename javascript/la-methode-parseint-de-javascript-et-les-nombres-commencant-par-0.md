La méthode parseInt() de Javascript et les nombres commençant par 0
===================================================================

[:type]: # "Erreur"
[:created_at]: # "2013/09/11"
[:modified_at]: # "2017/05/15"
[:tags]: # "javascript"

Si vous utilisez la méthode `parseInt()` pour parser des chaînes de caractères représentant des nombres commençant par 0, 
vous pouvez rencontrer ce problème :

```javascript
var test;
test = parseInt("01"); // test = 1
test = parseInt("02"); // test = 2
test = parseInt("03"); // test = 3
test = parseInt("04"); // test = 4
test = parseInt("05"); // test = 5
test = parseInt("06"); // test = 6
test = parseInt("07"); // test = 7
test = parseInt("08"); // test = 0
test = parseInt("09"); // test = 0
```

Sous Internet Explorer 8 et inférieur, la méthode `parseInt()` considère les nombres commençant par 0 comme étant en **base 8**.

Il faut donc utiliser le second paramètre de la méthode, pour préciser qu'on veut travailler en **base 10** :

```javascript
var test;
test = parseInt("01", 10); // test = 1
test = parseInt("02", 10); // test = 2
test = parseInt("03", 10); // test = 3
test = parseInt("04", 10); // test = 4
test = parseInt("05", 10); // test = 5
test = parseInt("06", 10); // test = 6
test = parseInt("07", 10); // test = 7
test = parseInt("08", 10); // test = 8
test = parseInt("09", 10); // test = 9
```
