Autoriser un serveur distant à se connecter à une base MySQL
============================================================

[:type]: # "Astuce"
[:created_at]: # "2013/02/13"
[:modified_at]: # "2017/05/15"
[:tags]: # "base-de-donnees mysql"

Pour autoriser une machine distante à se connecter à votre base de données MySQL, exécutez la requête suivante :

```sql
GRANT ALL privileges ON my_db.* TO my_user@my_server IDENTIFIED BY 'my_password';
```

**Explications :**

* Dans cet exemple, l'autorisation d'accès concerne toutes les tables de la base `my_db`.
* Le mot nom d'utilisateur et le mot de passe sont ceux de l'utilisateur de la base de données à laquelle on pourra se connecter.
* Le serveur est la machine souhaitant se connecter à la base. Cela peut être son IP ou son nom.

**Remarque :**
Si `my_user` doit être super-utilisateur, vous devez ajouter `WITH GRANT OPTION` à la requête :

```sql
GRANT ALL privileges ON my_db.* TO my_user@my_server IDENTIFIED BY 'my_password' WITH GRANT OPTION;
```
