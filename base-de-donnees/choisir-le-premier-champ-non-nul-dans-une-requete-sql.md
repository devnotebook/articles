Choisir le premier champ non nul dans une requête SQL
=====================================================

[:type]: # "Astuce"
[:created_at]: # "2012/09/25"
[:modified_at]: # "2020/11/07"
[:tags]: # "base-de-donnees"

Il est arrivé que vous ayez une table avec deux champs indépendants, ne devant pas être remplis simultanément.

Par exemple pour un utilisateur : s'il est étudiant le champ **formation** est rempli et s'il travaille c'est le champ **métier**. 
Pour simplifier l'utilisation de ces données, vous voulez pouvoir récupérer un champ **activité** qui contient soit la **formation**, 
soit le **métier**.

En SQL, vous pouvez donc utiliser la fonction `COALESCE()` :

```sql
SELECT COALESCE( formation, metier ) AS activite FROM user;
```

Parmi les noms de champ en paramètres, la fonction `COALESCE()` retourne le premier non nul.
