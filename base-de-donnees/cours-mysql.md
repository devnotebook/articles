Cours MySQL
===========

[:type]: # "Marque-page"
[:created_at]: # "2012/08/21"
[:modified_at]: # "2020/11/07"
[:tags]: # "base-de-donnees mysql"

Le site **OpenClassrooms** (anciennement Site du Zéro) propose un cours très détaillé sur le **Système de Gestion de Base de Donnée MySQL** :
[https://openclassrooms.com/fr/courses/1959476-administrez-vos-bases-de-donnees-avec-mysql](https://openclassrooms.com/fr/courses/1959476-administrez-vos-bases-de-donnees-avec-mysql)
