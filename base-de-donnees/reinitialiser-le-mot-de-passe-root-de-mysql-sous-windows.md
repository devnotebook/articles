Réinitialiser le mot de passe root de MySQL sous Windows
========================================================

[:type]: # "Astuce"
[:version]: # "MySQL 5.1"
[:sources]: # "[Forum WamServer](http://forum.wampserver.com/read.php?1,49856)"
[:created_at]: # "2015/08/13"
[:modified_at]: # "2020/11/07"
[:tags]: # "base-de-donnees mysql windows"

Si jamais vous avez perdu le mot de passe root de MySQL, il est possible de le réinitialiser, 
ou d'exécuter n'importe quelle commande SQL nécessitant normalement des droits administrateur.

Pour cela :

* Rendez-vous dans le répertoire MySQL (par exemple `D:\Dev\wamp\bin\mysql\mysql5.5.24`)
* Créez un fichier SQL contenant les requêtes à effectuer. Par exemple pour réinitialiser le mot de passe root :

    ```sql
    UPDATE mysql.user SET Password = PASSWORD ('nouveau_mot_de_passe') WHERE User = 'root';
    FLUSH PRIVILEGES;
    ```

* Arrêtez le service MySQL s'il est lancé
* Ouvrez une invite de commande et déplacez-vous dans le répertoire MySQL
* Lancez la commande suivante :

    ```bash
    bin\mysqld.exe --defaults-file="D:\Dev\wamp\bin\mysql\mysql5.5.24\my.ini" --init-file="D:Dev\wamp\bin\mysql\mysql5.5.24\my_sql_script.sql" --console
    ```

**Explications :**

* Modifiez la requête SQL pour définir le mot de passe que vous souhaitez utiliser
* La commande `mysqld` doit recevoir 2 paramètres valués :
  * Le fichier de configuration de MySQL à utiliser (prendre celui déjà existant)
  * Le fichier SQL d'initialisation à lancer au démarrage (c'est lui qui contient votre fameuse requête)
