Sauvegarder/Importer une base de données MySQL
==============================================

[:type]: # "Astuce"
[:version]: # "MySQL 5+"
[:created_at]: # "2012/09/05"
[:modified_at]: # "2017/05/02"
[:tags]: # "base-de-donnees mysql fonctions-utiles"

## Sauvegarder ##

Pour créer un dump de votre base de données, utilisez la commande suivante :

```bash
mysqldump --host=localhost --port=3306 --databases database1_name database2_name --user=my_user --password=my_password > path/to/dump.sql
```

Par défaut :

* l'hôte utilisé est ```localhost```
* le port utilisé est ```3306```

Vous devez spécifier la ou les bases de données à sauvegarder, ainsi que les login et mot de passe d'un utilisateur ayant le droit de consultation de la base.

## Importer ##

La commande d'import est similaire à celle de dump :

```bash
mysql --host=localhost --port=3606 --user=my_user --password=my_password --default_character_set utf8 database_name < path/to/dump.sql
```
