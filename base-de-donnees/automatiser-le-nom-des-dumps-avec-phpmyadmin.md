Automatiser le nom des dumps avec phpMyAdmin
============================================

[:type]: # "Astuce"
[:created_at]: # "2013/02/14"
[:modified_at]: # "2020/11/07"
[:tags]: # "base-de-donnees mysql"

Si vous utilisez **phpMyAdmin** pour sauvegarder une base de données, 
vous pouvez générer automatiquement le nom du fichier sql.

Vous pouvez définir par exemple un nom de la forme `<nom_bdd>_<date>`. Pour cela, utilisez le modèle de nom suivant :

```
@DATABASE@_%Y-%m-%d
```

Pour la base `devnotebook`, vous obtiendrez par exemple `devnotebook_2013-02-14.sql`.
