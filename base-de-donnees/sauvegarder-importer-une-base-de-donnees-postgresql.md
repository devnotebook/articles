Sauvegarder/Importer une base de données PostgreSQL
===================================================

[:type]: # "Astuce"
[:version]: # "PostgreSQL9.3/9.4"
[:created_at]: # "2015/06/26"
[:modified_at]: # "2020/11/08"
[:tags]: # "base-de-donnees postgresql windows fonctions-utiles"

## Se connecter ##

Pour pouvoir exporter/importer une base de données PostgreSQL vous devrez sans doute vous connecter avec 
l'utilisateur système `postgres`.

Ex:
```bash
sudo su - postgres
```

## Sauvegarder ##

Pour créer un dump de votre base de données utilisez la commande suivante :

```bash
pg_dump -U username -h localhost dbname > path/to/dump.sql
```

Par défaut :

* l'hôte utilisé est `localhost`
* le port utilisé est `5432`

D'autres options sont possibles.

**Remarque :**

Même si c'est la valeur par défaut, il est parfois nécessaire de préciser l'hôte dans la commande, 
pour indiquer au client postgres que vous accédez à la base via une connexion **TCP** et non **PEER**.

## Importer ##

La commande d'import est similaire à celle de dump :

```bash
psql -U username -h localhost dbname < path/to/dump.sql
```

Par défaut :

* l'hôte utilisé est `localhost`
* le port utilisé est `5432`
