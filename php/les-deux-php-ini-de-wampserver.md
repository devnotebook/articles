Les deux php.ini de WampServer
==============================

[:type]: # "Marque-page"
[:created_at]: # "2013/02/07"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

**WampServer** utilise deux fichiers `php.ini` 
(adaptez les chemins en fonction du dossier racine de WampServer et des versions d'apache et PHP) :

* `D:\Chemin\vers\wamp\bin\php\php5.3.13\php.ini`
* `D:\Chemin\vers\wamp\bin\apache\apache2.2.22\bin\php.ini`

Si vous utilisez PHP **en ligne de commande**, celui-ci utilisera le premier php.ini : 
celui qui se trouve **à la racine du répertoire de PHP**.

Si vous utilisez PHP **via votre navigateur**, le second php.ini sera utilisé : 
celui qui se trouve **dans l'arborescence d'Apache**.
