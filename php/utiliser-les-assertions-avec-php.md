Utiliser les assertions avec PHP
================================

[:type]: # "Marque-page"
[:sources]: # "[php.net](http://php.net/manual/fr/function.assert.php)"
[:created_at]: # "2013/02/11"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

Il est utile pour simplifier le débogage, d'utiliser les assertions d'un langage, 
que ce soit en Java ou en PHP. 
Les assertions ne s'activent qu'en mode développement, et seront tout simplement ignorées en production 
(= pas de perte de performance).

Voici les deux cas dans lesquels utiliser les assertions :

* Pour **valider les arguments** passés à une fonction **non publique**. 
On peut ainsi éviter de tester les paramètres d'une fonction private appelée dans une boucle, par exemple.
* Pour **valider des post-conditions**. Par exemple, pour vérifier que l'objet nouvellement créé à bien un ID.

Tutoriel pour utiliser les assertions avec PHP : 
[http://openclassrooms.com/courses/les-assertions-en-php](http://openclassrooms.com/courses/les-assertions-en-php).
