Impossible de télécharger les dépendances avec Composer et Git
==============================================================

[:type]: # "Erreur"
[:created_at]: # "2013/02/07"
[:modified_at]: # "2017/08/07"
[:tags]: # "php composer"

Sous Windows, lorsque vous essayer d'installer des dépendances avec Composer (le gestionnaire de dépendances de PHP), 
vous pouvez obtenir l'erreur suivante :

```
Loading composer repositories with package information
Installing dependencies
  - Installing twig/extensions (dev-master 5c2d515)
    Cloning 5c2d515d4624bdd588226d688173cf0399a4d8cf
 
  [RuntimeException]
  Failed to execute git checkout "5c2d515d4624bdd588226d688173cf0399a4d8cf" &
  & git reset --hard "5c2d515d4624bdd588226d688173cf0399a4d8cf"
 
  fatal: Not a git repository (or any of the parent directories): .git
```

Remplacez alors la commande :

```bash
composer install
```

par :

```bash
composer update --prefer-dist
```
