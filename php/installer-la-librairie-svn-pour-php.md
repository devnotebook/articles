Installer la librairie SVN pour PHP
===================================

[:type]: # "Astuce"
[:created_at]: # "2013/06/13"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

Pour pouvoir utiliser simplement **subversion** en PHP, vous pouvez utiliser la librairie **VersionControl_SVN**.

**Remarque :**

Pour git, la librairie **VersionControl_Git** existe également et s'installe de la même façon.

## Installation ##

Exécutez juste la commande suivante :

```bash
pear install VersionControl_SVN
```

En cas d'erreur :

```
Failed to download pear/VersionControl_SVN within preferred state "stable", latest release is version 0.5.0, 
stability "alpha", use "channel://pear.php.net/VersionControl_SVN-0.5.0" to install
install failed
```

exécutez plutôt la commande suivante pour indiquer le canal d'installation à utiliser :

```bash
pear install channel://pear.php.net/VersionControl_SVN-0.5.0
```

## Classe à corriger ##

Le fichier `Diff.php` de la librairie comporte une erreur. 
Il se trouve dans le répertoire `VersionControl\SVN\Command\` du répertoire PEAR 
(par exemple : `D:\Dev\wamp\bin\php\php5.3.13\pear\VersionControl\SVN\Command\`).

Tout à la fin du fichier, remplacez `summerize` par `summarize` 
(il était temps que l'été arrive apparemment :).
