Différences entre isset(), empty() et is_null()
===============================================

[:type]: # "Astuce"
[:sources]: # "[forum.phpfrance.com](http://forum.phpfrance.com/faq-tutoriels/isset-empty-null-sont-sur-bateau-t249889.html) | [www.php.net](https://www.php.net/manual/fr/types.comparisons.php)"
[:created_at]: # "2012/05/31"
[:modified_at]: # "2017/07/18"
[:tags]: # "php"

## isset($var) ##

Teste si `$var` existe et n'a pas pour valeur `null`. 
Cette fonction ne différencie donc pas une variable nulle d'une variable qui n'existe pas, 
ce qui peut être gênant pour les tableaux :

```php
$foo = null;
$bar = 'dummy';
$array = [
    'foo' => null,
    'bar' => 'dummy'
];
var_dump(
    isset($foo),
    isset($bar),
    isset($other),
    isset($array['foo']),
    isset($array['other'])
);

// Affiche :
//   bool(false)
//   bool(true)
//   bool(false)
//   bool(false)
//   bool(false)
```

**Remarque :**

Pour faire la différence entre une clé de tableau qui existe mais dont la valeur vaut `null` et une clé qui n'existe pas, il faut donc utiliser la fonction `array_key_exists()`.

## empty($var) ##

Teste si `$var` a pour valeur `null`, `0`, `false`, est un tableau ou un attribut d'objet vide.

```php
class Foo {
     public $bar; // est strictement équivalent à : public $bar = null;.
}
 
$foo = new Foo();
$a   = '';
$b   = 0;
$c   = '0';
$d   = null;
$e   = false;
$f   = [];
$g   = $foo->bar;
 
var_dump(
    empty($a),
    empty($b),
    empty($c),
    empty($d),
    empty($e),
    empty($f),
    empty($g)
);
 
// Affiche :
//     bool(true)
//     bool(true)
//     bool(true)
//     bool(true)
//     bool(true)
//     bool(true)
//     bool(true)
```

## is_null($var) ##

Teste si `$var` a pour valeur `null` (et pas `0`, `false` et autre). 
La variable doit également être définie (sinon warning PHP).

```php
var_dump(
    is_null(null),
    is_null(0),
    is_null($other)
);

// Affiche :
//     bool(true)
//     bool(false)
//     bool(true) + Notice: Undefined variable: other in /some/where/- on line xx
```

## Récapitulatif ##

| Expression    | gettype() | empty() | is_null() | isset() | boolean : if ($x) |
| ------------- | :-------: | :-----: | :-------: | :-----: | :---------------: |
| $x = "";      | string    | TRUE    | FALSE     | TRUE    | FALSE             |
| $x = null;    | NULL      | TRUE    | TRUE      | FALSE   | FALSE             |
| var $x;       | NULL      | TRUE    | TRUE      | FALSE   | FALSE             |
| $x indéfini   | NULL      | TRUE    | TRUE      | FALSE   | FALSE             |
| $x = [];      | array     | TRUE    | FALSE     | TRUE    | FALSE             |
| $x = false;   | boolean   | TRUE    | FALSE     | TRUE    | FALSE             |
| $x = true;    | boolean   | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = 1;       | int       | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = 42;      | int       | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = 0;       | int       | TRUE    | FALSE     | TRUE    | FALSE             |
| $x = -1;      | int       | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = "1";     | string    | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = "0";     | string    | TRUE    | FALSE     | TRUE    | FALSE             |
| $x = "-1";    | string    | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = "foo";   | string    | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = "true";  | string    | FALSE   | FALSE     | TRUE    | TRUE              |
| $x = "false"; | string    | FALSE   | FALSE     | TRUE    | TRUE              |

**Remarque :**

Pour savoir si une propriété d'un objet existe, utilisez la fonction `property_exists()` et non pas `isset()`. 
(Cf. [documentation](http://php.net/manual/fr/function.property-exists.php))
