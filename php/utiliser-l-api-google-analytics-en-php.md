Utiliser l'API Google Analytics en PHP
======================================

[:type]: # "Astuce"
[:sources]: # "[github.com](https://github.com/erebusnz/gapi-google-analytics-php-interface)"
[:created_at]: # "2013/04/23"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

Pour pouvoir utiliser les données de Google Analytics directement en PHP, 
il existe une petite api pour communiquer simplement avec Google : 
[GAPI](https://github.com/erebusnz/gapi-google-analytics-php-interface).

En deux lignes de code vous pouvez par exemple récupérer le nombre de consultations par page ces 30 derniers jours :

```php
$ga = new gapi('address@server.com', 'my_password');
 
// Récupération du nombre de visualisations, 
// pour chaque couple (url de la page, titre de la page)
$ga->requestReportData('71538601', ['pagePath', 'pageTitle'], ['pageviews']);
```
