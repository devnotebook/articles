Créer un arbre à partir d'un tableau d'objets
=============================================

[:type]: # "Astuce"
[:created_at]: # "2013/10/22"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

Il arrive qu'on récupère un tableau d'objets tous liés entre eux, alors qu'on voudrait un arbre.

Imaginons par exemple que le tableau contienne des objets `Node`, 
ayant entre autres les attributs `id` et `parentNodeId`. 
La structure qu'on souhaite ressemblerait à :

```
Array (
    [0] => stdClass Object (
 
        [node] => Node object (
            [...]
        )
        [children] => Array (
            [0] => stdClass Object (
 
                [node] => Node object  (
                    [...]
                )
                [children] => Array (
                    [0] => stdClass Object (
                        [node] => Node object (
                            [...]
                        )
                        [children] => Array()
                    )
                )
            )
        )
    )
    [1] => stdClass Object (
 
        [node] => Node object (
            [...]
        )
        [children] => Array()
    )
)
```

Voici la fonction qui permet de passer du tableau à l'arbre :

```php
/**
 * Construit un arbre à partir du tableau de nœuds en argument.
 *
 * @param array $node_list Tableau de nœuds
 * @return array Tableau d'objets avec deux attributs :
 *      - node : Nœud
 *      - children : Tableau d'objets de ce type
 */
public static function buildTreeNode(array $node_list) {
 
    $tree = null;
 
    $children = [];
 
    // Création d'une structure exploitable
    $node_std_object_list = [];
    foreach ($node_list as $node) {
        $node_std_object_list[] = (object) [
            'id' => $node->id,
            'parentNodeId' => $node->parentNodeId,
            'node' => $node,
            'children' => []
        ];
    }
 
    // Mémorisation des liens entre les nœuds
    foreach ($node_std_object_list as $node_std) {
        $children[$node_std->parentNodeId][] = $node_std;
    }
 
    // Création de l'arbre
    foreach ($node_std_object_list as $node_std) {
 
        if (isset($children[$node_std->id])) {
            $node_std->children = $children[$node_std->id];
        }
 
        unset($node_std->id);
        unset($node_std->parentNodeId);
    }
 
    if (!empty($children)) {
        $tree = array_shift($children);
    }
 
    return $tree;
}
```

**Explications :**

* La première boucle crée un tableau d'objets standards ayant chacun 4 attributs : 
l'id d'un nœud, celui de son parent, le nœud en question et la liste des nœuds enfants (vide pour l'instant).
* La deuxième boucle remplit l'attribut `children` de tous ces objets standards. 
On a alors la liste de tous les nœud avec pour chacun d'eux ses enfants.
* La dernière boucle "accroche" les nœuds les uns aux autres pour former l'arbre.

**Remarques :**

* A l'instar de la classe `Object` en java, il existe la classe `stdClass` (Standard Class) pour 
représenter des objets.
* Il est possible de caster un tableau en objet `stdClass` et vice-versa.
