La taille des fichiers uploadés via PHP
=======================================

[:type]: # "Astuce"
[:created_at]: # "2013/05/27"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

La taille des fichiers uploadés via PHP est limitée. 
Par défaut PHP fixe cette limite à `2Mo`, mais cela est bien sûr configurable.

Pour cela, éditez le fichier `php.ini` et modifiez la propriété `upload_max_filesize` avec la 
taille que vous souhaitez (ex: 16M).

Deux autres propriétés peuvent brider la taille maximale :

* `post_max_size` : Nombre d'octets transmissibles via une requête POST 
(si vous voulez uploader un fichier via un formulaire).
* `memory_limit` : Nombre d'octets que PHP peut stocker en mémoire.

Ces deux propriétés doivent être supérieures ou égales à `upload_max_filesize`.

On est souvent confronté à cette limite lorsqu'on utilise PHPMyAdmin pour uploader un dump SQL.
Par exemple, si vous avez `post_max_size: 3M` et `upload_max_filesize: 16M`, vous ne pourrez pas
uploadé un fichier de plus de 3Mo.
