Générateurs DPE/GES
===================

[:type]: # "Astuce"
[:created_at]: # "2015/07/31"
[:modified_at]: # "2017/08/09"
[:tags]: # "php"

Si vous créez un site proposant des logements, vous aurez sans doute besoin d'afficher 
leurs **Diagnostics de Performance Énergétique** (DPE) et leurs **Émissions de Gaz à effet de Serre** (GES) :

![Générateurs DPE/GES](./generateurs-dpe-ges-01.png)

Si vous avez déjà une image ou un document PDF représentant ces graphiques pas de problème. 
Sinon, voici [une petite API PHP](https://framagit.org/devnotebook/articles/-/blob/master/_files/dpeges.zip) 
pour les générer en HTML/CSS.

L'archive contient une classe PHP, un fichier CSS, un répertoire d'images et un fichier `index.php`.

Cette API est inspirée de celle proposée sur ce site : 
[http://dpe-ges.c-prod.com/telechargement/](http://dpe-ges.c-prod.com/telechargement/).
