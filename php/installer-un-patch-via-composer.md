Installer un patch via composer
===============================

[:type]: # "Astuce"
[:created_at]: # "2017/11/28"
[:modified_at]: # "2017/11/28"
[:tags]: # "php composer"

Lorsqu'il y a un bug dans l'une de vos dépendances gérées par composer, 
il faut éviter de le corriger en modifiant directement la lib. 
Sinon, la correction sera automatiquement écrasée lors de la prochaine installation/mise à jour de la lib.

Si un patch est proposé sur le site du fournisseur de la lib, 
mais pas encore intégré dans leur dernière release, vous pouvez demander à Composer de l'appliquer lors 
de l'installation des dépendances (i.e. `composer install`).

La lib [composer-patches](https://github.com/cweagans/composer-patches) pour Composer, permet cette fonctionnalité.
Pour l'utiliser, ajoutez-la dans votre `composer.json`. Par exemple :

```json
{
  // ...
  "require": {
    // ...
    "cweagans/composer-patches": "~1.0",
    "drupal/core": "~8.4.0"
  },
  // ...
  "extra": {
    // ...
    "patches": {
      "drupal/core": {
        "Quickedit missing css classes": "https://www.drupal.org/files/issues/2551373-35.patch"
      }
    }
  }
}
```

**Explications :**

* Le projet requiert la lib `composer-patches` comme dépendance.
* On a ausssi intégré `drupal/core`, que l'ont veut patcher.
* Après avoir téléchargé et installé cette dépendance, Composer va appliquer le patch présent à l'URL
https://www.drupal.org/files/issues/2551373-35.patch.

**Remarque :**

On peut appliquer plusieurs patchs successifs pour une ou plusieurs lib. Ex :

```json
{
  // ...
  "require": {
    // ...
    "cweagans/composer-patches": "~1.0",
    "vendor1/lib1": "~1.0",
    "vendor1/lib2": "~1.0",
    "vendor2/lib3": "~1.0"
  },
  // ...
  "extra": {
    // ...
    "patches": {
      "vendor1/lib1": {
        "Patch 1": "https://www.vendor1.org/lib1/patch1.patch",
        "Patch 2": "https://www.vendor1.org/lib1/patch2.patch"
      },
      "vendor2/lib3": {
        "Patch 3": "https://www.vendor2.org/lib3/patch.patch"
      }
    }
  }
}
```
