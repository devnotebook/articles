Impossible de générer l'autoloads pour les dépendances de Symfony avec Composer
===============================================================================

[:type]: # "Erreur"
[:created_at]: # "2013/02/07"
[:modified_at]: # "2017/08/07"
[:tags]: # "php composer"

Sous Windows, lorsque vous essayez d'installer les dépendances de Symfony avec Composer, vous pouvez obtenir 
l'erreur suivante :

```
Loading composer repositories with package information
Updating dependencies
Nothing to install or update
Generating autoload files
 
Script Sensio\Bundle\DistributionBundle\Composer\ScriptHandler::buildBootstrap h
andling the post-update-cmd event terminated with an exception
 
  [RuntimeException]
  An error occurred when generating the bootstrap file.
```
  
Exécutez alors les commandes suivantes :

```bash
composer update --no-scripts
php vendor/sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/bin/build_bootstrap.php
```
