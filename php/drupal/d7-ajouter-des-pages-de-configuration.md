[D7] Ajouter des pages de configuration
=======================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2015/08/10"
[:modified_at]: # "2018/02/01"  
[:tags]: # "drupal php"

Pour gérer certains paramètres propres à votre site, vous utilisez souvent les fonctions 
`variable_get()` et `variable_set()` de Drupal.

Drupal fournit une API pour pouvoir très rapidement créer un formulaire d'édition pour ces variables :

![Menu - page de configuration en BO](./d7-ajouter-des-pages-de-configuration-01.png)
![Page de configuration en BO](./d7-ajouter-des-pages-de-configuration-02.png)

Voici les différentes étapes pour ajouter une page de configuration.

## hook_menu() ##

Pour que Drupal référence vos nouvelles pages, déclarez-les dans un `hook_menu()` :

```php
// mon_module.module
 
/**
 * Implements hook_menu().
 */
function my_module_menu()
{
  // Page racine "Mon site"
  $items['admin/config/mon_site'] = array(
    'title' => t('My site'),
    'description' => t('Administration of the site.'),
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer mysite'),
    'position' => 'right',
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
 
  // Page "Général"
  $items['admin/config/mon_site/general'] = array(
    'title' => t('General'),
    'description' => t('Adjust global settings.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('my_module_admin_form_site_general'),
    'access arguments' => array('administer mysite'),
    'file' => 'my_module.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );
 
  // Page "Webservices"
  $items['admin/config/mon_site/webservices'] = array(
    'title' => t('General'),
    'description' => t('Adjust global settings.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('my_module_admin_form_site_ws'),
    'access arguments' => array('administer mysite'),
    'file' => 'my_module.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );
 
  return $items;
}
```

**Explications :**

* La première déclaration permet définir le premier élément de menu (`Mon site`).
* Les deux suivantes définissent les sous-éléments de ce menu (`Général` et `Webservices`).
* Les deux formulaires de configuration seront définis respectivement dans les fonctions 
de callback `my_module_admin_form_site_general()` et `my_module_admin_form_site_ws()`.
* Ces fonctions seront recherchées dans le fichier `my_module.pages.inc`.
* Pour accéder à ces page, il faudra avoir la permission `administer mysite`.

## Form callback ##

```php
// my_module.page.inc
 
/**
 * Form callback: administration variables
 */
function my_module_admin_form_site_general() {
 
  // Création d'un fieldset
  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
  );
 
  // Ajout d'un champ texte dans ce fieldset
  $form['search']['my_module_nb_results_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of results per page'),
    '#default_value' => variable_get('my_module_nb_results_per_page'),
    '#description' => t('Some description'),
  );
 
  // Ajout d'une liste déroulante dans ce fieldset
  $options = array(
    'value1' => t('Label 1'),
    'value2' => t('Label 2'),
  );
  $form['search']['my_module_first_result'] = array(
    '#type' => 'select',
    '#title' => t('First result'),
    '#options' => $options,
    '#default_value' => variable_get('my_module_first_result', 0),
    '#description' => t('Some description'),
  );
 
  return system_settings_form($form);
}
```

**Explications :**

* Le `fieldset` correspond à celui HTML, il permet de regrouper des champs.
* L'imbrication d'un champ dans un fieldset est reproduite dans le tableau php `$form`.
* L'enregistrement du formulaire est géré automatiquement par Drupal.
* Vous gérez seulement la valeur par défaut des champs 
(= valeur présente dans la variable correspondante, récupérée via `variable_get()`).

**Remarque :**

Le formulaire décrit dans cette fonction fonctionne exactement comme tout autre formulaire Drupal. 
Vous pouvez y ajouter des règles de validation de champs, ...
