Impossible d'importer ou de télécharger les traductions de l'interface
======================================================================

[:type]: # "Erreur"
[:sources]: # "<https://www.drupal.org/node/2503925>"
[:version]: # "Drupal 7/8"
[:created_at]: # "2016/06/21"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Après l'installation d'un site chez un hébergeur, un problème peut survenir lors de l'import/du téléchargement 
des fichiers de traduction de l'interface :

```
Warning: move_uploaded_file(translations://fr.po): failed to open stream: "Drupal\locale\StreamWrapper\TranslationsStream::stream_open" call failed in Drupal\Core\File\FileSystem->moveUploadedFile() (line 79 of core/lib/Drupal/Core/File/FileSystem.php).
 
Drupal\Core\File\FileSystem->moveUploadedFile('/tmp/phpxTna7m', 'translations://fr.po') (Line: 856)[...]
```

La solution consiste à vérifier les droits sur le répertoire `sites/default/files/translations`. 
S'il n'existe pas, créez-le et donner le droit d'écriture pour l'utilisateur apache.
