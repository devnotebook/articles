[D7] Effectuer des requêtes sur une autre base de données
=========================================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2014/09/18"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Il est possible d'effectuer des requêtes SQL sur une base de données autre que celle de Drupal, 
tout en utilisant les fonctions `db_select()`, `db_query()`, ....

Pour cela, il faut déclarer la ou les bases externes dans le fichier `site/default/settings.php` :

```php
$databases = array (
  'default' => array (
    'default' => array (
      'database' => 'drupal',
      'username' => 'username',
      'password' => 'password',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
  'ma_nouvelle_base' => array (
    'default' => array (
      'database' => 'db1',
      'username' => 'username2',
      'password' => 'password2',
      'host' => 'db.example.com',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);
```

Vous pouvez maintenant utiliser la nouvelle base dans vos modules, grâce à la fonction `db_set_active()` :

```php
// Sélection de la nouvelle base
db_set_active('ma_nouvelle_base');
 
// Exécution d'un requête
$results = db_query($sql);
 
// Retour à la base par défaut
db_set_active('default');
```
