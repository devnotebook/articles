[D8] Commandes drush utiles
===========================

[:type]: # "Astuce"
[:sources]: # "<https://drushcommands.com/>"
[:version]: # "Drupal 8, Druahs 8"
[:created_at]: # "2016/07/06"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Les [commandes drush pour Drupal 8](https://drushcommands.com/) sont en partie identiques à celles pour Drupal 7.
En fait, il s'agit surtout de la version de **drush** et non pas de celle de Drupal. 
Pour Drupal 8, il est conseillé d'utiliser la version **8.x** de drush.

Voici une liste de commandes drush bien pratiques :

## Features ##

| Fonction                                       | Commande  |
|------------------------------------------------|-----------|
| Exporter un nouveau composant dans une feature | drush cex |
| Importer les configurations du site            | drush cim |

## Modules ##

| Fonction                                               | Commande                                                      |
|--------------------------------------------------------|---------------------------------------------------------------|
| Information sur un module                              | drush pmi nom_module                                          |
| Télécharger un module                                  | drush dl nom_module                                           |
| Activer un module                                      | drush en nom_module                                           |
| Désinstaller un module                                 | drush pmu nom_module                                          |
| Mettre à jour les tables en base concernant un module  | drush updb nom_module                                         |
| Liste des modules communautaires activés               | drush pm-list --pipe --type=module --status=enabled --no-core |
| Liste des modules du cœur activés                      | drush pm-list --pipe --type=module --status=enabled --core    |

## Base de données ##

| Fonction                                               | Commande                                             |
|--------------------------------------------------------|------------------------------------------------------|
| Exécuter une commande SQL                              | drush sqlc "SELECT * FROM node;"                     |
| Créer un dump                                          | drush sql-dump > /chemin/fichier.sql                 |
| Vider une base de données                              | drush sql-drop                                       |
| Importer un dump                                       | drush sql-cli < /chemin/fichier.sql                  |
| Mettre à jour les tables en base pour tous les modules | drush updb (utile après une mise à jour de sécurité) |
| Mettre à jour entités en base                          | drush entup (utile après une mise à jour de module)  |

## Autres ##

| Fonction                                  | Commande                                                       |
|-------------------------------------------|----------------------------------------------------------------|
| Vider tous les caches                     | drush cr                                                       |
| Modifier le mot de passe d'un utilisateur | drush upwd --password="nouveau_mot_de_passe" login_utilisateur |
| Exécuter une tâche planifiée              | drush php-eval 'monmodule_cron();'                             |
| Exécuter du code PHP                      | drush php-eval 'echo "je suis du code php exécuté";'           |
| Connaître la version de Drupal            | drush status                                                   |
