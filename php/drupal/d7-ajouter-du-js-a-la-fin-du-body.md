[D7] Ajouter du JS à la fin du body
===================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2015/08/10"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Par défaut, Drupal rend la variable `$scripts` disponible dans votre `html.tpl.php`. 
Elle contient toutes les lib javascript ainsi que tous le code javascript inline 
que vous avez ajouté à Drupal via :

* le fichier `.info` de votre thème (`mon_theme.info`)
* la fonction `drupal_add_js()`

Vous avez donc quelque chose comme ça au début de votre fichier `html.tpl.php` :

```php
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title><?php print $head_title; ?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php print $head; ?>
</head>
```

Si vous déplacez `<?php print $scripts; ?>` tout à la fin du body, 
vous aurez sûrement des problèmes, car certains modules vont vouloir 
utiliser des lib javascript en amont (par exemple jQuery).

Un solution consiste à laissez `$scripts` où il se trouve et 
à créer une nouvelle variable `$footer_scripts`. 
Elle sera ajoutée à la fin du body et contiendra tout le javascript "non natif" dont 
aurez besoin pour votre site :

```php
  <?php if (isset($footer_scripts)) { print $footer_scripts; } ?>
</body>
</html>
```

Voici les différentes étapes à suivre pour pouvoir utiliser cette variable.

## Ajout de la variable au template html.tpl.php ##

Pour cela, utilisez le `hook_process_html()` :

```php
// mon_module.module
 
/**
 * Implements hook_process_html().
 */
function mon_module_process_html(&$variables) {
 
  // Ajout des scripts JS à mettre en pied de page dans la variable $footer_scripts
  $variables['footer_scripts'] = drupal_get_js('footer');
}
```

**Explication :**

La variable `$footer_scripts` aura pour valeur le code HTML permettant d'inclure tout le code JS 
dont le scope est `footer`.

## Ajout de javascript avec le scope footer ##

Pour cela, utilisez le `hook_preprocess_page()` :

```php
// mon_theme/template.php
 
/**
 * Implements hook_preprocess_page().
 */
function mon_theme_preprocess_page(&$vars) {
    
  // Ajout de code javascript inline
  $js = 'alert("Ceci est du code Javascript.");';
  drupal_add_js($js, array('type' => 'inline', 'scope' => 'footer'));

  // Exemple d'ajout de code javascript externe
  drupal_add_js(
    'http://maps.googleapis.com/maps/api/js?v=3', 
    array('type' => 'external', 'scope' => 'footer')
  );
}
```
