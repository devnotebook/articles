[D7] Créer un bloc
==================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2014/07/18"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Pour créer un bloc programmatiquement, vous devez déjà avoir [créé un module](./drupal/d7-creer-un-module).

Deux hooks vont être nécessaires dans le fichier `mymodule.module`. 
L'un pour **déclarer votre bloc**, l'autre pour **définir son contenu**.

## hook_block_info() ##

```php
/**
 * Implements hook_block_info();
 */
function mymodule_block_info() {
 
  $blocks['myblock'] = array(
    'info' => t('My block'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
 
  $blocks['myotherblock'] = array(
    'info' => t('My other block'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
 
  return $blocks;
}
```

**Explications :**

* Ce hook retourne la liste des blocs à définir, avec pour chacun d'eux, son **nom**, 
le **type de cache** à utiliser, ... 
(Cf. [Documentation](https://api.drupal.org/api/drupal/modules!block!block.api.php/function/hook_block_info/7))
* Vous pouvez ajouter autant de blocs que vous le souhaitez dans le tableau de résultats.

## hook_block_view() ##

```php
/**
 * Implements hook_block_view();
 */
function mymodule_block_view($delta = '') {
 
  switch ($delta) {
 
    case 'myblock' :
      $block['subject'] = t('My block');
      $block['content'] = '<p>Contenu de mon bloc.</p>';
      break;
 
    case 'myotherblock' :
      $block['content'] = 'Contenu de mon second bloc.';
      break;
  }
 
  return $block;
}
```

**Explications :**

* Ce hook reçoit le nom d'un bloc en argument et retourne son contenu sous forme de tableau de rendu. 
(Cf. [Documentation](https://api.drupal.org/api/drupal/modules!block!block.api.php/function/hook_block_view/7))
* Le tableau retourné doit au moins contenir la clé `content`, avec du texte simple ou du html en valeur.
* Souvent, chaque case effectue une ou plusieurs requêtes en base pour récupérer des données, 
puis prépare le texte à afficher.

**Remarque :**

Si vous affichez des nœuds dans votre bloc, vous pourrez appeler directement la fonction `node_view()` 
pour générer leur contenu html. Ex :

```php
$content = '<h2>' . t('Last published') . '</h2>';
$content .= '<ul>';
foreach ($node_list as $node) {
 
  $content .= '<li>';
  $content .= node_view($node, 'list');
  $content .= '</li>';
}
$content .= '</ul>'; 
 
$block['content'] = $content;
```
