[D8] Installer la dernière version de drush
===========================================

[:type]: # "Astuce"
[:version]: # "Drupal 8"
[:created_at]: # "2016/11/22"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Sur les dépôts des distributions linux, c'est souvent une vieille version de drush 
qui est disponible (ex: Debian 8.4 -> drush 5.x). Voici comment installer la dernière.

## Prérequis ##

**Composer** et **Git** doivent être installés.

### Composer ###

```bash
sudo apt-get install curl
sudo curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Vérification
composer --version
```

### GIT ###

```bash
sudo apt-get install git
 
# Vérification
git --version
```

## Installation ##

* Téléchargez drush :

```bash
sudo git clone --depth 1 https://github.com/drush-ops/drush.git /usr/local/src/drush
```

* Mettez-le à jour :

```bash
cd /usr/local/src/drush
sudo composer install
```

* Créez les liens symboliques suivant :

```bash
sudo ln -s /usr/local/src/drush/drush /usr/local/bin/drush
sudo ln -s /usr/local/src/drush/drush.complete.sh /etc/bash_completion.d/drush
```

* Vérifiez l'installation :

```bash
drush --version
```
