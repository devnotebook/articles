Drupal dans un sous-répertoire
==============================

[:type]: # "Astuce"
[:version]: # "Drupal 7/8"
[:created_at]: # "2017/06/04"
[:modified_at]: # "2017/11/20"
[:tags]: # "drupal php"

Si vous utilisez Drupal dans un sous-répertoire servi par Apache (ex : `/var/www/html/mon_site`), 
ou derrière un reverse proxy qui ajoute un contexte à l'URL (ex : `http://mon-domaine.fr/mon-contexte`), 
il est possible que seule la page d'accueil fonctionne.

Vous aurez alors une erreur 404 sur toutes les autres pages (ex : `http://mon-domaine.fr/mon-contexte/user/login`).

Il vous faut alors modifier le `.htaccess` à la racine du site, en modifiant la ligne :

```apacheconfig
# RewriteBase /
```

Décommentez-la et remplacer le `/` par le contexte ou le sous-répertoire. Ex :

```apacheconfig
# Modify the RewriteBase if you are using Drupal in a subdirectory or in a
# VirtualDocumentRoot and the rewrite rules are not working properly.
# For example if your site is at http://example.com/drupal uncomment and
# modify the following line:
# RewriteBase /drupal
#
# If your site is running in a VirtualDocumentRoot at http://example.com/,
# uncomment the following line:
RewriteBase /mon-contexte
```
