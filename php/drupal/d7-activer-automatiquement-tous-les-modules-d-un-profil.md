[D7] Activer automatiquement tous les modules d'un profil
=========================================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2014/09/18"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Si vous utiliser **Drush** dans Drupal, vous pouvez activer automatiquement tous les modules déclarés 
en dépendance de votre profil.

Pour cela utilisez cette commande :

```bash
drush en $(grep dependencies /path/to/my-site/profiles/my_profile/my_profile.info | sed -n 's/dependencies\[\]=\(.*\)/\1/p')
```
