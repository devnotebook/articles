[D7] Rediriger après la connexion
=================================

[:type]: # "Astuce"
[:sources]: # "[api.drupal.org](https://api.drupal.org/api/drupal/modules!user!user.api.php/function/hook_user_login/7)"
[:version]: # "Drupal 7"
[:created_at]: # "2014/06/17"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Pour rediriger l'utilisateur après sa connexion, on peut utiliser le `hook_user_login()` :

```php
/**
 * Implements hook_user_login().
 */
function my_module_user_login(&$edit, $account) {
 
    if (!isset($_POST['form_id']) || $_POST['form_id'] != 'user_pass_reset') {
 
        if (in_array('authenticated user', $account->roles)) {
 
            // Modification de l'url de destination
            $_GET['destination'] = 'admin/workbench/content/all';
        }
    }
}
```

L'exemple ci-dessus redirige l'utilisateur vers la page de gestion des contenus 
du module **workbench** (`admin/workbench/content/all`).
