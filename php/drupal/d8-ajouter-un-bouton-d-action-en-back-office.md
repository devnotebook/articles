[D8] Ajouter un bouton d'action en back-office
==============================================

[:type]: # "Astuce"
[:version]: # "Drupal 8"
[:created_at]: # "2017/07/20"
[:modified_at]: # "2017/11/20"
[:tags]: # "drupal php"

Lorsque vous êtes sur la page qui liste les types de contenu par exemple, 
il y a le bouton **Ajouter un type de contenu** en haut à gauche.

Plus généralement, lorsqu'on liste des éléments (nœuds, liens de menu, ...), 
on propose souvent un lien pour en ajouter de nouveaux.

Si vous utilisez les vues Drupal pour ajouter des pages de liste en back-office, 
vous vous voudrez probablement ce genre de bouton. Cela ne se fait pas dans la configuration de la vue, 
mais via un fichier `my_module.links.action.yml`. Par exemple :

```yaml
# Article
node.add.article
  route_name: node.add
  route_parameters:
    node_type: article
  title: 'Ajouter un Temps Fort'
  appears_on:
    - view.my_view_article.display_1
    - view.other_view.display_name
```

**Explication :**

On indique

* la route (et ses paramètres) vers laquelle devra pointer le bouton d'action
* le libellé du bouton (title)
* sur quelle(s) page(s) il devra apparaître (= ids de routes)

**Remarque :**

Pour une vue, l'id de la route est composée du préfixe 'view', du nom technique de la vue, 
puis du nom technique de l'affichage de la vue (une vue pouvant avoir plusieurs affichages).

Ces deux ID techniques sont visibles dans l'URL lorsque vous éditez un affichage d'une vue. 
Ex : http://www.mysite.fr/admin/structure/views/view/`my_view_article`/edit/`display_1`.
