[D8] Theming : définir une nouvelle apparence pour un élément
=============================================================

[:type]: # "Astuce"
[:sources]: # "[api.drupal.org](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/group/theme_render)"
[:version]: # "Drupal 8"
[:created_at]: # "2016/06/24"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

La plupart du temps dans Drupal, on définit la manière d'afficher des éléments via un 
tableau de theming côté PHP.

Par exemple, pour un bloc, on peut avoir un tableau du genre :

```php
$build = array(
  '#cache' => array(
    'contexts' => array('user'),
    'max-age' => Cache::PERMANENT,
  ),
  '#markup' => '<p>Hello ' . $who . '</p>',
);
```

Dans cet exemple, on ne précise pas l'habillage à utiliser. 
Drupal sélectionnera donc un template par défaut en fonction du type de l'élément 
(`block.html.twig` dans le cas d'un bloc).

## Habillage à utiliser ##

Pour utiliser votre propre template, il faut modifier le tableau et remplacer `#markup` par `#theme` :

```php
$build = array(
  '#cache' => array(
    'contexts' => array('user'),
    'max-age' => Cache::PERMANENT,
  ),
  '#theme' => 'my_hello',
  '#who' => $who,
);
```

## Déclaration de l'habillage ##

Pour que Drupal trouve votre habillage, vous devez implémenter le 
[hook_theme()](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/function/hook_theme) 
dans votre module.


```php
// my_module.module

/**
 * Implements hook_theme().
 */
function my_module_theme() {
  return [
    'my_hello' => [
      'template' => 'my_hello_box',
      'variables' => [
        'who' => 'World',
      ],
    ]
  ];
}
```

**Explications :**

* La fonction définit un nouvel habillage `my_hello`.
* Le template à utiliser est `my_hello_box.html.twig`.
* La variable `who` sera transmise au template, avec la valeur `'World'` par défaut.

## Template ##

Le template `my_hello_box.html.twig` placé dans le répertoire `templates/` de votre module 
peut ressembler à ça :

```twig
{# Affichage d'un message Hello sous forme de boîte #}
<div class="box">
    <p>{{ 'Hello %who !'|t({ 'who': who }) }}</p>
</div>
```

**Remarque :**

Vous pouvez placer votre template dans n'importe quel sous répertoire de `templates/`. 
Drupal saura le trouver.
