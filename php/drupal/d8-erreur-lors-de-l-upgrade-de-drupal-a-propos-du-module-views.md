[D8] Erreur lors de l'upgrade de Drupal à propos du module Views
================================================================

[:type]: # "Erreur"
[:version]: # "Drupal 8"
[:created_at]: # "2017/12/11"
[:modified_at]: # "2018/02/01"  
[:tags]: # "drupal php"

Lors de la mise à jour de Drupal (par exemple de 8.3.7 vers 8.4.3), vous pouvez rencontrer ce genre d'erreur :

```
$ drush updb -y
The following updates are pending:

views module : 
  Fix table names for revision metadata fields.

Do you wish to run all pending updates? (y/n): y
Après la mise à jour de views                                                    [ok]
Failed: InvalidArgumentException : The configuration property                      [error]
display.default.display_options.filters.my_custom_filter_id.value.2 doesn&#039;t
exist. dans Drupal\Core\Config\Schema\ArrayElement-&gt;get() (ligne 76 de
/var/www/ftvpro/web/core/lib/Drupal/Core/Config/Schema/ArrayElement.php).
Cache rebuild complete.                                                            [ok]
Finished performing updates.
```

Le module **Views** est incapable de mettre à jour la base de données correctement, 
à cause d'un **filtre de vue**.

Pour éviter ça, cherchez dans vos modules (ou ceux communautaires que vous utilisez) le filtre de vue en question.
(Ici `my_custom_filter`.)
Le module qui déclare ce filtre doit également proposer une mise à jour de la base via un fichier yaml.
Pour cet exemple, le module `my_module` doit contenir le fichier 
`config/schema/my_module.views.schema.yml` :

```yaml
# Schema for the views plugins of the my_module module.

views.filter.my_custom_filter:
  type: views.filter.in_operator
  label: 'My custom view filter'
```

Si ce n'est pas le cas, crééz-le et relancer la commande `drush updb`.

**Remarque** :
Si on regarde le fichier `comment.views.schema.yml` que propose le module **Comment**, 
on y trouve aussi ce genre de déclarations :

```yaml
views.argument.argument_comment_user_uid:
  type: views_argument
  label: 'Commented user ID'

views.field.comment_depth:
  type: views_field
  label: 'Comment depth'
  
views.row.comment_rss:
  type: views_row
  label: 'Comment'
  mapping:
    view_mode:
      type: string
      label: 'Display type'

views.sort.comment_ces_last_comment_name:
  type: views_sort
  label: 'Last comment name'
```
Le problème peut donc sûrement se produire pour les **arguments**, 
les **champs**, les **lignes** et les **tris** des vues déclarés dans les modules.
