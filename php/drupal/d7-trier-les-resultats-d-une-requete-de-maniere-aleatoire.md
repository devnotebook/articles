[D7] Trier les résultats d'une requête de manière aléatoire
===========================================================

[:type]: # "Astuce"
[:sources]: # "[api.drupal.org](https://api.drupal.org/api/drupal/includes%21database%21select.inc/function/SelectQuery%3A%3AorderRandom/7.x)"
[:version]: # "Drupal 7"
[:created_at]: # "2014/06/17"
[:modified_at]: # "2018/02/01"  
[:tags]: # "drupal php"

La fonction `orderRandom()` permet de trier les résultats d'une requête de manière aléatoire :

```php
db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('status', 1)
    ->range(0, 10)
    ->orderRandom();
```

La requête ci-dessus retourne 10 nœuds aléatoires, à l'état publié.
