[D7] Ajouter des variables au template node.tpl.php
===================================================

[:type]: # "Astuce"
[:sources]: # "[Documentation hook_node_view()](https://api.drupal.org/api/drupal/modules!node!node.api.php/function/hook_node_view/7)"
[:version]: # "Drupal 7"
[:created_at]: # "2014/07/18"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Lorsque vous affichez un nœud dans le template `node.tpl.php` ou une de ses surcharges 
(ex: `node--article.tpl.php`), vous avez souvent besoin d'effectuer des traitements particuliers.

Pour séparer la partie traitement de l'affichage, il est préférable de mettre le maximum de code 
PHP dans votre fichier `mymodule.module` (ou mieux, dans d'autres fichiers PHP à vous). 
Pour cela, utilisez le `hook_node_view()`.

Par exemple si dans le template d'un article on veut afficher les 3 derniers articles publiés :

```php
/**
 * Implements hook_node_view().
 */
function mymodule_node_view($node, $view_mode, $langcode) {
 
  global $language;
  if ($node->type === 'article) {
 
    // Last published articles
    $query = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 1)
      ->condition('bundle', 'article')
      ->orderBy('changed', 'DESC')
      ->range(0, 3);
    $nids = $query->execute()->fetchCol();
    $nodes = !empty($nids) ? node_load_multiple($nids) : array();
    
    $node->content['last_published_articles'] = $nodes;
  }
}
```

**Explications :**

* On vérfie le type de nœud, pour n'effectuer le traitement que pour les articles
* La requête récupère les nids des nœuds recherchés. Ils sont ensuite chargés.
* Les nœuds sont envoyés en paramètre au template. 
La variable `$content['last_published_articles']` sera disponible dans le template.

**Remarque :**

* En général on ajoute une condition sur le **type d’affichage** (Contenu complet, Accroche, ...) disponible via 
la variable `$view_mode`, pour éviter d'effectuer le traitement là où c'est inutile.
