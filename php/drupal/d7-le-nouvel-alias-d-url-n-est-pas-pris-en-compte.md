[D7] Le nouvel alias d'URL n'est pas pris en compte
===================================================

[:type]: # "Erreur"
[:version]: # "Drupal 7 (> 7.20)"
[:created_at]: # "2015/08/10"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Lorsque vous utilisez les alias d'URL de Drupal et `pathauto` pour les générer automatiquement, 
il vous arrive peut-être ce problème.

* Vous créez un contenu et vous laissez `pathauto` générer son alias URL par défaut.
* Ensuite, vous modifiez le contenu et saisissez un alias personnalisé.
* Et pourtant, le contenu a toujours l'URL automatique.

Le problème se produit probablement lorsque vous avez un site multilingue, 
que vous modifiez le pattern de génération de l'URL alors que vous avez déjà des contenus existants, 
et que vous utiliser l'option **Créer un nouvel alias et conserver l'alias existant**.

Une solution semble de modifier la fonction `path_load()` du fichier `path.inc` du cœur de Drupal.

Ajoutez cette ligne à la requête récupérant l'alias d'URL :

```php
->orderBy('pid', 'DESC')
```

De cette manière, vous êtes sûr que Drupal choisira le dernier alias généré (= alias personnalisé) et 
non pas l'ancien (automatique).

**Remarque :**

Je n'ai pas réussi à isoler le problème sur une installation vierge, avec un minimum de modules. 
Je l'ai reproduit sur deux sites très similaires assez importants. 
Dans les deux cas, l'ajout de cette ligne à solutionner le problème.
