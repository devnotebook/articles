[D7] Afficher le numéro de version du site en back-office
=========================================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2015/07/29"
[:modified_at]: # "2018/02/01" 
[:tags]: # "drupal php"

Il est utile de numéroter les différentes versions d'un site web, 
particulièrement quand il doit être déployé sur plusieurs environnements (ex: dev, recette, production, ...). 
Ce numéro peut correspondre à un tag svn par exemple.

Pour savoir dans quelle version se trouve chaque instance, 
voici comment l'afficher dans la page d'information système de Drupal en back-office :

![Version du site en BO](./d7-afficher-le-numero-de-version-du-site-en-back-office-01.png)

## hook_field_widget_form_alter() ##

Pour modifier le formulaire présent sur la page d'information système, 
il faut utiliser le `hook_field_widget_form_alter()`.

Dans ce hook, vous pouvez modifier un formulaire pour y ajouter/enlever des champs. 
Dans notre cas, l'objectif est d'ajouter un champ `version`, non modifiable :

```php
/**
 * Implements hook_field_widget_form_alter().
 */
function mon_module_form_alter(&$form, &$form_state, $form_id) {
 
  switch($form_id) {

    case 'system_site_information_settings':
      $form['site_information']['mon_site_version'] = array(
        '#type' => 'textfield',
        '#title' => t('Version'),
        '#value' => variable_get('mon_site_version'),
        '#attributes' => array('readonly' => 'readonly')
      );
      break;

    default:
      break;
  }
}
```

**Explications :**

* On ajoute un champ de type `textfield`, avec pour libellé `Version`, 
en lecture seule (= avec l'attribut HTML `readonly`).
* La valeur du champ sera une variable drupal ayant pour nom `mon_site_version`.

**Remarque :**

Pour mettre à jour le numéro de version automatiquement à la mise à jour du module, 
voir l'exemple dans cet article : 
[hook_update() dans D7](./drupal/d7-hook-update).
