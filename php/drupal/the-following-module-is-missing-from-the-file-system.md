The following module is missing from the file system
====================================================

[:type]: # "Erreur"
[:sources]: # "[www.drupal.org](https://www.drupal.org/node/2487215)"
[:version]: # "Drupal 7/8"
[:created_at]: # "2017/05/05"
[:modified_at]: # "2018/02/01"  
[:tags]: # "drupal php"

Si vous installez un module dans Drupal et que vous en supprimez les fichiers avant de le désinstaller, 
vous pouvez rencontrer une erreur du genre :

```
The following module is missing from the file system: 
paragraphs in drupal_get_filename() (line 240 of core/includes/bootstrap.inc).
```

Pour Drupal, le module est désinstallé. Pourtant, il en garde des traces et cela cause cette erreur.

Deux solutions sont alors possibles :

* Réinstaller le module, puis le désinstaller correctement
* Supprimer la référence en base, via la requête suivante :

```bash
drush sql-query "DELETE FROM key_value WHERE collection='system.schema' AND name='module_name';"
```
