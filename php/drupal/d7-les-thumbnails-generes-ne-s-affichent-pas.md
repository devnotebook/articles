[D7] Les thumbnails générés ne s'affichent pas
==============================================

[:type]: # "Erreur"
[:version]: # "Drupal 7"
[:created_at]: # "2016/03/09"
[:modified_at]: # "2018/02/01"  
[:tags]: # "drupal php"

Il arrive que Drupal génère des thumbnails avec des droits incorrects.
Apache ne peut alors pas les servir et ils ne s'affichent pas dans le site.

C'est probablement parce que les droits sur le répertoire `sites/default/files/` sont mauvais.

Exécutez ces commandes pour corriger ce problème :

```bash
find sites/default/files -type d -exec chmod 755 {} +
find sites/default/files -type f -exec chmod 644 {} +
chown -R www-data:www-data sites/default/files
```

**Plus d'informations ici :**

[https://www.drupal.org/node/244924](https://www.drupal.org/node/244924)
