[D7] Créer et traduire un contenu programmatiquement
====================================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:author]: # "2020/11/02" 
[:modified_at]: # "2020/11/02"  
[:tags]: # "drupal php"

Basiquement, pour créer et publier un contenu, ces lignes suffisent :

```php
$default_language = 'fr';
$node_type = 'article';
 
// Création d'un contenu vide
$node = new stdClass();
$node->type = $node_type;
$node->uid = 1;
$node->status = 1;
node_object_prepare($node);
$node->language = $default_language;
 
// Titre du contenu
$node->title = 'Titre de mon contenu'
 
// Autres champs du contenu
// [...(1)]
 
// Autres traductions
// [...(2)]
 
// Sauvegarde du contenu en base
node_save($node);
 
$nid = $node->nid;
```

Si le contenu est multilingue et que son titre est traduisible, 
il faut ajouter la ligne suivante (à la place de `[...(1)]`) :

```php
$node->title_field[$node->language][0]['value'] = 'Titre de mon contenu';
```

Pour traduire un ou plusieurs autres champs dans une autre langue que la langue par défaut, 
il faut ajouter les lignes suivantes (à la place de `[...(2)]` :

```php
$other_language = 'en';
 
// Traduction des champs
$node->title_field[$other_language][0]['value'] = 'The title of my content';
 
// Synchronisation et sauvegarde de la traduction en base
$handler = entity_translation_get_handler('node', $node);
$translation = array(
    'translate' => 0,
    'status' => 1,
    'language' => $other_language,
    'source' => $node->language,
);
$handler->setTranslation($translation, $node);
```

