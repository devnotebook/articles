Le module Help
==============

[:type]: # "Astuce"
[:version]: # "Drupal 7/8"
[:created_at]: # "2017/07/20"
[:modified_at]: # "2017/11/20"  
[:tags]: # "drupal php"

Le module Help fait partie du cœur de Drupal et est activé par défaut.

Il permet d'ajouter un message au haut de la page d'édition d'un contenu :
![Message formulaire d'édition](./le-module-help-01.png)

Pour cela il suffit d'éditer votre type de contenu, et de remplir la zone de texte 
sous le champ title (le HTML est accepté) :

![Configuration du module Help](./le-module-help-02.png)

Pour que le message apparaisse dans le formulaire d'édition, il faudra que le bloc **Aide** soit activé 
dans une des régions de votre thème d'administration. C'est le cas pour celui par défaut (`Seven`).
