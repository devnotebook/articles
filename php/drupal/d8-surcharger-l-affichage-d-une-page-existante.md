[d8] Surcharger l'affichage d'une page existante
================================================

[:type]: # "Astuce"
[:version]: # "Drupal 8"
[:created_at]: # "2016/10/09"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Drupal 8 propose nativement des pages pour gérer l'inscription, la connexion, l'oubli de mot passe.

Malheureusement actuellement il n'y a pas de suggestion de template proposée. (Comme on peut le voir habituellement en 
commentaire dans le code source lorsque le mode debug est activé.)

Il faut donc procéder autrement et utiliser les
[hook_form_alter()](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/hook_form_alter/) 
et 
[hook_theme()](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21theme.api.php/function/hook_theme/) 
classiques.

Par exemple, pour surcharger le formulaire de la page oubli de mot de passe :

```php
// mymodule.module
 
/**
 * Implements hook_form_alter()
 */
function mymodule_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
 
  // Si le formulaire est celui d'oubli de mot de passe
  if ($form_id == 'user_pass') {
    $form['#theme'] = ['my_register_form'];
  }
}
 
/**
 * Implements hook_themer()
 */
function mymodule_theme(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
 
  return [
    'my_password_form' => [
      'template' => 'user_forget_password_form',
      'render element' => 'form',
    ],
  ];
}
```

**Explications :**

* Le `hook_form_alter()` permet de modifier le thème à utiliser pour le formulaire. 
Le thème choisi doit exister ou être déclaré dans votre module.
* Le `hook_theme()` permet de déclarer le nouveau thème `my_password_form` et d'y affecter 
un template spécifique.

**Remarque :**

Par défaut, sans cette configuration, le template natif `form.html.twig` serait utilisé. 
Pour créer votre propre template il peut donc être pratique d'en faire une copie, 
la renommer (ici `user_forget_password_form.html.twig`) et de s'en servir comme base pour effectuer 
vos modifications.
