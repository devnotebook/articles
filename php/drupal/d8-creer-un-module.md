[D8] Créer un module
====================

[:type]: # "Astuce"
[:sources]: # "[www.drupal.org](https://www.drupal.org/developing/modules/8)"
[:version]: # "Drupal 8"
[:created_at]: # "2016/06/24"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

La création d'un module est très rapide dans Drupal. Dans cet exemple, on créera le module `mymodule`.

Commencez par créer le répertoire `mymodule`. Vous pouvez le placer directement dans modules, 
ou créer un répertoire intermédiaire qui regroupera tous vos modules 
(ex: `modules/myproject/mymodule` ou `modules/custom/mymodule`).

Basiquement, un module n'a besoin que de deux fichiers, tous deux à la racine du répertoire : 
`mymodule.info.yml` et `mymodule.module`.

## .info ##

Le fichier `.info.yml` permet de décrire votre module.

```yaml
name: Mon module
description: Module d'exemple.
package: Mon projet
type: module
version: '8.x-1.x'
core: '8.x'
project: 'mon_module'
```

**Explications :**

* Le package permet de regrouper les modules sur la page de liste des modules du back-office. 
Vous pouvez réutiliser le même package que celui d'un module existant.
* La version est celle du module, généralement en deux partie, celle de la version du cœur et celle du module.

## .module ##

Le fichier `.module` contiendra du code PHP. Pour l'instant, créez un 
fichier `mymodule.module` vide (avec uniquement `<?php` et au moins **un retour à la ligne**).

## Architecture ##

Votre module contiendra probablement par la suite deux répertoires principaux :

* src : répertoire contenant la grande majorité de votre code PHP, sous forme de classes d'objet
* templates : répertoire contenant les templates fournis par votre module

## Résultat ##

Une fois fait, vous devriez voir votre module en back-office :

![Créer un module](./d8-creer-un-module-01.png)

Vous pouvez l'activer via cette interface ou utiliser drush :

```bash
drush en mymodule -y
```
