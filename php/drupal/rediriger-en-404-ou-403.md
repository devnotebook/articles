Rediriger en 404 ou 403
=======================

[:type]: # "Astuce"
[:sources]: # "[www.drupal.org](https://www.drupal.org/node/1616360)"
[:version]: # "Drupal 7/8"
[:created_at]: # "2017/06/15"
[:modified_at]: # "2017/11/20"
[:tags]: # "drupal php"

Si vous voulez rediriger l'utilisateur vers la page 404 ("Page introuvable") ou 
403 ("Vous n'avez pas le droit d’accéder à cette page") de Drupal, vous utilisiez sans doute ça en Drupal 7 :

```php
return drupal_access_denied();
return drupal_not_found();
```

Voici l'équivalent pour Drupal 8 :

```php
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
 
throw new AccessDeniedHttpException();
throw new NotFoundHttpException();
```
