[D7] hook_update()
==================

[:type]: # "Astuce"
[:sources]: # "[Documentation hook_update()](https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_update_N/7)"
[:version]: # "Drupal 7"
[:created_at]: # "2015/07/29"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

## Description ##

Dans Drupal, le `hook_update()` permet d'exécuter du code PHP à la mise à jour d'un module.

Imaginons par exemple que vous ayez un numéro de version de votre site web, 
enregistré dans une variable drupal. 
Lors de la mise à jour de votre site, vous souhaiter incrémenter ce numéro de version.

Il suffit d'utiliser le `hook_update()`, et lorsque le module sera mis à jour, 
l'incrémentation sera appliquée automatiquement.

De plus, si vous avez plusieurs `hook_update()`, il seront tous exécutés un à un, 
dans l'ordre, lors de la mise à jour. 
Lors des mises à jour suivantes seuls les nouveaux hooks n'ayant pas encore été exécutés le seront.

## Application ##

### Hook ###

Par convention, ce hook doit être utilisé dans le fichier `mon_module.install` :

```php
/**
 * Update v1.0.1
 */
function mon_module_update_7101() {
  variable_set('mon_site_version', '1.0.1');
}
```

**Explication :**

Le numéro à la fin du hook correspond à la version du module. 
Le premier chiffre, par convention, désigne la version majeure de drupal utilisée.

### Mise à jour du module ###

Une fois la nouvelle version du module déployée sur votre site, vous devez lancer la mise à jour. 
Cela peut-être fait en back-office dans la page de gestion des modules, ou mieux, via une commande drush :

```bash
drush updb -y
```

### Mises à jours successives ###

Imaginons que vous ayez passé cette première mise à jour (`7101`) sur l'un de vos sites 
(exemple : l'instance de production), et qu'entre temps vous ayez créé deux nouvelles versions du module.

Le fichier `mon_module.install` devient :

```php
/**
 * Update v1.0.1
 */
function mon_module_update_7101() {
  variable_set('mon_site_version', '1.0.1');
}
 
/**
 * Update v1.0.2
 */
function mon_module_update_7102() {
  variable_set('mon_site_version', '1.0.2');
 
  // Autres traitements
}
 
/**
 * Update v1.0.3
 */
function mon_module_update_7103() {
  variable_set('mon_site_version', '1.0.3');
 
  // Autres traitements
}
```

Si vous lancez la mise à jour sur votre instance de production, 
les fonctions `mon_module_update_7102()` et `mon_module_update_7103()` seront exécutées dans cet ordre.

La fonction `mon_module_update_7101()` elle, ne sera pas exécutée.
