[D7] Remplir des champs de contenu programmatiquement
=====================================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2014/06/23"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Lorsqu'on [crée programmatiquement un contenu](./drupal/d7-creer-et-traduire-un-contenu-programmatiquement), 
voici comment remplir différents types de champ 
(les lignes ci-dessous sont à ajouter à la place de `[...(1)]` et/ou `[...(2)]` dans l'article lié).

## Champs multivalués ##

Dans Drupal, tous les champs sont potentiellement multivalués. 
Pour ajouter plusieurs valeurs à un même champ, il suffit d'ajouter une nouvelle ligne en 
incrémentant le `[0]` dans les lignes ci-dessous.

Par exemple pour un champ texte basique ayant 3 valeurs :

```php
$node->field_text[LANGUAGE_NONE][0]['value'] = 'ma première valeur';
$node->field_text[LANGUAGE_NONE][1]['value'] = 'ma deuxième valeur';
$node->field_text[LANGUAGE_NONE][2]['value'] = 'ma troisième valeur';
```

## Champs multilingues ##

Dans Drupal, tous les champs sont potentiellement multilingues. 
Pour ajouter une valeur dans une autre langue, il suffit d'ajouter une ligne en remplaçant 
`LANGUAGE_NONE` par une langue.

Exemple avec un champ texte basique et deux langues :

```php
$node->field_text['fr'][0]['value'] = 'ma valeur';
$node->field_text['en'][1]['value'] = 'my value';
```

## Champs texte ##

Pour remplir un champ texte basique, cette ligne suffit :

```php
$node->field_text[LANGUAGE_NONE][0]['value'] = 'ma valeur';
```

Si ce champ utilise un format de texte particulier, il faut le préciser. Exemple avec `simple_text` :

```php
$node->field_text[LANGUAGE_NONE][0]['value'] = 'ma valeur';
$node->field_text[LANGUAGE_NONE][0]['format'] = 'simple_text';
```

## Champs entier/décimal ##

Même principe pour les champs de type nombre :

```php
$node->field_number[LANGUAGE_NONE][0]['value'] = 42;
```

## Champs booléen ##

Encore la même chose pour les booléens, `$my_boolean` étant en fait un entier égal à 0 ou 1 :

```php
$my_boolean = 0;
$node->field_number[LANGUAGE_NONE][0]['value'] = $my_boolean;
```

## Champs image ##

Pour importer une image programmatiquement vous pouvez utiliser cette fonction :

```php
  /**
   * Copy the image in argument in the drupal upload dir, and return it.
   *
   * @param string $image_path Image path from the root directory
   * @return array an array representing the copied image
   */
  private function copy_image($image_path) {
 
    $root_dir_path = getcwd();
    $upload_sample_files_uri = file_default_scheme() . '://sample_data'; 
 
    $file_path = $root_dir_path . $image_path;
    $file = (object) array(
                'uid' => 1,
                'uri' => $file_path,
                'filemime' => file_get_mimetype($file_path),
                'status' => 1,
    );
    $file = file_copy($file, $upload_sample_files_uri);
    return (array) $file;
  }
```

Pour une image dans un répertoire `temp/` à la racine de Drupal ça donne ça :

```php
$node->field_image[LANGUAGE_NONE][0] = copy_image('/temp/mon_image.jpg');
```

## Champs lien ##

Un champ lien avec une URL, un libellé et d'éventuels attributs HTML :

```php
$node->field_link[LANGUAGE_NONE][0] = array(
  'url' => 'http://www.google.fr',
  'title' => 'Libellé du lien',
  'attributes' => array('title' => 'Contenu de l'attribut HTML title'),  
);
```

## Champs référence entre entités (entityref) ##

Le champ entityref stocke des id (et donc des nid pour des nœuds)  :

```php
$node->field_related_content[LANGUAGE_NONE][0]['target_id'] = $other_node->nid;
```

## Champs adresse ##

Le champ adresse découpe les adresses en 5 parties :

```php
$node->field_adresse[LANGUAGE_NONE][0] = array(
  'country' => 'FR',
  'locality' => 'Paris',
  'postal_code' => '75000',
  'thoroughfare' => '1, Avenue des Champs Élysées',
  'premise' => '2ème étage',
);
```

## Champs coordonnées - Bounds ##

Le champ coordonnées permet entre autres de stocker des limites géographiques : des bounds.

```php
$node->field_coordonnees[LANGUAGE_NONE][0]['input_format'] = GEOFIELD_INPUT_BOUNDS;
$node->field_coordonnees[LANGUAGE_NONE][0]['geom']['left'] = '2.320915';
$node->field_coordonnees[LANGUAGE_NONE][0]['geom']['top'] = '48.869911';
$node->field_coordonnees[LANGUAGE_NONE][0]['geom']['right'] = '2.350928';
$node->field_coordonnees[LANGUAGE_NONE][0]['geom']['bottom'] = '48.854086';
```

## Champs métadonnées ##

Le champ métadonnées ajoutera des balises méta dans le `<head>` de la page :

```php
$node->metatags[LANGUAGE_NONE] = array(
    'title' => array('value' => 'Contenu de la balise title de la page'),
    'description' => array('value' => 'Contenu de la balise méta description'),
    'abstract' => array('value' => 'Contenu de la balise méta abstract'),
    'keywords' => array('value' => 'Contenu de la balise méta keywords'),
);
```

D'autres clés peuvent être ajoutées au tableau.
