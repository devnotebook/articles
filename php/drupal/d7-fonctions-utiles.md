[D7] Fonctions utiles
=====================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2014/07/17"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

## String ##

### t($text, $params = array(), $options = array()) ###

**Description :**

Traduit une chaîne de caractères, avec d'éventuels paramètres.
Le troisième argument permet de spécifier un contexte ou une langue.

**Exemple :**

```php
$text = t('String with @myparam', array('@myparam' => $my_value));
```

[Documentation](https://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/t/7)


## Images ##

### image_style_url($style_name, $path) ###

**Description :**

Génère L’URL vers le thumbnail d'image correspondant au style d'image en premier argument.

**Exemple :**

```php
image_style_url(
  'image_thumbnail',
  $node->field_photos[$language->language][$index]['uri']
);
```

[Documentation](https://api.drupal.org/api/drupal/modules%21image%21image.module/function/image_style_url/7)


## URL ##

### l($text, $path, $options = array()) ###

**Description :**

Crée une balise `<a>` avec le premier argument comme libellé et le deuxième en `href`.

**Exemple :**

```php
image_style_url(
  'image_thumbnail',
  $node->field_photos[$language->language][$index]['uri']
);
```

[Documentation](https://api.drupal.org/api/drupal/includes%21common.inc/function/l/7)

### file_create_url($uri) ###

**Description :**

Génère l'URL vers un fichier ou une image, à partir de son uri (de la forme `public://mon-image-03.jpg`).

**Exemple :**

```php
file_create_url($node->field_logo[$language->language][0]['uri']);
```

[Documentation](https://api.drupal.org/api/drupal/includes%21file.inc/function/file_create_url/7)

### url($path = NULL, $options = array()) ###

**Description :**

Génère une URL interne ou externe, à partir d'une URL relative ou absolue.

**Exemple :**

`url('node/' . $node->nid)`

[Documentation](https://api.drupal.org/api/drupal/includes%21common.inc/function/url/7)

### drupal_get_path_alias($path = NULL, $path_language = NULL) ###
	
**Description :**

Retourne l'alias d'URL pour la page avec le chemin en argument

**Exemple :**

`drupal_get_path_alias('node/' . $node->nid)`

[Documentation](https://api.drupal.org/api/drupal/includes%21path.inc/function/drupal_get_path_alias/7)


## Objets et tableaux ##

### element_children(&$elements, $sort = FALSE) ###
	
**Description :**

Retourne le tableau en entrée, sans les valeurs dont la clé commence par `#`.	

[Documentation](https://api.drupal.org/api/drupal/includes%21common.inc/function/element_children/7)


## Développement ##

### dpm($variable) ###
	
**Description :**

Affiche le contenu de la variable, ses éléments dans le cas d'un tableau, 
ses attributs dans le cas d'un objet, de manière récursive. 
(Cette fonction est fournie par le module [devel](https://www.drupal.org/project/devel))

[Documentation](https://api.drupal.org/api/devel/devel.module/function/dpm/7)

### dpq($select) ###
	
**Description :**

Affiche la requête finale. (Cette fonction est fournie par le module [devel](https://www.drupal.org/project/devel))
 
[Documentation](https://api.drupal.org/api/devel/devel.module/function/dpq/7)
