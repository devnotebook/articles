[D7] Ajouter un mode d'affichage à un noeud
===========================================

[:type]: # "Astuce"
[:sources]: # "[Documentation hook_entity_info_alter()](https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_entity_info_alter/7)"
[:version]: # "Drupal 7"
[:created_at]: # "2014/07/18"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Pour créer un mode d'affichage programmatiquement, vous devez déjà avoir [créé un module](./drupal/d7-creer-un-module).

Par défaut, Drupal propose les modes d'affichage suivant : 
Contenu complet (= Full), Accroche (= Teaser) et RSS. 
Le ```hook_entity_info_alter()``` dans le fichier ```mymodule.module``` permet d'en ajouter de nouveaux.

```php
/**
 * Implements hook_entity_info_alter();
 */
function mymodule_entity_info_alter(&$entity_info) {
 
  $entity_info['node']['view modes']['my_view_mode'] = array(
    'label' => t('My view mode'),
    'custom settings' => FALSE,
  );
}
```

Après avoir vidé les caches, vous devriez voir votre nouveau mode d'affichage en back-office :

![Activation du mode d'affichage](./d7-ajouter-un-mode-d-affichage-a-un-noeud-01.png)
