Modifier un élément de liste dans un formulaire
===============================================

[:type]: # "Astuce"
[:sources]: # "[drupal.stackexchange.com](https://drupal.stackexchange.com/questions/127795/how-to-add-class-to-individual-radio-item-inside-radios/127797#answer-235981)"
[:version]: # "Drupal 7/8"
[:created_at]: # "2017/07/19"
[:modified_at]: # "2017/11/20"
[:tags]: # "drupal php"

Pour ajouter une classe CSS sur un élément de formulaire basique (input, select, ...), 
on ajoute la clé `#attributes` à son render array.

Par contre pas possible de le faire pour une liste de boutons radio par exemple. 
La classe s'ajoutera sur le conteneur à la place.

Il existe donc la clé `#after_build` pour remédier à ce problème (cf. [documentation](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement)). 
Elle attend une liste de noms de fonction en valeur.

Chacune de ces fonctions sera exécutée après coup pour modifier le render array de l'élément.
À ce moment de l'exécution, les sous-éléments (ici les boutons radio) ont déjà été ajoutés et 
peuvent donc être modifiés.

Par exemple dans ma méthode `buildForm()` :

```php
$form['my_field'] = [
  '#type' => 'radios',
  '#title' => t('My field'),
  '#options' => [
    0 => t('No'),
    1 => t('Yes'),
  ],
  '#after_build' => ['_my_module_radio_add_class']
];
```

Et dans mon fichier `my_module.module` :

```php
function _my_module_radio_add_class(array $element, FormState $form_state) {
  $options = array_keys($element['#options']);
 
  // Parcours des sous-éléments options
  foreach ($options as $values) {
    $element[$values]['#attributes']['class'][] = 'myclass';
  }
  return $element;
}
```
