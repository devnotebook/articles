[D7] Créer un script drush
==========================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2015/08/11"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Drush propose déjà pas mal de [fonctionnalités](./drupal/d8-commandes-drush-utiles). 
Il est en plus extensible. Voici comme ajouter votre propre script.

## Déclaration du script ##

Créez un fichier `my_module.drush.inc`, et implémentez-y le `hook_drush_command()` :

```php
// my_module.drush.inc
 
/**
 * Implements hook_drush_command().
 */
function my_module_drush_command()
{
  $items = array();
  $items['say_hello'] = array(
    'description' => t('Say "Hello"'),
    'arguments' => array(
      'who' => t('Who are you talking to ?'),
    ),
    'options' => array(
      'punctuation' => 'Which punctuation do you use ? (optional, if not provided, use ".")',
    ),
    'examples' => array(
      'drush say_hello Boby' => 'Says : "Hello Boby."',
      'drush mmsh Boby punctuation="!"' => 'Says : "Hello Boby !"',
      'drush mmsh Boby' => 'Uses the alias and says : "Hello Boby."',
    ),
    'aliases' => array('mmsh'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  return $items;
}
```

**Explications :**

* Le nom du script est `say_hello`. La fonction appelée derrière sera donc `drush_my_module_say_hello()`.
* Le script attend un **prénom en argument**, et éventuellement un signe de **ponctuation en option**.
* Dans cet exemple, l'alias correspond aux initiales de `My_Module_Say_Hello`. 
On peut appeler le script avec ou sans cet alias :

```bash
drush say_hello
drush mmsh
```

## Implémentation du script ##

Le script pourrait être implémenté ainsi :

```php
// my_module.drush.inc
 
/**
 * Dit bonjour.
 * @param string $who Nom de la personne à saluer
 */
function drush_logres_business_say_hello($who) {
 
  $start = new DateTime();
 
  if (empty($who)) {
 
    echo 'Say "Hello" to who ?';
 
  } else {
 
    // Récupération des options
    $punctuation = drush_get_option('punctuation');
    $punctuation = (!empty($punctuation)) ? ' ' . $punctuation : '.';
 
    // Traitement
    echo 'Hello ' . $who . $punctuation;
  }
 
  // Affichage du temps d'exécution
  $end = new DateTime();
  echo "\n" . t('Duration : !duration', array('!duration' => $end->diff($start)->format('%hH %imin %ss')) ) . "\n";
}
```

**Explications :**

* On vérifie la valeur en argument
* On récupère une éventuelle option
* On effectue le traitement souhaité (ici, on dit bonjour)
* On affiche le temps qu'a duré le script
