[D8] Créer un contenu programmatiquement
========================================

[:type]: # "Astuce"
[:version]: # "Drupal 8"
[:created_at]: # "2016/06/24"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Pour créer un nouveau contenu (ou n'importe quelle entité), quelques lignes suffisent :

```php
<?php
 
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityStorageException;

$node = Node::create([
  'type' => 'article',
  'langcode' => 'fr',
  'uid' => '1',
  'status' => 1,
]);
$node->setTitle('Mon premier article');
$node->set('field_my_text', 'du texte');
$node->set('field_my_float', 150.42);
$node->set('field_my_date', date('Y-m-d'));
 
try {
  $node->save();
}
catch (EntityStorageException $e) {
  \Drupal::logger('mymodule')->error("La création de l'article a échouée : @message", [
    '@message' => $e->getMessage()
  ]);
  $node = NULL;
}
```

**Explications :**

* Les arguments de la méthode `create()` permettent de définir le type de nœud, 
son créateur, son statut, sa langue, ...
* Tous les champs sont ensuite valués via la méthode `set()`.
