[D7] Hook après édition/suppression de contenu, multilingue ou non
==================================================================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2014/06/24"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Drupal fournit 3 hooks pour effectuer des traitements après création/édition/suppression de contenu.

## Pour un nœud ##

### À la création ###

```php
/**
 * Implements hook_node_insert().
 */
function my_module_node_insert($node) {
 
  // Do something
}
```

### À l'édition ###

```php
/**
 * Implements hook_node_update().
 */
function my_module_node_update($node) {
 
  // Do something
}
```

### À la suppression ###

```php
/**
 * Implements hook_node_delete().
 */
function my_module_node_delete($node) {
 
  // Do something
}
```

## Pour une entité multilingue ##

Dans le cadre de contenus (et autres entités) multilingues, 
3 autres hooks permettent de connaitre la langue pour laquelle la révision est créée/supprimmée.

La langue utilisée est disponible dans le hook vie la variable `$translation['language']`. 
Le type d'entité (ex: user, node, ...) est disponible dans la variable `$entity_type`.

### À la création ###

```php
/**
 * Implements hook_entity_translation_insert().
 */
function my_module_entity_translation_insert($entity_type, $entity, $translation, $values = array()) {
 
  // Do something
}
```

### À l'édition ###

```php
/**
 * Implements hook_entity_translation_update().
 */
function my_module_entity_translation_update($entity_type, $entity, $translation, $values = array()) {
 
  // Do something
}
```

### À la suppression ###

```php
/**
 * Implements hook_entity_translation_delete().
 */
function my_module_entity_translation_delete($entity_type, $entity, $langcode) {
 
  // Do something
}
```
