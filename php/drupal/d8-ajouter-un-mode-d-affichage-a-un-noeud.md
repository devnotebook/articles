[D8] Ajouter un mode d'affichage à un nœud
==========================================

[:type]: # "Astuce"
[:version]: # "Drupal 8"
[:created_at]: # "2016/07/01"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Dans Drupal 7, pour ajouter un nouveau mode d'affichage il fallait utiliser un hook 
(cf: [cet autre article](./drupal/d7-ajouter-un-mode-d-affichage-a-un-noeud)).

Dans Drupal 8.x, tout est faisable en back-office, via **Structure** > **Modes d'affichage**. 
Ensuite, comme avant, il faut activer le nouveau mode pour le type de nœud correspondant :

![Activation du mode d'affichage](./d8-ajouter-un-mode-d-affichage-a-un-noeud-01.png)

**Remarque :**

Dans Drupal 8, les modes d'affichage sont cloisonnés par entité. 
Si vous voulez un mode d'affichage **Liste** pour les utilisateurs et pour les nœuds, il faudra en créer deux.
