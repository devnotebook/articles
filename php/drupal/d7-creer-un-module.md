[D7] Créer un module
====================

[:type]: # "Astuce"
[:version]: # "Drupal 7"
[:created_at]: # "2014/07/18"
[:modified_at]: # "2018/02/01" 
[:tags]: # "drupal php"

La création d'un module est très rapide dans Drupal. Dans cet exemple, on créera le module `mymodule`.

Commencez par créer le répertoire `mymodule/`. Vous pouvez le placer directement 
dans `site/all/modules`, ou créer un répertoire intermédiaire qui regroupera tous vos modules 
(ex: `site/all/modules/myproject/mymodule/`, ou `site/all/modules/custom/mymodule/`).

Basiquement, un module n'a besoin que de deux fichiers, tous deux à la racine du répertoire : 
`mymodule.info` et `mymodule.module`.

## .info ##

Le fichier `.info` permet de décrire votre module.

```ini
name = mymodule
description = module de test
package = Mypackage
core = 7.x
version = "7.x-1.0"
```

**Explications :**

* Le package permet de regrouper les modules sur la page de liste des modules du back-office. 
Vous pouvez réutiliser le même package que celui d'un module existant.
* La version est celle du module, généralement en deux parties, celle de la version du cœur  de Drupal et 
celle du module
* [Cf. documentation officielle](https://www.drupal.org/node/542202)

## .module ##

Le fichier `.module` contiendra une bonne partie de votre code PHP, et surtout vos **hooks**.
Pour l'instant, créez un fichier `mymodule.module` vide.

## Résultat ##

Une fois fait, et après vidage des caches, vous devriez voir votre module en back-office :

![Nouveau module en BO](./d7-creer-un-module-01.png)
