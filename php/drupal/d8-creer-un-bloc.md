[D8] Créer un bloc
==================

[:type]: # "Astuce"
[:sources]: # "[www.drupal.org](https://www.drupal.org/node/2101565)"
[:version]: # "Drupal 8"
[:created_at]: # "2016/06/24"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Pour créer un bloc programmatiquement, vous devez déjà avoir [créé un module](./drupal/d8-creer-un-module).

Dans cet exemple, on créera un bloc qui affiche "Hello" et le nom de l'utilisateur connecté. 
On pourra personnaliser qui saluer si aucun utilisateur n'est connecté.

## Déclaration du bloc ##

Toute la déclaration/configuration du bloc se fait dans une classe PHP, 
placée traditionnellement dans le répertoire `src/Plugin/Block/` de votre module :

```php
<?php
 
namespace Drupal\my_module\Plugin\Block;
 
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
 
/**
 * @Block(
 * id = "hello_block",
 * admin_label = @Translation("My Hello block"),
 * category = @Translation("My project")
 * )
 */
class HelloBlock extends BlockBase {
 
  /**
   * {@inheritdoc}
   */
  public function build() {
 
    $current_user = \Drupal::currentUser();
    if (!$current_user->isAnonymous()) {
      $who = $current_user->getDisplayName();
    }
    else {
      $config = $this->getConfiguration();
      $who = isset($config['who']) ? $config['who'] : 'World';
    }
 
    $build = array(
      '#cache' => array(
         'contexts' => array('user'),
         'max-age' => Cache::PERMANENT,
      ),
      '#markup' => '<p>Hello ' . $who . '</p>',
    );
 
    return $build;
  }
 
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
 
    $form = parent::blockForm($form, $form_state);
 
    $config = $this->getConfiguration();
 
    $form['my_block_who'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Who ?'),
      '#default_value' => isset($config['my_block_who']) ? $config['my_block_who'] : 'world',
    );
 
    return $form;
  }
 
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_module_who'] = $form_state->getValue('my_module_who');
  }
 
}
```

**Explications :**

* La classe hérite de `BlockBase`, fournie par Drupal.
* Des annotations permettent de préciser le nom, le nom technique et le groupe (= catégorie) du bloc.
* La méthode principale est `build()`, qui retourne un tableau de theming Drupal pouvant être rendu à l'écran.
* Les deux autres permettent d'ajouter un champ au formulaire de configuration de bloc natif fourni par Drupal.

**Remarque :**

Le tableau de theming définit un cache par utilisateur (`#cache`), et le code HTML (`#markup`) 
constituant le contenu de votre bloc.

## Activation du bloc ##

Pour que Drupal trouve votre nouveau bloc, vous devez vider les caches.

* Allez ensuite en back-office, dans **Structure** > **Administration des blocs**
* Dans la région qui contiendra le bloc, cliquez sur **Placer le bloc**
* Trouvez votre module dans la liste qui apparaît et cliquez sur **Positionner le bloc**

![Positionner le bloc](./d8-creer-un-bloc-01.png)

Apparaît alors le formulaire d'administration du bloc.

Vous pouvez par défaut choisir pour quelles pages le module doit apparaître 
(en fonction de l'utilisateur, du contenu affiché en pleine page, de l'URL de la page, ...).

À ce paramétrage s'ajoute votre champ personnalisé **Who ?**.

## Affichage ##

Par défaut, le bloc est affiché en suivant le template `block.html.twig` fourni par Drupal et 
le thème que vous utilisez.

Pour surcharger ce template, vous pouvez [définir un nouvel habillage](./drupal/d8-theming-definir-une-nouvelle-apparence-pour-un-element) 
pour votre bloc et créer votre propre template twig.
