[d8] Ajouter des variables dans un render array existant
========================================================

[:type]: # "Astuce"
[:version]: # "Drupal 8"
[:created_at]: # "2017/11/08"
[:modified_at]: # "2018/02/01"
[:tags]: # "drupal php"

Les render array sont utilisés partout dans Drupal, pour générer des affichages.
Lorsqu'on crée un bloc côté PHP, il retourne un render array. Même chose pour un formulaire ou pour un contrôleur.

Les modules peuvent [définir leurs propres apparences pour les éléments](./drupal/d8-theming-definir-une-nouvelle-apparence-pour-un-element) : 
ils peuvent décrire de nouveaux render array 
([hook_theme()](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/function/hook_theme))
qui seront ensuite utilisables partout.

Pour ajouter des variables à un render array proposé par un autre module, 
il faut tout d'abord modifier sa définition, pour que la variable puissent être transmise au template. 
On utilise pour cela le [hook_theme_registry_alter()](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/function/hook_theme_registry_alter).

```php
/**
 * Implements hook_theme_registry_alter().
 */
function mon_module_theme_registry_alter(&$theme_registry) {
  $theme_registry['nom_du_render_array']['variables']['nouvelle_variable'] = 'valeur_par_defaut';
}
```

Maintenant que la variable "est autorisée", il faut l'ajouter au render array existant. 
Il s'agit d'un simple [hook_preprocess_HOOK()](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/function/hook_preprocess_HOOK).

```php
/**
 * Implements hook_preprocess().
 */
function mon_module_preprocess_nom_du_render_array(&$variables) {
  $variables['nouvelle_variable'] = 'valeur de la variable';
}
```

**Explication :**

- `nom_du_render_array` est la clé définie dans le `hook_theme()` du module qui le propose. 
C'est celle qu'on renseigne lorsqu'on utilise le render array (`'#theme" => 'nom_du_render_array'`).
- C'est cette clé qu'on utilise dans le nom de notre fonction `hook_preprocess_HOOK()`.

**Remarque :**

Comme après chaque implémentation de hook(), pensez à vider les caches.
