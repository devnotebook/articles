[eZ4] Utiliser les alias de fetch
=================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2013/02/14"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

Pour simplifier l'utilisation des fetch dans les templates, eZ Publish propose d'utiliser des alias.

## Exemple d'utilisation ##

Par exemple, si vous voulez compter récursivement les articles fils du nœud courant, le fetch standard serait :

```smarty
{def $nb = fetch( 'content', 'tree_count',
    hash( 
        'parent_node_id', $node.node_id,
        'class_filter_type', 'include',
        'class_filter_array', array( 'article' ) 
    ) 
)}
```

Si vous utilisez souvent ce même fetch, vous aimerez sans doute lui créer un alias. La syntaxe devient alors :

```smarty
{def $nb = fetch_alias( 
    'children_article_count', 
    hash( 'parent_node_id', $node.node_id ) 
)}
```

**Explications :**

* L'alias s'appelle `children_article_count()`
* Il n'a qu'un seul paramètre : l'ID du nœud parent.

## Configuration ##

Pour informer eZ Publish de votre alias, il faut le déclarer dans le fichier `fetchalias.ini`.

Par exemple, dans le fichier `monextension/settings/fetchalias.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[children_article_count]
# Compte récursivement le nombre d'articles sous le nœud dont l'ID est en paramètre
Module=content
FunctionName=tree_count
Constant[class_filter_type]=include
Constant[class_filter_array]=article
Parameter[parent_node_id]=parent_node_id
 
*/ ?>
```

**Explications :**

* La section (`children_article_count`) est le nom de votre alias, à utiliser dans votre template.
* Le **module** et la **fonction** sont ceux que vous auriez appelés dans le fetch standard.
* Les constantes sont les paramètres fixes que vous auriez passés au fetch standard.
* Les paramètres permettent de mapper le nom des paramètres de l'alias avec ceux du fetch standard.

**Remarques :**

* N'oubliez pas de vider les caches pour qu'eZ Publish prennent en compte cette configuration.
* EZ Publish fournit déjà des alias, visibles dans le fichier `settings/fetchalias.ini`.
