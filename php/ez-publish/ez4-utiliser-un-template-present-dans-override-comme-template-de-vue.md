[eZ4] Utiliser un template présent dans override/ comme template de vue
=======================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/04/16"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

* [Récupérez un 'nœud courant'](./ez-publish/ez4-obtenir-un-noeud-courant-dans-la-vue-d-un-module) 
dans la vue du module
* Déclarez une règle de surcharge dans `override.ini` :

```ini
<?php /* #?ini charset="utf-8"?
 
[mavue_mon_type_de_noeud]
Source=monmodule/mavue.tpl
MatchFile=monmodule/mavue/mon_type_de_noeud.tpl
Subdir=templates
Match[class_identifier]=mon_type_de_noeud
 
*/ ?>
```

* Déclarez cette surcharge pour le template dans le fichier `.php` de la vue du module :

```php
$res = eZTemplateDesignResource::instance();
$designKeys = array( array( 'class_identifier', $node->attribute( 'class_identifier' ) ),
                     array( 'parent_node_id', $node->attribute( 'parent_node_id' ) ) );
$res->setKeys( $designKeys );
```
