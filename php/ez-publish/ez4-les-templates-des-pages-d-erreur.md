[eZ4] Les templates des pages d'erreur
======================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/28"
[:modified_at]: # "2018/02/07"
[:tags]: # "ez-publish php"

Lorsqu'une erreur se produit, eZ Publish affiche une page avec un message d'erreur. 
Ces pages sont entièrement personnalisables, puisque elles utilisent des templates standards.

Voici la liste des templates de pages d'erreur fournis par défaut dans eZ Publish 
(dans `design/standard/templates/`) :

* `error/kernel/1.tpl` : Accès à une page avec autorisations insuffisantes. 
Affiche un message d'erreur plus la mire de login.
* `error/kernel/2.tpl` : Accès à un module inconnu.
* `error/kernel/21.tpl` : Accès à une vue inconnue.
* `error/kernel/22.tpl` : Accès à un module désactivé.
* `error/kernel/3.tpl` : Accès à une ressource indisponible ou verrouillée.
* `error/kernel/4.tpl` : Accès à un contenu déplacé. Redirige automatiquement vers le nouvel emplacement.
* `error/kernel/5.tpl` : Création d'un brouillon dans une langue invalide.
