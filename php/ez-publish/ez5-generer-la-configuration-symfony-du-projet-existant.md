[eZ5] Générer la configuration Symfony du projet existant
=========================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 5"
[:created_at]: # "2013/03/20"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

Lorsque vous migrez un site eZ Publish 4.x vers 5.x sans utiliser l'assistant d'installation automatisée, 
vous devez générer la configuration Symfony de votre site.

EZ Publish 5 est maintenant un projet Symfony et utilise la gère sa configuration en fichiers `.yml`. 
Pour passer des anciens `.ini` vers les nouveaux `.yml`, utilisez la commande suivante 
à la racine de votre projet :

```bash
php ezpublish/console ezpublish:configure --env=prod <group> <admin-siteaccess>
```

Remplacez `<group>` par votre groupe de siteaccess (ex: `mon_site`), 
et `<admin-siteaccess>` par le nom du siteaccess de votre back-office.

**Remarques :**

* Symfony permet de switcher simplement entre les environnements de production et de développement.
Pour chacun d'eux elle propose un fichier de configuration par défaut : `ezpublish/config/ezpublish_dev.yml` 
et `ezpublish/config/ezpublish_prod.yml`.  
Remplacez `--env=prod` par `--env=dev` pour utiliser la configuration de développement.
* Le groupe de siteaccess est une nouvelle notion introduite par eZ Publish 5.
