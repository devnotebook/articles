[eZ4] L'arbre des contenus n'est plus acessible en front-office et en back-office
=================================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/02"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Si plus aucun contenu ne s'affiche en front-office, seulement le header et les menus, 
et qu'en back-office l'onglet **Contenus** n'est plus accessible, il s'agît d'un problème de cache. 
**EZ Publish n'arrive plus à générer le cache**.

## Première approche ##

Vérifiez qu'eZ Publish a bien les droits d'écriture sur le répertoire qui contient le cache 
(`var/cache/` par défaut).

**Remarque :**

Ce problème de droits est fréquent sous linux, mais peut également se produire sous Windows 
(ex : Windows 7 Professional).

## Deuxième approche ##

Si le problème persiste, essayez la commande suivante à la racine du site, pour vraiment supprimer le cache :

```bash
php bin/php/ezcache.php --clear-all --purge
```

## Troisième approche ##

Supprimez tout le contenu du répertoire `var/cache/` à la main.

## Quatrième approche ##

Faîtes le ménage sur le serveur. Il se peut que le disque soit plein.
