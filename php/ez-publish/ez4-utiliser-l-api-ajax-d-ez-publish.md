[eZ4] Utiliser l'API Ajax d'eZ Publish
======================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2013/03/01"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

L'extension `ezjscore` d'eZ Publish permet d'appeler des fonctions PHP via des requêtes Ajax. 
Vous pouvez l'utiliser pour mettre à jour une partie de la page sans la recharger complètement.

## Le principe ##

Le javascript va lancer une requête Ajax à la vue `call` du module `ezjscore` 
(et donc appeler l'URL `/ezjscore/call`).
Cette vue va retourner le résultat d'une méthode PHP, en fonction de la configuration du fichier `ezjscore.ini`.

Le résultat est alors disponible côté js et peut être utilisé pour modifier une partie de la page.

## PHP ##

Les méthodes disponibles pour un appel Ajax doivent être implémentées dans des classes 
héritant de `ezjscServerFunctions`.

Par exemple dans le fichier `monextension/classes/MyServerCallFunctions.php` :

```php
<?php
 
/**
 * Classe de fonctions à appeler en ajax.
 */
class MyServerCallFunctions extends ezjscServerFunctions {
 
    /**
     * Retourne le message "Hello x !", avec x le premier élément du tableau de paramètres, 
     *  ou "world" si aucun paramètre n'est passé.
     *
     * @param array $args Arguments
     * @return string
     */
    public static function helloMessage( array $args ) {
 
        // Log de l'appel de la fonction
        eZLog::write( 'Appel Ajax : ' . __METHOD__, 'debug.log' );
 
        if ( !empty( $args ) ) {
            $message = 'Hello ' . $args[0] . ' !';
        } else {
            $message = 'Hello world !';
        }
        return $message;
    }
}
```

## Configuration ##

Pour que votre classe soit utilisable vous devez la déclarer le fichier `ezjscore.ini` :

Par exemple, dans le fichier `monextension/settings/ezjscore.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[ezjscServer]
# Liste des fonctions accessibles via des appels Ajax
FunctionList[]=my_function_name
 
[ezjscServer_my_function_name]
# Nom de la classe PHP
Class=MyServerCallFunctions
# Nom du fichier contenant la classe
File=extension/monextension/classes/MyServerCallFunctions.php
# Nom des fonctions proposées par la classe
Functions[]=my_function_name
 
*/ ?>
```

**Remarque :**

Une fois le fichier modifié, **videz les caches** et **régénérez les autoloads**, pour qu'eZ Publish 
trouve votre nouvelle classe.

## Un premier test ##

Vous pouvez déjà appeler votre fonction en tapant l'url suivante dans votre navigateur :

[http://monsite.com/index.php/ezjscore/call/my_function_name::helloMessage::dude](http://monsite.com/index.php/ezjscore/call/my_function_name::helloMessage::dude)

On appelle bien la vue `call` du module `ezjscore`, à laquelle on fournit le nom d'une fonction et 
la liste des arguments, séparés par `::`.

**Remarque :**

Cela ne fonctionne que si vous êtes connecté en tant qu'administrateur. 
Pour éviter ça, vous devez **autorisez d'autres rôles à accéder à la vue ezjscore/call**. 
Vous pouvez même définir des limitations, pour n'autoriser que l'accès à la fonction `my_function_name`.

## Javascript ##

Maintenant que votre fonction est accessible, voici comment l'utiliser dans vos template.

Tout d'abord, vous devez ajouter le code suivant à votre template, pour **inclure l'API Javascript d'ezjscore** :

```smarty
{ezscript_require( array( 'ezjsc::jquery', 'ezjsc::jqueryio' ) )}
```

**Remarques :**

* L'exemple présenté utilise jQuery. Vous pouvez également utiliser l'API **YUI** fournie avec eZ.
* Le premier élément du tableau (`ezjsc::jquery`) est facultatif si vous avez déjà inclus 
jQuery dans votre page, et peut même poser problème si la version de jQuery incluse est différente.

Voici maintenant le code Javascript :

```javascript
var  dataSent = {arg0: 'dude', arg1: 'not_used'};
$.ez( 
    'my_function_name::helloMessage::dude::not_used',
    dataSent,
    function(data) {        
        // Si l'appel Ajax a retourné une erreur
        if ( data.error_text ) {
            console.error('Erreur : ' + data.error_text )
        // Si l'appel Ajax a retourné des résultats
        } else if ( data.content.length > 0 ) {
            console.info('Résultat : ' + data.content);
        }
    }
 );
```

**Explications :**

* Si une erreur se produit, le message est disponible dans la variable `data.error_text`.
* Si l'appel réussit, le résultat est présent dans la variable `data.content`.
