[eZ4] Surcharger les icônes du site
===================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/03/23"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

EZ Publish permet de surcharger les différents icônes du site et en particulier ceux du back-office. 
Le fichier de paramétrage de ces icônes est `icon.ini`.

Le système de gestion des icônes n'est malheureusement pas très bien géré, 
et il est complexe de l'utiliser de manière "pluginisée".

Voici deux méthodes pour surcharger les icônes :

* La première, plus simple, oblige à ajouter des fichiers dans un répertoire extérieur à une extension.
* La seconde, plus propre, oblige à copier tous les icônes par défaut dans son extension.

## Méthode simple ##

Créez le fichier `icon.ini.append.php` dans votre extension.

Exemple de fichier `extension/mon_extension/settings/siteaccess/bo/icon.ini.append.php` :

```php
<?php /* #?ini charset="utf-8"?
 
[ClassIcons]
# Mapping entre un class_identifier et l'icône à utiliser
# Le chemin complet depuis les dossiers 16x16 et 32x32 doit être précisé
 
# Nouveaux icônes pour les classes créées
ClassMap[category]=custom/category.png
ClassMap[article]=custom/article.png
 
*/ ?>
```

**Remarques :**

Pour fonctionner, vous devez également créer les fichiers `category.png` et `article.png`, dans :

* `share/icons/crystal-admin/16x16_ghost/custom`
* `share/icons/crystal-admin/16x16_indexed/custom`
* `share/icons/crystal-admin/16x16_original/custom`
* `share/icons/crystal-admin/32x32/custom`

Pour chaque nouvelle classe de contenu avec un icône spécifique, 
ajoutez une entrée dans le fichier `icon.ini.append.php` de votre extension, 
et les images dans les 4 dossiers ci-dessus.

## Méthode propre ##

Commencez par copier le contenu du fichier `share/icons/crystal-admin/icon.ini` 
dans un fichier `icon.ini.append.php`, dans votre extension.

Exemple de fichier `extension/mon_extension/settings/siteaccess/bo/icon.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[IconSettings]
# Dossier dans lequel les thèmes des icônes seront recherchés
Repository=extension/mon_extension/share/icons
 
# Thème d'icônes à utiliser (= nom du dossier à créer dans extension/mon_extension/share/icons)
Theme=mon_theme
 
# Noms des dossiers d'icônes à utiliser en fonction de la situation (= normal/small/...)
# Ces dossiers devront être présents dans le dossier du thème (ici dans mon_theme)
# Contrairement au fichier icon.ini par défaut, on utilise le même répertoire pour les affichages "small", "ghost" et "original"
Size=normal
Sizes[normal]=32x32
Sizes[small]=16x16
Sizes[ghost]=16x16
Sizes[original]=16x16
 
[MimeIcons]
# Icône par défaut quand aucun mimetype ne correspond au fichier
Default=mimetypes/binary.png
 
# Mapping entre les mimetypes et les icônes à utiliser. 
# Le chemin complet depuis les dossiers 16x16 et 32x32 doit être précisé
MimeMap[]
MimeMap[text]=mimetypes/ascii.png
MimeMap[image]=mimetypes/image.png
MimeMap[video]=mimetypes/video.png
MimeMap[audio]=mimetypes/sound.png
MimeMap[application/x-gzip]=mimetypes/tgz.png
MimeMap[application/x-bzip2]=mimetypes/tgz.png
MimeMap[application/x-tar]=mimetypes/tgz.png
MimeMap[application/zip]=mimetypes/tgz.png
MimeMap[application/x-rpm]=mimetypes/rpm.png
MimeMap[application/vnd.ms-powerpoint]=mimetypes/powerpoint.png
MimeMap[application/msword]=mimetypes/word.png
MimeMap[application/vnd.ms-excel]=mimetypes/excel.png
MimeMap[application/pdf]=mimetypes/pdf.png
MimeMap[application/postscript]=mimetypes/pdf.png
MimeMap[text/html]=mimetypes/html.png
MimeMap[video/quicktime]=mimetypes/quicktime.png
MimeMap[video/video/vnd.rn-realvideo]=mimetypes/real_doc.png
 
[ClassGroupIcons]
# Icône par défaut pour un nouveau groupe de classes de contenu
Default=filesystems/folder.png
 
ClassGroupMap[]
ClassGroupMap[content]=filesystems/folder_txt.png
ClassGroupMap[users]=apps/kuser.png
ClassGroupMap[media]=filesystems/folder_video.png
ClassGroupMap[setup]=apps/package_settings.png
 
[Icons]
# Icône par défaut pour un élément divers
Default=mimetypes/empty.png
 
IconMap[]
IconMap[role]=actions/identity.png
IconMap[section]=actions/view_tree.png
IconMap[translation]=apps/locale.png
IconMap[pdfexport]=apps/acroread.png
IconMap[url]=apps/package_network.png
 
[ClassIcons]
# Icône par défaut pour une nouvelle classe de contenu
Default=mimetypes/empty.png
 
# Mapping entre un class_identifier et l'icône à utiliser
# Le chemin complet depuis les dossiers 16x16 et 32x32 doit être précisé
ClassMap[]
 
ClassMap[comment]=mimetypes/txt2.png
ClassMap[common_ini_settings]=apps/package_settings.png
ClassMap[company]=apps/kuser.png
ClassMap[file]=mimetypes/binary.png
ClassMap[folder]=filesystems/folder.png
ClassMap[forum]=filesystems/folder_man.png
ClassMap[forum_message]=mimetypes/txt2.png
ClassMap[forum_reply]=mimetypes/txt2.png
ClassMap[forum_topic]=mimetypes/txt2.png
ClassMap[gallery]=filesystems/folder_image.png
ClassMap[image]=mimetypes/image.png
ClassMap[link]=mimetypes/html.png
ClassMap[person]=apps/personal.png
ClassMap[poll]=mimetypes/log.png
ClassMap[product]=apps/package.png
ClassMap[product_review]=mimetypes/txt2.png
ClassMap[multiprice_product]=apps/package.png
ClassMap[dynamic_vat_product]=apps/package.png
ClassMap[quicktime]=mimetypes/quicktime.png
ClassMap[real_video]=mimetypes/real_doc.png
ClassMap[review]=mimetypes/txt2.png
ClassMap[template_look]=apps/package_settings.png
ClassMap[user]=apps/personal.png
ClassMap[user_group]=apps/kuser.png
ClassMap[weblog]=mimetypes/document.png
ClassMap[windows_media]=mimetypes/video.png
ClassMap[user]=apps/personal.png
ClassMap[user_group]=apps/kuser.png
 
# Nouveaux icônes pour les classes créées
ClassMap[frontpage]=custom/home.png
ClassMap[category]=custom/category.png
ClassMap[article]=custom/article.png
 
*/ ?>
```

Il faut ensuite copier le contenu du dossier share/icons/crystal-admin/16x16_original dans extension/mon_extension/share/icons/mon_theme/16x16 et le contenu du dossier share/icons/crystal-admin/32x32 dans extension/mon_extension/share/icons/mon_theme/32x32, pour conserver les icônes déjà existants.

Les nouveaux icônes sont à ajouter dans les dossiers `.../mon_theme/16x16/custom` 
et `.../mon_theme/32x32/custom`.

**Remarques :**

* Dans cet exemple, le nombre d'icônes est réduit, puisqu'on utilise le même fichier pour 
les affichages `small`, `ghost` et `indexed`.
* Si vous avez activé le `.htaccess` pour votre site, vous devez y ajouter une règle de réécriture, 
sans quoi vos icônes ne seront pas acessibles :

```apacheconfig
RewriteRule ^extension/[^/]+/share/icons/[^/]+/.* - [L]
```
