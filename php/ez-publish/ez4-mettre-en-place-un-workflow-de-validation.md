[eZ4] Mettre en place un workflow de validation
===============================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/06/15"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

EZ Publish propose une interface de **création de workflow**, et notamment de validation. 
Cela permet de définir des étapes de validation lors de la publication d'un contenu.

L'exemple le plus courant est le cas où un rédacteur soumet un article sur le site et 
attend sa validation par le responsable communication du site. C'est seulement une fois validé que 
l'article apparaitra publiquement sur le site.

Voici les étapes à suivre pour mettre en place un tel workflow.

## Créer un processus de workflow générique ##

* Rendez-vous dans le back-office du site, dans l'onglet **Administration** puis sur la page **Workflows**
* Cliquez sur le groupe `standard` ou créez un nouveau groupe de workflow. 
(Le groupe `standard` est largement suffisant sauf si vous avez des dizaines de workflow différents)
* Créez un **Nouveau processus de workflow** nommé par exemple `Workflow de validation générique`.
* **Ajoutez un évènement** de type approbation (`Évènement / Approuver`)
* Choisissez maintenant les éléments gérés par le workflow validation. Vous pouvez choisir :
  * les **sections** pour lesquelles le workflow s'appliquera (À priori `Standard` et/ou `Média`)
  * les **langues** concernées
  * si le workflow agit pour la **création, la modification** de l'objet, ou les deux
  * quels utilisateurs et groupes d'utilisateurs pourront **valider les contenus** avant leur publication
  * quels groupes d'utilisateurs pourront **publier leur contenu sans validation**

**Remarque :** 

Il est conseillé de **choisir les options les plus larges possible**
(ex: pour toutes les langues, pourr la création et la modification, ...). 
Vous pourrez affiner votre configuration par la suite (voir la suites).

## Créer un processus de workflow spécifique ##

Vous venez de créer un processus de workflow générique, qui sera appliqué sur tous les contenus, 
sans distinction. Ce n'est pas vraiment utilisable tel quel. 

On souhaite en général préciser que ça ne doit concerner que telle ou telle classe de contenu. 
Pour cela, on utilise des **multiplexeurs**. 
Les multiplexeurs sont aussi des processus de workflow, mais avec des options différentes des précédentes. 

* Créez un **Nouveau processus de workflow** nommé par exemple `Workflow de validation des articles`.
* **Ajoutez un évènement** de type multiplexer (`Évènement / Multiplexer`)
* Choisissez maintenant les éléments gérés par le workflow de validation. Vous pouvez choisir :
  * les **sections** pour lesquelles le workflow s'appliquera 
  (À priori `Standard` et/ou `Média`, ou un section que vous avez créée)
  * les **langues** concernées
  * les **classes de contenus** concernées
  * si le workflow agit pour la **création, la modification** de l'objet, ou les deux
  * les groupes d'utilisateurs **non concernés par la validation**
  * le processus de workflow à lancer (ex: `Workflow de validation générique`)

## Définir quand déclencher le workflow ##

Votre processus workflow est maintenant créé, mais il vous reste encore à indiquer à eZ Publish à quel 
moment il doit se déclencher. Pour cela :

* Cliquez sur **Déclencheurs** dans le menu de gauche
* Choisissez `content publish before`, pour que le processus de validation soit effectué **avant** 
la publication et affectez-y votre nouveau processus de workflow 
(choisissez **celui avec le multiplexeur, pas le générique**).

## Mettre en place le cronjob workflow ##

Les étapes sont terminées côté administration, il reste cependant une dernière chose à mettre en place : 
le **cronjob workflow**.

Si vous êtes sous Linux/Unix, vous devez mettre à jour votre **crontab** pour que le cronjob soit exécuté à 
intervalle régulier :

```bash
EZPUBLISHROOT=/chemin/vers/ezpublish/mon_site
PHP=/chemin/vers/le/binaire/php
0,15,30,45 * * * *      cd $EZPUBLISHROOT; $PHP runcronjobs.php -q 2>&1 
```

**Explications :**

* `EZPUBLISHROOT` indique le chemin absolu vers la racine de votre site, 
* `PHP` le chemin vers le binaire php 
* la dernière ligne que le fichier `runcronjobs.php` devra être exécuter **tous les quarts d'heure**.

Ce fichier va lancer les différents cronjobs de votre site, définis dans `cronjob.ini`. 
Vérifiez donc que dans ce fichier de configuration, vous ayez ce paramétrage :

```ini
<?php /* #?ini charset="utf-8"?
 
[CronjobSettings]
ScriptDirectories[]=cronjobs
Scripts[]=workflow.php 
 
*/ ?>
```
