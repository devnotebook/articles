[eZ4] Le front-office n'est plus accessible aux utilisateurs anonymes
=====================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/07/11"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Vous souhaitez autoriser l'accès au front-office aux utilisateurs non connectés, 
mais suite à une modification des droits et rôles, le front-office n'est plus accessible ?

Message d'erreur :

```
Accès refusé
Vous n'avez pas le droit d'accéder à cette zone.
```

Lorsque vous modifiez les politiques de sécurité du rôle `Anonymous`, 
veillez bien à **laisser le droit d'accès à la vue `user/login`**, pour le siteaccess du front-office. 
Sans ça, les utilisateurs du site n'auront plus accès au front-office.
