[eZ4] Passer des paramètres à une vue via l'url
===============================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/22"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

En PHP standard, lorsqu'on veut passer des paramètres à une page via l'url, 
on utilise quelque chose de la forme `http://monsite/?param1_name=param1_value&param2_name=param2_value`.

EZ Publish masque ce fonctionnement lors de l'utilisation des modules et des vues. 
Il différencie deux types de paramètres :

* **ordonnées**
* **non ordonnés**

Exemple d'URL :

`http://monsite/index.php/mon_module/ma_vue/value1/(param3url)/value3/(param2url)/value2`

Dans cet exemple on a 3 paramètres :

* `param1`, qui est un paramètre ordonné
* `param2url` et `param3url` qui sont non ordonnés

Le `param1` étant ordonné, vous n'avez pas besoin de le nommer.
En revanche, `param2url` et `param3url` doivent être précisés entre parenthèses 
avant leurs valeurs respectives.

Ce fonctionnement n'est pas automatique, vous devez informer eZ Publish des paramètres susceptibles 
d'être passés à votre vue, grâce au fichier `module.php`.

Par exemple, dans le fichier `monextension/modules/monmodule/module.php` :

```php
<?php
$ViewList = array();
$ViewList['sample'] = array(
    'functions' => array( 'sample' ),
    'script' => 'sample.php',
    'params' => array( 'param1' )
    'unordered_params' => array( 
        'param2url' => 'param2',
        'param3url' => 'param3' 
    )
);
 
$FunctionList['sample'] = array();
```

Pour l'URL d'exemple et cette configuration, on aura dans mon fichier de vue `mavue.php` :

```php
<?php
echo $Params['param1']; // Affiche 'value1'
echo $Params['param2']; // Affiche 'value2'
echo $Params['param3']; // Affiche 'value3'
```

Pour les paramètres non ordonnés `param2url` et `param3url`, on a spécifié dans le fichier `module.php`
que leur valeurs seraient disponibles avec les clés `param2` et  `param3`.

Pour `param1` qui est ordonné, la clé `param1` est disponible sans être renommée.
