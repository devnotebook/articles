[eZ4] Modifier le groupe de classes d'une classe de contenu
===========================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/27"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Si vous avez beaucoup de classes de contenu à créer, ou si vous n'utilisez que très peu  
de celles fournies par eZ Publish, vous avez intérêt à **créer votre propre groupe de classes**.

Pour cela, allez dans **Administration** > **Classes** et cliquez sur le bouton **Nouveau groupe de classes**.

Pour regrouper vos nouvelles classes et les natives dont vous avez besoin (ex : Image, Folder, ...), 
vous pouvez déplacer ou copier/déplacer les classes de contenu d'un groupe vers un autre.

Pour cela, allez sur la page de détail de la classe que vous voulez copier (ou déplacer), 
et cliquez sur ![Le bouton Groupe de classes](./ez4-modifier-le-groupe-de-classes-d-une-classe-de-contenu-01.png) en haut. 
En bas de la page apparaît un bloc permettant de choisir le ou les groupes dans lesquels 
placer la classe de contenu :

![Groupes de la classe](./ez4-modifier-le-groupe-de-classes-d-une-classe-de-contenu-02.png)

**Remarque :**

Pour être sûr de ne pas dégrader un fonctionnement natif sans le vouloir, il est plus sûr de "copier" les
classes natives dans un autre groupe plutôt que de les retirer du groupe initial.
