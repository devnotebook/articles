[eZ4] Activer la publication / dépublication automatique de vos contenus
========================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/07/16"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Par défaut eZ Publish propose les champs **Date de publication** et **Date de dépublication** pour la classe 
de contenu **Article**, mais rien n'est automatisé.

La publication et la dépublication sont deux éléments différents et chacun d'eux doit être activé 
de manière indépendante dans eZ Publish.

## Publication automatique ##

Pour cela on utilise le système de workflow d'eZ Publish :

* Allez dans l'onglet **Administration** du back-office, puis sur **Workflows**.
* Cliquez sur le groupe de workflow `Standard`, ou créez-en un nouveau pour y ranger votre workflow.
* Cliquez sur le bouton **Nouveau processus de workflow**.
* Donnez-lui un nom et créez un évènement de type `Évènement / Attendre jusqu'à`.

Vous devez maintenant informer eZ Publish des classes de contenu qui contiennent un champ date indiquant 
quand publier le contenu. Par exemple le champ **Date de publication** pour la classe de contenu **Article**. 

Pour cela :

* Sélectionnez la classe de contenu concernée (ex: `Article`).
* Cliquez sur le bouton **Mettre à jour les champs**.
* Sélectionnez le champ date qui servira de référence (ex: `Date de publication`).
* Cliquez sur le bouton **Sélectionner l'attribut**.
* Validez

Vous venez donc de créer un workflow qui vérifie la date de publication d'un contenu et décide s'il doit être publié. Ce n'est pas fini, il faut maintenant demander à eZ publish de déclencher ce workflow avant chaque publication d'un contenu. Pour cela :

* Toujours dans l'administration, allez dans **Déclencheurs**.
* Affectez votre nouveau workflow à l'évènement `content publish before`, c'est à dire "avant la publication".
* Validez pour **Appliquer les changements**.

La publication automatique à la date souhaitée est maintenant effective.

**Remarques :**

* Vous pouvez configurer le workflow pour plusieurs classes de contenu en même temps
* Les traitement exécutés par le workflow sont exécutés via des cronjobs fournis par eZ Publish. 
Pour que la publication fonctionne, le cronjob `workflow` doit obligatoirement être exécuté. 
([Plus d'informations sur les cronjobs](./ez-publish/parametrer-les-taches-planifiees).)

## Dépublication automatique ##

La dépublication automatique est réalisée via le script `unpublish.php` fourni par eZ Publish. 
Vous avez seulement deux choses à effectuer pour mettre en place cette fonctionalité :

* Activez le **cronjob principal**, qui exécute entre autres le script `unpublish.php`. 
Pour cela, vous devez planifier l'exécution de la commande suivante au niveau du système, 
au moins un fois par jour :

```bash
php runcronjobs.php -q 2>&1
`

* Définissez les classes de contenu contenant un attribut de type Date, 
dont le nom technique est `unpublish_date`. Pour cela, vous devez surcharger le fichier `content.ini`.

Par exemple, dans le fichier `monextension/settings/content.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[UnpublishSettings]
# Noeuds racines des arborescences pour lesquelles appliquer la dépublication
RootNodeList[]=2
 # Liste des ID des classes de contenu à prendre en compte
ClassList[]=46
 
*/ ?>
```

**Explications :**

* La dépublication ne concernera que les contenus de l'abre des contenus (càd ni les utilisateurs, 
ni ce qu'il y a dans la médiathèque)
* La dépublication ne concernera que les contenus de type `Article` (dont l'ID est par défaut `46`)
