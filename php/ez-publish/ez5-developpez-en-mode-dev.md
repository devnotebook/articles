[eZ5] Développez en mode dev
============================

[:type]: # "Astuce"
[:version]: # "eZ Publish 5"
[:created_at]: # "2013/05/02"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

Avec eZ Publish 5, pour activer la console de développement de Symfony vous devez **modifier la 
configuration Apache**, à priori dans votre virtual host.

Remplacez `index.php` par `index_dev.php` :

```apacheconfig
DirectoryIndex index.php
...
RewriteRule .* /index.php
```

**Remarque :**

EZ Publish et Symfony doivent également être configurés en mode développement.
