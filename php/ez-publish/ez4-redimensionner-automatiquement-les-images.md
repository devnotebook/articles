[eZ4] Redimensionner automatiquement les images
===============================================

[:type]: # "Astuce"
[:sources]: # "[Documentation image.ini](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Configuration-files/image.ini)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/03/29"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Pour éviter que l'image uploadée par le contributeur ne dégrade l'affichage, si elle est trop grande par exemple,
vous pouvez la redimensionner automatiquement.
Le redimensionnement consiste à créer un thumbnail de l'image, qui sera utilisé à sa place à l'affichage.

## Prérequis ##

Pour générer les thumbnails, un convertisseur d'images doit être installé sur la machine. (ex: ImageMagick)
Pour l'activer, créez ou modifiez le fichier ```/settings/override/image.ini.append.php``` :

```ini
<?php /* #?ini charset="utf-8"?
 
[ImageMagick]
IsEnabled=true
# Chemin vers l'exécutable
ExecutablePath=C:/Program Files/ImageMagick-6.7.3-Q16
# Nom de l'exécutable
Executable=convert.exe
 
*/ ?>
```

## Configuration ##

Créez ou modifiez le fichier ```extension/mon_extension/settings/siteaccess/mon_siteaccess/image.ini.append.php``` 
en ajoutant :

```ini
<?php /* #?ini charset="utf-8"?
 
[AliasSettings]
AliasList[]=mon_nom_de_definition
 
[mon_nom_de_definition]
Reference=
Filters[]
Filters[]=geometry/scaledownonly=86;47
 
*/ ?>
```

**Explication :**

```86``` et ```47``` sont la largeur et la hauteur maximalesde l'image en pixels. 
Lors de l'affichage, si l'une de ces deux mesures est trop importante, 
l'image est redimensionnée proportionnellement (= un thumbnail est généré et il remplacera l'image à l'affichage).

**Remarque :**

On peut également ne limiter que la largeur ou la hauteur, en utilisant ```scalewidth``` 
et ```scaleheight``` au lieu de ```scaledownonly```.

## Utilisation ##

Pour que l'image soit redimensionnée, dans le template appelez :

```smarty
{attribute_view_gui attribute=$node.data_map.image image_class='mon_nom_de_definition'}
```

ou :

```smarty
<img src="{$node.data_map.image.content[mon_nom_de_definition].full_path|ezroot( 'no' )}" alt="" />
```
