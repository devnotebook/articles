[eZ4] data error dans le back-office, au chargement des sous-éléments
=====================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/07/30"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Lorsque vous chargez une page de back-office affichant l'arbre des contenus à gauche, 
ou le tableau des sous-éléments, il arrive parfois que le chargement échoue.

![Data error en BO](./ez4-data-error-dans-le-back-office-au-chargement-des-sous-elements-01.png)

Ce chargement est effectué par un appel AJAX, et il fonctionne mal si vous utilisez le **mode_cgi d'apache**.

Si cette erreur se produit trop souvent (notamment en développement), 
vous pouvez désactiver le **mode cgi**. 

Sous Windows, si vous utilisez Wamp, il suffit de décocher **cgi_module** dans 
**apache** > **Modules Apache**, ou de modifier votre `httpd.conf` et commenter la ligne correspondante.

Sous Debian, vous pouvez le faire en ligne de commande :

```bash
sudo a2dismod cgi
sudo service apache2 reload
```
