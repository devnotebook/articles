[eZ4] ID des noeuds racines du site
==================================

[:type]: # "Astuce"
[:sources]: # "[Documentation NoteSettings](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Configuration-files/content.ini/NodeSettings)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/07/15"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Par défaut, eZ Publish initialise les arbres des **contenus**, des **utilisateurs**, des **médias**, ... 
avec des nœuds racines spécifiques.
Cette configuration est spécifiée dans le fichier `content.ini`. Elle peut donc être surchargée.

Voici la configuration par défaut :

```ini
[NodeSettings]
# Le node ID du noeud racine de l'arbre des contenus
RootNode=2
# Le node ID du noeud racine de l'arbre des utilisateurs
UserRootNode=5
# Le node ID du noeud racine de la médiathèque
MediaRootNode=43
# Le node ID du noeud racine de l'arbre de configuration
SetupRootNode=48
# Le node ID du noeud racine de l'arbre de design
DesignRootNode=58
```

**Remarques :**

* Les deux derniers arbres sont dépréciés et masqués par défaut dans eZ Publish.
* Même sans avoir besoin de les modifier, il est intéressant connaître ces valeurs si on en a besoin
lors d'un fetch depuis l'une de ces racines. On peut même envisager de récupérer la valeur via `ezini()` 
plutôt que de l'utiliser en dur.
