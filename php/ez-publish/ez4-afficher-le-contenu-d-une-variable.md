[eZ4] Afficher le contenu d'une variable
========================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/09/21"
[:modified_at]: # "2018/02/07"
[:tags]: # "ez-publish php"

Pour connaitre le contenu d'une variable dans eZ Publish il existe plusieurs pour faciliter le débogage.

## Côté PHP ##

### La fonction print_r() ###

Cette fonction couplée avec les balises `<pre>`, affiche le contenu de la variable de manière 
récursive et structurée.

```php
echo '<pre>';
print_r( $variable, false);
echo '</pre>';
```

affichera

![print_r()](./ez4-afficher-le-contenu-d-une-variable-01.png)

### La fonction var_dump() ###

Cette fonction affiche la même chose `que print_r()`, mais de manière typée.
Contrairement à l'autre, elle affiche aussi les valeurs `null` ou `false`.

```php
echo '<pre>';
var_dump( $variable );
echo '</pre>';
```

affichera

![print_r()](./ez4-afficher-le-contenu-d-une-variable-02.png)

### La fonction eZDebug::writeDebug() ###

Cette fonction affiche la même chose que la fonction `var_dump()`, 
mais dans la partie debug de bas de page (si le mode debug est activé).

```
eZDebug::writeDebug( $variable );
```

affichera

![print_r()](./ez4-afficher-le-contenu-d-une-variable-03.png)

## Dans un template ##

## L'opérateur attribute() ##

Cet opérateur affiche le contenu de tous les attributs de la variable, 
si celle-ci est un objet ou un tableau. Le second paramètre définit le niveau de profondeur à afficher.

```smarty
{$variable|attribute( 'show', 1 )}
```

affichera

![print_r()](./ez4-afficher-le-contenu-d-une-variable-04.png)

```smarty
{$variable|attribute( 'show', 2 )}
```

affichera

![print_r()](./ez4-afficher-le-contenu-d-une-variable-05.png)

## La fonction debug-log ##

Cette fonction fait la même chose que `eZDebug::writeDebug()`, mais côté template.

```smarty
{debug-log var=$variable}
```

affichera

![print_r()](./ez4-afficher-le-contenu-d-une-variable-03.png)
