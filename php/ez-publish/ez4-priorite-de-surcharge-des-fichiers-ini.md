[eZ4] Priorité de surcharge des fichiers .ini
=============================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/10/11"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

Dans eZ Publish on utilise tout le temps les fichiers de configuration `.ini`. 
Il y en a un peu partout dans l'arborescence d'un projet eZ Publish et il est donc facile de s'y perdre.

## Où trouver les fichiers .ini ? ##

* Toujours dans un répertoire settings.
* Dans le répertoire `settings/` à la racine du projet ou dans ceux des extensions.

## Différence en les fichiers .ini et .ini.append.php ##

Les fichiers `.ini` sont les fichiers par défaut, fournis par eZ Publish et par les extensions. 
Lorsque vous surchargez ces fichiers, utilisez les fichiers `.ini.append.php` (ex: `site.ini.append.php`).

Si dans votre extension, vous avez besoin de créer un **nouveau** fichier de configuration, 
utilisez donc l'extension `.ini`.
Si vous distribuer votre extension et que la personne qui l'utilise veur surcharger une propriété, 
elle créera elle, un fichier `.ini.append.php`.

## La hiérarchie de surcharge ##

Voici l'ordre de prise en compte des fichiers `.ini` (et `.ini.append.php`), 
**du moins important au plus important**. Les chemins sont relatifs à la racine de l'application.

* `settings/` : Vous y trouverez tous les fichiers de configuration par défaut. 
Ils impactent indifféremment tous les siteaccess. **Ces fichiers ne doivent pas être modifiés !**
* `extension/mon_extension/settings/siteaccess/mon_siteaccess/` : 
Seule la configuration du siteaccess `mon_siteaccess` sera impactée.
* `settings/siteaccess/mon_siteaccess/` : Idem, seule la configuration du siteaccess `mon_siteaccess` 
sera impactée.
* `extension/mon_extension/settings/` : 
Le cas le plus couramment utilisé. Vous modifier ici toutes les configurations que vous voulez, 
pour tous les siteaccess.
* `settings/override/` : C'est le niveau le plus haut. Tout ce qui est ici sera pris en compte en priorité, 
quel que que soit le siteaccess.

## Priorité entre les extensions ##

L'ordre d'activation des extensions dans le fichier `site.ini` a une importance. 
La première extension déclarée surcharge la deuxième, qui surcharge la troisième et ainsi de suite.
