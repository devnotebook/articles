[eZ4] Passer des paramètres à la vue content/view
=================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/04/16"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

* Appelez le module sans utiliser l'url_alias et ajouter les paramètres entre parenthèses. 
Par exemple `/content/view/full/2/(mon_param)/sa_valeur`
* Les valeurs passées sont récupérables via :

```smarty
{$view_parameters.ma_var} 
```
