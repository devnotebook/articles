[eZ4] Un système de forums élaboré
==================================

[:type]: # "Astuce"
[:sources]: # "[Extension xrowForum](http://projects-legacy.ez.no/svn/xrowforum.zip)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/12/05"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

EZ Publish fournit par défaut 3 classes de contenus pour créer des forums : 
**Forum**, **Forum topic** et **Forum reply**. 
L'extension `ezwebin` fournit les templates associés et vous pouvez ainsi créer des forums, 
des conversations et des messages.

Malgré cela, il manque beaucoup de fonctionnalités courantes attendues sur un forum : 
une messagerie privée, un suivi des discussions, des statistiques, du BBCode, 
un affichage en tableau des forums et des sujets, une gestion de rangs, de la modération, 
la possibilité de signaler un abus, ...

Ces fonctionnalités sont implémentées par l'extension [xrowForum](http://projects-legacy.ez.no/svn/xrowforum.zip). Elle fournit :

* une **interface dans le back-office** pour administrer les modérateurs, les rangs et les paramètres des forums.
* une nouvelle classe de contenu **Forums**, qui permet de regrouper dans un même affichage tous les forums enfants.
* une **interface front-office de messagerie privée** et d'ajout de contacts

L'extension distribuée sur le repo svn est mal internationalisée et mal traduite. 
Voici [la même version corrigée](https://framagit.org/devnotebook/articles/-/blob/master/_files/xrowforum.zip).
