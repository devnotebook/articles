[eZ5] L'arbre des contenus n'est plus disponible dans le back-office
====================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 5"
[:created_at]: # "2013/05/01"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

Avec eZ Publish 5, le mot de passe de la base de données est stocké à deux endroits : **dans eZ** et **dans Symfony**.

Si tout fonctionne correctement dans le back-office, excepté l'arbre des contenus qui n'apparait pas, 
c'est probablement que **les deux mots de passes sont différents**.

Pour vérifier qu'ils sont bien configurés, vérifier dans ces deux fichiers :

* `ezpublish_legacy/override/site.ini.append.php` 
* `ezpublish/config/ezpublish.yml`

S'il y a d'autres problèmes dans le back-office, comme l'impossibilité de naviguer dans les contenus 
via les éléments enfants, c'est sans doute un [problème de cache](./ez-publish/ez4-l-arbre-des-contenus-n-est-plus-acessible-en-front-office-et-en-back-office).
