[eZ4] Récupérer les urls et les chemins vers les répertoires
============================================================

[:type]: # "Marque-page"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/12/12"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

EZ Publish fournit la classe `eZSys`, propose pas mal de méthodes pour récupérer par exemple :

* Le chemin vers le répertoire var
* L'url du serveur
* Le port utilisé
* Le chemin vers le répertoire du projet
* La version de php
* ...

Cette classe se trouve dans `lib/ezutils/classes/ezsys.php`.
