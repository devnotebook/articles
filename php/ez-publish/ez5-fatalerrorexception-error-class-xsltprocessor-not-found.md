[eZ5] FatalErrorException: Error: Class 'XSLTProcessor' not found
=================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 5"
[:created_at]: # "2013/08/19"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

Si vous rencontrez l'erreur suivante après l'installation d'eZ Publish 5 :

```
FatalErrorException: Error: Class 'XSLTProcessor' not found in 
[...]\vendor\ezsystems\ezpublish-kernel\eZ\Publish\Core\FieldType\XmlText\Converter\Html5.php line 77
```

C'est que l'extension `xsl` n'est pas activée pour PHP.
