[eZ4] Un système de chat
========================

[:type]: # "Marque-page"
[:sources]: # "[Extension eZ phpFreeChat](http://projects-legacy.ez.no/svn/ezphpfreechat.zip)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/12/10"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

Si vous avez besoin d'un système de chat dans votre site eZ Publish, 
voici une extension qui peut faire l'affaire : [eZ phpFreeChat](http://projects-legacy.ez.no/svn/ezphpfreechat.zip).

![Interface de l'extension eZ phpFreeChat](./ez4-un-systeme-de-chat-01.png)

Elle fournit un module pour eZ Publish, dont l'unique vue affiche un chat. 
Le chat est composé d'une discussion principale visible par tous les utilisateurs connectés au chat 
et permet des discussions privées entre deux utilisateurs (en cliquant sur leur nom).

**Remarque :**

L'extension utilise la **version 1.3 de phpfreechat**, qui existe maintenant en **2.1.1**.
