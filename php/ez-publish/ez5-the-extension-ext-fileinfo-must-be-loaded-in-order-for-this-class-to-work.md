[eZ5] The extension "ext/fileinfo" must be loaded in order for this class to work
=================================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 5, WampServer 2"
[:created_at]: # "2013/08/08"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

Lorsque vous migrez vers eZ Publish Community Project 2013.06, vous pouvez rencontrer cette erreur :

```apacheconfig
The extension "ext/fileinfo" must be loaded in order for this class to work.
```

**Fileinfo** est une extension pour PHP. 
Elle est généralement déjà packagée sous Linux, mais pas sous Windows avec **WampServer**. 

Activez l'extension `php_fileinfo` et redémarrez apache.
