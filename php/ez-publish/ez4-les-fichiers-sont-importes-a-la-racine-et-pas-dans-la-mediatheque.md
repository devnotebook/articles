[eZ4] Les fichiers sont importés à la racine et pas dans la médiathèque
=======================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/10/23"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

Vous pouvez modifier l'emplacement où les contenus sont créés par défaut, selon leur classe de contenu. 
Ainsi, eZ Publish définit par exemple qu'un contenu **Image** doit être placé dans `medias/images` et 
qu'un contenu Fichier dans `medias/files`.

Si vous renommez ces emplacements (ex: `Fichiers` à la place de `Files`) et 
lancez le cronjob de régénération des urls, `medias/files` devient `medias/fichiers`.

Si vous ne modifiez pas la configuration, eZ Publish ne trouve plus l'emplacement et 
importe les fichiers à la racine.

Vous devez donc surcharger le fichier `content.ini` en réécrivant les emplacements déjà existants et 
surtout en modifiant ceux que vous avez renommés.

Par exemple, dans le fichier `monextension/settings/content.ini.append.php` :

```ini
 
<?php /* #?ini charset="utf-8"?
 
[RelationAssignmentSettings]
ClassSpecificAssignment[]
ClassSpecificAssignment[]=user,user_group;utilisateurs/membres
ClassSpecificAssignment[]=image;medias/images
ClassSpecificAssignment[]=video;medias/multimedia
ClassSpecificAssignment[]=file;medias/fichiers
ClassSpecificAssignment[]=quicktime;medias/multimedia
ClassSpecificAssignment[]=windows_media;medias/multimedia
ClassSpecificAssignment[]=real_video;medias/multimedia
ClassSpecificAssignment[]=flash;medias/multimedia
*/ ?>
```
