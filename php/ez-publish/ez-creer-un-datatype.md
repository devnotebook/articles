[eZ] Créer un datatype
======================

[:type]: # "Marque-page"
[:sources]: # "[Tutoriel complet](https://docs.google.com/document/d/10YZe3at2Sq3Sg4NDQ5M_vcbeLdZDVs_E85Fg_I43Uj8/edit?pli=1)"
[:version]: # "eZ Publish 4/5"
[:created_at]: # "2013/08/05"
[:modified_at]: # "2018/02/12"
[:tags]: # "ez-publish php"

Dans eZ Publish 4, un datatype est un type de champ pour une classe de contenu.

EZ Publish en fournit un certain nombre : image, ligne de texte, texte riche, case à cocher, nombre, ... 
Vous pouvez également créer votre propre datatype, pour facilité la contribution et l'affichage d'un champ.

On peut par exemple imaginer un champ couleur, avec côté back-office une pipette ou une palette de couleur 
pour choisir facilement sa couleur.

Voici un [tutoriel complet pour créer son propre datatype](https://docs.google.com/document/d/10YZe3at2Sq3Sg4NDQ5M_vcbeLdZDVs_E85Fg_I43Uj8/edit?pli=1), 
rédigé par Jérôme Vieilledent et Nicolas Pastorino pour PHP Solutions.
