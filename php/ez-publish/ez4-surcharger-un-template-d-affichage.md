[eZ4] Surcharger un template d'affichage
========================================

[:type]: # "Astuce"
[:sources]: # "[Documentation officielle](http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Template-override-conditions)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/03/25"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

J'ai créé une nouvelle classe de contenu **Article**, et je souhaite modifier le template utilisé pour les articles.

Je dois d'abord commencer par déclarer mon extension comme une **extension de design**, 
dans le fichier `design.ini`.

Exemple de fichier `extension/mon_extension/settings/siteaccess/site/design.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[ExtensionSettings]
DesignExtensions[]=mon_extension
 
*/ ?>
```

Ensuite, il faut déclarer les règles de surcharge que l'on souhaite appliquer, 
dans le fichier `override.ini`.

Exemple de fichier `extension/mon_extension/settings/siteaccess/site/override.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[full_article]
Source=node/view/full.tpl
MatchFile=node/view/full/article.tpl
Subdir=templates
Match[class_identifier]=article
 
*/ ?>
```

* On définit une section `full_article`, pour surcharger l'affichage pleine page (`full`) des articles.
* La source définit le template à surcharger. Dans le cas de l'affichage full, 
c'est `node/view/full.tpl` qui est surchargé.
* `Matchfile` définit le template qui remplacera celui par défaut.
* `Subdir` définit dans quel sous-dossier de `extension/mon_extension/design/mon_design/override/` 
le `matchfile` sera recherché.
* Le `Match[class_identifier]` définit pour quelle(s) classe(s) de contenu la règle de surcharge doit s'appliquer.

Dans mon extension, j'ai donc :

```
mon_extension
    | settings
          | siteaccess
                | site
                      | design.ini.append.php
                      | override.ini.append.php
    | override
          | templates
                | node
                      | view
                           | full
                                 | article.tpl
```

**Remarque :**

La liste des Match disponibles en fonction du template à surcharger est visible dans la 
[documentation officielle](http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Template-override-conditions). 
