Paramétrer les tâches planifiées
================================

[:type]: # "Astuce"
[:sources]: # "[Documentation officielle](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Features/Cronjobs/Running-cronjobs)"
[:version]: # "eZ Publish 4/5"
[:created_at]: # "2012/07/18"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Pour fonctionner correctement, eZ Publish à besoin que des scripts soient exécutés à intervalles réguliers. 
Ces scripts permettent de supprimer les brouillons, effectuer les opérations de workflow, 
mettre à jours alias d'url des contenus, indexer les contenus pour la recherche, ...

Sous linux, la définition des tâches planifiées est stockée dans des **crontab**. 
Ces fichiers définissent quelles commandes doivent être exécutées, par quel utilisateur et à quel intervalle.

EZ Publish 4 fournit une crontab, qui peut être utilisée comme base pour votre installation. 
Il s'agit du fichier `ezpublish.cron`, à la racine.

## Pour eZ Publish 4 ##

```bash
# Chemin absolu vers la racine de votre projet (à modifier).
EZPUBLISHROOT=/path/to/the/ez/publish/directory
 
# Emplacement de l'exécutable PHP en mode ligne de commande (à modifier si besoin).
PHP=/usr/local/bin/php
 
# Liste des cronjobs principaux à exécuter
# à 5h00 tous les matins
0 5 * * * cd $EZPUBLISHROOT && $PHP runcronjobs.php -q 2>&1
 
# Liste des cronjobs "infrequent" d'eZ Publish
# à 4h20 tous les dimanches matin
20 4 * * 7 cd $EZPUBLISHROOT && $PHP runcronjobs.php -q infrequent 2>&1
 
# Liste des cronjobs "frequent" d'eZ Publish
# tous 15 minutes
0,15,30,45 * * * * cd $EZPUBLISHROOT && $PHP runcronjobs.php -q frequent 2>&1
``` 

## Pour eZ Publish 5 ##

A vous de créer le fichier `ezpublish.cron` à la racine, cat celui présent dans le sous-répertoire
`ezpublih_legacy/` n'est pas à jour. 

```bash
# Chemin absolu vers la racine de votre projet (à modifier).
EZPUBLISHROOT=/path/to/the/ez/publish/directory
 
# Emplacement de l'exécutable PHP en mode ligne de commande (à modifier si besoin).
PHP=/usr/local/bin/php
 
# Environnement (prod ou dev)
ENV=prod
 
# Liste des cronjobs principaux à exécuter
# à 5h00 tous les matins
0 5 * * * cd $EZPUBLISHROOT && $PHP ezpublish/console --env=$ENV ezpublish:legacy:script runcronjobs.php -q 2>&1
 
# Liste des cronjobs "infrequent" d'eZ Publish
# à 4h20 tous les dimanches matin
20 4 * * 7 cd $EZPUBLISHROOT && $PHP ezpublish/console --env=$ENV ezpublish:legacy:script runcronjobs.php -q infrequent 2>&1
 
# Liste des cronjobs "frequent" d'eZ Publish
# tous 15 minutes
0,15,30,45 * * * * cd $EZPUBLISHROOT && $PHP ezpublish/console --env=$ENV ezpublish:legacy:script runcronjobs.php -q frequent 2>&1
```

## Exécution ##

Pour que cette crontab soit utilisée par votre système, exécutez la commande suivante, 
en remplaçant le chemin par celui de votre projet :

```bash
crontab /var/www/mon_projet/ezpublish.cron
# Vérification
crontab -l
```

