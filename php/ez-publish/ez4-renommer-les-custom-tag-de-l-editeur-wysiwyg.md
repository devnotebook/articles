[eZ4] Renommer les custom tag de l'editeur WYSIWYG
==================================================

[:type]: # "Astuce"
[:sources]: # "[Documentation CustomTagSettings](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Configuration-files/content.ini/CustomTagSettings)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/07/26"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Par défaut, eZ Publish propose des tags personnalisés dans son éditeur WYSIWYG (eZ Online Editor ou ezoe). 
Si les termes `quote`, `underline` ou autres ne vous conviennent pas, 
vous pouvez les renommer dans le fichier `content.ini` :

Par exemple, dans le fichier `monextension/settings/content.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?

[CustomTagSettings]
AvailableCustomTags[]=quote
CustomTagsDescription[quote]=Citation
 
*/ ?>
```

* `AvailableCustomTags` est la liste des tags personnalisés qui seront disponibles dans l'éditeur
* `CustomTagsDescription` est la liste des libellés qu'auront ces tags

Dans l'exemple ci-dessus, le tag personnalisé `quote`, sera nommé `Citation` dans la liste en BO.
