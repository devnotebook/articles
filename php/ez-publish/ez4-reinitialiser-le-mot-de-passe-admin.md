[eZ4] Réinitialiser le mot de passe admin
=========================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/11"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

Si vous ne vous souvenez plus du mot de passe de l'administrateur, ou si on vous donne un site existant sans le mot 
de passe admin, vous pouvez vous en sortir en consultant directement la base de données.

Utilisez phpmyadmin ou un autre interface, pour consulter la base de données :

* dans la table `ezuser`, trouver la ligne contenant l'utilisateur `admin`.
* modifiez la valeur du champ `password_hash`, pour qu'elle devienne `bab77ccf06f0b1f982e11c60f344c3c2`.

L'utilisateur a maintenant pour mot de passe : `admin`.

**Remarques :**

* Cette technique fonctionne uniquement si le type de hash est 2 (`password_hash_type`).
* La requête sql à exécuter pour changer le mot de passe est la suivante 
(ar défaut l'admin a l'ID 14) :

```sql
UPDATE ezuser SET password_hash="bab77ccf06f0b1f982e11c60f344c3c2" WHERE contentobject_id="14"
```
