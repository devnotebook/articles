[eZ5] Les répertoires dans le cœur d'eZ Publish 5
=================================================

[:type]: # "Marque-page"
[:version]: # "eZ Publish 5"
[:created_at]: # "2013/12/18"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

Le cœur d'eZ Publish 5 se trouve dans le répertoire `vendor/ezsystems/ezpublish-kernel/` de l'application.

Voici quelques répertoires utiles qu'il contient :

* `eZ/Publish/API/Repository/` : contient les interfaces des services avec les signatures de toutes leurs méthodes.
* `eZ/Publish/Core/` : contient l'implémentation des interfaces du répertoire précédent
  * `Repository/` : contient l'implémentation de ces mêmes services.
  * `Base/Exceptions/` : contient toutes les exceptions fournies par eZ, et surtout leurs constructeurs.
  * `Persistence/Legacy/Content/Search/Gateway/CriterionHandler/` : 
  contient les critères de recherche fournis par eZ.
  * `Persistence/Legacy/Content/Search/Gateway/SortClauseHandler/` : 
  contient les méthodes de tri fournies par eZ.

**Remarque :**

Les 4 derniers répertoires, sont **sous `eZ/Publish/Core/`**.
