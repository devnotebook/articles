[eZ4] Echapper un bloc XML pour l'utiliser en javascript
========================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/10"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

## Problème ##

Imaginons que vous vouliez stocker le contenu d'un champ de type bloc XML dans une variable javascript.

D'habitude, on affiche le champ de cette manière dans le template :

```smarty
{attribute_view_gui attribute=$node.data_map.description}
```

D'autre part, si on veut échapper une chaîne pour pouvoir sans risque l'utliiser en javascript, 
on utiliser la fonction de template ```wash()```.

Pas facile à première vue de concilier les deux.


## Solution ##

Stockez le rendu de votre bloc XML dans une variable :

```smarty
{set-block variable=$description}
    {attribute_view_gui attribute=$node.data_map.description}
{/set-block}
```

Échappez la variable pour pouvoir utiliser son contenu dans du code javascript :

```smarty
<script>
jsVar = "{$description|trim()|wash('javascript')}"; 
</script>
```

**Remarque :**

Si la variable contient des retours à la ligne, cela causera des erreurs. 
Pour cela on peut [créer un opérateur de template](./ez-publish/ez4-creer-un-operateur-de-template) 
qui recherchera et supprimera tous ces retours à la ligne.

```php
/**
 * Supprime tous les retours à la ligne.
 * Cette fonction est utile pour injecter du code html généré par un template dans du javascript. (exemple : dans une infobulle Google Map)
 * @param string $string Chaîne à nettoyer
 */
public static function removeWraps( $string ) {
    $string = str_replace( '\r\n', '', $string );
    $string = str_replace( '\n', '', $string );
    $string = str_replace( '\r', '', $string );
    $string = str_replace( CHR( 10 ), '',$string );
    $string = str_replace( CHR( 13 ), '', $string );
    return $string;
}
```
