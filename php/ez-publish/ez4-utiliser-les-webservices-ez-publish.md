[eZ4] Utiliser les webservices eZ Publish
=========================================

[:type]: # "Astuce"
[:sources]: # "[Documentation REST API eZPublish](https://doc.ez.no/eZ-Publish/Technical-manual/4.7/Features/Rest-API/Resources/Services/List-of-services)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/11"
[:modified_at]: # "2018/02/12"
[:tags]: # "ez-publish php"

EZ Publish fournit une l'extension `ezprestapiprovider`, désactivée par défaut.
Elle [expose des webservices basiques](https://doc.ez.no/eZ-Publish/Technical-manual/4.7/Features/Rest-API/Resources/Services/List-of-services) 
permettant par exemple de récupérer les informations d'un nœud.

## Activation ##

Activez l'extension `ezprestapiprovider` dans votre `site.ini`.

Dans `settings/override/site.ini.append.php`, ajoutez une ligne :

```ini
<?php /* #?ini charset="utf-8"?
 
[ExtensionSettings]
ActiveExtensions[]=ezprestapiprovider
 
*/
?>
```

Vous devez avoir activé le `.htaccess` pour votre site (renommez le fichier `.htaccess_root` en `.htaccess`), 
qui contient une règle de réécriture indispensable :

```apacheconfig
DirectoryIndex index.php
 
RewriteEngine On
RewriteRule ^/api/ /index_rest\.php [L]
RewriteRule ^api/ index_rest.php [L]
RewriteRule ^index_rest\.php - [L]
```

Cette règle permet d'appeler les webservices en utilisant `/api/` au lieu de `/index_rest.php/`.

## Authentification ##
 
Pour ajouter une authentification, modifier le fichier `rest.ini`.

Dans `settings/override/rest.ini.append.php`, ajoutez une ligne :

```ini
<?php /* #?ini charset="utf-8"?
 
[Authentication]
RequireAuthentication=enabled
AuthenticationStyle=ezpRestBasicAuthStyle
 
*/
?>
```

Ces lignes définissent que pour accéder aux webservices, une authentification est requise, 
de type **Basic Auth** (= via les rôles d'eZ Publish).
 
Vous pouvez également :

* désactiver l'authentification (`RequireAuthentication=disabled`)
* utiliser **oAuth** (`AuthenticationStyle=ezpRestOauthAuthenticationStyle`).

## Utilisation ##

Une fois ces configurations terminées et les caches vidés, 
vous pouvez accéder aux webservices via une url de type : `http://mon-site.dev/api/ezp/content/node/<node_id>.`

Cette url est composée de plusieurs parties :

* `http://mon-site.dev` :  la base de l'url
* `/api/ezp/` : le nom du préfixe (`api`) et le nom du provider (`ezp`), 
tous deux définis dans `rest.ini`.
* `content/node/` : le nom du service appelé
* `<node_id>` : le ou les paramètres à transmettre

Le webservice appelé dans cet exemple est [documenté ici](https://doc.ez.no/eZ-Publish/Technical-manual/4.7/Features/Rest-API/Resources/Services/List-of-services/Content-node-service). 
On peut ainsi connaitre les paramètres attendus et le format de retour.

**Pour aller plus loin :** [Créer des webservices avec eZ Publish](./ez-publish/ez4-creer-des-webservices-avec-ez-publish)
