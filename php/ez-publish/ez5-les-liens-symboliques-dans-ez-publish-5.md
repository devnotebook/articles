[eZ5] Les liens symboliques dans eZ Publish 5
=============================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 5"
[:created_at]: # "2013/03/15"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

EZ Publish 5 utilise le principe des **assets** de Symfony. Les fichiers statiques (css, js, images, ...) 
que vous utilisez dans vos bundles doivent donc être aussi présents dans le répertoire `web/`.

De plus, tous les fichiers uploadés via le back-office (qui est encore en eZ Publish 4), 
sont stockés par défaut dans le répertoire `ezpublish_legacy/var/storage/`.
De la même manière, ils doivent aussi se retouver dans le répertoire `web/` pour être servis par apache.

Pour mettre à jour votre répertoire `web/`, vous avez le choix entre **copier tous les fichiers statiques**,
ou **créer des liens symboliques**.

Pour cela eZ Publish a surchargé la console de Symfony, et vous propose ces deux commandes 
(à lancer à la racine de votre projet) :

```bash
php ezpublish/console assets:install --symlink web
php ezpublish/console ezpublish:legacy:assets_install --symlink web
```

**Explications :**

* La première commande crée des liens symboliques dans le répertoire `web/`, 
pointant **vers les ressources des bundles**.
* La seconde crée des liens pointant vers les **ressources du répertoire `ezpublish_legacy/`**.

**Remarque :**

L'option `--symlink web` est facultative. Si vous la retirer (ou si elle ne fonctionne pas), eZ Publish
créera des copies des fichiers au lieu des liens symboliques.
