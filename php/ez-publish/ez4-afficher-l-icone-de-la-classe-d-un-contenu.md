[eZ4] Afficher l'icône de la classe d'un contenu
================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/22"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Pour rendre le back-office plus simple et plus convivial, eZ Publish permet de 
[définir un icône pour chaque classe de contenu](./ez-publish/ez4-surcharger-les-icones-du-site).

Pour afficher ces icônes dans vos template, eZ Publish fournit un opérateur de template : `class_icon()`. 
Grâce à lui, vous pouvez simplement insérer l'icône d'une classe de contenu :

```smarty
{'folder'|class_icon( 'small', "Texte alternatif à l'image" )}
```

Ce qui affichera ![Icône de la classe de contenu Dossier](./ez4-afficher-l-icone-de-la-classe-d-un-contenu-01.png).

**Remarques :**

* `small` affiche l'icône en 16x16 px, `normal` en 32x32 px.
* Pour afficher l'icône d'un groupe de classes, utiliser l'opérateur de template 
[classgroup_icon()](http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Template-operators/Miscellaneous/classgroup_icon).
