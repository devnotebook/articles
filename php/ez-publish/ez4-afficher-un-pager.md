[eZ4] Afficher un pager
=======================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/10"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

EZ Publish possède un pager prêt à l'emploi (ou presque) : 
`extension/ezwebin/design/ezwebin/templates/content/search.tpl`.

Pour l'utiliser, appelez-le dans votre template affichant une liste d'éléments :

```smarty
{* Nombre maximum d'éléments par page *}
{def $page_limit = 10}
 
{* Nombre total d'éléments *}
{def $nb_total_articles = fetch( 
    'content', 'list_count',
    hash( 
        'parent_node_id', $node.node_id,
        'class_filter_type', 'include',
        'class_filter_array', array( 'article' ) 
    ) 
)}
 
{* Liste des éléments depuis $view_parameters.offset jusqu'à $page_limit *}
{def $articles = fetch( 
    'content', 'list',
    hash( 
        'parent_node_id', $node.node_id,
        'offset', $view_parameters.offset,
        'limit', $page_limit,
        'class_filter_type', 'include',
        'class_filter_array', array( 'article' ) 
    ) 
)}
 
{* Affichage des noms des éléments *}
{foreach $articles as $article }
    {$article.name|wash()}
{/foreach}
 
{* Affichage du pager *}
{include name=navigator
         uri='design:navigator/google.tpl'
         page_uri=$node.url_alias
         item_count=$nb_total_articles
         view_parameters=$view_parameters
         item_limit=$page_limit}
```

**Remarques :**

* `page_uri` contient l'url de la page dans laquelle vous afficher le pager. 
Si vous êtes dans la vue d'un de vos modules, `page_uri` ressemblera plutôt à 
`mon_module/ma_vue`.
* `item_limit` définit le nombre maximum d'éléments à afficher. 
Ce paramètre doit être passé en paramètre pour le fetch qui récupère la liste des éléments. 
On peut rendre ce nombre paramétrable via un fichier .ini (ex: `mon_extension.ini`).
* Le template du pager peut être surchargé comme n'importe quel autre template.

Le template du pager a besoin des paramètres de la vue (`$view_parameters`). 
Si vous êtes dans la vue d'un de vos modules, vous devez gérer ce paramètre vous même et 
le transmettre à votre template.

Pour cela, ajoutez un paramètre non ordonné à votre vue, dans le fichier `module.php` :

```php
$ViewList['ma_vue'] = array(
    'functions' => array( 'ma_vue' ),
    'script' => 'maVue.php',
    'unordered_params' => array( 'offset' => 'Offset' )
);
```

Dans votre vue récupérez l'offset et passez-le à votre template :

```php
$viewParameters = array( 'offset' => 0 );
if ( isset( $Params['Offset'] ) ) {
    $viewParameters['offset'] = $Params['Offset'];
}
$tpl->setVariable( 'view_parameters', $viewParameters );
```
