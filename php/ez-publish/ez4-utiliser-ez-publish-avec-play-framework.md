[eZ4] Utiliser eZ Publish avec Play! Framework
==============================================

[:type]: # "Astuce"
[:sources]: # "[Site officiel Play! Framework](https://www.playframework.com)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/30"
[:modified_at]: # "2018/02/07"
[:tags]: # "ez-publish api-web php play-framework"

## Play! Framework ##

[Play! est un framework](http://www.playframework.org/) simple de développement MVC web open-source basé sur Java.

Ce n’est pas un framework Java EE puisqu’il n’implémente pas la norme servlet. 
Il n’a donc pas besoin de serveur d’application comme Tomcat ou WebSphere et utilise le « serveur http » 
[JBoss Netty](http://www.jboss.org/netty) permettant de meilleures performances.

Il utilise une **architecture stateless** ce qui rend son utilisation idéale pour le développement de 
services web REST.

## Pourquoi utiliser Play! ? ##

Vous pouvez faire à peu près autant de chose avec PHP qu'avec Java, mais si la solution dont vous avez 
besoin est déjà existante en Java, pourquoi la recréer en PHP ?

Play! étant basé sur Java, vous pouvez l'utiliser comme interface entre une solution Java et 
votre projet eZ Publish.

## L'architecture eZ Publish + Play! ##

![Architecture eZ Publish - Play](./ez4-utiliser-ez-publish-avec-play-framework-01.png)

EZ Publish communique avec Play! en lui soumettant des requêtes REST via HTTP. 
Play! lui retourne en réponse les données au format JSON (par défaut). 
Play! fournit donc des services web utilisables par eZ Publish.
