[eZ4] Modifier automatiquement les droits sur les fichiers eZ Publish sous linux
================================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/07/24"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Plutôt que d'exécuter des commandes de type `chmod` pour modifier les droits de lecture/écriture et 
exécution sur les différents fichiers d'eZ Publish, vous pouvez simplement exécuter un fichier bash 
fourni par eZ : `modfix.sh`.

A la racine du projet, lancez la commande suivante :

```bash
./bin/modfix.sh 
```
