[eZ5] Créer un bundle
=====================

[:type]: # "Astuce"
[:version]: # "eZ Publish 5"
[:created_at]: # "2013/09/18"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

## Qu'est-ce qu'un Bundle ? ##

Bundle est le nom Symfony pour un module, une brique, une extension (terme eZ Publish 4.X). 
Il peut contenir tout ou une partie du design, une API technique, une fonctionnalité particulière, ...

Il peut être dépendant d’autres Bundles, mais est réutilisable. Il est identifié par un **nom de domaine** (namespace) 
et un **nom de Bundle** (finissant par `Bundle`), le tout concaténé.

## Création du Bundle ##

La création du Bundle peut se faire via ligne de commande, 
à partir du répertoire racine de l’application eZ Publish :

```bash
php ezpublish/console generate:bundle --namespace="MonNamespace/MonBundle"
```

**Explications :**

* Le paramètre `namespace` est le **nom du namespace et le nom du Bundle concaténés**.
* Le nom de domaine peut être le nom du projet, celui de l’entreprise, ... Il peut contenir des `/`.
* Le nom du Bundle doit finir par `Bundle`.

### Nom du Bundle ###

L’assistant de création du Bundle propose alors le nom final du Bundle : `MonNamespaceMonBundle`.

Appuyez sur `Entrée` pour conserver ce nom standard.

**Remarque :**

Si vous créez le Bundle principal de votre application, et que vous souhaitez avoir un namespace et 
un nom de Bundle identique, c'est à cette étape que vous pouvez simplifier le nom final pour 
éviter d'avoir `MonNamespaceMonNamespaceBundle`.

### Emplacement du Bundle ###

Vous pouvez ensuite choisir le chemin où se trouvera le Bundle.

Par défaut il sera créé dans le répertoire `src/` à la racine de l’application eZ Publish, 
ce qu'il est préférable de conserver : appuyez sur `Entrée`.

### Format de la configuration ###

Choisissez `yml` comme format de configuration, et validez avec `Entrée`.

### Génération et autres configurations ###

Pour une première fois, choisissez de **générer la structure complète du Bundle**.

Appuyez sur `Entrée` pour confirmer toute la génération.

Même chose pour les questions suivantes.

## Fichiers générés ##

L’assistant de création de Bundle a généré l’arborescence suivante :

![Arborescence bundle généré](./ez5-creer-un-bundle-01.png)

Comme tout bundle Symfony, il se compose de 3 répertoires principaux :

* `Controller/` : vous y créerez vos contrôleurs (équivalents de vos modules dans eZ4).
* `Resources/` : s'y trouvent tous les fichiers non PHP (templates Twig, fichiers de config, js, CSS, ...).
* `Tests/` : répertoire contenant vos tests unitaires.
