[eZ4] Créer des webservices avec eZ Publish
===========================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/21"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

Avant de commencer, vous devez savoir [appeler les webservices natif d'eZ Publish](./ez-publish/ez4-utiliser-les-webservices-ez-publish).

Dans cet exemple, on considèrera que vous venez de créer et d'activer une nouvelle extension : `monextension`.

Cette extension aura l'arborescence suivante :

```
monextension
    classes
        controller
            rest_controller.php
        provider
            rest_provider.php
        view
            view_controller.php
    settings
        rest.ini.append.php
```

**Remarque :**

Une fois tous ces fichiers créés et remplis, vous devrez **vider les caches** et **régénérer les autoloads**.

## Configuration ##

Vous aller devoir déclarer un nouveau **provider** de webservices (ou plusieurs).

Cette déclaration se fait via le fichier `rest.ini`, 
qui permet également de choisir le type d'authentification à utiliser.

Exemple de fichier `rest.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[Authentication]
RequireAuthentication=enabled
AuthenticationStyle=ezpRestBasicAuthStyle
 
[ApiProvider]
ProviderClass[monprovider]=ezxRestApiProvider
 
*/
?>
```

**Explications :**

* `RequireAuthentication=enabled` permet d'activer l'authentification pour l'accès aux webservices
* `AuthenticationStyle=ezpRestBasicAuthStyle` active l'authentification basique d'eZ Publish (via les rôles)
* `[ApiProvider]` est la section pour gérer les providers
* `ProviderClass[monprovider]` définit un nouveau provider. 
C'est la base qui sera utilisée lors de l'appel des webservices qui fournira (ex : http://mon_site/api/monprovider/v1/foo).
* `ezxRestApiProvider` est le nom de la classe PHP qui contiendra le provider

## Provider ##

La classe `ezxRestApiProvider` va lister toutes les routes valides gérées par le provider.

Exemple de fichier `rest_provider.php` :

```php
<?php
 
class ezxRestApiProvider implements ezpRestProviderInterface {
 
    /**
     * Retourne les routes versionnées au provider
     *
     * @return array
     */
    public function getRoutes() {
 
        $routes = array( 
            new ezpRestVersionedRoute( 
                new ezcMvcRailsRoute( '/foo', 'ezxRestController', 'foo' ), 
                1 
            ),
            new ezpRestVersionedRoute( 
                new ezcMvcRailsRoute( '/foo', 'ezxRestController', 'fooBar' ), 
                2 
            )
        );
 
        return $routes;
    }
 
    /**
     * Returns associated with provider view controller
     *
     * @return ezpRestViewController
     */
    public function getViewController() {
        return new ezxRestApiViewController();
    }
 
}
?>
```

**Explications :**

La route `new ezpRestVersionedRoute( new ezcMvcRailsRoute( '/foo', 'ezxRestController', 'fooBar' ), 2 )` 
se lit comme ceci :

* `new ezpRestVersionedRoute()` : on déclare une nouvelle route, qui aura pour version `2`. 
À chaque version, on attribue un service à utiliser.
* `/foo` : nom du service à appeler dans l'url (`/api/monprovider/foo/2`), 
auquel on ajoutera la version.
* `ezxRestController`, nom de la classe contenant la méthode à exécuter.
* `fooBar`, nom de la méthode à utiliser. Cette méthode sera recherchée dans la classe `ezxRestController` 
et avec le nom `doFooBar()`. 
(On préfixe donc le nom de la méthode par `do` et on y on ajoute une majuscule.)

Pour appeler la première version du service et exécuter la fonction `doFoo()`, 
on appelle donc `/api/monprovider/foo/1`, pour la seconde `/api/monprovider/foo/2`.

La méthode `getViewController()` détermine le contrôleur de vue à utiliser. 
C'est lui qui transformera les données pour être utilisables dans une requête HTTP.

## Contrôleur ##

Le contrôleur est la classe qui contient les différentes méthodes préparant les données 
à retourner via les webservices.

Chaque méthode doit retourner un objet `ezcMvcResult`. 
On y stocke tout ce que l'on veut dans le champ variables.

Exemple de fichier `rest_controller.php` :

```php
<?php
class ezxRestController extends ezcMvcController {
 
    public function doFoo() {
        $res = new ezcMvcResult();
        $res->variables['message'] = 'This is FOO !';
        $res->variables['var'] = array( 'Hello world' );
        return $res;
    }
 
    public function doFooBar() {
        $res = new ezcMvcResult();
        $res->variables['message'] = 'This is FOOBAR !';
        return $res;
    }
}
?>
```

## Contrôleur de vue ##

Pour préparer la réponse HTTP retournée par les webservices, et notamment le format de sortie,
on a besoin d'un contrôleur de vue.

Pour cet exemple, les données seront transformées sous forme de **flux JSON**.

Exemple de fichier `view_controller.php` :

```php
<?php
class ezxRestApiViewController implements ezpRestViewControllerInterface {
 
    /**
    * Crée le flux de sortie retourné au contrôleur
    *
    * @param ezcMvcRoutingInformation $routeInfo
    * @param ezcMvcRequest $request
    * @param ezcMvcResult $result
    * @return ezcMvcView
    */
    public function loadView( ezcMvcRoutingInformation $routeInfo, ezcMvcRequest $request, ezcMvcResult $result ) {
 
        return new ezpRestJsonView( $request, $result );
    }
}
?>
```

## Récapitulatif ##

Lorsqu'on appelle un webservice, une méthode est exécutée dans un premier contrôleur.
Elle retourne un objet contenant des données.
Un contrôleur de vue est ensuite utilisée pour formater les données et préparer la réponse HTTP.
