[eZ4] Régénérer les alias d'URL
===============================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/31"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

Lorsque vous déplacez ou renommez des nœuds, eZ Publish ne modifie pas automatiquement leurs alias d'url, 
pour éviter de briser les liens existants.

Pour forcer la mise à jour de l'url, vous pouvez appeler le script `bin/php/updateniceurls.php` 
depuis la racine de votre site :

```bash
php bin/php/updateniceurls.php
```
