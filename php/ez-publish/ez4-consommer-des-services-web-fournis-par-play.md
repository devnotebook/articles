[eZ4] Consommer des services web fournis par Play!
==================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/09/06"
[:modified_at]: # "2018/02/07"
[:tags]: # "ez-publish api-web php play-framework"

Voici une extension eZ Publish permettant de récupérer des webservices fournis par Play! : 
[AT Play Connector](https://framagit.org/devnotebook/articles/-/blob/master/_files/atplayconnector.zip).

Cette extension propose une application Play! d'exemple, correspondant aux exemples des articles précédents. 
Pour chaque service web de cette application, une méthode PHP et un `fetch()` permettent de récupérer son résultat côté eZ Publish.

Ces méthodes sont implémentées dans le fichier `classes/ATWSFunctionCollection.php` 
(à partir de la racine de l'extension), dont voici un extrait :

```php
<?php
 
class ATWSFunctionCollection extends ATWebServices {
 
    /**
     * Appel du service web /test
     *
     * @param string $nom Nom des locataires recherchés
     * @param string $dateNaissance Date de naissance  des locataires recherchés
     * @param string $lang Langue des résultats
     */
    public static function test() {
 
        // Nom du service web
        $wsName = '/test';
 
        // Construction de la requête
        $wsRequest = $wsName;
 
        return array( 'result' => parent::getJSONResult( $wsRequest ) );
    }
 
    /**
     * Appel du service web /helloWorld
     *
     * @param string $name Nom de la personne à saluer
     */
    public static function helloWorld( $name ) {
 
        // Nom du service web
        $wsName = '/helloWorld';
 
        // Construction de la requête avec les paramètres
        $wsRequest = $wsName . '?';
 
        if ( $name != null ) {
 
            $wsRequest .= 'name=' . urlencode( $name );
        }
 
        return array( 'result' => parent::getJSONResult( $wsRequest ) );
    }
}
```

**Explications :**

* Cette classe étend la classe `ATWebservices` également fournie par l'extension.
* Chaque méthode ne fait que préparer le début d'une requête HTTP, 
avec le nom du service web et les paramètres à envoyer.
* Ces requêtes HTTP sont complétées et soumises au services web de Play! 
via la méthode `getJSONResult()` de la classe `ATWebServices`.

Le fichier `modules/atplayws/function_definition.php` (à partir de la racine de l'extension) 
déclare des `fetch()` utilisables depuis les templates :

```php
<?php
 
$FunctionList = array( );
 
$FunctionList['test'] = array(
        'name' => 'test',
        'operation_types' => array( 'read' ),
        'call_method' => array(
                'class' => 'ATWSFunctionCollection',
                'method' => 'test' ),
        'parameter_type' => 'standard',
        'parameters' => array( )
);
 
$FunctionList['hello_world'] = array(
        'name' => 'hello_world',
        'operation_types' => array( 'read' ),
        'call_method' => array(
                'class' => 'ATWSFunctionCollection',
                'method' => 'helloWorld' ),
        'parameter_type' => 'standard',
        'parameters' => array(
                array( 'name' => 'name',
                        'type' => 'string',
                        'required' => true ),
        )
);
```

**Explications :**

Pour chaque fetch, on déclare son **nom**, la **méthode PHP correspondante** à appeler, 
ainsi que les **paramètres** à lui fournir.

**Remarque :**

Pour plus de détails sur l'utilisation de l'extension, consultez le fichier `README.txt` présent à sa racine.
