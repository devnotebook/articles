[eZ4] Indexer le contenu des fichiers
=====================================

[:type]: # "Astuce"
[:sources]: # "[Extension eztika](http://projects-legacy.ez.no/svn/eztika.zip)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/10"
[:modified_at]: # "2021/07/14"
[:tags]: # "ez-publish php"

Pour que le contenu des fichiers soit indexé et ainsi recherchable, 
il faut utiliser l'extension [eztika](http://projects.ez.no/eztika).

Elle convertit les documents .pdf, .xls ou autres, en fichiers texte, 
qu'elle peut ensuite parser et indexer. Elle a besoin pour cela d'exécuter le fichier 
`extension/eztika/bin/eztika`.

* Installez et activez l'extension **eztika**
* Vérifier que le fichier `extension/eztika/bin/eztika` est bien exécutable
* Régénérez les autoloads :

```bash
php bin/php/ezpgenerateautoloads.php -e
```

En cas de problème, consultez le fichier `INSTALL.TXT` présent dans le répertoire de l'extension.
