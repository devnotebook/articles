[eZ4] Modifier les paramètres d'envoi de mail
=============================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/10"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

EZ Publish propose un système d'[envoi de mails, par collecte d'informations](./ez-publish/ez4-creer-un-formulaire-d-envoi-de-mail). 
Le template du mail envoyé est 
`extension/mon_extension/design/mon_design/templates/content/collectedinfomail/my_form.tpl`, 
avec `my_form` le nom de la classe de contenu gérant le formulaire d'envoi de mail.

Les paramètres du mail doivent être définis directement dans ce template, 
par **définition de blocs**. 

Pour modifier le sujet du mail, utilisez par exemple:

```smarty
{set-block scope=root variable=subject}Mon sujet de mail{/set-block}
```

La liste des paramètres acceptés (ici `subject`) sont listés dans le fichier 
`kernel/content/collectinformation.php` à partir de la `ligne 272` :

* `subject` : sujet de l'email
* `email_receiver` : email du destinataire
* `email_cc_receivers` : destinataires en copie
* `email_bcc_receivers` : destinataires en copie cachée
* `email_sender` : email de l'expéditeur
* `email_reply_to` : email de réponse
* `redirect_to_node_id` : node_id du nœud vers lequel effectuer une redirection après l'envoi de l'email
