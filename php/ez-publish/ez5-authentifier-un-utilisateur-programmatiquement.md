[eZ5] Authentifier un utilisateur programmatiquement
====================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 5"
[:created_at]: # "2018/02/12"
[:modified_at]: # "2018/02/12"
[:tags]: # "ez-publish php"

Pour authentifier programmatiquement un utilisateur en front-office, vous pouvez utiliser cette méthode :

```php
<?php

use eZ\Publish\Core\MVC\Symfony\Security\User as SecurityUser;
use eZ\Publish\API\Repository\Values\User\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
 

    /**
     * Connecte l'utilisateur en argument.
     *
     * @param User $user
     *   Utilisateur eZ Publish récupéré depuis le repository
     *
     * @throws \Exception Si une erreur survient lors de la récupération du service de gestion de jeton.
     */
    public function login(User $user)
    {
        // Authentification pour Symfony
        $roles =['ROLE_USER'];
        $security_user = new SecurityUser($user, $roles);
        $security_user->setAPIUser($user);

        $token = new UsernamePasswordToken($security_user, null, 'ezpublish_front', $roles);
        $this->container->get('security.token_storage')->setToken($token);
        
        // Authentification pour le repo eZ Publish
        $this->repository->setCurrentUser($user);
    }
```
