[eZ4] Error ocurred using URI: /stylesheets/t2/classes-colors.css
==================================================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/22"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

Il peut arriver de trouver ce message dans `error.log` :

```
[ May 22 2012 12:44:51 ] [127.0.0.1] index:
Undefined module: stylesheets
[ May 22 2012 12:44:51 ] [127.0.0.1] error/view.php:
Error ocurred using URI: /stylesheets/t02/site-colors.css
[ May 22 2012 12:44:51 ] [127.0.0.1] index:
Undefined module: stylesheets
[ May 22 2012 12:44:52 ] [127.0.0.1] error/view.php:
Error ocurred using URI: /stylesheets/t02/classes-colors.css
```

Ce message est dû à deux fichiers css qui ne sont pas trouvés par eZ Publish.

Dans les fichiers `design.ini` et `page_head_style.tpl`, 
est définit par défaut un fichier css à inclure :

```ini
<?php /* #?ini charset="utf-8"?
 
[StylesheetSettings]
SiteCSS=stylesheets/t02/site-colors.css
ClassesCSS=stylesheets/t02/classes-colors.css
 
*/ ?>
```

Ces fichiers n'existent pas, car le dossier n'est pas `t02/` mais `t2/` 
et parce qu'il manque le chemin depuis la racine du site (`design/base/`).

Si vous voulez éviter qu'eZ Publish essaie d'inclure ces fichiers css, 
il suffit de surcharger le fichier `page_head_style.tpl` ou le fichier `design.ini`. 
La deuxième solution est la plus simple. 

Ajoutez donc ça dans `monextension/settings/design.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[StylesheetSettings]
ClassesCSS=
SiteCSS=
 
*/ ?>
```
