[eZ4] Créer un cronjob
======================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/22"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

Pour effectuer une tâche à intervalle régulier, eZ Publish propose un système de scripts : des **cronjobs**.

Pour créer un cronjob, vous devez avoir créé et activé votre extension. 
Dans le répertoire de l'extension, créez un répertoire `cronjobs/`, qui contiendra vos scripts.

## Script ##

Voici un exemple simple d'un script qui affiche Hello world, et logue qu'il a bien été lancé.

```php
<?php
/*
 * Script d'exemple, affichant Hello world.
 */
 
//-----------------------------------------------------------------------------------------------------\\
//-------------------------------------------  INITIALISATION  ----------------------------------------\\
//-----------------------------------------------------------------------------------------------------\\
 
// Inclusion de la classe eZUser
require_once( 'kernel/classes/datatypes/ezuser/ezuser.php' );
 
// Répertoire des logs
$logFolder = 'var/monsite/log/cronjobs/';
// Fichier de logs
$logFile = 'sample.log';
 
// Connexion d'un utilisateur spécifique ayant les droits
// nécessaires pour effectuer le traitement
$cronjobUser = ezUser::loginUser( 'user_login', 'user_password' );
 
//-----------------------------------------------------------------------------------------------------\\
//---------------------------------------------  TRAITEMENTS  -----------------------------------------\\
//-----------------------------------------------------------------------------------------------------\\
 
// Log du début de l'exécution du script
eZLog::write( 'Démarrage du script d\'exemple', $logFile, $logFolder );
 
if ( is_object( $cronjobUser ) ) {
    
    $message = 'Hello world !';
    
    // Autres traitements...
    
    // Affichage du message sur la sortie standard (invite de commande)
    $cli->output( $message );
    
} else {
 
    $message = 'ERROR : L\'utilisateur "cronjobUser" n\'a pas pu être identifié !';
    
    // Stockage de l'erreur dans les logs
    eZLog::write( $message, $logFile, $logFolder );
    
    // Affichage du message d'erreur sur la sortie standard
    $cli->output( $message );
}
 
eZLog::write( 'Fin du script d\'exemple', $logFile, $logFolder );
 
?>
```

**Explications :**

* On connecte un utilisateur spécifique aux cronjobs, 
disposant des droits nécessaires pour effectuer les opérations souhaitées 
(dans cet exemple ça n'a pas d'intérêt puisqu'on ne fait pas d'autres opérations en base).
* On logue le début et la fin de l'exécution du script, pour être sûr que le script a bien été exécuté, 
avec succès ou non, dans un fichier portant le même nom que le cronjob.
* On affiche des informations sur le traitement en cours sur la sortie standard, 
via la variable `$cli` directement accessible dans le script et sa méthode `output()`.
* Le mot de passe et le login de l'utilisateur ainsi que le chemin vers le dossier des logs, 
peuvent être stockés dans un fichier de configuration, surtout s'ils communs à plusieurs scripts.

## Configuration ##

Pour qu'eZ Publish trouve votre cronjob, vous devez le déclarer dans le fichier `cronjob.ini`.

Par exemple dans le fichier `monextension/settings/cronjob.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[CronjobSettings]
# Déclare que l'extension possède des cronjobs
ExtensionDirectories[]=monextension
 
# Déclare un cronjob au nom 'sample', dont le script est dans le fichier monextension/cronjobs/sample.php
[CronjobPart-sample]
Scripts[]=sample.php
 
*/ 
?>
```
N'oubliez pas de **vider les caches** ensuite.

## Utilisation ##

Pour exécuter ce cronjob, utilisez la commande suivante à la racine de votre site :

```bash
php runcronjobs.php sample
```

**Remarques :**

* EZ Publish fournit déjà des cronjobs, présents dans le répertoire `cronjobs/` à la racine. 
Ils sont [documentés ici](http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Features/Cronjobs/The-cronjob-scripts).
* Pour lancer un cronjobs sur un seul siteacces, précédez son nom par `-s mon_siteacces` dans la commande.
* A priori, on souhaite que nos cronjobs s'exécutent à intervalle régulier comme des tâches planifiées.
Il suffit donc au niveau système de paramétrer la **crontab** pour d'exécuter la commande.
