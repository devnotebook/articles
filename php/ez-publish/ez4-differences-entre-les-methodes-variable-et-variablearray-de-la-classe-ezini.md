[eZ4] Différences entre les méthodes variable() et variableArray() de la classe eZINI
=====================================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2013/04/25"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

La classe `eZINI` propose deux méthodes pour récupérer des variables sous forme de tableau : 
`variable()` et `variableArray()`.

## variable() ##

C'est la méthode habituelle que vous utilisez pour récupérer des chaînes de caractères.
Par exemple pour récupérer la valeur dans cette configuration :

```ini
[MySection]
MyProperty=value
```

La méthode retournera :

```
value1
```

Elle fonctionne aussi pour un tableau de valeurs :

```ini
[MySection]
MyProperty[]
MyProperty[]=value1
MyProperty[]=value2
...
```

La méthode retournera :

```
Array (
  '0' => value1
  '1' => value2
  ...
)
```

## variableArray() ##

Cette méthode permet de récupérer un tableau de valeurs pour ce genre de configuration :

```ini
[MySection]
MyProperty=value1;value2;value3;...
```

La méthode retournera :

```
Array (
  '0' => value1
  '1' => value2
  '2' => value3
  ...
)
```
