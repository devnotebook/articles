[eZ4] Définir les fichiers css et javascript à inclure pour la page courante
============================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/02/11"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

Il y a **3 moyens d'inclure des fichiers CSS et Javascript** dans une page eZ Publish.

## Directement dans le header ##

Comme toute page html, on peut définir des fichiers CSS et Javascript dans la balise `<header>`. 
À priori vous pourrez le modifier dans votre fichier `pagelayout.tpl`.

Par exemple, dans le fichier `monextension/design/mon_design/override/templates/pagelayout.tpl` :

```smarty
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash}" lang="{$site.http_equiv.Content-language|wash}">
<head>
 
{def $pagedata         = ezpagedata()
     $pagestyle        = $pagedata.css_classes
     $locales          = fetch( 'content', 'translation_list' )
     $pagedesign       = $pagedata.template_look
     $current_node_id  = $pagedata.node_id
     $current_node     = fetch( 'content', 'node', hash( 'node_id', $pagedata.node_id ) )}
 
<link rel="stylesheet" type="text/css" href="/extension/monextension/design/mon_design/stylesheets/mon_design.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js" charset="utf-8"></script>
 
{include uri='design:page_head.tpl'}
{include uri='design:page_head_style.tpl'}
{include uri='design:page_head_script.tpl'}
</head>
```

**Explication :**

Une feuille CSS présente dans `monextension/` et un script JS ont été inclus à la page.

**Remarque :**

De la même manière, le fichier JS peut être inclus à la fin du `<body>` à la place, 
ce qui est préférable d'un point de vue optimisation.

##Dans les propriétés du fichier design.ini ##

Vous pouvez définir des fichiers CSS et Javascript à inclure sur toutes vos pages, 
en front-office ou en back-office, dans le fichier `design.ini`.

Par exemple, dans le fichier `monextension/settings/design.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[StylesheetSettings]
# CSS Pour le front office
FrontendCSSFileList[]=structure.css
FrontendCSSFileList[]=global.css
FrontendCSSFileList[]=override.css
FrontendCSSFileList[]=wysiwyg.css
FrontendCSSFileList[]=noscript.css
 
[JavaScriptSettings]
JavaScriptList[]=https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js
```

Ces fichiers seront automatiquement ajoutés via les templates `page_head_style.tpl` et 
`page_head_script.tpl` à inclure dans votre balise `<header>`. 

Par exemple, dans le fichier `monextension/design/mon_design/override/templates/pagelayout.tpl` :

```smarty
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$site.http_equiv.Content-language|wash}" lang="{$site.http_equiv.Content-language|wash}">
<head>
{include uri='design:page_head_style.tpl'}
{include uri='design:page_head_script.tpl'}
</head>
```

## Pour une page spécifique uniquement ##

Si vous avez besoin d'un fichier CSS ou Javascript particulier pour une page précise de votre site, 
l'extension `ezjscore` fournie avec eZ Publish propose des **opérateurs de template** bien pratiques :

```smarty
{ezcss_require('override.css')}
{ezscript_require('lib/jquery-ui-1.8.16.custom.min.js')}
```

**Remarques :**

* Si vous utilisez `ezcss_require()` ou `ezscript_require()`, les fichiers que vous passez en arguments 
sont mémorisés par eZ Publish.  
C'est à l'appel des opérateurs `ezcss_load()` et `ezscript_load()` qu'ils seront inclus effectivement au 
flux de la page. Il vous faut donc ajoutez ces derniers dans votre header/pied de page. 

* Si le mode développement est désactivé, `ezcss_load()` et `ezscript_load()` vont automatiquement 
minifier tous le CSS et le Javascript en deux fichiers, qui seront mis en cache.  

* Si l'une de vos feuilles CSS contient un `@import` pour en inclure une autre, 
cette dernière ne sera pas mis en cache et sera alors introuvable. **Évitez-donc d'utiliser @import !**
