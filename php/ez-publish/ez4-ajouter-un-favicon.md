[eZ4] Ajouter un favicon
========================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/10"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

Par défaut, le favicon utilisé est `design/standard/images/favicon.ico`.

Vous pouvez le [surcharger comme un template](./ez-publish/ez4-surcharger-un-template-d-affichage), 
en créant le fichier `mon_extension/design/mon_design/images/favicon.ico`, 
et en ajoutant la règle de surcharge dans `override.ini` :

```ini
<?php /* #?ini charset="utf-8"?
 
[favicon]
Source=images/favicon.ico
MatchFile=images/favicon.ico
Subdir=templates
 
*/ ?>
```
