[eZ4] Obtenir un noeud courant dans la vue d'un module
=====================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/04/16"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

* Déclarez le paramètre `NodeID` comme **paramètre ordonné** de la vue.
* Récupérez le nœud, côté PHP, dans la vue de votre module :

```php
$nodeID = intval( $Params['NodeID'] );
$node   = eZContentObjectTreeNode::fetch( $nodeID );
```

* Déclarez la variable `$node` pour le template :

```php
$tpl->setVariable( 'node', $node );
```

* Renvoyer le `node_id` dans le résultat du module :

```php
$Result['node_id'] = $myNodeId;
return $Result;
```
