[eZ4] Créer ses fetch personnalisés
===================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2013/02/14"
[:modified_at]: # "2018/02/08"
[:tags]: # "ez-publish php"

EZ Publish fournit un grand nombre de fonctionnalités, accessibles dans les templates via des fetch 
([voir la documentation](http://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Template-fetch-functions)).

Vous pouvez créer vos propres fetch via le système de function_definition des modules.

## Le module ##

* Commencez par créer un nouveau module (ou utilisez un module déjà existant dans votre extension).
* Si c'est un nouveau, vous devez le déclarer dans le fichier `module.ini`.

Par exemple, dans le fichier `monextension/settings/module.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[ModuleSettings]
ExtensionRepositories[]=monextension
ModuleList[]=monmodule
```

## Le PHP ##

Vous n'avez qu'un seul fichier à créer dans votre extension : `modules/monmodule/function_definition.php`.

Ce fichier contient un tableau php qui liste les fonctions disponibles. 
Pour chacune d'elle, vous préciserez **son nom**, **son type** (lecture ou écriture), **la méthode PHP à appeler** 
et **les paramètres** à lui passer.

Par exemple :

```php
<?php
$FunctionList = array();
 
$FunctionList['tree_unique'] = array( 
    'name' => 'tree_unique',
    'operation_types' => array( 'read' ),
    'call_method' => array( 
        'class' => 'ATContentFunctionCollection',
        'method' => 'fetchObjectTree' 
    ),
    'parameter_type' => 'standard',
    'parameters' => array( 
        array( 
            'name' => 'parent_node_id',
            'type' => 'integer',
            'required' => true 
        )  
    ) 
);
```

**Explications :**

* Cette fonction s'appelle `tree_unique`
* Elle est de type `lecture` (elle n'effectue pas de modifications sur les données, mais en retourne)
* Elle retourne le résultat de la méthode `fetchObjectTree()` de la classe `ATContentFunctionCollection`
* Elle a un paramètre obligatoire : l'ID du nœud parent

**Remarque :**

Vous devez bien sur avoir créer une classe ATContentFunctionCollection possédant la méthode fetchObjectTree (). 
Le nom de la classe n'a pas d'importance, mais dans le cœur d'eZ Publish on ajoute FunctionCollection pour mettre en 
évidence les fonctionnalités utilisables dans les fetch.

## Dans les templates ##

Vous pouvez maintenant utiliser votre fetch **après avoir vider les caches**. 
Utilisez-le comme n'importe quel fetch natif :

```smarty
{def $node_list = fetch( 
    'monextension', 'tree_unique', 
    hash( 'parent_node_id', $node.node_id ) 
)}
```
