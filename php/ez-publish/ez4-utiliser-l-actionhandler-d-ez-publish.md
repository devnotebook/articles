[eZ4] Utiliser l'ActionHandler d'eZ Publish
===========================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/06/06"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

## Les actions dans eZ Publish ##

Tout d'abord, qu'est-ce qu'une action dans eZ Publish ?

Il s'agit d'un traitement effectué à la soumission d'un formulaire pointant vers `content/action`.

**Exemple :**

```smarty
<form method="post" action="{'content/action'|ezurl( 'no' )}">
    <label for="message">{'Message'|i18n( 'sample' )} :</label>
    <textarea id="message" name="message"></textarea>
 
    <input type="hidden" name="ContentObjectID" value="{$node.object.id}" />
    <input type="hidden" name="NodeID" value="{$node.node_id}" />
    <input type="submit" name="LogMessageButton" value="Log" />
</form>
```

Le formulaire est constitué d'un **textarea** permettant à l'utilisateur de saisir son message, 
d'un **bouton submit** et de **champs cachés**. 
Notez que le champ `ContentObjectID` est obligatoire, même si l'action n'utilise pas sa valeur 
(vous pouvez retourner `-1` dans ce cas).

Le nom du submit correspond au nom de l'action à effectuer. EZ Publish référence déjà beaucoup 
d'actions (notamment toutes celles relatives aux boutons submit du back-office), mais vous pouvez en rajouter.


## Créer ses propres actions ##

Supposons qu'on veuille loguer le message du textarea et le `node_id` de la page 
à la soumission du formulaire.

Pour qu'eZ Publish trouve vos actions, vous devez déclarer votre extension comme ayant des actions, 
dans le fichier `site.ini`.

Par exemple dans `extension/monextension/settings/site.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[ActionSettings]
ExtensionDirectories[]=monextension
 
*/ ?>
```

Dans votre extension, créez un répertoire `actions/`, contenant le fichier `content_actionhandler.php` :

```php
<?php
include_once( 'lib/ezutils/classes/ezoperationhandler.php' );
 
function monextension_ContentActionHandler( &$module, $http, $objectID ) {
 
    // Action de loguer un message
    if ( $http->hasPostVariable( 'LogMessageButton' ) ) {
        // Si le formulaire soumis contient un message non vide
        if ( $http->hasPostvariable( 'message' ) && $http->postvariable( 'message' ) != '' ) {
            $message = $http->postvariable( 'message' );
            $nodeID  = $http->postvariable( 'NodeID' );
 
            $logMessage = 'La page ' . $nodeID . ' a été affichée et on a soumis le message : ' . $message;
            eZLog::write( $logMessage );
        }
 
        // Redirection vers la page du formulaire
        $module->redirectTo( '/content/view/full/' . $nodeID );
        return;
    }
}
?>
```

**Explications :**

* Ce fichier contient une fonction dont le nom est obligatoirement préfixé par le celui de votre extension.
* Toutes les actions sont listées, soit par une suite de if, soit dans un switch, 
en fonction du nom de l'action envoyé par le formulaire (`LogMessageButton` dans cet exemple).
* Chaque action redirige vers une page, ou vers la vue d'un module.

**Remarque :**

N'oubliez pas de **vider les caches** et **régénérer les autoloads** pour qu'eZ Publish trouve votre nouvel 
ActionHandler.
