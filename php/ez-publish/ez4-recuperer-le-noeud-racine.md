[eZ4] Récupérer le noeud racine
===============================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/04/12"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Pour récupérer le nœud racine dans un template, au moins deux méthodes sont possibles :

```smarty
{def $root_node = fetch( 'content', 'node', hash( 'node_path', '/') )}
```

et

```smarty
{def $root_node = fetch( 'content', 'node', hash( 'node_id', ezini( 'NodeSettings', 'RootNode', 'content.ini' ) )}
```

**Remarque :**

La deuxième solution retourne le **nœud racine pour le siteaccess courant** alors que la première 
retourne **toujours le nœud root** (dont le node_id est 2).
