[eZ4] Créer un opérateur de template
====================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/06/01"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Les opérateurs de template sont des fonctions utilisables dans les template, 
qui pointent vers des fonctions PHP. EZ Publish en fournit plusieurs dizaines, 
comme `explode()`, `fetch()`, `i18n()`, ...

Voici comment créer vos propres opérateurs, effectuant tel ou tel traitement, et retournant une valeur de retour.

## MonAPI ##

Dans cet exemple on supposera qu'on possède la classe `MonAPI`, 
contenant des méthodes PHP "utiles" à notre projet :

```php
/**
 * Classe contenant les méthodes PHP utiles au projet.
 *
 * Rq : Tout nouvel opérateur doit également être référencé 
 * dans le fichier eztemplateautoload.php.
 */
class MonAPI {
 
    /**
     * Méthode retournant "Hello world XX YY" avec XX remplacé par $who et YY remplacé par $suffix.
     * 
     * @param string $who A qui dire bonjour
     * @param string $suffix Ponctuation de fin de bonjour
     */
    public static function sayHelloWorld( $who, $suffix ) {
        return 'Hello world ' . $who . ' ' . $suffix;
    }
 
    /**
     * Méthode retournant "Goodbye" autant de fois que $nb
     * 
     * @param int $nb Nombre de fois à dire au revoir
     */
    public static function sayGoodbye( $nb ) {
    
        $goodbye = '';
        
        for ( $i=0; $i < $nb; $i++ ) {
            $goodbye .= 'Goodbye';
        }
    
        return $goodbye;
    }
}
```

Les méthodes ci-dessus étant indispensables et pratiques, 
vous souhaitez pouvoir les utiliser directement dans vos templates.

Pour cela, vous devez avoir créé et activé votre extension. 
Dans le répertoire de l'extension, créez un dossier `autoloads/`, qui contiendra vos opérateurs de template.

## Configuration ##

Pour qu'eZ Publish charge les classes PHP contenues dans ce dossier, 
vous devez déclarer votre extension comme ayant un dossier `autoloads/`, 
dans le fichier `site.ini.append.php` de votre extension.

Par exemple dans `extension/monextension/settings/site.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[TemplateSettings]
ExtensionAutoloadPath[]=monextension
 
*/ ?>
```

EZ Publish va maintenant chercher le fichier `extension/monextension/autoloads/eztemplateautoload.php`. 
Créez donc ce fichier de la forme :

```php
$eZTemplateOperatorArray = array();
 
$eZTemplateOperatorArray[] = array(
    'class' => 'MaClasseTemplateOperators',
    'operator_names' => array( 'say_hello_world', 'say_goodbye' )
);
```

**Explications :**

* Vous déclarez dans un tableau toutes les classes PHP contenant vos opérateurs de template. 
Dans cet exemple on a seulement la classe `MaClasseTemplateOperators` 
(garder "TemplateOperators" à la fin du nom de la classe permet d'identifier rapidement son utilité).
* Vous devez indiquer son nom et la liste des opérateurs qu'elle contient.

## MaClasseTemplateOperators ##

Dans le fichier `MaClasseTemplateOperators.php`, on aura :

```php
/**
 * Classe contenant différents opérateurs de templates de test.
 *
 * Rq : Tout nouvel opérateur doit également être référencé dans le fichier eztemplateautoload.php
 */
class MaClasseTemplateOperators {
 
    function MaClasseTemplateOperators() {
    }
 
    function operatorList() {
        return array( 'say_hello_world', 'say_goodbye' );
    }
 
    function namedParameterPerOperator() {
        return true;
    }
 
    function namedParameterList() {
        return array(
            'say_hello_world' => array(
                'who' => array( 
                    'type' => 'string',
                    'required' => true,
                    'default' => 'everybody' 
                ),
                'suffix' => array( 
                    'type' => 'string',
                    'required' => true,
                    'default' => '!'
                )
            ),
            'say_goodbye' => array(
                'nb' => array( 
                    'type' => 'int',
                    'required' => true,
                    'default' => 10
                )
            ),
        );
    }
 
    function modify( $tpl, $operatorName, $operatorParameters, &$rootNamespace, &$currentNamespace, &$operatorValue, &$namedParameters ) {
        switch ( $operatorName )
        {
            case 'say_hello_world':
            {
                $who           = $namedParameters['who'];
                $suffix        = $namedParameters['suffix'];
                $operatorValue = MonAPI::sayHelloWorld( $who, $suffix );
            }
            break;
 
            case 'say_goodbye':
            {
                $nb            = $namedParameters['nb'];
                $operatorValue = MonAPI::sayGoodbye( $nb );
            }
            break;
        }
    }
}
```

**Explication :**

Cette classe contient 3 méthodes importantes :

* `operatorList()` : C'est la liste des noms de vos opérateurs.
* `namedParameterList()` : C'est la liste des paramètres que peuvent recevoir vos opérateurs. 
Pour chacun d'eux, vous indiquer **le nom et le type du paramètre**, 
ainsi que **sa valeur par défaut** et **s'il est obligatoire**.
* `modify()` : Cette méthode va être utilisée par eZ Publish lorsqu'il parsera votre template. 
C'est elle qui va appeler la "vraie" méthode PHP (de la classe `MonAPI`) avec les paramètres 
utilisés dans le template. et qui va retourner le résultat au template.

**Remarque :**

Vous ne devez ni modifier la signature de la méthode `modify()`, ni le nom de la variable `$operatorValue`, 
sans quoi le template operator ne fonctionnera plus.

## Pour résumer ##

Dans votre extension vous devez avoir :

```
- monextension
    - autoloads
        - eztemplateautoload.php
        - MaClasseTemplateOperators.php
    - classes
        - MonAPI.php
    - settings
        - site.ini.append.php
```

Vous devez également **régénérer les autoloads**, pour qu'eZ Publish trouve vos nouvelles classes :

* soit dans le back-office, dans 
**Administration** > **Extensions** > **Regénérer le tableau de chargement des classes des extensions**
* soit en ligne de commande à la racine du projet : 

```bash
php bin/php/ezpgenerateautoloads.php
```

## Utilisation ##

C'est terminé, vous pouvez désormais profiter de ces précieux opérateurs. Par exemple, dans un template :

```smarty
<p class"message1">
    {say_hello_world( 'John Doe', '?' )}
</p>
<p class"message2">
    {say_goodbye( 4 )}
</p>
```
