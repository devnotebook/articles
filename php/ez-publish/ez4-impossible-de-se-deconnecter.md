[eZ4] Impossible de se déconnecter
==================================

[:type]: # "Erreur"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/08"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Si vous ne parvenez pas à vous déconnecter, ou si vous êtes automatiquement connecté en tant qu'un 
utilisateur lorsque vous arriver sur une certaine page, vérifiez les points suivants :

* Vérifiez que l'**utilisateur anonyme utilisé par défaut** est le bon. 
Dans `site.ini` vous devez avoir `AnonymousUserID=10` 
(`10` par défaut, ou un autre si vous en avez choisi un autre).
* Videz les caches.
* Vérifiez que vous êtes toujours connecté malgré l'appel de la page `http://mon_site.com/user/logout`.
* Vérifiez que vous n'avez pas **modifié la configuration des droits et rôles** récemment.
* Vérifiez que vous n'avez pas **installé une nouvelle extension** récemment .
* Vérifiez que sur la ou les pages qui posent problème, **une fonction ne vous connecte pas en tant 
qu'un autre utilisateur** sans vous reconnecter correctement par la suite.
