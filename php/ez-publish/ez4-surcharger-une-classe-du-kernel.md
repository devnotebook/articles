[eZ4] Surcharger une classe du kernel
=====================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/03/29"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Pour corriger un bug ou étendre les fonctionnalités natives d'eZ Publish, 
on peut être tenter de modifier les fichiers du cœur du CMS, dans le dossier `kernel/`.

Il faut toujours éviter de modifier ces fichiers, surtout qu'il est possible de le faire proprement dans une extension, 
grâce au système d'autoload.

## Pré-requi ##

Vous devez d'abord **autoriser les surcharges des classes du kernel**, dans le fichier `config.php`, 
à la racine du site. (Si ce fichier n'existe pas, copiez le fichier `config.php-RECOMMENDED` et 
renommez-le en `config.php`.)

Il faut modifier la constante `EZP_AUTOLOAD_ALLOW_KERNEL_OVERRIDE` pour la rendre égale à `true` :

```php
define( 'EZP_AUTOLOAD_ALLOW_KERNEL_OVERRIDE', true );
```

## Surcharge ##

Vous pouvez maintenant copiez dans votre extension le fichier du kernel que vous souhaitez modifier.

Par exemple `extension/mon_extension/classes/override/ezcontentobjecttreenode.php`.

Il faut ensuite **régénérer les autoloads** et **vider le cache**, 
via les commande suivantes exécutées à la racine du site :

```bash
php bin/php/ezpgenerateautoloads.php -o
php bin/php/ezpgenerateautoloads.php
php bin/php/ezcache.php --clear-all
```

Votre classe surcharge maintenant celle du kernel, et est référencée dans le fichier `var/autoload/ezp_override.php` :

```php
<?php
/**
 * Autoloader definition for eZ Publish Kernel overrides files.
 *
 * @copyright Copyright (C) 1999-2012 eZ Systems AS. All rights reserved.
 * @license http://www.gnu.org/licenses/gpl-2.0.txt GNU General Public License v2
 * @version  2012.2
 * @package kernel
 *
 */
 
return array(
    'eZContentObjectTreeNode' => 'extension/apitools/classes/override/ezcontentobjecttreenode.php',
);
 
?>
```
