[eZ4] Configuration Apache et PHP pour eZ Publish avec WampServer
=================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4, WampServer 2.2, Apache 2.2, PHP 5.3"
[:created_at]: # "2012/12/04"
[:modified_at]: # "2018/02/12"
[:tags]: # "ez-publish php"

Voici la configuration à utiliser pour développer un site eZ Publish avec WampServer. 
Elle s'articule autour de **3 fichiers de configuration** :

* `httpd.conf` : Fichier de configuration du serveur Apache
* `httpd-vhosts.conf` : Fichier de configuration apache pour les hôtes virtuels
* `php.ini` : Configuration de php

**Remarque :**

Dans cet exemple, la version 2.2 64 bits de WampServer est utilisée, avec Apache 2.2.22 et PHP 5.3.13.

## Configuration finale ##

* Url du site : [http://mon_site.loc/index.php](http://mon_site.loc/index.php)
* Chemin absolu vers le projet : `D:\Dev\php_projects\MonSite\`
* Chemin absolu vers WampServer : `D:\Dev\wamp\`

## Httpd.conf ##

Ce fichier se trouve dans `bin\apache\apache2.2.22\conf\`, à partir de la racine de WampServer.

Tout en bas du fichier, activez les hôtes virtuels en décommentant cette ligne :

```apacheconfig
Include conf/extra/httpd-vhosts.conf
```

Voici un [exemple du fichier httpd.conf](https://framagit.org/devnotebook/articles/-/blob/master/_files/httpd.conf).

## Modules Apache ##

Via l'interface de Wamp, activez les modules Apache suivants :

![Modules Apache activés](./ez4-configuration-apache-et-php-pour-ez-publish-avec-wampserver-01.png)

## Hôtes virtuels ##

Éditez le fichier host (`C:\Windows\System32\drivers\etc\hosts`) en tant qu'administrateur.

Pour pouvoir l'enregistrer avec Notepad++, lancez l'éditeur en tant qu'administrateur 
(Clic-droit sur l'exécutable) et ouvrez ensuite le fichier `host`.

Ajoutez la ligne

```
127.0.0.1          mon_site.loc
```

Lorsque vous utiliserez l'adresse `mon_site.loc`, votre navigateur saura ainsi qu'il s'agit de votre 
ordinateur et non pas d'une machine sur internet.

Modifiez maintenant le fichier `httpd-vhosts.conf` (ex : `D:\Dev\wamp\bin\apache\apache2.2.22\conf\extra\httpd-vhosts.conf`) 
en ajoutant les lignes :

```apacheconfig
<VirtualHost *:80>
    ServerName mon_site.loc
    DocumentRoot D:/Dev/php_projects/MonSite
 
    <Directory D:/Dev/php_projects/MonSite>
        Options Indexes FollowSymLinks MultiViews
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
```

Voici un [exemple du fichier httpd-vhosts.conf](https://framagit.org/devnotebook/articles/-/blob/master/_files/httpd-vhosts.conf).

## Php.ini ##

WampServer utilise deux fichiers `php.ini` différents. Le premier est utilisé par votre eZ Publish ou 
n'importe quel site servi par votre serveur Apache. 
Le second est utilisé lorsque vous appelez PHP via l'invite de commande. 
Voici où les trouver depuis la racine de votre dossier WampServer :

* `bin\apache\apache2.2.22\bin\php.ini`
* `bin\php\php5.3.13\php.ini`

Le plus simple est d'utiliser la même configuration entre les deux lorsque vous êtes en phase de développement. 

Voici les propriétés à éditer :

```ini
# Temps maximum d'exécution de script (en s)
max_execution_time = 480 
# Temps maximum pour uploader un fichier
max_input_time = 180
# Taille maximale des fichiers uploadables
post_max_size = 8M
 
# Zone de date
date.timezone = "Europe/Paris"
```

Voici un [exemple du fichier php.ini](https://framagit.org/devnotebook/articles/-/blob/master/_files/php.ini).

**Remarque :**

Vous pouvez également [activer XDebug](./php/debugger-en-php-avec-xdebug) 
pour pouvoir déboguer de manière optimale.

## Extensions PHP ##

Comme pour les modules Apache, activez ces extensions PHP via l'interface de WampServer :

![Extensions de PHP activées](./ez4-configuration-apache-et-php-pour-ez-publish-avec-wampserver-02.png)
