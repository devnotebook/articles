[eZ4] Gérer efficacement le cache
=================================

[:type]: # "Astuce"
[:sources]: # "[Documentation cache-clock](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Template-functions/Miscellaneous/cache-block)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/04/16"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

* Pour qu'un template **ne soit pas mis en cache**, utilisez :

```smarty
{set-block scope=global variable=cache_ttl}0{/set-block}
```

* Pour **qu'une partie de ce template soit mise en cache**, utilisez :

```smarty
{cache-block}
    <p>Ce texte sera mis en cache</p>
{/cache-block}
```

* Pour qu'un cache soit mis à jour **si un nœud fils (ou plus profond) est modifié**, utilisez :

```smarty
{cache-block subtree_expiry=$my_node.node_id}
    <p>Ce texte sera conservé en cache jusqu'à ce qu'un fils de $my_node soit modifié.</p>
{/cache-block}
```

Pour qu'un cache soit créé **pour chaque valeur différente** de certains paramètres, utilisez :

```smarty
{cache-block keys=array( $param1, $param2 )}
    <p>
    Ce texte sera conservé en cache sous plusieurs versions, 
    une pour chaque couple {$param1} et {$param2} différent.
    </p>
{/cache-block}
```
