[eZ4] Trier les résultats d'un fetch selon l'ordre défini en back-office
========================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/31"
[:modified_at]: # "2018/02/02"
[:tags]: # "ez-publish php"

Dans le back-office d'eZ Publish, vous pouvez définir l'ordre dans lequel doivent être listés 
les enfants d'un nœud. Pour cela, cliquer sur l'onglet **classement**, et choisissez la méthode et le 
sens du tri à appliquer.

Pour que ce tri soit appliqué également en front-office, utilisez le paramétrage suivant dans votre template :

```smarty
{def $node_list = fetch( 'content', 'list', 
    hash( 
        'parent_node_id', $node.node_id,
        'sort_by', $node.sort_array 
    ) 
)}
```

**Explication :**

Le type de tri configuré en BO est récupérable à partir du nœud via `$node.sort_array`.
