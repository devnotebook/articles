[eZ4] Utiliser le layout d'impression
======================================

[:type]: # "Astuce"
[:sources]: # "[Documentation layout.ini](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Configuration-files/layout.ini)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/02"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Il est courant de proposer une fonction d'impression pour les articles de son site.

Cette fonction peut appeler simplement la fonction **Imprimer** du navigateur en javascript (`window.print()`), 
ou définir un affichage de l'article épuré.
EZ Publish propose un système de layout pour **définir plusieurs gabarits d'affichage**. 
Celui d'impression notamment, qui permet d'afficher un article sans les menus, le header et le footer.

Alors que layout principal utilise le template `pagelayout.tpl` partout dans le site, 
celui d'impression utilise `print_pagelayout.tpl`.

**Affichage en mode page, avec le layout principal :**

![Affichage en mode page, avec le layout principal](./ez4-utiliser-le-layout-d-impression-01.png)

**Affichage en mode impression, avec le layout d'impression :**

![Affichage en mode impression, avec le layout d'impression](./ez4-utiliser-le-layout-d-impression-01.png)

## Comment afficher un article avec ce layout épuré ? ##

Appelez simplement l'article avec une URL de type : `http://mon_site.com/layout/set/print/content/view/full/<node_id>`.

**Remarques :**

* Vous pouvez définir d'autres layout, par exemple pour afficher un contenu sous forme xml ou json. 
Le template devra toujours avoir pour nom `nomdulayout_pagelayout.tpl`, et l'url la forme 
`http://mon_site.com/layout/set/<nomdulayout>/content/view/full/<node_id>`. 
Dans ce cas, déclarez votre nouveau layout dans le fichier `layout.ini`.
* Vous pouvez appeler d'autres vues que content/view. 
Il suffit de les faire précéder par `/layout/set/<nomdulayout>/`.
