[eZ4] Créer un Content Edit Handler personnalisé
================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/06/07"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Pour ajouter des traitements spécifiques lors de la création ou la modification d'objets, 
vous pouvez créer un **Content Edit Handler personnalisé**. 
On peut par exemple créer automatiquement un nouvel article souhaitant la bienvenue à un 
utilisateur lors de son inscription.

## Configuration ##

Pour cela, vous devez déclarer votre extension comme possédant un Content Edit Handler dans `content.ini` :

Par exemple, dans le fichier `monextension/settings/content.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[EditSettings]
ExtensionDirectories[]=monpremier
 
*/ ?>
```

Avec cette configuration, eZ Publish va chercher le fichier `extension/monextension/content/monpremierhandler.php`.

## Content Edit Handler ##

Créez le répertoire `content/` et le fichier `monpremierhandler.php`. 
Ce fichier doit contenir une classe, qui étend `eZContentObjectEditHandler`.

Pour qu'eZ Publish prenne en compte cette classe, **videz les caches** et **régénérer les autoloads**.

Voici maintenant un exemple simple pour loguer le nom d'un utilisateur lorsqu'il est publié :

```php
<?php
class MonToutPremierHandler extends eZContentObjectEditHandler {
 
    /**
     * Effectue des opérations au moment de la soumission du formulaire de la vue /content/edit, après la vérification des champs.
     */
    function fetchInput( $http, &$module, &$class, $object, &$version, $contentObjectAttributes, $editVersion, $editLanguage, $fromLanguage ) {
 
        // Exemples :
 
        // Si l'action courante est l'enregistrement en base
        // (liste des actions disponibles dans kernel/content/module.php)
        if( $module->isCurrentAction( 'Store' ) ) {
 
            // Traitement ...
        }
 
        // Si un input "Name" a été soumis
        if( $http->hasPostVariable( 'Name' ) ) {
 
            // Traitement ...
        }
    }
 
    /**
     * Retourne la liste des paramètres POST HTTP qui déclencheront une action.
     * (Retourne un tableau vide par défaut.)
     */
    static function storeActionList() {
 
        return array();
    }
 
    /**
     * Effectue des opérations au moment de la publication de l'objet.
     *
     * Rq : L'objet a déjà été affecté à un noeud à ce moment.
     *
     * @param int $contentObjectID
     * @param eZContentObjectVersion $contentObjectVersion
     */
    function publish( $contentObjectID, $contentObjectVersion ) {
 
        // Exemple : On logue le nom d'un utilisateur lorsqu'il s'inscrit.
 
        // Récupération de l'objet publié
        $object = eZContentObject::fetch( $contentObjectID );
 
        // Récupération de la classe de l'objet
        $contentClass = $object->attribute('content_class');
 
        // Si l'objet est de type utilisateur et qu'il vient d'être créé
        if ( $contentClass->Identifier == 'user' && $contentObjectVersion == 1 ) {
 
            $message = 'Nouvel utilisateur inscrit : ' . $object->Name;
            eZLog::write( $message );
        }
    }
}
?>
```

La classe `eZContentObjectEditHandler` possédant 3 méthodes abstraites, 
vous devez les implémentez dans votre classe.

`fetchInput()` :

Cette méthode permet d'effectuer des traitements au moment où le formulaire de la 
vue `content/edit/` est soumis. Les paramètres de la fonction permettent d'effectuer des 
traitements différents selon l'action en cours, les valeurs postées, l'objet édité, ...

`storeActionList()` :

Cette méthode retourne la liste des paramètres POST HTTP qui déclencheront une action, 
ou un tableau vide s'il n'y en a pas.

`publish()` :

Cette méthode permet d'effectuer des traitements au moment où l'objet est publié pour la première fois, 
ou lorsqu'une nouvelle version est publiée. L'objet a déjà été affecté à un nœud quand cette méthode est 
exécutée.

**Remarque :**

Vous pouvez également surcharger la méthode `validateInput()`, 
pour effectuer des vérification complémentaires sur les champs de l'objet. 
(Voir l'article [[eZ4] Créer une validation de champ spécifique pour l'édition de contenus](./ez-publish/ez4-creer-une-validation-de-champ-specifique-pour-l-edition-de-contenus).)
