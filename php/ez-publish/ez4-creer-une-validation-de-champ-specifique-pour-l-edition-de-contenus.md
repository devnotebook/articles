[eZ4] Créer une validation de champ spécifique pour l'édition de contenus
=========================================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/06/07"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

Lorsqu'un contenu est créé, eZ Publish vérifie la validité des valeurs envoyées. 
Pour un champ de type "Nombre entier" par exemple, il vérifie qu'on a bien un nombre et pas des lettres 
pour valeur.

Ces vérifications sont cependant limitées et il peut être intéressant d'en ajouter. 
On peut imaginer par exemple une classe de contenu **Contrat**, ayant un champ **numéro de la forme XX-YYYY**, 
avec XX deux lettres et YYYY quatre chiffres. 
Vous pouvez effectuer une vérification sur ce champ, 
en étendant la classe `eZContentObjectEditHandler` d'eZ Publish.

Pour cela, vous devez déclarer votre extension comme possédant un **Content Edit Handler** dans `content.ini` :

Par exemple, dans le fichier `monextension/settings/content.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[EditSettings]
ExtensionDirectories[]=monpremier
 
*/ ?>
```

Avec cette configuration, eZ Publish va chercher le fichier 
`extension/monextension/content/monpremierhandler.php`.

Créez le répertoire `content/` et le fichier `monpremierhandler.php`. 
Ce fichier doit contenir une classe qui étend `eZContentObjectEditHandler`.

Voici maintenant un exemple simple pour vérifier que le champ `contract_number` 
d'un contenu de type `contract` commence bien par un `c` :

```php
<?php
class MonPremierHandler extends eZContentObjectEditHandler {
 
    /**
     * Effectue des opérations au moment de la soumission du formulaire de la vue /content/edit, après la vérification des champs.
     */
    function fetchInput( $http, &$module, &$class, $object, &$version, $contentObjectAttributes, $editVersion, $editLanguage, $fromLanguage ) {
    }
 
    /**
     * Retourne la liste des paramètres POST HTTP qui déclencheront une action.
     * (Retourne un tableau vide par défaut.)
     */
    static function storeActionList() {
 
        return array();
    }
 
    /**
     * Effectue des opérations au moment de la publication de l'objet.
     *
     * Rq : L'objet a déjà été affecté à un noeud à ce moment.
     *
     * @param int $contentObjectID
     * @param eZContentObjectVersion $contentObjectVersion
     */
    function publish( $contentObjectID, $contentObjectVersion ) {
    }
 
    /**
     * Effectue des vérifications supplémentaires sur les champs soumis à l'édition d'un contenu.
     *
     * @return array
     */
    function validateInput( $http, &$module, &$class, $object, &$version, $contentObjectAttributes, $editVersion, $editLanguage, $fromLanguage, $validationParameters ) {
 
        $result = array( 'is_valid' => true, 'warnings' => array() );
 
        if ( $class->Identifier == 'contract' ) {
 
            // Récupération de la liste des attributs modifiés
            $contentObjectAttributes = $object->contentObjectAttributes();
 
            // Recherche de l'attribut 'contract_number'
            foreach ( $contentObjectAttributes as $contentObjectAttribute ) {
 
                if ( $contentObjectAttribute->contentClassAttributeIdentifier() == 'contract_number' ) {
 
                    // Récupération du numéro de contrat envoyé
                    $postValues     = $http->attribute('post');
                    $contractNumber = $postValues['ContentObjectAttribute_' . $contentObjectAttribute->DataTypeString . '_data_text_' . $contentObjectAttribute->ID];
 
                    // Si le numéro de contrat ne commmence pas par 'c'
                    if ( strpos( $contractNumber, 'c' ) !== 0 ) {
 
                        $result['warnings'][] = array( 
                            'text' => ezpI18n::tr( 
                                'extension/monextension/validation', 
                                "The contract number should begin with a 'c'." 
                            ) 
                        );
                        $result['is_valid']   = false;
                    }
                }
            }
        }
 
        return $result;
    }
}
?>
```

La classe `eZContentObjectEditHandler` possédant 3 méthodes abstraites, 
vous devez les implémentez dans votre classe (même si vous les laissez vides) : 
`fetchInput()`, `storeActionList()` et `publish()`.

Une quatrième méthode va nous intéresser : `validateInput()`.
C'est elle qui permet d'ajouter une validation personnalisée.

Elle possède les paramètres suivants :

* `$http` : les éléments soumis par le formulaire et les variables de session.
* `$module` : le module courant (= `content`).
* `$class` : une instance de la classe de contenu de l'objet édité.
* `$object` : l'objet modifié.
* `$contentObjectAttributes` : le tableau des attributs de l'objet, **avant** modification.
* `$version` : le numéro de version de l'objet.

Par défaut, cette méthode retourne un tableau d'erreur vide :

```php
$result = array( 'is_valid' => true, 'warnings' => array() );
```

Pour chaque champ vérifié, en cas d'erreur, on stocke le message d'erreur à afficher. Ex :

```php
$result['warnings'][] = array( 
    'text' => ezpI18n::tr( 
        'extension/monextension/validation', 
        'The contract number should begin with \'c\'.'
    ) 
);
$result['is_valid']   = false;
```
