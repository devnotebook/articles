[eZ4] Créer un lien de téléchargement d'un fichier
==================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/09/08"
[:modified_at]: # "2018/02/07"
[:tags]: # "ez-publish php"

Pour créer un lien de téléchargement vers le fichier d'un contenu de type **File**, utilisez :

```smarty
{concat( 
    'content/download/', $object.data_map.file.contentobject_id,
    '/', $object.data_map.file.id, 
    '/version/', $object.data_map.file.version , 
    '/file/', $object.data_map.file.content.original_filename|urlencode() 
)|ezurl( 'no' )}
```

**Explication :**

La variable `$object` doit être un objet de contenu, soit une instance de `eZContentObject`.
