[eZ4] Ajouter des boutons à la websitetoolbar
=============================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/08/02"
[:modified_at]: # "2018/02/06"
[:tags]: # "ez-publish php"

EZ Publish inclut par défaut l'extension `ezwt`, qui propose une barre d'outils 
pour l'édition en front-office :

![Websitetoolbar](./ez4-ajouter-des-boutons-a-la-websitetoolbar-01.png)

Cette barre d'outils utilise bien sûr des templates, que vous pouvez surcharger. 
Voici un exemple pour ajouter un bouton de déconnexion à cette barre d'outils.

## Surcharger le template existant ##

Ajoutez la règle de surcharge suivante dans le fichier 
`extension/mon_extension/settings/siteaccess/site/override.ini.append.php` :

```ini
[website_toolbar]
Source=parts/website_toolbar.tpl
MatchFile=parts/website_toolbar.tpl
Subdir=templates
```

Copiez le fichier `extension/ezwt/design/standard/templates/parts/website_toolbar.tpl` 
dans votre extension (`extension/mon_extension/design/mon_design/templates/parts/website_toolbar.tpl`).

## Ajouter le bouton ##

Ajouter le code de votre bouton avant le template d'aide :

```smarty
{* Ajout d'un bouton pour se déconnecter *}
<div id="ezwt-monextensionaction-logout" class="ezwt-actiongroup">
    <a class="logout_button"
       href="{'user/logout'|ezurl( 'no' )}"
       title="{'Me déconnecter'|i18n( 'user/login' )}">
        <img src="{'global/picto_logout.png'|ezimage( 'no' )}" alt="" />
    </a>
</div>
 
{include uri='design:parts/websitetoolbar/help.tpl'}
```

**Remarques :**

* Le div conteneur, avec la classe `ezwt-actiongroup` définit un nouveau groupe de boutons.
* Vous pouvez choisir à quel endroit de la barre votre bouton doit apparaître (ici à la fin), 
et vous pouvez ajouter un bouton à un groupe déjà existant.

**Résultat :**

![Websitetoolbar avec déconnexion](./ez4-ajouter-des-boutons-a-la-websitetoolbar-02.png)
