[eZ4] Les préférences utilisateur
=================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2013/04/29"
[:modified_at]: # "2018/02/09"
[:tags]: # "ez-publish php"

Le module `user` d'eZ Publish fournit une vue pour stocker simplement des préférences utilisateurs : 
`user/preferences`.

Concrètement, ces préférences sont stockées en base de données, 
avec pour clé le couple `(user_id, preference_name)` et pour valeur celle de notre choix.

Ce fonctionnement est souvent utilisé en back-office, pour afficher tel ou tel bloc de la page, 
comme la barre de droite par exemple.  
Pour la partie publique côté front-office il est à éviter, car tous les utilisateurs anonymes auront la même 
préférence. (Ou alors il ne faut pas leur permettre de modifier la valeur.)

## Créer/modifier une préférence ##

### Côté template ou HTML ###

Il suffit d'appeler l'URL `user/preferences`, en proposant par exemple un lien à l'utilisateur. 

Pour afficher/masquer la barre de droite du back-office, par exemple on a juste ce genre de liens :

```smarty
{* Afficher la barre *}
<a href="{'user/preferences/set/admin_right_menu_show/1'|ezurl('no')}">
    Afficher la barre de droite
</a>

{* Masquer la barre *}
<a href="{'user/preferences/set/admin_right_menu_show/0'|ezurl('no')}">
    Masquer la barre de droite
</a>
```

**Explication :**

Pour créer/modifier une préférence, il faut appeler la vue `user/preferences`, avec 3 paramètres : 
`set`, le **nom** de la préférence puis sa **valeur**.

### Côté PHP ###

Il faut utiliser la méthode `setValue()` de la classe `eZPreferences` :

```php
eZPreferences::setValue( 'my_preference_name', 'my_value' );
```

**Remarque :**

Par défaut, la préférence sera associée à l'utilisateur connecté. 
Un troisième argument est disponible (`$user_id`), pour l'affecter à un autre utilisateur.

## Récupérer la valeur de la préférence ##

### Côté template ###

```smarty
{ezpreference( 'my_preference_name' )}
```

**Explication :**

L'utilisateur courant est automatiquement utilisé.

### Côté PHP ###

Il faut utiliser la méthode `value()` de la classe `eZPreferences` :

```php
eZPreferences::value( 'my_preference_name' );
```

**Remarques :**

* Pour récupérer la valeur pour un autre utilisateur que celui connecté, utilisez le second argument facultatif.
* La méthode `values()` de la classe `eZPreferences` permet de récupérer toutes les préférences 
d'un utilisateur.
