[eZ4] Diminuer le taux de compression des images
================================================

[:type]: # "Astuce"
[:sources]: # "[Documentation MIMETypeSettings](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Configuration-files/image.ini/MIMETypeSettings)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/03/29"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Si les images du site sont trop lourdes, ou de mauvaise qualité, vous pouvez modifier le taux de compression utilisé 
lors de la génération des thumbnails.

Pour cela, créez ou modifiez le fichier `image.ini.append.php` dans votre extension.

Exemple, pour le fichier `extension/mon_extension/image.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[MIMETypeSettings] :
Quality[]=image/jpeg;100
Quality[]=image/png;100
Quality[]=image/gif;100
 
*/ ?>
```

Pour chaque format d'image, vous pouvez définir le taux de compression par rapport à l'image originale. 
100% correspond à la qualité maximale utilisable.
