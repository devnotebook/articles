[eZ4] Internationaliser un texte
================================

[:type]: # "Astuce"
[:sources]: # "[Documentation i18n()](https://doc.ez.no/eZ-Publish/Technical-manual/4.x/Reference/Template-operators/Formatting-and-internationalization/i18n)"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/03/29"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Créez un fichier de traduction dans votre extension :

`extension/mon_extension/translations/ma_lang/translation.ts` avec `ma_lang` l'identifiant d'un langage 
(ex: fre-FR).

Malgré l'extension `ts`, il est formaté en XML :

```xml
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
 
<!-- Contexte -->
<context>
    <name>mon/contexte</name>
    <message>
        <source>Ma phrase à traduire</source>
        <translation>Ma phrase traduite</translation>
    </message>
</context>
 
</TS>
```

Pour traduire un mot ou une phrase dans un template, utilisez la méthode `i18n()` :

```smarty
{* La ligne suivante affichera 'Ma phrase traduite' *}
{'Ma phrase à traduire'|i18n( 'mon/contexte' )}
```

**Remarque :**

Les contextes permettent de regrouper les traductions, par thème, par page, etc.
