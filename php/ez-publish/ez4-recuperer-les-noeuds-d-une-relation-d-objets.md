[eZ4] Récupérer les noeuds d'une relation d'objets
==================================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/03/30"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Lorsqu'un objet a un champ de type relation d'objets, il peut retourner un ou plusieurs objets liés.

## Dans un template ##

Pour récupérer les nœuds principaux des objets liés, utilisez dans un template :

```smarty
{def $related_nodes = array()}
{if $node.data_map.attribut_relation_objets.has_content}
 
    {def $related_objects = $node.data_map.relationlist_attribute.content.relation_list}
 
    {def $node_id_list = array()}
    {foreach $related_objects as $object_item}
        {set $node_id_list = $liste_node_id|append( $object_item.node_id )}
    {/foreach}
    {set $related_nodes = fetch( 'content', 'node', hash( 'node_id', $node_id_list ) )}
 
    {if is_object( $related_nodes )}
        {set $related_nodes = array( $related_nodes )}
    {/if}
 
    {undef $node_id_list}
    {undef $related_objects}
{/if}
```

**Explications :**

* On vérifie que l'attribut `relationlist_attribute` de type `relation d'objets` a un contenu.
* Si oui, on en parcourt les éléments et on stocke le `node_id` de chacun d'eux
* On récupère tous les nœuds correspondant à la liste des node_id avec le `fetch()`.
* S'il n'y a qu'un résultat, le fetch retourne un objet. On le met donc dans un tableau.

**Remarque :**

Si on sait qu'on n'aura jamais plus d'un objet lié, on peut faire simplement :

```smarty
{def $related_node = false()}
{if $node.data_map.attribut_relation_objets.has_content}
    {set $related_node = fetch( 'content', 'node', hash( 'node_id', $node.data_map.attribut_relation_objets.content.relation_list.0.node_id ) )}
{/if}
```

## Côté PHP ##

```php
$relatedNodeList = array();
 
$dataMap = $node->dataMap();
$relationListAttribute = $dataMap['relationlist_attribute'];
 
if ( $relationListAttribute->hasContent() ) {
 
    $relatedObjects = $relationListAttribute->content();
    $nodeIdList = array();
 
    foreach ( $relatedObjects['relation_list'] as $object_item ) {
        $nodeIdList[] = $object_item['node_id'];
    }
 
    $relatedNodeList = parent::fetch( $nodeIdList );
}
```

**Explications :**

Le fonctionnement est similaire à celui dans le template.  
De la même manière, on peut utiliser une version simplifiée quand la relation est unique :

```php
$relatedNode = false;
 
$dataMap = $node->dataMap();
$relationListAttribute = $dataMap['temp'];
 
if ( $relationListAttribute->hasContent() ) {
    $relatedObjects = $relationListAttribute->content();
    $relatedNode = parent::fetch( $relatedObjects['relation_list'][0]['node_id'] );
}
```
