[eZ4] Créer un formulaire d'envoi de mail
=========================================

[:type]: # "Astuce"
[:version]: # "eZ Publish 4"
[:created_at]: # "2012/05/10"
[:modified_at]: # "2018/02/01"
[:tags]: # "ez-publish php"

Voici une méthode pour créer un formulaire d'envoi de mail en utilisant au maximum l'API d'eZ Publish.

* Commencez par créer une nouvelle classe de contenu. 
* Ajoutez des champs `sujet` et `message` de type **collecteur d'informations**.
* Déclarez maintenant le formulaire dans `collect.ini`.

Par exemple dans `extension/mon_extension/settings/collect.ini.append.php` :

```ini
<?php /* #?ini charset="utf-8"?
 
[InfoSettings]
# Associe l'identifier de la classe de contenu au type de collection d'information
TypeList[my_form]=my_form
TypeAttribute=collection_type
 
[EmailSettings]
# Définit que les informations collectées doivent être envoyées par email
SendEmailList[my_form]=enabled
SendEmailAttribute=collection_email
 
[DisplaySettings]
# Définit l'action a effectué une fois que les informations sont collectées (ici on affiche le résultat = message informant que l'email a bien été envoyé)
DisplayList[]
DisplayList[my_form]=result
 
[CollectionSettings]
# Autorise les utilisateurs anonymes à utiliser le formulaire
CollectAnonymousDataList[]
CollectAnonymousDataList[my_form]=enabled
 
# Autorise un utilisateur à utiliser plusieurs fois le formulaire
CollectionUserData=multiple
CollectionUserDataList[my_form]=multiple
 
*/ ?>
```

Créez les templates nécessaires :

* `extension/mon_extension/design/mon_design/override/templates/full/my_form.tpl`
* `extension/mon_extension/design/mon_design/templates/content/collectedinfo/my_form.tpl`
* `extension/mon_extension/design/mon_design/templates/content/collectedinfomail/my_form.tpl`

Le premier contient le **formulaire d'envoi de mail**  
Le deuxième contient la **page de résultat** à afficher une fois que le mail a été envoyé  
Le troisième contient le **contenu du mail et ses paramètres** (destinataire, sujet, ...)  

**Remarques :**

* Pour que le fichier `extension/mon_extension/design/mon_design/override/templates/full/my_form.tpl` 
soit utilisé, il faut [ajouter une règle de surcharge](./ez-publish/ez4-surcharger-un-template-d-affichage) 
dans le fichier `override.ini`.
* Vous pouvez [modifier les paramètres de l'email](./ez-publish/ez4-modifier-les-parametres-d-envoi-de-mail) envoyé.
