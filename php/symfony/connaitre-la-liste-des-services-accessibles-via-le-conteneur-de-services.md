Connaître la liste des services accessibles via le conteneur de services
========================================================================

[:type]: # "Astuce"
[:version]: # "Symfony 3"
[:created_at]: # "2015/11/06"
[:modified_at]: # "2017/08/09"
[:tags]: # "symfony php"

Avec Symfony, on utilise très souvent le conteneur de services. 
Par exemple dans un contrôleur, si on veut récupérer le logger de Monolog :

```php
$logger = $this->get('logger');
```

Pour connaitre la liste de tous les services disponibles, utilisez la commande suivante :

```bash
php app/console container:debug
```
