Utiliser Doctrine avec la console de Symfony
============================================

[:type]: # "Astuce"
[:sources]: # "[symfony.com - console](https://symfony.com/doc/current/doctrine/console.html) | [symfony.com - reverse engeneering](https://symfony.com/doc/current/doctrine/reverse_engineering.html)"
[:version]: # "Symfony 3"
[:created_at]: # "2013/02/08"
[:modified_at]: # "2017/08/07"
[:tags]: # "symfony php"

Voici un récapitulatif des commandes de base de la console de Symfony pour Doctrine :

* Créer une base de données :

```bash
php bin/console doctrine:database:create
```

* Créer/mettre à jour le schéma des tables dans la base :

```bash
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force
```
La première commande permet de visualiser les requêtes à exécuter. La seconde les exécute.

* Générer le code d'une entité :

```bash
php bin/console generate:doctrine:entity
```
Renseignez ensuite le nom de l'entité avec le nom du bundle devant (ex: `BlogBundle:Article`), 
le format de configuration à utiliser, les éventuels champs à créer, 
et si vous souhaitez également générer le code du repository.

* Compléter le code d'une entité (champs, getter() et setter()) :

```bash
php bin/console doctrine:generate:entities BlogBundle:Article
```
À adapter en fonction de l'entité à mettre à jour.
