Utiliser un schéma spécifique pour la base de données PostgreSQL
================================================================

[:type]: # "Astuce"
[:version]: # "Symfony 4"
[:created_at]: # "2019/11/21"
[:modified_at]: # "2020/04/14"
[:tags]: # "symfony php postgresql"

Il n'est pas possible de préciser le schéma à utiliser dans l'url de connexion à la base de données PostgreSQL :

```ini
DATABASE_URL=pgsql://my_user:my_pwd@localhost:5432/my_db
```

Par défaut, c'est le schéma `public` de postgres qui est utilisé.

Pour en changer, il faut exécuter cette commande SQL sur la base de données :

```sql
ALTER USER my_user SET search_path = my_custom_schema;
```
