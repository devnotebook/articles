S'authentifier dans le Swagger d'API Platform
=============================================

[:type]: # "Astuce"
[:sources]: # "[api-platform.com](https://api-platform.com/docs/core/jwt/#documenting-the-authentication-mechanism-with-swaggeropen-api)"
[:version]: # "API Platform 2.x"
[:created_at]: # "2019/08/28"
[:modified_at]: # "2019/08/28"
[:tags]: # "symfony php"

Lorsqu'on utilise **JWT** pour l'authentification dans API Platform, on a besoin d'ajouter ce jeton à nos requêtes pour communiquer avec l'API.
API Platform propose un bouton pour faire ça facilement dans **Swagger** :

![Bouton d'authentification dans Swagger](./s-authentifier-dans-le-swagger-d-api-platform-01.png)

Ce bouton n'apparaît pas par défaut. Il faut l'activer dans la configuration d'API Platform :

```yaml
# config/package/api_platform.yaml
api_platform:
    swagger:
        api_keys:
            apiKey:
                name: Authorization
                type: header
```
