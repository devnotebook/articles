L'EntityManager de Doctrine pour Symfony
========================================

[:type]: # "Astuce"
[:sources]: # "[Documentation Doctrine](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/getting-started.html)"
[:version]: # "Doctrine 2"
[:created_at]: # "2013/02/08"
[:modified_at]: # "2017/08/07"
[:tags]: # "symfony php"

Tout d'abord, voici comment récupérer cet **EntityManager**, depuis un **Contrôleur** :

```php
// Récupération de l'entity manager
$entityManager = $this->getDoctrine()->getManager();
```

Voici les principales méthodes de l'EntityManager 
([voir toutes les méthodes](https://github.com/doctrine/doctrine2/blob/master/lib/Doctrine/ORM/EntityManager.php)).

## persist($entity) ##

Cette méthode signale à Doctrine que l'objet doit être enregistré. 
Elle ne doit être utilisée que pour un nouvel objet et non pas pour une mise à jour.

Ex :

```php
// Crée l'article et le signale à Doctrine.
$article1 = new Article;
$article1->setTitre('Mon dernier weekend');
$entityManager->persist($article);
```

## flush() ##

Met à jour la base à partir des objets signalés à Doctrine.
Tant qu'elle n'est pas appellée, rien n'est modifié en base.

Ex :

```php
// Crée l'article en base et met à jour toutes les entités persistées modifiées.
$entityManager->persist($article);
$entityManager->flush();
```

## clear($nomEntity = null) ##

Annule tous les `persist()` en cours. 
Si le nom d'une entité est précisé (son namespace complet ou son raccourci), 
seuls les `persist()` sur les entités de ce type seront annulés.

Ex :

```php
$entityManager->persist($article);
$entityManager->persist($commentaire);
$entityManager->clear();
// N'exécutera rien, car les deux persists sont annulés par le clear
$entityManager->flush();
```

## detach($entity) ##

Annule le persist() effectué sur l'entité en argument. 
Au prochain `flush()`, aucun changement ne sera donc appliqué à l'entité.

Ex :

```php
$entityManager->persist($article);
$entityManager->persist($commentaire);
$entityManager->detach($article);
// Enregistre $commentaire mais pas $article
$entityManager->flush();
```

## contains($entity) ##

Retourne `true` si l'entité donnée en argument est gérée par l'EntityManager 
(= s'il y a eu un `persist()` sur l'entité).

Ex :

```php
$entityManager->persist($article);
var_dump($entityManager->contains($article)); // Affiche true
var_dump($entityManager->contains($commentaire)); // Affiche false
```

## refresh($entity) ##

Rafraîchit l'entité donnée en argument pour la mettre dans l'état où elle se trouve en base de données. 
Cela écrase et annule donc tous les changements qu'il a pu y avoir sur l'entité depuis le dernier `flush()`.

Ex :

```php
$article->setTitre('Un nouveau titre');
$entityManager->refresh($article);
var_dump($article->getTitre()); // Affiche « Un ancien titre »
```

## remove($entity) ##

Signale à Doctrine qu'on veut supprimer l'entité en argument de la base de données. 
Effectif au prochain `flush()`.

Ex :

```php
$entityManager->remove($article);
// Exécute un DELETE sur $article
$entityManager->flush();
```
