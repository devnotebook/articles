Tips - Symfony
==============

[:type]: # "Astuce"
[:sources]: # "[symfony.com](https://symfony.com/doc/current/index.html)"
[:version]: # "Symfony 4+"
[:created_at]: # "2021/07/24"
[:modified_at]: # "2021/07/24"
[:tags]: # "symfony php"

## Console

### Liste des commandes disponibles

```bash
bin/console
```

### Conteneur de services

```bash
bin/console debug:autowiring
```

Cela liste les interfaces disponibles.

Pour en rechercher une concernant un sujet (ex: cache, mail, markdown)

```bash
bin/console debug:autowiring markdown
```

Pour voir toutes les implémentations de ces interfaces

```bash
bin/console debug:container
```

Ou pour avoir le détail sur l'une d'entre elles (ex: markdown)

```bash
bin/console debug:container markdown
```

Pourvoir les paramètres présents dans le conteneur

```bash
bin/console debug:container markdown
```

### Configuration

```bash
bin/console debug:container --parameters
```

Cela liste toutes la configuration par défaut pour le bundle.


Pour voir la config actuelle

```bash
bin/console debug:config MyBundle
```

### Liste des routes et de leur contrôleur

```bash
bin/console debug:router --show-controllers
```

Affiche la liste de toutes les routes et les contrôleurs/actions associés :

### Création auto

La commande `make` permet de générer un squelette de classe 
(Command, TwigExtension, ...) :

```bash
bin/console make
```

## Dump

```php
dump($someVariable);
```

Pour afficher une variable et **stopper l'exécution** :

```php
// "Dump and Die"
dd($someVariable);
``````
   
## Twig

Pour afficher toute la syntaxe ainsi que les variables globales disponibles dans Twig :

```bash
bin/console debug:twig
```

## Injection de dépendances

### Argument binding via services.yaml

La section `_default` est héritée par toutes les déclarations suivantes.  
C'est pourquoi si on y utilise `bind` pour binder des arguments, ils seront
disponibles pour tous les services.

On peut typer ces arguments. Ex:

```yaml
services:
  _default:
    bind:
      bool $isDebug: '%kernel.debug%'
      Psr\Log\LoggerInterface $someLogger: '@monolog.logger.some_logger'
```

Quand il y a plusieurs implémentations d'une interface 
(par exemple LoggerInterface, cf.`bin/console debug:autowiring log`), on peut 
accéder à celui que l'on veut en nommant l'argument comme indiqué.

Ex:

```php
public function __constructor(LoggerInterface $consoleLogger) {}
// à la la place de
public function __constructor(LoggerInterface $logger) {}
```

#### Alias de service

Un alias de service peut être créé en une ligne :

```yaml
services:
      Some\Path\To\SomeService $newName: '@id_of_the_target_service'
```


## Logging

Monolog peut loguer des messages sur des channel spécifiques. 
Pour cela il faut :

- Déclarer ce nouveau channel

```yaml
# ex: dans monolog.yaml
monolog:
  channels: ['my_new_channel']
```

- Utiliser le nouveau service référencé par Monolog 
(cf.`bin/console debug:autowiring log`)


```php
public function __constructor(LoggerInterface $myNewChannelLogger) {}
```


## Bundles sympas

### StofDoctrineExtensionsBundle

Ajoute des extensions doctrine (Slug, Blameable, Softdeleteable, ...)

[Dépôt Git](https://symfony.com/doc/current/bundles/StofDoctrineExtensionsBundle/index.html)

### KnpTimeBundle

Ajoute le filter twig `ago`, pour afficher depuis quand la date est passée.

[Dépôt Git](https://github.com/KnpLabs/KnpTimeBundle)

### Foundry

Ajoute une commande Symfony (`make:factory`) pour générer des Factory pour les entités.

[Dépôt Git](https://github.com/zenstruck/foundry)

Compatible avec le bundle ci-dessous

### FakerBundle

Permet de générer du faux contenu de manière intelligente.

[Dépôt Git](https://github.com/fzaninotto/Faker)






















