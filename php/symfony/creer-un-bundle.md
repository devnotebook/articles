Créer un Bundle
===============

[:type]: # "Astuce"
[:sources]: # "[symfony.com](http://symfony.com/doc/current/bundles/SensioGeneratorBundle/commands/generate_bundle.html)"
[:version]: # "Symfony 3"
[:created_at]: # "2013/02/06"
[:modified_at]: # "2017/08/09"
[:tags]: # "symfony php"

Pour créer un Bundle avec Symfony, utilisez la commande :

```bash
php app/console generate:bundle
```

* Choisissez si votre bundle sera commun à plusieurs applications ou non
* Saisissez le nom de votre bundle, finissant par Bundle (ex: `MonBundle`). 
Si vous avez mis oui à l'étape précédente, préfixez le d'un namespace (ex: `MonSite\MonBundle`).
* Laissez le répertoire cible `src/` inchangé.
* Choisissez le format de configuration.

Si vous préférez le créer à la main, voici ce qu'il faut retenir pour le rendre opérationnel :

* Le code source se trouve dans `src/MonBundle/`
* Le seul fichier obligatoire doit être à sa racine : `MonBundle.php`.
* Vous devez instancier votre bundle dans le noyau pour qu'il soit chargé.
Pour cela, ajoutez la ligne `new MonBundle\MonBundle(),` dans `AppKernel.php`.
* Pour que vos routes soient reconnues, il faut ajouter ces lignes au fichier `app/config.routing.yml` :

```yaml
mon_bundle:
    resource: "@MonBundle/Controller/"
    type:     annotation
    prefix:   /
```

* Pour que vos services soient reconnus, il faut ajouter cette ligne au fichier `app/config.yml` :
```yaml
imports:
    # [...]
    - { resource: "@MonBundle/Resources/config/services.yml" }
```
