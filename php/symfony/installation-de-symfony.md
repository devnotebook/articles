Installation de Symfony
=======================

[:type]: # "Astuce"
[:sources]: # "[symfony.com](https://symfony.com/doc/current/setup.html)"
[:version]: # "Symfony 4+"
[:created_at]: # "2021/07/23"
[:modified_at]: # "2021/07/23"
[:tags]: # "symfony php"

Cet article est une synthèse de la documentation officielle :
[Installing & Setting up the Symfony Framework](https://symfony.com/doc/current/setup.html).

## Prérequis

- Symfony **4.4** : **PHP 7.1** / Symfony **5.3** : **PHP 7.2**
- Extensions PHP : Ctype, iconv, JSON, PCRE, Session, SimpleXML et Tokenizer
- [Composer](http://getcomposer.org/)

Pour faciliter l'installation et le développement avec Symfony, [téléchargez](https://symfony.com/download) 
l'exécutable `symfony`.

La première fois que vous lancez exécutez `symfony`, il vérifie automatiquement les prérequis
et vous propose de l'ajouter aux exécutables système.

## Installation

### Nouveau projet

L'installation consiste à initialiser un nouveau projet Symfony.

Deux choix principaux s'offrent à vous :

- installer la version minimale
- installer une version plus complète, avec la majorité des composants nécessaires à une application web

```bash
symfony new my_project_name --full
# Ou, sans l'exécutable, juste avec Composer
composer create-project symfony/website-skeleton my_project

symfony new my_project_name
# Ou, sans l'exécutable, juste avec Composer
composer create-project symfony/skeleton my_project
```

Si vous ne souhaitez pas la version courante de Symfony, vous pouvez en spécifier une autre :

```bash
symfony new my_project --version=4.4 
# Ou, sans l'exécutable, juste avec Composer
composer create-project symfony/skeleton:"^4.4" my_project
```

**Note :** vous pouvez également remplacer le numéro de version par `lts` ou `next`.

### Récupération d'un projet existant

Si vous récupérez un projet Symfony depuis un gestionnaire de version - au hasard, Git -
il vous faut juste **téléchargez les dépendances** via Composer :

```bash
cd my_projects
git clone [...]

cd my_project/
composer install
```

Généralement, les configurations par défaut sont définies dans le fichier `.env`, et
celles propres à l'environnement doivent être surchargées dans un fichier `.env.local` 
(tous deux à la racine).  
Dans ce dernier, on retrouvera uniquement les propriétés du premier que l'on souhaite modifier.
Typiquement, on y indiquera les informations de connexion à la base de données.

### Permissions sur les fichiers

Les répertoires `<my_project>/var/cache/` et `<my_project>/var/log/` doivent être accessibles en écriture.  

(Plus d'informations ici : [Setting up or Fixing File Permissions](https://symfony.com/doc/current/setup/file_permissions.html).)

De plus, si vous souhaitez utiliser la console symfony (`bin/console`) plus facilement, vous devez la rendre
exécutable :

```bash
cd my-project/
chmod +x bin/console
```

**Remarque :** Cela n'est pas nécessaire si vous l'utilisez à travers PHP (ex : `php bin/console about`).

## Démarrage de l'application

En phase de développement, on peut se passer d'un serveur web classique comme apache ou nginx.  
À la place, l'exécutable `symfony` peut en lancer un pour nous :

```bash
cd my-project/
symfony server:start
```

Plus d'informations ici : [Symfony Local Web Server](https://symfony.com/doc/current/setup/symfony_server.html)

Si vous préférez un serveur web classique, voici des exemples pour les configurer : 
[Configuring a Web Server](https://symfony.com/doc/current/setup/web_server_configuration.html)

## Ajout de dépendances

Si vous souhaitez ajouter des composants Symfony prêts à l'emploi, vous pouvez 
en récupérer les bundles avec Composer.  
Symfony lui a ajouté le plugin **Flex**, pour vous éviter d'avoir à les configurer. 

De plus, cela vous permet d'installer un composant sans savoir où le trouver. Par exemple,
vous avez besoin d'un logger ? Lancez la commande :

```bash
cd my-project/
composer require logger
```

Vous souhaitez un débogueur en mode dev ? Lancez la commande :

```bash
cd my-project/
composer require --dev debug
```

## Sécurité des dépendances

Pour vérifier la présence de failles connues parmi les dépendances de votre projet,
`symfony` propose une commande :

```bash
symfony check:security
```
