Éviter les injections de wildcards dans les requêtes avec Doctrine
==================================================================

[:type]: # "Astuce"
[:sources]: # "[stackoverflow.com](https://stackoverflow.com/a/7893670/2674501)"
[:version]: # "Doctrine 2"
[:created_at]: # "2019/12/10"
[:modified_at]: # "2019/12/10"
[:tags]: # "symfony php"

Le query builder de doctrine protège automatiquement des injections SQL, car elle utilise des requêtes
préparées où sont injectés les paramètres :

```php
// Utilisateurs dont le nom est 'Toto'
$userName = 'Toto';
->where('u.name = :userName')
->setParameter('userName', $userName);
```

Par contre si on utilise un `LIKE` pour retourner tous les utilisateurs dont le nom **commence par** :

```php
// Utilisateurs dont le nom commence par 'To'
$prefix = 'To';
->where('u.name LIKE :userNameStart')
->setParameter('userNameStart', '%'.$prefix);
```
Si `$prefix` est une donnée provenant d'un formulaire, on est pas à l'abri qu'elle ne contienne pas d'autres wildcards (`_` ou `%`).  
Il est préférable de l'échapper :

```php
// Utilisateurs dont le nom commence par 'Toto'
$prefix = 'To';
->where('u.name LIKE :userNameStart')
->setParameter('userNameStart', '%'.addcslashes($prefix, '%_'));
```
