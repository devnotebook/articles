Créer une page
===============

[:type]: # "Astuce"
[:sources]: # "[symfony.com](https://symfony.com/doc/current/setup.html)"
[:version]: # "Symfony 4+"
[:created_at]: # "2021/07/24"
[:modified_at]: # "2021/07/24"
[:tags]: # "symfony php"

Cet article est une synthèse de la documentation officielle :
[Create your First Page in Symfony](https://symfony.com/doc/current/page_creation.html).

## Généralités

Créer une page, c'est indiquer quelle réponse générer lorsqu'on reçoit une requête auprès d'une certaine URL.

Plus, précisément, la partie de l'URL qui nous intéresse est appellée la **route**, ou en français, le **chemin**.
Par exemple pour les URL `http://mon-site.com/recherche` et `http://mon-site.com/article/1234-mon-article`, 
les routes seront respectivement `/recherche` et `/article/1234-mon-article`.

Le générateur de réponse est une fonction PHP appelée **contrôleur**. Dans Symfony, elle doit retourner un
objet `Response`, contenant du texte (HTML, JSON, ...), une fichier binaire (ex: fichier, image, ...), ...

On appelle **route** le chemin présent dans l'URL permettant de déterminer qu'elle génération on souhaite.  

## Routes et Controllers

Par défaut, les routes se déclarent dans le fichier `config/routes.yaml`. Par exemple :

```yaml
# the "app_lucky_number" route name is not important yet
my_route_name:
    path: /some-path-to/my_page
    controller: App\Controller\MyPageController::myFunctionToGenerateThePage
```

On a ici définit que les requêtes sur la route `/some-path-to/my_page` devait recevoir une réponse
générée par la méthode `myFunctionToGenerateThePage()` de la classe `App\Controller\MyPageController`.

Généralement, les contrôleurs seront des méthodes de classes placées dans le répertoire `src/Controller`. 

### Annotations

Pour faciliter le mapping route-controller, vous pouvez déclarer vos routes en annotations à la place d'utiliser le fichier
`routes.yaml`. Cela regroupe ainsi la route et le contrôleur au même endroit.

Pour cela, la dépendance `annotations` doit être installée :

```bash
coomposer require annotations
```

Vous pouvez dès lors indiquer la route en annotation de votre contrôleur :

```php
<?php
// src/Controller/MyPageController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyPageController
{
    /**
     * @Route("/some-path-to/my_page", name="my_route_name")
     */
    public function myFunctionToGenerateThePage(): Response
    {
        return new Response(
            '<html><body>Hey guys!</body></html>'
        );
    }
}
```

**Remarque :** Si vous utilisez PHP 8, vous pouvez utiliser directement les annotations du langage :

```php
#[Route('/some-path-to/my_page', name: 'my_route_name')]
```

### Liste des routes

Pour lister les routes disponible et les contrôleurs associés, utilisez la commande suivante :

```bash
bin/console debug:router
```

## Moteur de template

Pour faciliter l'écriture de pages HTML, Symfony propose le moteur de templates **Twig**.

Il faut installer la dépendance :

```bash
coomposer require twig
```

Vous pouvez désormais utiliser ce moteur dans vos contrôleurs. Pour y avoir accès, la solution
la plus simple est de faire étendre vos contrôleurs de la classe `AbstractController`. Ex :

```diff
  // src/Controller/MyPageController.php

  // ...
+ use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

- class MyPageController
+ class MyPageController extends AbstractController
  {
      // ...
  }
```

La méthode `render()` vous permet de transformer un template twig en du code HTML et de 
le placer dans un objet `Response` :

```php
<?php
// src/Controller/MyPageController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyPageController extends AbstractController
{
    /**
     * @Route("/some-path-to/my_page", name="my_route_name")
     */
    public function myFunctionToGenerateThePage(): Response
    {
        return $this->render('some-path-to/my-page.html.twig', [
            'who' => 'guys',
        ]);
    }
}
```

```twig
{# templates/some-path-to/my-page.html.twig #}
<html>
<body>
    <h1>Hey {{ who }}!</h1>
</body>
</html>
``` 

**Remarques :** 

Ici, on a définit la variable `who` avec pour valeur `guys` et on l'a transmise au template
en argument de la fonction `render()`. La syntaxe `{{ my_var }}` permet de l'afficher.

En général, on n'ajoute pas les balises `<html>` et `<body>` directement dans le template.
À la place, on utilise **l'héritage** de Twig et notre template étend alors `base.html.twig`, par exemple.

## Structure du projet

Voici les principaux répertoires de votre projet Symfony :

```
├── bin/
│   └── console     # exécutable pour lancer les commandes Symfony
├── config/         # répertoire contenant les fichiers de config des routes, services et dépendances
├── public/         # répertoire contenant tous les fichiers accessibles publiquement
│   ├── a-public-file.txt
│   └── index.php
├── src/            # répertoire contenant tout votre code PHP
├── templates/
├── var/            # répertoire contenant les fichiers auto-générés
│   ├── cache/
│   └── log/
├── vendor/         # répertoire contenant les dépendances installées avec Composer (dont Symfony !)
```
