SAVEPOINT DOCTRINE_2 does not exist
===================================

[:type]: # "Erreur"
[:version]: # "Symfony 5+"
[:created_at]: # "2025/02/12"
[:modified_at]: # "2025/02/12"
[:tags]: # "symfony php"

Lors de l’exécution d’une migration Doctrine pour le déploiement d’un projet **Symfony 5.x**, vous lancez généralement
cette commande :

```bash
php bin/console doctrine:migration:migrate --all-or-nothing --no-interaction
```

Vous pouvez alors obtenir de MySQL l’erreur `SAVEPOINT DOCTRINE_2 does not exist`. Cela veut dire que **MySQL** 
n’arrive pas à utiliser le système de transaction pour effectuer un rollback.

À défaut de permettre à MySQL d’effectuer son rollback correctement, vous pouvez lui éviter d’avoir à le faire, 
en corrigeant la migration qui pose le problème.

Plutôt que d’exécuter toutes les migrations d’une traite, exécutez-les une par une, via la commande suivante :

```bash
php bin/console doctrine:migration:migrate next --no-interaction
```

Si la migration échoue, le vrai message d’erreur SQL qui vous intéresse s’affichera, et pas celle concernant le 
rollback. Si elle réussit, relancez la commande jusqu’à trouver la migration fautive.

Pour connaître l’état des migrations, utilisez la commande **status** :

```bash
php bin/console doctrine:migration:status
```
