[Symfony 4] L'utilisateur connecté est anonyme
==============================================

[:type]: # "Erreur"
[:version]: # "Symfony 4"
[:created_at]: # "2019/08/27"
[:modified_at]: # "2019/09/19"
[:tags]: # "symfony php"

Si vous utilisez **JWT** pour gérer l'authentification entre le backend Symfony et un front autre (ex: Angular), 
il peut arriver que le jeton "se perde" entre les deux.

Par exemple :

* Vous vous connectez via la mire de login en front, recevez bien un jeton JWT en réponse
* Vous lancer une autre requête auprès du backend, et vous voyez bien (dans la console de votre navigateur) que le jeton JWT est passé dans les headers
* Dans votre contrôleur Symfony, vous récupérez l'utilisateur connecté (par exemple via `AbstractController::getUser()`) mais il est `null`

Si vous utilisez **PHP-FPM** avec **Apache** le problème peut venir du passage des headers entre les deux.
Symfony préconise d'ajouter la ligne suivante dans le Virtual host de votre application :

```apacheconfig
SetEnvIfNoCase ^Authorization$ "(.+)" HTTP_AUTHORIZATION=$1
```

Pour vérifier si vous avez ce symptôme, il suffit de consulter le **profiler Symfony**.
Parmi les headers de la partie **Request / Response**, vous devriez trouver le jeton JWT (comme c'est le cas dans la console de votre navigateur).
