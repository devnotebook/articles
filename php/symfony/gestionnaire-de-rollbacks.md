Gestionnaire de rollbacks
=========================

[:type]: # "Astuce"
[:version]: # "Symfony 6"
[:created_at]: # "2023/07/13"
[:modified_at]: # "2023/07/13"
[:tags]: # "symfony php"
[:version]: # "PHP 8.2"

## Problématique

La plupart des SGBD proposent un fonctionnement en transactions qui permet, en cas d’erreur, de retrouver les données
telles qu’ells étaient au début de la transaction.

Schématiquement, cela se passe ainsi :

- On déclare le début d’une transaction
- On effectue toutes les opérations que l’on souhaite
- Si l’on considère que tout s’est bien passé, on valide la transaction et toutes les modifications de données induites
  par les opérations sont entérinées
- Si, à l’inverse, une opération échoue, on effectue un rollback qui remet les données comme elles étaient au début

Ce fonctionnement est facile à mettre en œuvre dans **Symfony**, avec
[Doctrine](https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/transactions-and-concurrency.html).
C’est plutôt simple et efficace, mais cela ne concerne **que** les écritures en base de données.

On peut avoir exactement le même besoin pour des écritures via des appels API.

### Exemple avec une création de compte

- Je crée un compte utilisateur dans mon application (donnant lieu à une écriture en base de données)
- J’appelle une API externe d’authentification, pour lui demander de créer ce compte utilisateur (dans sa base de
  données)
- J’appelle une autre API externe de gestion de fiches client, pour lui demander d’en créer une nouvelle pour cet
  utilisateur (dans sa base de données)

L’appel à la seconde API échoue. Sans fiche client, l’application ne fonctionnera pas correctement et on préfère donc
annuler complètement la création du compte. Il nous faut donc annuler :

- l’écriture dans la BDD de l’application
- l’écriture réalisée auprès de la 1<sup>re</sup> API

Cet exemple est encore relativement simple s’il n’y a que deux traitements à annuler, mais s’ils sont nombreux, un
service dédié aux rollbacks va vite devenir utile.

## Service de gestion de rollbacks

Voici un exemple simple d’un tel service, avec deux méthodes

- `registerRollbackOperation()` : ajoute une opération de rollback dans un registre interne au service
- `rollbackAll()` : exécute toutes les opérations de rollbacks du registre

**Note** :

Contrairement à celui de Doctrine, on ne déclare pas de début de transaction, et on ne valide pas de transaction.  
On ne peut donc pas réinitialiser le registre pour commencer une seconde transaction.  
On peut tout de même gérer plusieurs lots de rollbacks indépendants, en utilisant plusieurs
instances du service en parallèle.

```php
<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class RollbackService
{
    private array $rollbackOperations = [];

    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * Ajoute une opération de rollback au registre.
     *
     * @param string $pool Lot d’opération
     * @param callable $rollbackFunction Fonction à exécuter lors du rollback
     * @param array $rollbackArguments Arguments à passer à la fonction de rollback
     * @param array $context Contexte de l’opération, utile pour les logs
     *
     * @return void
     */
    public function registerRollbackOperation(
        string $pool,
        callable $rollbackFunction,
        array $rollbackArguments,
        array $context,
    ): void {
        $this->rollbackOperations[] = [
            'pool' => $pool,
            'function' => $rollbackFunction,
            'arguments' => $rollbackArguments,
            'context' => $context,
        ];
    }

    /**
     * Dépile et exécute les opérations de rollback du registre, une à une, de la dernière à la première ajoutée.
     *
     * @return void
     */
    public function rollbackAll(): void
    {
        $totalNbOfOperations = \count($this->rollbackOperations);
        $index = 0;
        while ($rollbackOperation = array_pop($this->rollbackOperations)) {
            $index++;
            $this->logger->debug(
                sprintf('Rollback %s/%s - %s', $index, $totalNbOfOperations, $rollbackOperation['pool']),
            );

            try {
                $rollbackOperation['function'](...$rollbackOperation['arguments']);
                $this->logger->debug(
                    sprintf(
                        'Rollback %s/%s successful - %s %s',
                        $index,
                        $totalNbOfOperations,
                        $rollbackOperation['pool'],
                        $this->argsAndContextToString($rollbackOperation),
                    ),
                );
            } catch (\Exception $e) {
                $this->logger->error(
                    sprintf(
                        'Failure for rollback %s/%s - %s : %s %s',
                        $index,
                        $totalNbOfOperations,
                        $rollbackOperation['pool'],
                        $e->getMessage(),
                        $this->argsAndContextToString($rollbackOperation),
                    ),
                    ['exception' => $e],
                );
            }
        }
    }

    private function argsAndContextToString(array $rollbackOperation): string
    {
        return sprintf(
            '(args : %s; context : %s)',
            $this->encode($rollbackOperation['arguments']),
            $this->encode($rollbackOperation['context']),
        );
    }

    private function encode(mixed $context): string
    {
        return $this->serializer->serialize($context, 'json');
    }
}

```

Exemple d’utilisation, combiné avec les transactions Doctrine:

```php
<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

class AccountCreationService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Api1Service $api1Service,
        private readonly Api2Service $api2Service,
        private readonly RollbackService $rollbackService,
    ) {
    }

    public function createAccountFromAdminInterface(array $data): void
    {
        $this->entityManager->beginTransaction();
        try {
            $this->persistUser($data);
            $this->createAccount($data);
            $this->createCustomerSheet($data);

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->rollbackService->rollbackAll();
            $this->entityManager->rollBack();

            throw $e;
        }
    }

    public function persistUser(array $data): void
    {
        $this->entityManager->persist(User::fromData($data));
        $this->entityManager->flush();
    }

    public function createAccount(array $data): void
    {
        $accountId = $this->api1Service->createAccount($data);

        $this->rollbackService->registerRollbackOperation(
            'API 1',
            $this->api1Service->deleteAccount(...),
            [$accountId],
            [
                'description' => 'Account creation from admin interface',
            ],
        );
    }

    public function createCustomerSheet(array $data): void
    {
        $customerSheetId = $this->api2Service->createCustomerSheet($data);

        $this->rollbackService->registerRollbackOperation(
            'API 2',
            $this->api2Service->deleteCustomerSheet(...),
            [$customerSheetId],
            [
                'description' => 'Account creation from admin interface',
            ],
        );
    }
}
```

**Explications** :

- À la création d’un compte depuis l’interface d’admin, on veut exécuter 3 opérations, que l’on place dans un try-catch
- Comme on veut être capable de réinitialiser des données dans la BDD de notre application via **Doctrine**, on encadre
  ces opérations avec les méthodes `beginTransaction()` et `commit()` de l’entity Manager.
- Les deux méthodes de création auprès des API sont similaires : on demande la création d’un utilisateur, puis on
  déclare sa suppression comme opération de rollback.
- En cas d’erreur, on déclenche les opérations de rollback de notre `RollbackService` et le rollback de **Doctrine**.

**Notes** :

- La méthode `User::fromData()` simule l’instanciation d’une entité `User` à partir d’un tableau de données
- La notation `myfunction(...)` permet de générer une `callable` depuis **PHP 8.2**.
- On utilise le Sérialiseur de Symfony pour transformer les tableaux de contexte et d’arguments en chaîne JSON pour
  les logs

## Aller plus loin

### Organisation des services

Si on appelle à plusieurs endroits les fonctions de création pour les API, on peut imaginer la structure suivante :

- Service\Api1Service
- Service\Api1ServiceWithRollbacks

Le premier service effectue les appels basiques à l’API (typiquement des requêtes HTTP).  
Le second service utilise le premier. Il contient des méthodes comme le `createCustomerSheet()` ci-dessus,
qui gèrent plusieurs appels successifs à l’API.

### Rollback des suppressions/modifications

Pour rétablir les données telles qu’avant suppression ou modification, il faut les passer en argument de l’opération
de rollback. Pour cela, il faut déjà les avoir sous la main, et donc les avoir récupérées auprès de l’API.

On peut donc imaginer une méthode `updateCustomerSheet()` qui effectue ces étapes :

- Appel de l’API pour mémoriser la fiche client actuelle
- Appel de l’API pour mettre à jour la fiche avec les nouvelles données
- Déclaration de l’opération de callback : un appel à l’API pour mettre à jour la fiche avec les données mémorisées

### RollbackService

On pourrait implémenter un `RollbackService` plus complet/complexe, permettant :

- de gérer des lots d’opérations de rollback, que l’on pourrait appeler **transactions**
- d’intégrer directement le rollback doctrine
