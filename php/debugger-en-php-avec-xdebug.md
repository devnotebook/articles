Débugger en PHP avec XDebug
===========================

[:type]: # "Astuce"
[:created_at]: # "2012/09/14"
[:modified_at]: # "2017/07/31"
[:tags]: # "php"

Lorsque vous développez en PHP, déboguer avec une interface graphique peut vous faire gagner 
beaucoup de temps. Les IDE comme **Eclipse** ou **NetBeans** proposent ce genre 
d'interface pour Java, mais également pour PHP.

Grâce à eux, vous pouvez mettre des points d’arrêt, exécuter le programme pas à pas et 
voir l’évolution des valeurs des variables en temps réel, à chaque étape de l'exécution 
du script.

Pour vérifier l’installation de XDebug sur votre serveur, créez un page PHP 
contenant l'appel à la méthode `phpinfo()`.

Une page de la forme suivante devrait apparaître :

![Début phpinfo()](./debugger-en-php-avec-xdebug-01.png)

Plus bas dans la page, l'encadré suivant devrait apparaître si XDebug est activé :

![XDebug phpinfo()](./debugger-en-php-avec-xdebug-02.png)

## Installation de XDebug pour WampServer ##

XDebug est installé par défaut sur WampServer, mais n’est pas configuré en mode remote 
(débogage à distance).
Pour modifier la configuration de Xdebug :

* Éditez votre fichier `php.ini` (icône Wampserver > PHP > php.ini)
* Modifiez ainsi les lignes de la section `xdebug` du fichier :
    
```ini
[xdebug]
xdebug.remote_enable=on
xdebug.remote_host="127.0.0.1"
xdebug.remote_port=9000
xdebug.remote_handler="dbgp"
xdebug.remote_mode=req
```

*  Enregistrez le fichier et redémarrez WampServer.


## Installation de XDebug sous Debian ##

* Commencez par installer le paquet XDebug

```bash
sudo apt-get install php5-xdebug
```

* Éditez le fichier de configuration de XDebug :

```bash
sudo gedit /etc/php5/conf.d/xdebug.ini
```

* Modifiez ainsi les lignes de la section XDEBUG Extension du fichier :

```ini
[xdebug]
xdebug.remote_enable=on
xdebug.remote_host="127.0.0.1"
xdebug.remote_port=9000
xdebug.remote_handler="dbgp"
xdebug.remote_mode=req
```

* Sauvegardez le fichier et redémarrez Apache :

```bash
sudo /etc/init.d/apache2 restart
```

## Module easy Xdebug pour Firefox ##

Pour simplifier le démarrage d’une session XDebug, il existe [l'extension easy Xdebug 
pour Firefox](https://addons.mozilla.org/en-us/firefox/addon/easy-xdebug/). 
Une fois installée, deux icônes s’affichent dans la barre d'outils. 
Lorsque le débogage n'est pas actif, cliquez sur 
![Activer Easy XDebug](./debugger-en-php-avec-xdebug-03.jpg)
pour l'activer. Pour le désactiver, cliquez sur
![Désactiver Easy XDebug](./debugger-en-php-avec-xdebug-04.png)

## Configuration d’Eclipse pour XDebug ##

Il s’agit maintenant d’indiquer à Eclipse que vous souhaitez utiliser XDebug pour déboguer votre projet :

* Menu **Window** > **Preferences**
* Sélectionnez l’item **PHP** > **Debug**
* À la ligne **PHP Debugger** choisissez **XDebug**

![Config XDebug Netbeans](./debugger-en-php-avec-xdebug-05.jpg)

* Cliquez sur le lien **configure...** de XDebug
* Sélectionnez **XDebug** dans la liste **Installed Debuggers**
* Bouton **Configure**
* Dans la liste Accept remote session (JIT), choisissez **any**
* Cliquez deux fois sur **OK**


## Configuration de NetBeans pour XDebug ##

* Clic-droit sur votre projet > **Properties**
* Cliquez sur **Run configuration** dans la barre de gauche
* Remplissez les champs demandés :
  * Run as : Local Web Site
  * Project URL : l'url de la page d'accueil de votre site (ex : `http://monsite.com`)
  * Index File : le fichier index de votre site (a priori `index.php`)
* Cliquez sur le bouton **Advanced...**
* Sélectionnez **Do Not Open Web Browser**
* Cliquez deux fois sur **OK**


## Activer le débogueur pour NetBeans ##

Pour activer le débogueur, ouvrez NetBeans et cliquez sur **Debug Project** (Ctrl + F5). 
Dans votre navigateur, cliquez ensuite sur le bouton activer le débogage de 
l'extension easy Xdebug (sous Firefox).

**Remarque :**
Si vous utilisez un autre navigateur que Firefox, vous pouvez activer le débogage 
en ajoutant `?XDEBUG_SESSION_START=netbeans-xdebug` à la fin de votre URL. 
Un cookie sera ainsi créé et repéré par NetBeans, pour utiliser le débogage.
