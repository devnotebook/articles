Limiter la version à mettre à jour avec Composer
================================================

[:type]: # "Astuce"
[:sources]: # "[getcomposer.org](https://getcomposer.org/doc/articles/versions.md#tilde)"
[:created_at]: # "2017/04/20"
[:modified_at]: # "2017/08/09"
[:tags]: # "php composer"

Lorsque vous mettez à jour vos dépendances avec Composer, vous ne souhaitez pas avoir la dernière version 
disponible.

Par exemple, si j'ai un site Drupal en **v8.2.7** et qu'une mise à jour de sécurité sort, 
je peux préférer passer en **v8.2.8** plutôt qu'en **8.3.1**.

Dans ce cas on peut préciser ça dans le fichier `composer.json` :

```json
{
...
  "require": {
    "drupal/core": "~8.2.0",
    ...
  },
...
}
```

**Explications :**

* **~8.2.0** signifie >= 8.2.0 & < 8.3.0
* **~8.2** signifie >= 8.2 & < 9
* Il existe aussi le signe `^`, moins restrictif : ^8.2.1 signifie >=8.2.1 & < 9
