DomPDF : Affichage des images et prise en compte des feuilles de styles
=======================================================================

[:type]: # "Erreur"
[:sources]: # "[ditchx.net](http://ditchx.net/archives/130)"
[:created_at]: # "2014/11/23"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

DomPDF peut poser problème pour afficher les images et prendre en compte les feuilles de style.

A priori c'est parce que les URL vers ces fichiers sont relatives et commencent par un `/`. 
Pour éviter ce problème il suffit de modifier deux fichiers de l'API en supprimant 
ce caractère au début des URL, avec ce code :

```php
// Suppression du / devant l'url 
$url = ltrim($url, '/');
```

Modifiez `dompdf/include/dompdf.cls.php` :

```php
  protected function _process_html() {
    $this->save_locale();
 
    $this->_tree->build_tree();
 
    $this->_css->load_css_file(Stylesheet::DEFAULT_STYLESHEET, Stylesheet::ORIG_UA);
 
    $acceptedmedia = Stylesheet::$ACCEPTED_GENERIC_MEDIA_TYPES;
    $acceptedmedia[] = $this->get_option("default_media_type");
 
    // <base href="" />
    $base_nodes = $this->_xml->getElementsByTagName("base");
    if ( $base_nodes->length && ($href = $base_nodes->item(0)->getAttribute("href")) ) {
      list($this->_protocol, $this->_base_host, $this->_base_path) = explode_url($href);
    }
 
    // Set the base path of the Stylesheet to that of the file being processed
    $this->_css->set_protocol($this->_protocol);
    $this->_css->set_host($this->_base_host);
    $this->_css->set_base_path($this->_base_path);
 
    // Get all the stylesheets so that they are processed in document order
    $xpath = new DOMXPath($this->_xml);
    $stylesheets = $xpath->query("//*[name() = 'link' or name() = 'style']");
 
    foreach($stylesheets as $tag) {
      switch (strtolower($tag->nodeName)) {
        // load <link rel="STYLESHEET" ... /> tags
        case "link":
          if ( mb_strtolower(stripos($tag->getAttribute("rel"), "stylesheet") !== false) || // may be "appendix stylesheet"
            mb_strtolower($tag->getAttribute("type")) === "text/css" ) {
            //Check if the css file is for an accepted media type
            //media not given then always valid
            $formedialist = preg_split("/[\s\n,]/", $tag->getAttribute("media"),-1, PREG_SPLIT_NO_EMPTY);
            if ( count($formedialist) > 0 ) {
              $accept = false;
              foreach ( $formedialist as $type ) {
                if ( in_array(mb_strtolower(trim($type)), $acceptedmedia) ) {
                  $accept = true;
                  break;
                }
              }
 
              if (!$accept) {
                //found at least one mediatype, but none of the accepted ones
                //Skip this css file.
                continue;
              }
            }
 
            $url = $tag->getAttribute("href");
 
            // Suppression du / devant l'url
            $url = ltrim($url, '/');
 
            $url = build_url($this->_protocol, $this->_base_host, $this->_base_path, $url);
 
            $this->_css->load_css_file($url, Stylesheet::ORIG_AUTHOR);
          }
          break;
```

Et `dompdf/include/image_frame_decorator.cls.php` :

```php
  function __construct(Frame $frame, DOMPDF $dompdf) {
    parent::__construct($frame, $dompdf);
    $url = $frame->get_node()->getAttribute("src");
 
     // Suppression du / devant l'url
     $url = ltrim($url, '/');
 
    $debug_png = $dompdf->get_option("debug_png");
    if ($debug_png) print '[__construct '.$url.']';
 
    list($this->_image_url, /*$type*/, $this->_image_msg) = Image_Cache::resolve_url(
      $url,
      $dompdf->get_protocol(),
      $dompdf->get_host(),
      $dompdf->get_base_path(),
      $dompdf
    );
 
    if ( Image_Cache::is_broken($this->_image_url) &&
         $alt = $frame->get_node()->getAttribute("alt") ) {
      $style = $frame->get_style();
      $style->width  = (4/3)*Font_Metrics::get_text_width($alt, $style->font_family, $style->font_size, $style->word_spacing);
      $style->height = Font_Metrics::get_font_height($style->font_family, $style->font_size);
    }
  }
```
