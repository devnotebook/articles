Créer des graphiques en PHP
===========================

[:type]: # "Marque-page"
[:created_at]: # "2014/11/23"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

L'API [JpGraph](http://jpgraph.net/) permet de créer tous les types de graphiques en PHP.

En quelques lignes de code, vous pouvez créer un camembert, un histogramme ou d'autres graphiques plus complexes.

Voici un aperçu de la galerie disponible sur le site :

![Galerie JpGraph](./creer-des-graphiques-en-php-01.png)
