Autoriser Composer à utiliser toute la mémoire dont il a besoin
===============================================================

[:type]: # "Astuce"
[:sources]: # "[stackoverflow.com](https://stackoverflow.com/a/36107762/2674501)"
[:created_at]: # "2020/02/14"
[:modified_at]: # "2020/02/14"
[:tags]: # "php composer"

Pour lancer Composer sans la limite de mémoire imposée par la configuration de PHP, on peut utiliser cette commande :

```bash
php -d memory_limit=-1 $(which composer) update
```

**Explications :**

* On lance Composer via PHP, ce qui permet de surcharger la valeur de `memory_limit` (`-1` pour infinie)
* `which` est une commande système qui affiche le chemin complet vers un exécutable (ici  `composer`)
* On ajoute ensuite les arguments classiques à passer à Composer

