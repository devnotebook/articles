Apache sert de vieux fichiers statiques
=======================================

[:type]: # "Erreur"
[:created_at]: # "2015/04/20"
[:modified_at]: # "2017/08/08"
[:tags]: # "php"

Si vous développez avec **VirtualBox** et Apache, il vous êtes peut-être arrivé le même problème.

Vous avez une image ou un fichier CSS servi par Apache. 
Vous le modifiez et lorsque vous tentez d'y accéder via le navigateur c'est l'ancienne version que vous voyez.

C'est un **bug de VirtualBox** qui cause le problème ([voir le ticket](https://www.virtualbox.org/ticket/819)).

Pour l'éviter, ajouter la ligne suivante dans votre configuration d'Apache 
(`.htaccess`, `httpd.conf`, configuration de virtualhost, ...) :

```apacheconfig
EnableSendfile Off
```

**Remarque :**
N'oubliez pas de redémarrer Apache après ça.
