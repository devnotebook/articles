$facebook->getUser() always returns 0
=====================================

[:type]: # "Erreur"
[:created_at]: # "2013/12/23"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

Si vous utilisez le SDK Facebook pour PHP en suivant l'exemple 
fourni avec [les sources](https://github.com/facebook/facebook-php-sdk) vous aurez peut-être ce problème : 
**la méthode `getUser()` qui retourne toujours 0**.

De nombreuses personnes ont eu ce problème (cf. [recherche Google](https://www.google.fr/#q=facebook+getuser(+return+0)), 
pour des raisons diverses et variées.

Voici une explication possible : 
le SDK ne trouve pas le certificat fourni avec les sources (`fb_ca_chain_bundle.crt`).

Pour savoir si vous avez cette erreur, consultez les logs d'Apache et cherchez le message : 
```
Invalid or no certificate authority found, using bundled information.
``` 
Si vous le trouvez, c'est que le certificat ne se trouve pas au niveau de votre script.

Pour spécifier le bon chemin vers le fichier, utilisez cette ligne de code :

```php
\Facebook::$CURL_OPTS[CURLOPT_CAINFO] = getcwd() . '/path_depuis_le_repertoire_courant/fb_ca_chain_bundle.crt';
```
