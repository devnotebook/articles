Mocker des fonctions natives PHP
================================

[:type]: # "Astuce"
[:version]: # "PHP 7, PHPUnit 6"
[:created_at]: # "2019/05/10"
[:modified_at]: # "2024/06/18"
[:tags]: # "php"

PHPUnit fournit des outils pour mocker des classes et leurs méthodes. Ex :

```php
$valueINeed = 'my_value';

$myMockedObject = $this->createMock(MyClassToMock::class);
$myMockedObject->method('myMethodToMock')
    ->willReturn($valueINeed);
```

Ce n'est pas le cas pour les fonctions natives, comme `time()` ou `random_bytes()`.  
C'est problématique si vous voulez tester une méthode comme celle-ci :

```php
// src/Generator/TokenGenerator.php
namespace App\Generator;

use App\Exception\TokenGenerationException;

class TokenGenerator
{
    public function generateBase64Token(int $byteLength): string
    {
        if ($byteLength <= 0) {
            throw new \InvalidArgumentException('The length must be greater than zero');
        }

        try {
            $token = random_bytes($byteLength);
        } catch(\Exception $e) {
            throw new TokenGenerationException('An unexpected error has occurred');
        }

        return base64_encode($token);
    }
}
```

## Gérer le retour d'une fonction native

À la place de mocker la fonction, vous pouvez la redéfinir à votre convenance,
dans le namespace où vous en avez besoin.

Par exemple :

```php
namespace App\Generator;

function random_bytes(int $length) {
    return 'fake_random_string';
}
```

Lors de l'exécution, PHP cherchera la fonction dans le namespace courant, puis dans le namespace racine `/` s'il ne la trouve pas.  
(À noter que si vous utilisez `\random_bytes()`, PHP cherchera alors uniquement dans le namespace racine.)

On peut ainsi tester la méthode correctement :

```php
// tests/What/Ever/Probably/Just/App/Generator/TokenGeneratorTest.php
namespace App\Generator {
    function random_bytes(int $length) {
        return 'fake_random_string';
    }
}

namespace What\Ever\Probably\Just\App\Generator {

    use App\Generator\TokenGenerator;
    use PHPUnit\Framework\TestCase;

    class TokenGeneratorTest extends TestCase
    {
        /** @var TokenGenerator */
        private $tokenGenerator;

        public function testGenerateBase64Token(): void
        {
            $validLength = 128;
            $generated = $this->tokenGenerator->generateBase64Token($validLength);

            $expected = 'ZmFrZV9yYW5kb21fc3RyaW5n';

            $this->assertEquals($expected, $generated);
        }

        public function testGenerateBase64TokenWithInvalidLength(): void
        {
            $invalidLength = -12;

            $this->expectException(\InvalidArgumentException::class);

            $this->tokenGenerator->generateBase64Token($invalidLength);
        }

        protected function setUp(): void
        {
            parent::setUp();
            $this->tokenGenerator = new TokenGenerator();
        }
    }
}
```

**Remarque :** Quand il y a plusieurs namespace utilisés dans un même fichier PHP,
on précise le code concerné en l'encadrant par des ``{}``.

## Gérer plusieurs retours d'une fonction native

Imaginons maintenant qu'on veuille tester le cas où la fonction `random_bytes()` lève une exception,
puisque la documentation nous informe que cela peut arriver (en cas de problème mémoire).

Il faut donc que notre "fonction de surcharge" puisse retourner notre valeur "bouchon" ou lever une exception.  
La difficulté c'est qu'on souhaite sans modifier sa signature, qu'elle ait un comportement différent
selon le cas de test où on l'appelle.

Pour ça il y a une astuce (assez sale) : **utiliser une variable globale** pour indiquer à la fonction le comportement à suivre.

On peut ainsi modifier notre fonction de surcharge et implémenter un 3e cas de test :

```php
// tests/What/Ever/Probably/Just/App/TokenGeneratorTest.php
namespace {
    $randomBytesFuncThrowsException = false;
}

namespace App\Generator {

    function random_bytes(int $length) {
        global $randomBytesFuncThrowsException;

        if (isset($randomBytesFuncThrowsException) && $randomBytesFuncThrowsException === true) {
            throw new \Exception();
        }

        return 'fake_random_string';
    }
}

namespace What\Ever\Probably\Just\App\Generator {

    use App\Exception\TokenGenerationException;
    use App\Generator\TokenGenerator;
    use PHPUnit\Framework\TestCase;

    class TokenGeneratorTest extends TestCase
    {
        // [...]

        public function testGenerateBase64TokenWithUnexpectedError(): void
        {
            global $randomBytesFuncThrowsException;

            $randomBytesFuncThrowsException = true;
            $noMatterWhatLength = 666;

            $this->expectException(TokenGenerationException::class);

            $this->tokenGenerator->generateBase64Token($noMatterWhatLength);
        }

        // [...]
    }
}
```

**Remarque :** Pour déclarer une variable globale, on se place dans le namespace racine.

## DateTime

Un autre cas courant et problématique est le constructeur `DateTime()`.

Quand on veut la date courante, on l'utilise souvent sans argument et là encore, pas moyen de mocker l'objet généré. 
À chaque test l'heure aura un peu avancé.

La solution toute simple consiste à utiliser le premier argument du constructeur :

```php
// Remplacer
$dateTime = new \DateTime();

// par
$dateTime = new \DateTime(sprintf('@%s', time()));
```

On peut alors utiliser la technique présentée plus haut, et **surcharger la fonction** `time()` :

```php
// tests/Some/Where/SomethingWhichDealsWithDateTest.php
namespace Here\I\Instantiate\Datetime {
    function time()
    {
        return 1557481811;
    }
}

namespace Some\Where {

    class SomethingWhichDealsWithDateTest extends KernelTestCase
    {

        private $someServiceWhichInstantiatesDateTime;

        public function testMethodWhichInstantiatesDatetime(): void
        {
            $this->someServiceWhichInstantiatesDateTime->getDatetimeInstance();

            // TODO: test something
        }
    }

    // [...]
}
```

## Déclaration multiple

A fortiori, tous vos tests seront exécutés dans un même contexte, ce qui veut dire que toute nouvelle fonction
créée ne doit l’être qu’une seule fois.

Si vous avez besoin de mocker la date courante dans deux classes de test et que vous déclarez dans chacune 
d’elle une fonction `time()`, vous aurez le droit à une erreur du genre :

```
Fatal error: Cannot redeclare Here\I\Instantiate\Datetime\time()
```

Pour éviter cela, englobez votre déclaration en utilisant `function_exists()`. Exemple :

```php
namespace App\Helper {

    if (!\function_exists('Here\I\Instantiate\Datetime')) {
        function time(): int
        {
            // 21/12/2023 14:56:08
            return 1703166968;
        }
    }
}
```

Attention, cela veut dire que **la date courante sera la même partout**.
