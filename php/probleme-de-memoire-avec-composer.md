Problème de mémoire avec composer
=================================

[:type]: # "Erreur"
[:source]: # "[stackoverflow.com](http://stackoverflow.com/questions/18116261/php-composer-update-cannot-allocate-memory-error-using-laravel-4)"
[:created_at]: # "2017/03/24"
[:modified_at]: # "2017/08/09"
[:tags]: # "php composer"

Si vous utilisez Composer dans une VM, vous pouvez avoir une erreur de mémoire dûe à un problème de swap :

```
Installation failed, reverting ./composer.json to its original content.
The following exception is caused by a lack of memory or swap, or not having swap configured
Check https://getcomposer.org/doc/articles/troubleshooting.md#proc-open-fork-failed-errors for details
 
[ErrorException]
proc_open(): fork failed - Cannot allocate memory
```

Plusieurs solutions sont décrites dans [ce ticket sur Stackoverflow](http://stackoverflow.com/questions/18116261/php-composer-update-cannot-allocate-memory-error-using-laravel-4).

Celle (temporaire) qui marche particulièrement bien est la suivante. Il suffit de lancer les commandes :

```bash
/bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
/sbin/mkswap /var/swap.1
/sbin/swapon /var/swap.1
```
