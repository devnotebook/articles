Les nouveaux opérateurs introduits par PHP 7
============================================

[:type]: # "Astuce"
[:version]: # "PHP 7.x"
[:sources]: # "[php.net](http://php.net/manual/fr/migration70.new-features.php)"
[:created_at]: # "2019/02/06"
[:modified_at]: # "2019/12/11"
[:tags]: # "php fonctions-utiles"

PHP 7 introduit 2 nouveaux opérateurs : `??` et `<=>`, nommés respectivement **Null coalescent** et **Spaceship**.

## Null coalescent ##

Cet opérateur permet de simplifier l'affectation des valeurs par défaut via les opérateurs ternaires :

```php
$value = (isset($x) && $x !== null) ? $x : $defaultValue;
```

peut maintenant s'écrire : 

```php
$value = $x ?? $defaultValue;
```

Comme l'indique [la documentation](http://php.net/manual/fr/migration70.new-features.php#migration70.new-features.null-coalesce-op),
il permet de vérifier si une variable existe **et** est non nulle.

**Remarque :**
Il ne faut pas le confondre avec l'opérateur ternaire "abrégé" (depuis PHP 5.3), à savoir `?:`.
Celui-ci permet juste d'omettre la partie centrale, sans vérifier l'existence de la variable.

```php
$value = $x ?: $valueIfXIsFalse;
```

`$value` devient `$x` si `$x` est considéré comme "vrai" . `$value` devient `$valueIfXIsFalse` si `$x` est considéré comme "faux".  
Contrairement au null coalescent, il reverra un **warning si `$x` n'est pas défini**.

## Spaceship ##

Cet opérateur permet de simplifier la comparaison entre deux variables :

```php
$comparison = ($a < $b) ? -1 : (($a > $b) ? 1 : 0);
```

peut maintenant s'écrire :  

```php
$comparison = $a <=> $b;
```

Comme l'indique [la documentation](http://php.net/manual/fr/migration70.new-features.php#migration70.new-features.spaceship-op),
cet opérateur retourne donc `-1`, `1` ou `0` selon la différence entre `$a` et `$b`.
