Installer PEAR pour PHP et WampServer
=====================================

[:type]: # "Astuce"
[:created_at]: # "2013/06/13"
[:modified_at]: # "2017/08/07"
[:tags]: # "php"

PEAR est un gestionnaire de librairies pour PHP, permettant d'étendre les fonctionnalités de PHP par simple 
ligne de commande.
Il tend toutefois à disparaître au profit de **Composer**.

## Installation ##

### Récupération de l'exécutable ###

Si le fichier `go-pear.bat` n'est pas présent dans le répertoire de PHP de **WampServer** :

* Téléchargez le fichier `go-pear.phar` ([http://pear.php.net/go-pear.phar](http://pear.php.net/go-pear.phar)).
* Copiez le fichier dans le répertoire de PHP de WampServer (ex : `D:\Dev\wamp\bin\php\php5.3.13\`).

### Exécution de l'installeur ###

Une fois le fichier batch en place, lancez l'invite de commande et exécutez-le :

```bash
cd D:\Dev\wamp\bin\php\php5.1.13\
go-pear
```

Durant l'installation, appuyez toujours sur `Entrée` ou `Y`, pour utiliser les paramètres par défaut.

### Configuration de l'environnement ###

Lancez le fichier `PEAR_ENV.reg` présent dans le répertoire PHP de WampServer, 
pour mettre à jour les clés de registre nécessaires. (Double-cliquez sur le fichier pour le lancer.)

## Mise à jour de PEAR ##

Pour mettre à jour votre installation, et vérifier que PEAR est bien installé, exécutez la commande suivante :

```bash
pear channel-update pear.php.net
```
