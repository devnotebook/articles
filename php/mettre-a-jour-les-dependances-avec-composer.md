Mettre à jour les dépendances avec Composer
===========================================

[:type]: # "Astuce"
[:created_at]: # "2013/11/18"
[:modified_at]: # "2017/08/07"
[:tags]: # "php composer"

Pour mettre à jour les dépendances d'un projet Symfony (ou pas), vous pouvez utiliser **Composer**.

À l'instar d'`apt` ou `yum` pour des distributions Linux, 
cet outil va mettre à jour votre application par une simple ligne de commande.

Pour **mettre à jour Composer**, utilisez :

```bash
composer self-update
```

Pour **mettre à jour les dépendances** du projet, utilisez :

```bash
composer update
```

Pour ne mettre à jour qu'une seule dépendance, ajoutez-la à la commande :

```bash
composer update ma/dépendance
```

**Remarques :**

* Pour modifier les dépendances, éditez le fichier `composer.json`.
* Il ne faut pas confondre :
    * `composer update` : va chercher la dernière version disponible sur le dépôt et l'installer, 
    * `composer install` : va chercher la version installée lors du dernier `composer update` sur le dépôt 
    et l'installe. Ce numéro de version est stocké dans le fichier `composer.lock`. 
    Cela permet d'avoir une version identique entre chaque environnement.  
