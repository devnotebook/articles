Auditer du code PHP
===================

[:type]: # "Astuce"
[:created_at]: # "2019/05/21"
[:modified_at]: # "2019/05/21"
[:tags]: # "php"

Voici trois outils utiles pour auditer du code PHP de manière statique :

* [PHPStan](https://github.com/phpstan/phpstan)
* [SonarQube](https://www.sonarqube.org/)
* [PHPStorm](https://www.jetbrains.com/phpstorm/)


## PHPStan

### Installation

```bash
cd my_project
wget https://github.com/phpstan/phpstan-shim/raw/master/phpstan.phar
```

### Utilisation

```bash
php phpstan analyse src -l 0
```

**Explications :**

* `src` est le répertoire à analyser. Plusieurs peuvent être passés, séparés par des espaces.
* L'option `-l` permet de choisir le niveau d'acceptation des erreurs, avec `0` le moins strict possible, et `7` pour le plus strict.


## SonarQube

### Installation

Créez un fichier `sonar-project.properties ` à la racine de votre projet, contenant les lignes suivantes :

```ini
sonar.projectKey=my:project
sonar.projectName=My project
sonar.projectVersion=1.0
sonar.sources=src
sonar.sourceEncoding=UTF-8
```

**Explication :** `src` est le répertoire à analyser

**Remarques :**
* Pour retirer certains fichiers/répertoires de l'anlayse, utilisez l'option `sonar.exclusions`
* Les autres options sont listées [dans la documentation officielle](https://docs.sonarqube.org/latest/analysis/analysis-parameters/)


Lancez **SonarQube** via Docker :

```bash
docker pull sonarqube
docker run -d --name sonarqubedocker_sonarqube_1 -p 9000:9000 sonarqube
```

* Accédez à SonarQube avec votre navigateur via cette adresse : [http://localhost:9000](http://localhost:9000).
* L'interface indique que le site est en cours de maintenance. Attendez qu’il soit opérationnel.
* Connectez-vous en tant qu'administrateur (`admin`/`admin`).

Lancez **Sonar runner** avec Docker, pour exécuter l'analyse :

```bash
docker run --link sonarqubedocker_sonarqube_1:sonarqube \
  --entrypoint /opt/sonar-runner-2.4/bin/sonar-runner \
  -e SONAR_USER_HOME=/data/.sonar-cache \
  -v $(pwd):/data -u $(id -u) sebp/sonar-runner \
    -Dsonar.host.url=http://sonarqube:9000 \
    -Dsonar.jdbc.url=jdbc:h2:tcp://sonarqube/sonar \
    -Dsonar.jdbc.username=sonar \
    -Dsonar.jdbc.password=sonar \
    -Dsonar.jdbc.driverClassName=org.h2.Driver \
    -Dsonar.embeddedDatabase.port=9092
```

### Utilisation

L'application SonarQube affiche maintenant le rapport d'analyse de votre pojet.

Voici la page d'accueil du rapport, à partir de laquelle on accède aux alertes remontées et au code associé.

![Accueil rapport SonarQube](./auditer-du-code-php-01.png)


## PHPStorm

PHPStorm affiche des alertes directement dans le code, en fonction des réglages utilisés.
Ceux-ci peuvent être personnalisés par projet ou au niveau global, via **Settings** > **Editor** > **Inspections**.

L'IDE propose aussi une vue d'ensemble de tout le projet.
Pour l'utiliser, faites un clic-droit sur le répertoire à scanner (ex `src`), puis cliquez sur **Inspect code...**

![Rapport PHPStorm](./auditer-du-code-php-02.png)

Le rapport peut ensuite être exporté au format **HTML** ou **XML**.
