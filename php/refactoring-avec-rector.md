Refactoring avec Rector
=======================

[:type]: # "Marque-page"
[:sources]: # "[github.com](https://github.com/rectorphp/rector)"
[:created_at]: # "2022/01/14"
[:modified_at]: # "2022/01/14"
[:tags]: # "php"

Pour faire du refactoring automatique en PHP, on peut utiliser son IDE ou la lib 
[Rector](https://github.com/rectorphp/rector) (installable via **Composer**).

Elle permet notamment d'automatiser la migration de code PHP vers une version plus
récente de PHP.

Quelques exemples :
- Opérateurs `??` et `?:`
- Promotion de propriété de constructeur

Il est également possible de créer ses propres règles de refactoring.

Grafikart a sorti une [vidéo de présentation de l'outil](https://grafikart.fr/tutoriels/rector-php-refactor-1977).
