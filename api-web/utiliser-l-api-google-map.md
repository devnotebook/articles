Utiliser l'API Google Map
=========================

[:type]: # "Astuce"
[:sources]: # "[developers.google.com](https://developers.google.com/maps/documentation/javascript/)"
[:created_at]: # "2015/08/05"
[:modified_at]: # "2020/11/11"
[:tags]: # "api-web cartographie"

Voici une liste d'utilisations courantes de l'API Google Map :

* Créer une carte
* Afficher des points (= marqueurs)
* Afficher un point avec un marqueur personnalisé
* Afficher une bulle au clic sur un marqueur (= InfoWindow)

## Google Key API ##

Pour pouvoir utiliser l'API Google, vous devez tout d'abord avoir un compte Google et générer une clé.

* Rendez-vous à [cette adresse](https://console.developers.google.com) et connectez-vous à votre compte Google
* Créez un nouveau projet en cliquant sur **Continuer**
* Renseignez les noms de domaines qui auront le droit d'utiliser la clé (ex: `*.mon-site.com`, `127.0.0.1`, `localhost`, ...)
* Copiez la clé de l'API :

  ![Copier la clé d'API Google Map](./utiliser-l-api-google-map-01.png)

## Une carte basique ##

![Google Map basique](./utiliser-l-api-google-map-02.png)

Voici le code HTML/Javascript pour afficher une carte simple, sans marqueur :

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <style>
       #map {
        height: 400px;
        width: 600px;
       }
    </style>
  </head>
  <body>
    <h3>My Google Maps Demo</h3>
    <div id="map"></div>
    <script>
    function initMap() {
        var paris_latlong = {lat: 48.866667, lng: 2.333333};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: paris_latlong
        });
    }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap"></script>
  </body>
</html>
```

**Explications :**

* Le code HTML ne contient qu'un élément `div`, dans lequel la carte sera générée en javascript. 
Cet élément **doit avoir une largeur et une hauteur** (cf. CSS).
* On inclue la lib javascript de Google, en lui passant la clé générée précédemment.
* Le code générant la carte ne sera exécuté qu'une fois toute la page chargée
* Une fois le script chargé, la fonction `initMap()` est appelée en callback.
* Pour initialiser la carte, il faut fournir au moins deux paramètres :
les **coordonnées du point central** de la carte (latitude/longitude), et le **niveau du zoom** à appliquer.

**Remarque :**

Comme présenté dans la [documentation officielle](https://developers.google.com/maps/documentation/javascript/adding-a-google-map),
Google conseille d'indiquer la clé d'API lors de l'appel à la librairie javascript. 
Il est toutefois possible de s'en passer (en phase de test par exemple), en modifiant l'URL : 

```html
<script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
```

## Afficher des marqueurs ##

![Afficher des marqueurs](./utiliser-l-api-google-map-03.png)

Si on veut afficher des marqueurs, le code javascript devient :

```javascript
function initMap() {

    // Création de la carte
    var paris_latlong = {lat: 48.866667, lng: 2.333333};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: paris_latlong
    });
    
    // Ajout de marqueurs
    var markerList = [
        { lat: 48.837347, lng: 2.291787, title: "Marqueur n°1" },
        { lat: 48.879681, lng: 2.379958, title: "Marqueur n°2" },
        { lat: 48.822399, lng: 2.498793, title: "Marqueur n°3" }
    ];
    
    for (var i = 0, length = markerList.length; i < length; i++) {
        var latLng = new google.maps.LatLng(markerList[i].lat, markerList[i].lng);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: markerList[i].title
        });
    }
}
```

**Explications :**

* On initialise un tableau contenant la liste des marqueurs, avec leurs coordonnées (latitude/longitude) 
et un libellé à afficher au survol (title).
* On parcourt ce tableau et on instancie des marqueurs, rattachés à la carte créée précédemment.

**Remarque :**

Pour centrer automatiquement la carte sur ces points, vous pouvez [suivre cet article](./api-diverses/centrer-automatiquement-une-carte-en-fonction-des-points-affiches).

## Afficher un marqueur personnalisé ##

Il est agréable d'utiliser ses propres icônes de marqueur.

![Afficher un marqueur personnalisé](./utiliser-l-api-google-map-05.png)

Pour cela, il n'y a quasiment rien à ajouter :

```javascript
function initMap() {
    
    // Création de la carte
    var paris_latlong = {lat: 48.866667, lng: 2.333333};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: paris_latlong
    });
    
    // Ajout du marqueur
    var marker = new google.maps.Marker({
        position: paris_latlong,
        map: map,
        title: 'Paris',
        icon: 'http://www.devnotebook.fr/public/articles/api-diverses/utiliser-l-api-google-map-06.png'
    });
}
```
**Explication :**

On a juste ajouté l'attribut `icon` au marqueur.

## Afficher une bulle d'information ##

![Afficher des marqueurs](./utiliser-l-api-google-map-04.png)

Pour afficher [une bulle d'information](https://developers.google.com/maps/documentation/javascript/infowindows) (`InfoWindow`) 
au clic sur le marqueur, voici ce que devient le code javascript :

```javascript
function initMap() {

    // Création de la carte
    var paris_latlong = {lat: 48.866667, lng: 2.333333};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: paris_latlong
    });
    
    // Ajout du marqueur
    var marker = new google.maps.Marker({
        position: paris_latlong,
        map: map,
        title: 'Paris'
    });
 
    // Ajout d'une InfoWindow
    var htmlContent = '<div class="info-window-container">';
        htmlContent += ' <h2>Mon marqueur</h2>';
        htmlContent += ' <p>Une petite description</p>';
        htmlContent += ' <p><a href="https://developers.google.com/maps/documentation/javascript/infowindows">Lien vers l\'API Google</a></p>';
        htmlContent += '</div>';
    var infoWindow = new google.maps.InfoWindow({
        content: htmlContent
    });
 
    marker.addListener('click', function() {
        infoWindow.open(map, marker);
    });
}
```

**Explications :**

* On stocke du HTML dans une variable javascript.
* On instancie un objet `InfoWindow` avec pour contenu ce code HTML.
* On ajoute un écouteur pour l'évènement clic sur le marqueur, pour que l'InfoWindow s'ouvre.
