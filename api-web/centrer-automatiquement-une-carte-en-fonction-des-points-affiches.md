Centrer automatiquement une carte en fonction des points affichés
=================================================================

[:type]: # "Astuce"
[:created_at]: # "2015/08/05"
[:modified_at]: # "2020/11/08"
[:tags]: # "api-web cartographie"

Lorsque vous affichez plusieurs points (= marqueurs) sur une Google Map, vous voulez en général que la 
carte soit suffisamment dézoomée pour permettre de les afficher tous.

## Carte par défaut ##

Pour afficher une carte avec plusieurs marqueurs, vous pouvez suivre le deuxième exemple de 
[cet article](./api-diverses/utiliser-l-api-google-map).

Pour rappel, le code javascript utilisé est celui-ci :

```javascript
function initMap() {
 
    // Création de la carte
    var paris_latlong = {lat: 48.866667, lng: 2.333333};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: paris_latlong
    });
 
    // Ajout de marqueurs
    var markerList = [
        { lat: 48.837347, lng: 2.291787, title: "Marqueur n°1" },
        { lat: 48.879681, lng: 2.379958, title: "Marqueur n°2" },
        { lat: 48.822399, lng: 2.498793, title: "Marqueur n°3" }
    ];
 
    for (var i = 0, length = markerList.length; i < length; i++) {
        var latLng = new google.maps.LatLng(markerList[i].lat, markerList[i].lng);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: markerList[i].title
        });
    }
}
```

Par défaut, le centre et le niveau de zoom sont définis lors de l'initialisation de la carte. 
Il serait plus intéressant que ces valeurs soit calculées automatiquement.

## Limiter la carte ##

Pour cela Google Map permet de définir les limites de la carte : **les bounds**.

Le code est alors légèrement modifié :

```javascript
function initMap() {
 
    // Création de la carte
    var paris_latlong = {lat: 48.866667, lng: 2.333333};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: paris_latlong
    });
 
    // Initialisation des limites de la carte
    var bounds = new google.maps.LatLngBounds();
 
    // Ajout de marqueurs
    var markerList = [
        { lat: 48.837347, lng: 2.291787, title: "Marqueur n°1" },
        { lat: 48.879681, lng: 2.379958, title: "Marqueur n°2" },
        { lat: 48.822399, lng: 2.498793, title: "Marqueur n°3" }
    ];
 
    for (var i = 0, length = markerList.length; i < length; i++) {
        var latLng = new google.maps.LatLng(markerList[i].lat, markerList[i].lng);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: markerList[i].title
        });
 
        // Ajout des coordonnées du marqueur aux limites
        bounds.extend(markerList[i].lat, markerList[i].lng);
    }
 
    // Application des limites à la carte
    map.fitBounds(bounds);
}
```

**Explications :**

* On instancie un objet limites (= `LatLngBounds`) et on lui transmet les coordonnées de 
tous les marqueurs affichés.
* On demande ensuite à la carte de respecter ces limites.

On obtient alors ce résultat :

![Centrage de la carte avec 3 points](./centrer-automatiquement-une-carte-en-fonction-des-points-affiches-01.png)

Si on enlève le 3ème marqueur, la carte est automatiquement recentrée :

![Centrage de la carte avec 2 points](./centrer-automatiquement-une-carte-en-fonction-des-points-affiches-02.png)

### Inconvénient ###

Le problème de cette technique, c'est que si vous n'affichez qu'un seul point, le zoom sera très important :

![Centrage de la carte avec 1 point](./centrer-automatiquement-une-carte-en-fonction-des-points-affiches-03.png)

## Zoom maximal fixe ##

La solution la plus simple est d'ajouter un **zoom maximal** à la carte, lors de son initialisation :

```javascript
var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: paris_latlong,
    maxZoom: 11
});
```

### Inconvénient ###

Le gros problème, c'est que l'utilisateur sera bloqué et **ne pourra pas zoomer plus** que la valeur 
que vous aurez définie.

## Zoom calculé ##

**À la place** d'utiliser le zoom maximal, on peut modifier le zoom automatiquement
en fonction des limites de la carte :

```javascript
var zoomChangeBoundsListener = google.maps.event.addListener(map, 'bounds_changed', function(event) {
    google.maps.event.removeListener(zoomChangeBoundsListener);
    map.setZoom(Math.min( 11, map.getZoom()));
});
```

**Explications :**

* On ajoute un écouteur sur l'évènement `bounds_changed` fourni par Google Map.
* L'écouteur est supprimé afin de laisser l'utilisateur zoomer/dézoomer à sa guise.
* Quand les limites de la carte sont modifiées, le zoom est à nouveau calculé pour ne pas être 
supérieur à la valeur souhaitée.

**Remarque :**

Il faut ajouter ce code **après l'initialisation de la carte**, mais **avant d'appeler la méthode `fitBounds()`**. 
Sans quoi l'écouteur ne captera pas l'évènement.
