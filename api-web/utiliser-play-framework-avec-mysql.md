Utiliser Play! Framework avec MySQL
===================================

[:type]: # "Astuce"
[:version]: # "Play! Framework 2"
[:created_at]: # "2012/09/05"
[:modified_at]: # "2018/02/07"
[:tags]: # "api-web play-framework"

Pour que Play! puisse se connecter à une base de données MySQL en Java, vous avez **deux fichiers à modifier**.

## conf/application.conf ##

Voici un exemple de configuration avec une base de données MySQL de base dans WAMPServer.

```ini
# Database configuration
# ~~~~~ 
# You can declare as many datasources as you want.
# By convention, the default datasource is named `default`
#
# db.default.driver=org.h2.Driver
# db.default.url="jdbc:h2:mem:play"
# db.default.user=sa
# db.default.password=
#
# You can expose this datasource via JNDI if needed (Useful for JPA)
# db.default.jndiName=DefaultDS
 
db.default.driver=com.mysql.jdbc.Driver
db.default.url="jdbc:mysql://localhost:3306/database_name" 
db.default.user="root" 
db.default.password=""
```

Les 4 dernières lignes permettent de choisir :

* le driver Java à utiliser,
* l'adresse, le port et le nom de la base de données à laquelle se connecter
* le login et le mot de passe de l'utilisateur à utiliser

## project/Build.scala ##

```scala
import sbt._
import Keys._
import PlayProject._
 
object ApplicationBuild extends Build {
 
    val appName         = "MyApplication"
    val appVersion      = "1.0-SNAPSHOT"
 
    val appDependencies = Seq(
     // Dépendances du projet :
     "mysql" % "mysql-connector-java" % "5.1.20"
    )
 
    val main = PlayProject(appName, appVersion, appDependencies, mainLang = JAVA).settings(
      // Configuration du projet :     
    )
 
}
```

La ligne à ajouter est : `"mysql" % "mysql-connector-java" % "5.1.20"`

Pour que Play! prenne en compte ces modifications (en particulier la modification de `Build.scala`), 
vous devez relancer Play! et lui demander de **recharger la configuration** :

```bash
cd path/to/my/play/application
play
$ reload
$ update
$ run
```
