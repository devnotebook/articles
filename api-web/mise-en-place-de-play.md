Mise en place de Play!
======================

[:type]: # "Astuce"
[:version]: # "Play! Framework 2"
[:created_at]: # "2012/09/05"
[:modified_at]: # "2018/02/07"
[:tags]: # "api-web play-framework"

## Installation ##

Tout d'abord, vous avez besoin d'un **JDK 6** ou supérieur pour pouvoir utiliser Play! en Java.

* Téléchargez la dernière version de [Play! Framework](http://playframework.org/).
* Extrayez l'archive où vous le souhaitez. (Par exemple D:\dev\play-2.0.3)
* Ajoutez le dossier racine de Play! à votre PATH système. 
Si vous êtes sou Linux, utilisez cette commande `export PATH=$PATH:/path/to/play-2.0.3`.
Si vous êtes sous Windows, modifiez les variables d'environnement.
* Redémarrez votre ordinateur
* Vérifiez l'installation. La commande suivante ne doit pas retourner d'erreur :

```bash
play help
```

## Création d'une application avec Play! ##

Si vous utilisez eclipse, Netbeans, ou un autre IDE pour Java possédant un workspace, 
il est préférable de créer votre application dans ce workspace. 

Play! peut créer l'application pour vous, via la commande :

```bash
play new chemin/vers/le/workspace/nom_application
```

* Choisissez le nom de votre application (Par exemple `PlayWS`)
* Choisissez `simple Java application`

![Console nouvelle appli Play!](./mise-en-place-de-play-01.png)

* Play! a maintenant créé la base de fichiers nécessaires à votre application.

## Utiliser Play! dans eclipse ##

Pour utiliser Play! avec eclipse, il faut **"eclipsifier" votre application**. 
Pour cela, utilisez les commandes suivantes :

* Rendez-vous dans le dossier contenant votre nouvelle application et lancez Play! :

```bash
cd chemin/vers/le/workspace/nom_application
play
```

* Dans l'invite de commande de Play!, appelez la commande `eclipsify` :

```bash
$ eclipsify
```

* Lancez eclipse, choisissez "Importer" > "Importer un projet existant dans le workspace".
* Sélectionnez le dossier racine de l'application générée par Play! et validez.

**Remarque :**
Si vous obtenez une erreur pendant la commande eclipsify ressemblant à `class file has wrong version 50.0, 
should be 49.0`, c'est que vous devez **mettre à jour votre JDK** ou modifier vos variables d'environnement.
