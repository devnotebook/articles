Utiliser l'API Google Analytics en PHP
======================================

[:type]: # "Marque-page"
[:sources]: # "[Github GAPI](https://github.com/erebusnz/gapi-google-analytics-php-interface)"
[:created_at]: # "2013/04/23"
[:modified_at]: # "2017/05/16"
[:tags]: # "api-web php google-analytics"

Pour pouvoir utiliser les données de Google Analytics directement en PHP, 
il existe une petite api pour communiquer simplement avec Google : 
[GAPI](http://code.google.com/p/gapi-google-analytics-php-interface/).

En deux lignes de code, vous pouvez ainsi récupérer le nombre de visualisations par page pour les 30 derniers jours :

```php
$ga = new gapi('address@server.com', 'my_password');
 
// Récupération du nombre de visualisations, pour chaque couple (url de la page, titre de la page)
$ga->requestReportData('71538601', ['pagePath', 'pageTitle'], ['pageviews']);
```
