Utiliser Google Analytics pour un Intranet
==========================================

[:type]: # "Astuce"
[:sources]: # "[ww.statstory.com](http://www.statstory.com/intranet-tracking-with-google-analytics-piwik/)"
[:created_at]: # "2013/06/07"
[:modified_at]: # "2017/05/16"
[:tags]: # "api-web google-analytics"

Google Analytics fonctionne très bien même en intranet, tant que les utilisateurs du site ont accès à internet. 
Cela n'est vrai qu'à une condition : le site intranet doit avoir un nom de domaine finissant par **".qqchose"** 
(.fr, .com, ...).

Par exemple, `http://mon-intranet.fr`, `http://mon-intranet.pwet` fonctionneront, mais pas `http://mon-intranet`.

Dans ce cas particulier, vous pouvez quand même utiliser Google Analytics en ajoutant cette ligne au code 
javascript fourni par Google :

```javascript
_gaq.push(['_setDomainName', 'none']);
```
