Grouper des points sur une Google Map
=====================================

[:type]: # "Astuce"
[:sources]: # "[developers.google.com](https://developers.google.com/maps/documentation/javascript/marker-clustering#understanding-marker-clustering)"
[:created_at]: # "2012/09/28"
[:modified_at]: # "2017/05/17"
[:tags]: # "api-web cartographie"

Lorsque vous affichez une Google Map avec beaucoup de points rapprochés, ils risquent de se chevaucher. 
Pour éviter ce problème, vous pouvez les regrouper en utiliser le **MarkerClustering**.

L'autre avantage est le temps de chargement de la carte. 
Puisqu'il y a des regroupements, il y a moins de points à afficher.

Voici un exemple :
![Exemple de Marker clustering](./grouper-des-points-sur-une-google-map-01.png)

À gauche une carte avec beaucoup de points et à droite, la même avec des regroupements.

Pour utiliser le regroupement, il suffit de créer la carte et les points, 
mettre les points dans un tableau et créer un nouvel objet `MarkerClusterer`.

Voici un aperçu du code à ajouter pour créer des points regroupés :

```javascript
// Soit data un tableau d'objets ayant des latitudes et longitudes
var markers = [];
for (var i = 0; i < 100; ++i) {
    var marker = new google.maps.Marker({
      "lat": data[i].lat,
      "long": data[i].long
    });
    markers.push(marker);
}
var markerCluster = new MarkerClusterer(map, markers);
```

**Remarque :**
Il faut également ajouter le fichier `markerclusterer.js` à votre page.
